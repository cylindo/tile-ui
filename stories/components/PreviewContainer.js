import React from 'react';
import styled from 'styled-components';
import { AppContainer, Box } from '../../src';

export const PreviewContainer = ({ config, children }) => (
  <AppContainer {...config}>
    <Box p={20}>{children}</Box>
  </AppContainer>
);
