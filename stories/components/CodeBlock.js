import React from 'react';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { tomorrowNightBlue } from 'react-syntax-highlighter/styles/hljs';
import { Box } from '../../src';

export const CodeBlock = ({ code = '' }: { code: string }) => (
  <Box borderRadius={1} backgroundColor="#002451" marginTop={10} marginBottom={10} padding={4}>
    <SyntaxHighlighter wrapLines language="jsx" style={tomorrowNightBlue}>
      {code}
    </SyntaxHighlighter>
  </Box>
);
