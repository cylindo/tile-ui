import React from 'react';
import { storiesOf, addDecorator } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import { Welcome } from '@storybook/react/demo';
import { withInfo } from '@storybook/addon-info';
import {
  AppContainer,
  Button,
  Backdrop,
  Card,
  Flex,
  Modal,
  H1,
  P,
  Separator,
  Text,
  Image,
  Avatar,
  Dropdown,
  Tabs,
  Tab,
} from '../src';
import { PreviewContainer } from './components';
import { Theme } from './Theme';
import { Grid } from './Grid';
import { Icon } from './Icon';
import { Form, SimpleForm } from './Form';
import { HOC, FaaChild } from './Patterns';
import faker from 'faker';

storiesOf('Welcome', module).add('to Storybook', () => <Welcome showApp={linkTo('Button')} />);

/**
 * Theme
 */
storiesOf('Theme', module).add('Default Theme', () => <Theme />);

/**
 * Grid
 */
storiesOf('Grid', module)
  .addDecorator(getStory => <PreviewContainer>{getStory()}</PreviewContainer>)
  .add('Grid overview', () => <Grid />);

/**
 * Button
 */
storiesOf('Button', module)
  .addDecorator(getStory => <PreviewContainer>{getStory()}</PreviewContainer>)
  .add('Default prop', withInfo()(() => <Button onClick={action('clicked')}>Hello Button</Button>))
  .add(
    'Danger button',
    withInfo()(() => (
      <Button color="danger" onClick={action('clicked')}>
        Hello Button
      </Button>
    ))
  )
  .add(
    'With icon',
    withInfo()(() => (
      <Button icon="emoticon" onClick={action('clicked')}>
        Hello Button
      </Button>
    ))
  )
  .add('Only icon', withInfo()(() => <Button icon="emoticon" onClick={action('clicked')} />))
  .add(
    'Round button',
    withInfo()(() => (
      <Button shape="round" onClick={action('clicked')}>
        I'm Round
      </Button>
    ))
  )
  .add(
    'Hollow Buttons',
    withInfo()(() => (
      <Flex>
        <Button mr={2} buttonType="hollow" onClick={action('clicked')}>
          Primary
        </Button>
        <Button mr={2} buttonType="hollow" color="success" onClick={action('clicked')}>
          Success
        </Button>
        <Button mr={2} buttonType="hollow" color="danger" onClick={action('clicked')}>
          Danger
        </Button>
        <Button mr={2} buttonType="hollow" color="warning" onClick={action('clicked')}>
          Warning
        </Button>
        <Button mr={2} buttonType="hollow" color="info" onClick={action('clicked')}>
          Info
        </Button>
        <Button mr={2} buttonType="hollow" color="grey" onClick={action('clicked')}>
          Grey
        </Button>
      </Flex>
    ))
  );

storiesOf('Icon', module)
  .addDecorator(getStory => <PreviewContainer>{getStory()}</PreviewContainer>)
  .add('Simple icon', withInfo()(() => <Icon />));

storiesOf('Form', module)
  .addDecorator(getStory => <PreviewContainer>{getStory()}</PreviewContainer>)
  .add('Form example', withInfo()(() => <Form />))
  .add('Simple Form', withInfo()(() => <SimpleForm />));

storiesOf('React Patterns', module)
  .addDecorator(getStory => <PreviewContainer>{getStory()}</PreviewContainer>)
  .add('Higher Order Components', withInfo()(() => <HOC />))
  .add('Function as a child', withInfo()(() => <FaaChild />));

storiesOf('Tabs', module)
  .addDecorator(getStory => <PreviewContainer>{getStory()}</PreviewContainer>)
  .add(
    'Tabs',
    withInfo()(() => (
      <Tabs>
        <Tab id="hello" label="Label">
          Hello
        </Tab>
        <Tab id="second" label="Second">
          Second
        </Tab>
      </Tabs>
    ))
  );

storiesOf('Dropdown', module)
  .addDecorator(getStory => <PreviewContainer>{getStory()}</PreviewContainer>)
  .add(
    'Dropdown Example',
    withInfo()(() => (
      <Dropdown
        posX="right"
        actions={[
          {
            action: 'blabla',
            label: 'Do something',
          },
          {
            action: 'nananana',
            label: 'Do something else',
          },
        ]}
      >
        {props => <Button {...props} buttonType="hollow" color="grey" icon="more" />}
      </Dropdown>
    ))
  );

storiesOf('Modal', module)
  .addDecorator(getStory => <PreviewContainer>{getStory()}</PreviewContainer>)
  .add(
    'Backdrop',
    withInfo()(() => (
      <React.Fragment>
        <Flex>This content is behind the backdrop</Flex>
        <Backdrop />
      </React.Fragment>
    ))
  )
  .add(
    'Modal',
    withInfo()(() => (
      <React.Fragment>
        <Flex>This content is behind the backdrop</Flex>
        <Modal
          onHide={() => {
            console.log('## Hide was triggered ##');
          }}
          title="My Modal"
        >
          {({ onHide }) => (
            <React.Fragment>
              <P>Hello, this is a modal with awesome contents</P>
              <Text>Here we have text</Text>
              <Image
                mb={10}
                borderRadius={10}
                width={10}
                height={10}
                src="https://scontent-arn2-1.xx.fbcdn.net/v/t1.15752-9/40940598_1923667920989717_6882283636951875584_n.png?_nc_cat=0&oh=27468b035ddc471bec3d318bb3ba68d9&oe=5C35B2EE"
              />

              <Avatar
                profile={{
                  name: 'John Smith',
                  avatar: faker.image.avatar(),
                }}
              />
              <Button onClick={onHide} ml="auto">
                Hello
              </Button>
            </React.Fragment>
          )}
        </Modal>
      </React.Fragment>
    ))
  );
