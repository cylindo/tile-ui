import React from 'react';
import styled from 'styled-components';
import { lighten, darken, transparentize } from 'polished';
import { AppContainer, H1, H2, Spacer, P, Row, Col, Box, Separator } from '../../src';
import { StorySection } from '../components/StorySection';
import { BreakPointDisplay } from './BreakPointDisplay';
import { map } from 'lodash';

const SimpleGrid = () => (
  <Row>
    <Col flex={1}>
      <ColBox>
        <P>Box 1</P>
      </ColBox>
    </Col>
    <Col flex={1}>
      <ColBox>
        <P>Box 2</P>
      </ColBox>
    </Col>
    <Col flex={1}>
      <ColBox>
        <P>Box 3</P>
      </ColBox>
    </Col>
  </Row>
);

const SimpleGridFlexBreakPoints = () => (
  <Row>
    <Col flex={[3, 2, 1]}>
      <ColBox>
        <P>Box 1</P>
      </ColBox>
    </Col>
    <Col flex={[1, 2, 3]}>
      <ColBox>
        <P>Box 2</P>
      </ColBox>
    </Col>
    <Col flex={[3, 2, 1]}>
      <ColBox>
        <P>Box 3</P>
      </ColBox>
    </Col>
  </Row>
);

const SimpleGridWidthBreakPoints = () => (
  <Row>
    <Col width={[1, 1 / 2, 1 / 3]}>
      <ColBox>
        <P>Box 1</P>
      </ColBox>
    </Col>
    <Col width={[1, 1 / 4, 1 / 3]}>
      <ColBox>
        <P>Box 2</P>
      </ColBox>
    </Col>
    <Col width={[1, 1 / 4, 1 / 3]}>
      <ColBox>
        <P>Box 3</P>
      </ColBox>
    </Col>
  </Row>
);

const SimpleGridMixedBreakPoints = () => (
  <Row>
    <Col display={['none', 'block']} width={[1, 1 / 3]} flex={[0, 1]}>
      <ColBox>
        <P>Will hide when small</P>
      </ColBox>
    </Col>
    <Col width={[1, 1 / 3]} flex={[0, 1]}>
      <ColBox>
        <P>Box 2</P>
      </ColBox>
    </Col>
    <Col width={[1, 1 / 3]} flex={[0, 2]}>
      <ColBox>
        <P>Box 3</P>
      </ColBox>
    </Col>
  </Row>
);

const SimpleGridMargins = () => (
  <Row>
    <Col width={[1, 1 / 3]} marginBottom={2}>
      <ColBox>
        <P>Box 1</P>
      </ColBox>
    </Col>
    <Col width={[1, 1 / 3]} marginBottom={2}>
      <ColBox>
        <P>Box 2</P>
      </ColBox>
    </Col>
    <Col width={[1, 1 / 3]} marginBottom={2}>
      <ColBox>
        <P>Box 3</P>
      </ColBox>
    </Col>

    <Col width={[1, 1 / 3]} marginBottom={2}>
      <ColBox>
        <P>Box 1</P>
      </ColBox>
    </Col>
    <Col width={[1, 1 / 3]} marginBottom={2}>
      <ColBox>
        <P>Box 2</P>
      </ColBox>
    </Col>
    <Col width={[1, 1 / 3]} marginBottom={2}>
      <ColBox>
        <P>Box 3</P>
      </ColBox>
    </Col>
  </Row>
);

const AlignExample = () => (
  <Row>
    <Col width={[1, 1 / 3]}>
      <ColBox>
        <P>Box 1</P>
      </ColBox>
    </Col>
    <Col width={[1, 1 / 3]}>
      <ColBox>
        <P>Box 3</P>
      </ColBox>
    </Col>
  </Row>
);

const Display = () => (
  <Row>
    <Col width={[1, 1 / 3]}>
      <ColBox>
        <P>Box 1</P>
      </ColBox>
    </Col>
    <Col width={[1, 1 / 3]}>
      <ColBox>
        <P>Box 3</P>
      </ColBox>
    </Col>
  </Row>
);

const sections = [
  {
    title: 'Simple flex grid',
    description:
      'Row using flex, no gutter. No breakpoints have been defined, thus the layout will not changed when the window is resized.',
    section: SimpleGrid,
  },
  {
    title: 'flex grid with breakpoints',
    description: 'Row using flex, no gutter.',
    section: SimpleGridFlexBreakPoints,
  },

  {
    title: 'flex grid with breakpoints',
    description: 'Row using width, no gutter.',
    section: SimpleGridWidthBreakPoints,
  },

  {
    title: 'flex grid with breakpoints',
    description: 'Row using width and flex, no gutter.',
    section: SimpleGridMixedBreakPoints,
  },

  {
    title: 'flex grid with margins',
    description: 'Columns with margins',
    section: SimpleGridMargins,
  },

  {
    title: 'Align left and right ',
    description: 'Example with left right alignment ',
    section: AlignExample,
  },

  {
    title: 'Display',
    description: 'Responsive display',
    section: Display,
  },
];

export const Grid = ({ config }) => (
  <ThemeWrapper>
    <H1>Grid</H1>
    <BreakPointDisplay />
    {map(sections, (section, i) => (
      <StorySection key={i} {...section} />
    ))}
  </ThemeWrapper>
);

const ThemeWrapper = styled.div`
  padding: 80px;
`;

const ColBox = styled.div`
  display: flex;
  width: 100%;
  height: 200px;
  text-align: center;
  align-content: center;
  justify-content: center;
  align-items: center;
  outline: 1px solid ${props => props.theme.color.primary};
  background-color: ${props => transparentize(0.8, props.theme.color.primary)};
`;
