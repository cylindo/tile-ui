// @flow
import React from 'react';

import { CodeBlock } from '../components';
import { AddClickWrapper } from './HOC';
import { Toggle } from './FaaChild';

import { H1, P, Separator, Box, Button, Row } from '../../src/';

const example1 = 'const HOC = BaseComponent => EnhancedComponent';

const example2 = `
// Declaration
export const AddClickWrapper = ComponentToWrap => ({ onClick, ...rest }) => (
  <ButtonWrapper onClick={onClick}>
    <ComponentToWrap {...rest} />
  </ButtonWrapper>
);
`;

const example3 = `
// Usage
import { AddClickWrapper } from './HOC';

const ComponentToEnhance = () => {...}
const ClickableComponent = AddClickWrapper(ComponentToEnhance);

<ClickableComponent onClick={() => alert('click!')} />
`;

const rpExample1 = `
<Toggle>
    {({ on, toggle }) => (
      <Button buttonType={on ? 'raised' : 'hollow'} onClick={toggle}>
        I'm {on ? 'on' : 'off'}
      </Button>
    )}
</Toggle>
`;

const rpExample2 = `
export class Toggle extends React.Component {

  // Initial state
  state = {
    on: false,
  };

  // toggle function
  toggle = () => {
    const { on } = this.state;

    this.setState({
      on: !on,
    });
  };

  render() {
    const { children } = this.props;
    const { on } = this.state;

    return children({
      on,
      toggle: this.toggle,
    });
  }
}
`;

export const HOC = () => (
  <React.Fragment>
    <H1>Higher Order Components (HOC)</H1>
    <Separator />
    <P>
      A higher order component is a function that accepts a components and returns the passed component with additional
      features and enhancements. The syntax in its simplest form looks like this:
    </P>
    <CodeBlock code={example1} />

    <P>
      Now, lets make a HOC that adds extra functionality to a component. In this case we will make a Component that adds
      a click handler to any component.
    </P>

    <CodeBlock code={example2} />

    <P>And this is how we use it:</P>

    <ClickableCodeBlock onClick={() => alert('click!')} />
  </React.Fragment>
);

export const FaaChild = () => (
  <React.Fragment>
    <H1>Function as a child</H1>
    <Separator />
    <P>
      Function as a child is a pattern where we use a function as a child instead of using a Component. We define the
      function as the child, and we invoke and pass arguments to it from the parent. By using this pattern you can add
      functionality and state to simple components.
    </P>
    <CodeBlock code={rpExample1} />
    <ToggleExample />
    <Separator />

    <P>Let's take a look at how the Toggle component is made</P>

    <CodeBlock code={rpExample2} />
  </React.Fragment>
);

const ToggleExample = () => (
  <Row>
    <Button mr={2} buttonType="hollow" onClick={() => {}}>
      I'm static
    </Button>
    <Toggle>
      {({ on, toggle }) => (
        <Button buttonType={on ? 'raised' : 'hollow'} onClick={toggle}>
          I'm {on ? 'on' : 'off'}
        </Button>
      )}
    </Toggle>
  </Row>
);

const ClickableCodeBlock = AddClickWrapper(() => <CodeBlock code={example3} />);
