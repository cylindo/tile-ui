import React from 'react';
import styled from 'styled-components';

export const AddClickWrapper = ComponentToWrap => ({ onClick, ...rest }) => (
  <ButtonWrapper onClick={onClick}>
    <ComponentToWrap {...rest} />
  </ButtonWrapper>
);

const ButtonWrapper = styled.div`
  cursor: pointer;
  background: none;
  border: none;
  font: inherit;
  width: 100%;
  transition: all 200ms ease;

  &:hover {
    opacity: 0.5;
  }
`;
