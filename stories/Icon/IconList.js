import React from 'react';
import styled, { withTheme } from 'styled-components';
import { lighten, darken, transparentize } from 'polished';
import { AppContainer, Icon, H1, H2, Spacer, P, Row, Col, Box, Separator, Container, HoverTransition } from '../../src';
import { StorySection } from '../components/StorySection';
import * as icons from '../../src/components/elements/Icon/icons';

import { map } from 'lodash';

export const IconList = () => {
  const _icons = map(icons, (key, val) => val);

  return (
    <Row flexWrap="wrap" mx={-4}>
      {map(_icons, i => (
        <Box flex={1 / 5} minWidth={1 / 5} key={i}>
          <HoverTransition transition="scale">
            <Box borderRadius={1} py={8} px={8} elevation={4} m={4}>
              <Row mb={5} justifyContent="center">
                <Icon size={30} icon={i} />
              </Row>
              <Row justifyContent="center">
                <P fontWeight="bold">"{i}"</P>
              </Row>
            </Box>
          </HoverTransition>
        </Box>
      ))}
    </Row>
  );
};
