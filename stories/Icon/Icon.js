import React from 'react';
import styled, { withTheme } from 'styled-components';
import { map } from 'lodash';
import { lighten, darken, transparentize } from 'polished';
import { AppContainer, H1, H2, Spacer, P, Row, Col, Box, Separator, Container } from '../../src';
import { StorySection } from '../components/StorySection';
import { IconList } from './IconList';

const SimpleGrid = () => <Row />;

const sections = [
  {
    title: 'List of all icons',
    description: 'See all icons in TILE',
    section: IconList,
  },
];

export const Icon = ({ config }) => (
  <React.Fragment>
    {map(sections, (section, i) => (
      <StorySection key={i} {...section} />
    ))}
  </React.Fragment>
);
