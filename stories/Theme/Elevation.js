import React from 'react';
import { transparentize } from 'polished';
import styled, { withTheme } from 'styled-components';
import { map } from 'lodash';
import { H2 } from '../../src';

const ElevationLevel = styled.div`
  width: 200px;
  height: 200px;
  box-shadow: ${props => props.elevation};
  background: ${props => transparentize(0.5, props.theme.color.separator)};
  display: inline-block;
  margin: ${props => props.theme.uiSize[3]}px;
`;

const ElevationLabel = styled.div`
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;

  h2 {
    text-align: center;
  }
`;

export const Elevation = withTheme(({ theme }) => (
  <React.Fragment>
    {map(theme.elevation, (value, key) => (
      <ElevationLevel elevation={value} key={key}>
        <ElevationLabel>
          <H2>Level {key}</H2>
        </ElevationLabel>
      </ElevationLevel>
    ))}
  </React.Fragment>
));
