import React from 'react';
import styled, { withTheme } from 'styled-components';
import { P } from '../../src';
import { map, isArray, isObject } from 'lodash';

export const ColorSwatch = styled.div`
  width: 80px;
  height: 80px;
  border-radius: 64px;
  background: ${props => props.color};
  margin-bottom: ${props => props.theme.uiSize[3]}px;
  box-shadow: ${props => props.theme.elevation[2]};
`;

const Color = styled.div`
  display: inline-block;
  padding: ${props => props.theme.uiSize[2]}px ${props => props.theme.uiSize[3]}px;
  width: 156px;
`;

export const Colors = withTheme(({ theme }) => (
  <React.Fragment>
    {map(theme.color, (value, key) => {
      if (isArray(value) || isObject(value)) return null;
      return (
        <Color key={key}>
          <ColorSwatch name={key} color={value} />
          <P marginBottom={0} fontWeight="bold">
            {key}
          </P>
          <P marginBottom={0} fontWeight="thin" fontSize="small">
            {value}
          </P>
        </Color>
      );
    })}
  </React.Fragment>
));
