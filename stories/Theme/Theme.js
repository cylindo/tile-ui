import React from 'react';
import styled, { withTheme } from 'styled-components';
import { AppContainer, H1, H2, P, Tag } from '../../src';
import { Colors } from './Colors';
import { Typography } from './Typography';
import { Elevation } from './Elevation';
import { StorySection } from '../components';
import image from './assets/tile_header.png';

export const Theme = ({ config }) => {
  console.log('## THEME CONFIG ##', config);

  return (
    <AppContainer {...config} rebootOnLoad>
      <ThemeWrapper>
        <ThemeHeader>
          <TopImage />
          <H1 marginBottom={4}>Component Library</H1>
          <H2 color="muted">by Cylindo</H2>
        </ThemeHeader>

        <StorySection title="Colors" description="These are the colors.." section={() => <Colors />} />
        <StorySection title="Typography" description="This is the Typography.." section={() => <Typography />} />
        <StorySection title="Elevation" description="This is the elevation levels.." section={() => <Elevation />} />

        <Tag mr={2} bg="primary">
          Primary
        </Tag>
        <Tag mr={2} bg="success">
          Success
        </Tag>
        <Tag mr={2} bg="danger">
          Danger
        </Tag>
        <Tag mr={2} bg="warning">
          Warning
        </Tag>
        <Tag mr={2} bg="grey">
          Grey
        </Tag>
      </ThemeWrapper>
    </AppContainer>
  );
};

const ThemeWrapper = styled.div`
  padding: 80px;
`;

const TopImage = styled.div`
  width: 256px;
  height: 256px;
  background: url(${image});
  background-size: cover;
  background-repeat: no-repeat;
  box-shadow: ${props => props.theme.elevation[4]};
  margin: 64px auto;
  border-radius: 24px;
`;

const ThemeHeader = styled.div`
  text-align: center;
`;
