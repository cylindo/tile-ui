import React from 'react';
import { H1, H2, H3, P, Label } from '../../src';

export const Typography = () => (
  <React.Fragment>
    <H1>This is a H1 Tag</H1>
    <H2>This is a H2 Tag</H2>
    <H3>This is a H3 Tag</H3>
    <P marginBottom={0}>This is a Paragraph Tag</P>
    <P marginBottom={0} fontWeight="bold">
      This is a bold Paragraph Tag
    </P>
    <P marginBottom={0} color="muted">
      This is a muted Paragraph Tag
    </P>
    <Label>This is a label</Label>
  </React.Fragment>
);
