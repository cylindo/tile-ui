import React from 'react';
import { withFormik } from 'formik';

import { utils, FormCheckbox, Form, Row, Col, FormInput, DatePicker, FormSelect } from '../../src';
import { isEmail, isDateInFuture } from '../../src/utils/form';
import { StorySection } from '../components/StorySection';
import { Separator } from '../../dist/index.es';

export class FormExample extends React.Component {
  selectOptions = [
    { value: 'Food', label: 'Food', id: 'food' },
    { value: 'Being Fabulous', label: 'Being Fabulous', id: 'being' },
    { value: 'Ken Wheeler', label: 'Ken Wheeler', id: 'ken' },
    { value: 'ReasonML', label: 'ReasonML', id: 'reasonml' },
    { value: 'Unicorns', label: 'Unicorns', id: 'unicorns' },
    { value: 'Kittens', label: 'Kittens', id: 'kittens' },
  ];

  onSubmit = values => {
    setTimeout(() => {
      alert(JSON.stringify(values, null, 2));
    }, 1000);
  };

  render() {
    return (
      <React.Fragment>
        <StorySection
          title="Input"
          description="TILE form elements are simple view components designed to work with Formik, a form library that removes the main pains when dealing with forms in react."
          section={() => <SimpleForm options={this.selectOptions} onSubmit={this.onSubmit} />}
        />
      </React.Fragment>
    );
  }
}

const SimpleFormInner = ({
  values,
  touched,
  errors,
  dirty,
  isSubmitting,
  handleChange,
  setFieldValue,
  handleBlur,
  handleSubmit,
  handleReset,
  ...rest
}) => {
  const { options } = rest;

  const formikProps = {
    values,
    touched,
    errors,
    setFieldValue,
    handleBlur,
  };

  return (
    <Form isSubmitting={isSubmitting} onSubmit={handleSubmit}>
      <FormInput placeholder="this is a placeholder" label="Input" id="input" {...formikProps} />

      <FormInput label="Email" id="email" placeholder="Enter your email" required={false} {...formikProps} />

      <FormSelect options={options} id="topic" label="Topics" {...formikProps} />

      <FormCheckbox label="Yes?" id="checkbox" {...formikProps} />

      <DatePicker id="date" label="Date" placeholder="Select a date" {...formikProps} />

      <Separator />

      <Row mx={-2}>
        <Col px={2}>
          <DatePicker id="dateA" label="Date A" placeholder="Select a date" {...formikProps} />
        </Col>

        <Col px={2}>
          <DatePicker
            id="dateB"
            label="Date B"
            type="date"
            selectedDays={[new Date(values.dateA)]}
            placeholder="Must be after A"
            {...formikProps}
          />
        </Col>
      </Row>
    </Form>
  );
};

const SimpleForm = withFormik({
  mapPropsToValues: () => ({
    newInput: '',
    email: '',
    date: new Date(),
    dateA: null,
    dateB: null,
  }),
  validate: (values, props) => {
    const { dateA, dateB } = values;

    const errors = {};
    if (!utils.isEmail(values.email)) errors.email = 'oh nooo!';

    return errors;
  },
  handleSubmit: (values, { setSubmitting, props }) => {
    const { onSubmit } = props;
    setSubmitting(false);
    onSubmit(values);
  },
})(SimpleFormInner);
