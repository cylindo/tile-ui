'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var styled = require('styled-components');
var styled__default = _interopDefault(styled);
var React = require('react');
var React__default = _interopDefault(React);

/**
 * @category Common Helpers
 * @summary Is the given argument an instance of Date?
 *
 * @description
 * Is the given argument an instance of Date?
 *
 * @param {*} argument - the argument to check
 * @returns {Boolean} the given argument is an instance of Date
 *
 * @example
 * // Is 'mayonnaise' a Date?
 * var result = isDate('mayonnaise')
 * //=> false
 */
function isDate (argument) {
  return argument instanceof Date
}

var is_date = isDate;

var MILLISECONDS_IN_HOUR = 3600000;
var MILLISECONDS_IN_MINUTE = 60000;
var DEFAULT_ADDITIONAL_DIGITS = 2;

var parseTokenDateTimeDelimeter = /[T ]/;
var parseTokenPlainTime = /:/;

// year tokens
var parseTokenYY = /^(\d{2})$/;
var parseTokensYYY = [
  /^([+-]\d{2})$/, // 0 additional digits
  /^([+-]\d{3})$/, // 1 additional digit
  /^([+-]\d{4})$/ // 2 additional digits
];

var parseTokenYYYY = /^(\d{4})/;
var parseTokensYYYYY = [
  /^([+-]\d{4})/, // 0 additional digits
  /^([+-]\d{5})/, // 1 additional digit
  /^([+-]\d{6})/ // 2 additional digits
];

// date tokens
var parseTokenMM = /^-(\d{2})$/;
var parseTokenDDD = /^-?(\d{3})$/;
var parseTokenMMDD = /^-?(\d{2})-?(\d{2})$/;
var parseTokenWww = /^-?W(\d{2})$/;
var parseTokenWwwD = /^-?W(\d{2})-?(\d{1})$/;

// time tokens
var parseTokenHH = /^(\d{2}([.,]\d*)?)$/;
var parseTokenHHMM = /^(\d{2}):?(\d{2}([.,]\d*)?)$/;
var parseTokenHHMMSS = /^(\d{2}):?(\d{2}):?(\d{2}([.,]\d*)?)$/;

// timezone tokens
var parseTokenTimezone = /([Z+-].*)$/;
var parseTokenTimezoneZ = /^(Z)$/;
var parseTokenTimezoneHH = /^([+-])(\d{2})$/;
var parseTokenTimezoneHHMM = /^([+-])(\d{2}):?(\d{2})$/;

/**
 * @category Common Helpers
 * @summary Convert the given argument to an instance of Date.
 *
 * @description
 * Convert the given argument to an instance of Date.
 *
 * If the argument is an instance of Date, the function returns its clone.
 *
 * If the argument is a number, it is treated as a timestamp.
 *
 * If an argument is a string, the function tries to parse it.
 * Function accepts complete ISO 8601 formats as well as partial implementations.
 * ISO 8601: http://en.wikipedia.org/wiki/ISO_8601
 *
 * If all above fails, the function passes the given argument to Date constructor.
 *
 * @param {Date|String|Number} argument - the value to convert
 * @param {Object} [options] - the object with options
 * @param {0 | 1 | 2} [options.additionalDigits=2] - the additional number of digits in the extended year format
 * @returns {Date} the parsed date in the local time zone
 *
 * @example
 * // Convert string '2014-02-11T11:30:30' to date:
 * var result = parse('2014-02-11T11:30:30')
 * //=> Tue Feb 11 2014 11:30:30
 *
 * @example
 * // Parse string '+02014101',
 * // if the additional number of digits in the extended year format is 1:
 * var result = parse('+02014101', {additionalDigits: 1})
 * //=> Fri Apr 11 2014 00:00:00
 */
function parse (argument, dirtyOptions) {
  if (is_date(argument)) {
    // Prevent the date to lose the milliseconds when passed to new Date() in IE10
    return new Date(argument.getTime())
  } else if (typeof argument !== 'string') {
    return new Date(argument)
  }

  var options = dirtyOptions || {};
  var additionalDigits = options.additionalDigits;
  if (additionalDigits == null) {
    additionalDigits = DEFAULT_ADDITIONAL_DIGITS;
  } else {
    additionalDigits = Number(additionalDigits);
  }

  var dateStrings = splitDateString(argument);

  var parseYearResult = parseYear(dateStrings.date, additionalDigits);
  var year = parseYearResult.year;
  var restDateString = parseYearResult.restDateString;

  var date = parseDate(restDateString, year);

  if (date) {
    var timestamp = date.getTime();
    var time = 0;
    var offset;

    if (dateStrings.time) {
      time = parseTime(dateStrings.time);
    }

    if (dateStrings.timezone) {
      offset = parseTimezone(dateStrings.timezone);
    } else {
      // get offset accurate to hour in timezones that change offset
      offset = new Date(timestamp + time).getTimezoneOffset();
      offset = new Date(timestamp + time + offset * MILLISECONDS_IN_MINUTE).getTimezoneOffset();
    }

    return new Date(timestamp + time + offset * MILLISECONDS_IN_MINUTE)
  } else {
    return new Date(argument)
  }
}

function splitDateString (dateString) {
  var dateStrings = {};
  var array = dateString.split(parseTokenDateTimeDelimeter);
  var timeString;

  if (parseTokenPlainTime.test(array[0])) {
    dateStrings.date = null;
    timeString = array[0];
  } else {
    dateStrings.date = array[0];
    timeString = array[1];
  }

  if (timeString) {
    var token = parseTokenTimezone.exec(timeString);
    if (token) {
      dateStrings.time = timeString.replace(token[1], '');
      dateStrings.timezone = token[1];
    } else {
      dateStrings.time = timeString;
    }
  }

  return dateStrings
}

function parseYear (dateString, additionalDigits) {
  var parseTokenYYY = parseTokensYYY[additionalDigits];
  var parseTokenYYYYY = parseTokensYYYYY[additionalDigits];

  var token;

  // YYYY or ±YYYYY
  token = parseTokenYYYY.exec(dateString) || parseTokenYYYYY.exec(dateString);
  if (token) {
    var yearString = token[1];
    return {
      year: parseInt(yearString, 10),
      restDateString: dateString.slice(yearString.length)
    }
  }

  // YY or ±YYY
  token = parseTokenYY.exec(dateString) || parseTokenYYY.exec(dateString);
  if (token) {
    var centuryString = token[1];
    return {
      year: parseInt(centuryString, 10) * 100,
      restDateString: dateString.slice(centuryString.length)
    }
  }

  // Invalid ISO-formatted year
  return {
    year: null
  }
}

function parseDate (dateString, year) {
  // Invalid ISO-formatted year
  if (year === null) {
    return null
  }

  var token;
  var date;
  var month;
  var week;

  // YYYY
  if (dateString.length === 0) {
    date = new Date(0);
    date.setUTCFullYear(year);
    return date
  }

  // YYYY-MM
  token = parseTokenMM.exec(dateString);
  if (token) {
    date = new Date(0);
    month = parseInt(token[1], 10) - 1;
    date.setUTCFullYear(year, month);
    return date
  }

  // YYYY-DDD or YYYYDDD
  token = parseTokenDDD.exec(dateString);
  if (token) {
    date = new Date(0);
    var dayOfYear = parseInt(token[1], 10);
    date.setUTCFullYear(year, 0, dayOfYear);
    return date
  }

  // YYYY-MM-DD or YYYYMMDD
  token = parseTokenMMDD.exec(dateString);
  if (token) {
    date = new Date(0);
    month = parseInt(token[1], 10) - 1;
    var day = parseInt(token[2], 10);
    date.setUTCFullYear(year, month, day);
    return date
  }

  // YYYY-Www or YYYYWww
  token = parseTokenWww.exec(dateString);
  if (token) {
    week = parseInt(token[1], 10) - 1;
    return dayOfISOYear(year, week)
  }

  // YYYY-Www-D or YYYYWwwD
  token = parseTokenWwwD.exec(dateString);
  if (token) {
    week = parseInt(token[1], 10) - 1;
    var dayOfWeek = parseInt(token[2], 10) - 1;
    return dayOfISOYear(year, week, dayOfWeek)
  }

  // Invalid ISO-formatted date
  return null
}

function parseTime (timeString) {
  var token;
  var hours;
  var minutes;

  // hh
  token = parseTokenHH.exec(timeString);
  if (token) {
    hours = parseFloat(token[1].replace(',', '.'));
    return (hours % 24) * MILLISECONDS_IN_HOUR
  }

  // hh:mm or hhmm
  token = parseTokenHHMM.exec(timeString);
  if (token) {
    hours = parseInt(token[1], 10);
    minutes = parseFloat(token[2].replace(',', '.'));
    return (hours % 24) * MILLISECONDS_IN_HOUR +
      minutes * MILLISECONDS_IN_MINUTE
  }

  // hh:mm:ss or hhmmss
  token = parseTokenHHMMSS.exec(timeString);
  if (token) {
    hours = parseInt(token[1], 10);
    minutes = parseInt(token[2], 10);
    var seconds = parseFloat(token[3].replace(',', '.'));
    return (hours % 24) * MILLISECONDS_IN_HOUR +
      minutes * MILLISECONDS_IN_MINUTE +
      seconds * 1000
  }

  // Invalid ISO-formatted time
  return null
}

function parseTimezone (timezoneString) {
  var token;
  var absoluteOffset;

  // Z
  token = parseTokenTimezoneZ.exec(timezoneString);
  if (token) {
    return 0
  }

  // ±hh
  token = parseTokenTimezoneHH.exec(timezoneString);
  if (token) {
    absoluteOffset = parseInt(token[2], 10) * 60;
    return (token[1] === '+') ? -absoluteOffset : absoluteOffset
  }

  // ±hh:mm or ±hhmm
  token = parseTokenTimezoneHHMM.exec(timezoneString);
  if (token) {
    absoluteOffset = parseInt(token[2], 10) * 60 + parseInt(token[3], 10);
    return (token[1] === '+') ? -absoluteOffset : absoluteOffset
  }

  return 0
}

function dayOfISOYear (isoYear, week, day) {
  week = week || 0;
  day = day || 0;
  var date = new Date(0);
  date.setUTCFullYear(isoYear, 0, 4);
  var fourthOfJanuaryDay = date.getUTCDay() || 7;
  var diff = week * 7 + day + 1 - fourthOfJanuaryDay;
  date.setUTCDate(date.getUTCDate() + diff);
  return date
}

var parse_1 = parse;

/**
 * @category Day Helpers
 * @summary Add the specified number of days to the given date.
 *
 * @description
 * Add the specified number of days to the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} amount - the amount of days to be added
 * @returns {Date} the new date with the days added
 *
 * @example
 * // Add 10 days to 1 September 2014:
 * var result = addDays(new Date(2014, 8, 1), 10)
 * //=> Thu Sep 11 2014 00:00:00
 */
function addDays (dirtyDate, dirtyAmount) {
  var date = parse_1(dirtyDate);
  var amount = Number(dirtyAmount);
  date.setDate(date.getDate() + amount);
  return date
}

var add_days = addDays;

/**
 * @category Millisecond Helpers
 * @summary Add the specified number of milliseconds to the given date.
 *
 * @description
 * Add the specified number of milliseconds to the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} amount - the amount of milliseconds to be added
 * @returns {Date} the new date with the milliseconds added
 *
 * @example
 * // Add 750 milliseconds to 10 July 2014 12:45:30.000:
 * var result = addMilliseconds(new Date(2014, 6, 10, 12, 45, 30, 0), 750)
 * //=> Thu Jul 10 2014 12:45:30.750
 */
function addMilliseconds (dirtyDate, dirtyAmount) {
  var timestamp = parse_1(dirtyDate).getTime();
  var amount = Number(dirtyAmount);
  return new Date(timestamp + amount)
}

var add_milliseconds = addMilliseconds;

var MILLISECONDS_IN_HOUR$1 = 3600000;

/**
 * @category Hour Helpers
 * @summary Add the specified number of hours to the given date.
 *
 * @description
 * Add the specified number of hours to the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} amount - the amount of hours to be added
 * @returns {Date} the new date with the hours added
 *
 * @example
 * // Add 2 hours to 10 July 2014 23:00:00:
 * var result = addHours(new Date(2014, 6, 10, 23, 0), 2)
 * //=> Fri Jul 11 2014 01:00:00
 */
function addHours (dirtyDate, dirtyAmount) {
  var amount = Number(dirtyAmount);
  return add_milliseconds(dirtyDate, amount * MILLISECONDS_IN_HOUR$1)
}

var add_hours = addHours;

/**
 * @category Week Helpers
 * @summary Return the start of a week for the given date.
 *
 * @description
 * Return the start of a week for the given date.
 * The result will be in the local timezone.
 *
 * @param {Date|String|Number} date - the original date
 * @param {Object} [options] - the object with options
 * @param {Number} [options.weekStartsOn=0] - the index of the first day of the week (0 - Sunday)
 * @returns {Date} the start of a week
 *
 * @example
 * // The start of a week for 2 September 2014 11:55:00:
 * var result = startOfWeek(new Date(2014, 8, 2, 11, 55, 0))
 * //=> Sun Aug 31 2014 00:00:00
 *
 * @example
 * // If the week starts on Monday, the start of the week for 2 September 2014 11:55:00:
 * var result = startOfWeek(new Date(2014, 8, 2, 11, 55, 0), {weekStartsOn: 1})
 * //=> Mon Sep 01 2014 00:00:00
 */
function startOfWeek (dirtyDate, dirtyOptions) {
  var weekStartsOn = dirtyOptions ? (Number(dirtyOptions.weekStartsOn) || 0) : 0;

  var date = parse_1(dirtyDate);
  var day = date.getDay();
  var diff = (day < weekStartsOn ? 7 : 0) + day - weekStartsOn;

  date.setDate(date.getDate() - diff);
  date.setHours(0, 0, 0, 0);
  return date
}

var start_of_week = startOfWeek;

/**
 * @category ISO Week Helpers
 * @summary Return the start of an ISO week for the given date.
 *
 * @description
 * Return the start of an ISO week for the given date.
 * The result will be in the local timezone.
 *
 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the start of an ISO week
 *
 * @example
 * // The start of an ISO week for 2 September 2014 11:55:00:
 * var result = startOfISOWeek(new Date(2014, 8, 2, 11, 55, 0))
 * //=> Mon Sep 01 2014 00:00:00
 */
function startOfISOWeek (dirtyDate) {
  return start_of_week(dirtyDate, {weekStartsOn: 1})
}

var start_of_iso_week = startOfISOWeek;

/**
 * @category ISO Week-Numbering Year Helpers
 * @summary Get the ISO week-numbering year of the given date.
 *
 * @description
 * Get the ISO week-numbering year of the given date,
 * which always starts 3 days before the year's first Thursday.
 *
 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
 *
 * @param {Date|String|Number} date - the given date
 * @returns {Number} the ISO week-numbering year
 *
 * @example
 * // Which ISO-week numbering year is 2 January 2005?
 * var result = getISOYear(new Date(2005, 0, 2))
 * //=> 2004
 */
function getISOYear (dirtyDate) {
  var date = parse_1(dirtyDate);
  var year = date.getFullYear();

  var fourthOfJanuaryOfNextYear = new Date(0);
  fourthOfJanuaryOfNextYear.setFullYear(year + 1, 0, 4);
  fourthOfJanuaryOfNextYear.setHours(0, 0, 0, 0);
  var startOfNextYear = start_of_iso_week(fourthOfJanuaryOfNextYear);

  var fourthOfJanuaryOfThisYear = new Date(0);
  fourthOfJanuaryOfThisYear.setFullYear(year, 0, 4);
  fourthOfJanuaryOfThisYear.setHours(0, 0, 0, 0);
  var startOfThisYear = start_of_iso_week(fourthOfJanuaryOfThisYear);

  if (date.getTime() >= startOfNextYear.getTime()) {
    return year + 1
  } else if (date.getTime() >= startOfThisYear.getTime()) {
    return year
  } else {
    return year - 1
  }
}

var get_iso_year = getISOYear;

/**
 * @category ISO Week-Numbering Year Helpers
 * @summary Return the start of an ISO week-numbering year for the given date.
 *
 * @description
 * Return the start of an ISO week-numbering year,
 * which always starts 3 days before the year's first Thursday.
 * The result will be in the local timezone.
 *
 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the start of an ISO year
 *
 * @example
 * // The start of an ISO week-numbering year for 2 July 2005:
 * var result = startOfISOYear(new Date(2005, 6, 2))
 * //=> Mon Jan 03 2005 00:00:00
 */
function startOfISOYear (dirtyDate) {
  var year = get_iso_year(dirtyDate);
  var fourthOfJanuary = new Date(0);
  fourthOfJanuary.setFullYear(year, 0, 4);
  fourthOfJanuary.setHours(0, 0, 0, 0);
  var date = start_of_iso_week(fourthOfJanuary);
  return date
}

var start_of_iso_year = startOfISOYear;

/**
 * @category Day Helpers
 * @summary Return the start of a day for the given date.
 *
 * @description
 * Return the start of a day for the given date.
 * The result will be in the local timezone.
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the start of a day
 *
 * @example
 * // The start of a day for 2 September 2014 11:55:00:
 * var result = startOfDay(new Date(2014, 8, 2, 11, 55, 0))
 * //=> Tue Sep 02 2014 00:00:00
 */
function startOfDay (dirtyDate) {
  var date = parse_1(dirtyDate);
  date.setHours(0, 0, 0, 0);
  return date
}

var start_of_day = startOfDay;

var MILLISECONDS_IN_MINUTE$1 = 60000;
var MILLISECONDS_IN_DAY = 86400000;

/**
 * @category Day Helpers
 * @summary Get the number of calendar days between the given dates.
 *
 * @description
 * Get the number of calendar days between the given dates.
 *
 * @param {Date|String|Number} dateLeft - the later date
 * @param {Date|String|Number} dateRight - the earlier date
 * @returns {Number} the number of calendar days
 *
 * @example
 * // How many calendar days are between
 * // 2 July 2011 23:00:00 and 2 July 2012 00:00:00?
 * var result = differenceInCalendarDays(
 *   new Date(2012, 6, 2, 0, 0),
 *   new Date(2011, 6, 2, 23, 0)
 * )
 * //=> 366
 */
function differenceInCalendarDays (dirtyDateLeft, dirtyDateRight) {
  var startOfDayLeft = start_of_day(dirtyDateLeft);
  var startOfDayRight = start_of_day(dirtyDateRight);

  var timestampLeft = startOfDayLeft.getTime() -
    startOfDayLeft.getTimezoneOffset() * MILLISECONDS_IN_MINUTE$1;
  var timestampRight = startOfDayRight.getTime() -
    startOfDayRight.getTimezoneOffset() * MILLISECONDS_IN_MINUTE$1;

  // Round the number of days to the nearest integer
  // because the number of milliseconds in a day is not constant
  // (e.g. it's different in the day of the daylight saving time clock shift)
  return Math.round((timestampLeft - timestampRight) / MILLISECONDS_IN_DAY)
}

var difference_in_calendar_days = differenceInCalendarDays;

/**
 * @category ISO Week-Numbering Year Helpers
 * @summary Set the ISO week-numbering year to the given date.
 *
 * @description
 * Set the ISO week-numbering year to the given date,
 * saving the week number and the weekday number.
 *
 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} isoYear - the ISO week-numbering year of the new date
 * @returns {Date} the new date with the ISO week-numbering year setted
 *
 * @example
 * // Set ISO week-numbering year 2007 to 29 December 2008:
 * var result = setISOYear(new Date(2008, 11, 29), 2007)
 * //=> Mon Jan 01 2007 00:00:00
 */
function setISOYear (dirtyDate, dirtyISOYear) {
  var date = parse_1(dirtyDate);
  var isoYear = Number(dirtyISOYear);
  var diff = difference_in_calendar_days(date, start_of_iso_year(date));
  var fourthOfJanuary = new Date(0);
  fourthOfJanuary.setFullYear(isoYear, 0, 4);
  fourthOfJanuary.setHours(0, 0, 0, 0);
  date = start_of_iso_year(fourthOfJanuary);
  date.setDate(date.getDate() + diff);
  return date
}

var set_iso_year = setISOYear;

/**
 * @category ISO Week-Numbering Year Helpers
 * @summary Add the specified number of ISO week-numbering years to the given date.
 *
 * @description
 * Add the specified number of ISO week-numbering years to the given date.
 *
 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} amount - the amount of ISO week-numbering years to be added
 * @returns {Date} the new date with the ISO week-numbering years added
 *
 * @example
 * // Add 5 ISO week-numbering years to 2 July 2010:
 * var result = addISOYears(new Date(2010, 6, 2), 5)
 * //=> Fri Jun 26 2015 00:00:00
 */
function addISOYears (dirtyDate, dirtyAmount) {
  var amount = Number(dirtyAmount);
  return set_iso_year(dirtyDate, get_iso_year(dirtyDate) + amount)
}

var add_iso_years = addISOYears;

var MILLISECONDS_IN_MINUTE$2 = 60000;

/**
 * @category Minute Helpers
 * @summary Add the specified number of minutes to the given date.
 *
 * @description
 * Add the specified number of minutes to the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} amount - the amount of minutes to be added
 * @returns {Date} the new date with the minutes added
 *
 * @example
 * // Add 30 minutes to 10 July 2014 12:00:00:
 * var result = addMinutes(new Date(2014, 6, 10, 12, 0), 30)
 * //=> Thu Jul 10 2014 12:30:00
 */
function addMinutes (dirtyDate, dirtyAmount) {
  var amount = Number(dirtyAmount);
  return add_milliseconds(dirtyDate, amount * MILLISECONDS_IN_MINUTE$2)
}

var add_minutes = addMinutes;

/**
 * @category Month Helpers
 * @summary Get the number of days in a month of the given date.
 *
 * @description
 * Get the number of days in a month of the given date.
 *
 * @param {Date|String|Number} date - the given date
 * @returns {Number} the number of days in a month
 *
 * @example
 * // How many days are in February 2000?
 * var result = getDaysInMonth(new Date(2000, 1))
 * //=> 29
 */
function getDaysInMonth (dirtyDate) {
  var date = parse_1(dirtyDate);
  var year = date.getFullYear();
  var monthIndex = date.getMonth();
  var lastDayOfMonth = new Date(0);
  lastDayOfMonth.setFullYear(year, monthIndex + 1, 0);
  lastDayOfMonth.setHours(0, 0, 0, 0);
  return lastDayOfMonth.getDate()
}

var get_days_in_month = getDaysInMonth;

/**
 * @category Month Helpers
 * @summary Add the specified number of months to the given date.
 *
 * @description
 * Add the specified number of months to the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} amount - the amount of months to be added
 * @returns {Date} the new date with the months added
 *
 * @example
 * // Add 5 months to 1 September 2014:
 * var result = addMonths(new Date(2014, 8, 1), 5)
 * //=> Sun Feb 01 2015 00:00:00
 */
function addMonths (dirtyDate, dirtyAmount) {
  var date = parse_1(dirtyDate);
  var amount = Number(dirtyAmount);
  var desiredMonth = date.getMonth() + amount;
  var dateWithDesiredMonth = new Date(0);
  dateWithDesiredMonth.setFullYear(date.getFullYear(), desiredMonth, 1);
  dateWithDesiredMonth.setHours(0, 0, 0, 0);
  var daysInMonth = get_days_in_month(dateWithDesiredMonth);
  // Set the last day of the new month
  // if the original date was the last day of the longer month
  date.setMonth(desiredMonth, Math.min(daysInMonth, date.getDate()));
  return date
}

var add_months = addMonths;

/**
 * @category Quarter Helpers
 * @summary Add the specified number of year quarters to the given date.
 *
 * @description
 * Add the specified number of year quarters to the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} amount - the amount of quarters to be added
 * @returns {Date} the new date with the quarters added
 *
 * @example
 * // Add 1 quarter to 1 September 2014:
 * var result = addQuarters(new Date(2014, 8, 1), 1)
 * //=> Mon Dec 01 2014 00:00:00
 */
function addQuarters (dirtyDate, dirtyAmount) {
  var amount = Number(dirtyAmount);
  var months = amount * 3;
  return add_months(dirtyDate, months)
}

var add_quarters = addQuarters;

/**
 * @category Second Helpers
 * @summary Add the specified number of seconds to the given date.
 *
 * @description
 * Add the specified number of seconds to the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} amount - the amount of seconds to be added
 * @returns {Date} the new date with the seconds added
 *
 * @example
 * // Add 30 seconds to 10 July 2014 12:45:00:
 * var result = addSeconds(new Date(2014, 6, 10, 12, 45, 0), 30)
 * //=> Thu Jul 10 2014 12:45:30
 */
function addSeconds (dirtyDate, dirtyAmount) {
  var amount = Number(dirtyAmount);
  return add_milliseconds(dirtyDate, amount * 1000)
}

var add_seconds = addSeconds;

/**
 * @category Week Helpers
 * @summary Add the specified number of weeks to the given date.
 *
 * @description
 * Add the specified number of week to the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} amount - the amount of weeks to be added
 * @returns {Date} the new date with the weeks added
 *
 * @example
 * // Add 4 weeks to 1 September 2014:
 * var result = addWeeks(new Date(2014, 8, 1), 4)
 * //=> Mon Sep 29 2014 00:00:00
 */
function addWeeks (dirtyDate, dirtyAmount) {
  var amount = Number(dirtyAmount);
  var days = amount * 7;
  return add_days(dirtyDate, days)
}

var add_weeks = addWeeks;

/**
 * @category Year Helpers
 * @summary Add the specified number of years to the given date.
 *
 * @description
 * Add the specified number of years to the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} amount - the amount of years to be added
 * @returns {Date} the new date with the years added
 *
 * @example
 * // Add 5 years to 1 September 2014:
 * var result = addYears(new Date(2014, 8, 1), 5)
 * //=> Sun Sep 01 2019 00:00:00
 */
function addYears (dirtyDate, dirtyAmount) {
  var amount = Number(dirtyAmount);
  return add_months(dirtyDate, amount * 12)
}

var add_years = addYears;

/**
 * @category Range Helpers
 * @summary Is the given date range overlapping with another date range?
 *
 * @description
 * Is the given date range overlapping with another date range?
 *
 * @param {Date|String|Number} initialRangeStartDate - the start of the initial range
 * @param {Date|String|Number} initialRangeEndDate - the end of the initial range
 * @param {Date|String|Number} comparedRangeStartDate - the start of the range to compare it with
 * @param {Date|String|Number} comparedRangeEndDate - the end of the range to compare it with
 * @returns {Boolean} whether the date ranges are overlapping
 * @throws {Error} startDate of a date range cannot be after its endDate
 *
 * @example
 * // For overlapping date ranges:
 * areRangesOverlapping(
 *   new Date(2014, 0, 10), new Date(2014, 0, 20), new Date(2014, 0, 17), new Date(2014, 0, 21)
 * )
 * //=> true
 *
 * @example
 * // For non-overlapping date ranges:
 * areRangesOverlapping(
 *   new Date(2014, 0, 10), new Date(2014, 0, 20), new Date(2014, 0, 21), new Date(2014, 0, 22)
 * )
 * //=> false
 */
function areRangesOverlapping (dirtyInitialRangeStartDate, dirtyInitialRangeEndDate, dirtyComparedRangeStartDate, dirtyComparedRangeEndDate) {
  var initialStartTime = parse_1(dirtyInitialRangeStartDate).getTime();
  var initialEndTime = parse_1(dirtyInitialRangeEndDate).getTime();
  var comparedStartTime = parse_1(dirtyComparedRangeStartDate).getTime();
  var comparedEndTime = parse_1(dirtyComparedRangeEndDate).getTime();

  if (initialStartTime > initialEndTime || comparedStartTime > comparedEndTime) {
    throw new Error('The start of the range cannot be after the end of the range')
  }

  return initialStartTime < comparedEndTime && comparedStartTime < initialEndTime
}

var are_ranges_overlapping = areRangesOverlapping;

/**
 * @category Common Helpers
 * @summary Return an index of the closest date from the array comparing to the given date.
 *
 * @description
 * Return an index of the closest date from the array comparing to the given date.
 *
 * @param {Date|String|Number} dateToCompare - the date to compare with
 * @param {Date[]|String[]|Number[]} datesArray - the array to search
 * @returns {Number} an index of the date closest to the given date
 * @throws {TypeError} the second argument must be an instance of Array
 *
 * @example
 * // Which date is closer to 6 September 2015?
 * var dateToCompare = new Date(2015, 8, 6)
 * var datesArray = [
 *   new Date(2015, 0, 1),
 *   new Date(2016, 0, 1),
 *   new Date(2017, 0, 1)
 * ]
 * var result = closestIndexTo(dateToCompare, datesArray)
 * //=> 1
 */
function closestIndexTo (dirtyDateToCompare, dirtyDatesArray) {
  if (!(dirtyDatesArray instanceof Array)) {
    throw new TypeError(toString.call(dirtyDatesArray) + ' is not an instance of Array')
  }

  var dateToCompare = parse_1(dirtyDateToCompare);
  var timeToCompare = dateToCompare.getTime();

  var result;
  var minDistance;

  dirtyDatesArray.forEach(function (dirtyDate, index) {
    var currentDate = parse_1(dirtyDate);
    var distance = Math.abs(timeToCompare - currentDate.getTime());
    if (result === undefined || distance < minDistance) {
      result = index;
      minDistance = distance;
    }
  });

  return result
}

var closest_index_to = closestIndexTo;

/**
 * @category Common Helpers
 * @summary Return a date from the array closest to the given date.
 *
 * @description
 * Return a date from the array closest to the given date.
 *
 * @param {Date|String|Number} dateToCompare - the date to compare with
 * @param {Date[]|String[]|Number[]} datesArray - the array to search
 * @returns {Date} the date from the array closest to the given date
 * @throws {TypeError} the second argument must be an instance of Array
 *
 * @example
 * // Which date is closer to 6 September 2015: 1 January 2000 or 1 January 2030?
 * var dateToCompare = new Date(2015, 8, 6)
 * var result = closestTo(dateToCompare, [
 *   new Date(2000, 0, 1),
 *   new Date(2030, 0, 1)
 * ])
 * //=> Tue Jan 01 2030 00:00:00
 */
function closestTo (dirtyDateToCompare, dirtyDatesArray) {
  if (!(dirtyDatesArray instanceof Array)) {
    throw new TypeError(toString.call(dirtyDatesArray) + ' is not an instance of Array')
  }

  var dateToCompare = parse_1(dirtyDateToCompare);
  var timeToCompare = dateToCompare.getTime();

  var result;
  var minDistance;

  dirtyDatesArray.forEach(function (dirtyDate) {
    var currentDate = parse_1(dirtyDate);
    var distance = Math.abs(timeToCompare - currentDate.getTime());
    if (result === undefined || distance < minDistance) {
      result = currentDate;
      minDistance = distance;
    }
  });

  return result
}

var closest_to = closestTo;

/**
 * @category Common Helpers
 * @summary Compare the two dates and return -1, 0 or 1.
 *
 * @description
 * Compare the two dates and return 1 if the first date is after the second,
 * -1 if the first date is before the second or 0 if dates are equal.
 *
 * @param {Date|String|Number} dateLeft - the first date to compare
 * @param {Date|String|Number} dateRight - the second date to compare
 * @returns {Number} the result of the comparison
 *
 * @example
 * // Compare 11 February 1987 and 10 July 1989:
 * var result = compareAsc(
 *   new Date(1987, 1, 11),
 *   new Date(1989, 6, 10)
 * )
 * //=> -1
 *
 * @example
 * // Sort the array of dates:
 * var result = [
 *   new Date(1995, 6, 2),
 *   new Date(1987, 1, 11),
 *   new Date(1989, 6, 10)
 * ].sort(compareAsc)
 * //=> [
 * //   Wed Feb 11 1987 00:00:00,
 * //   Mon Jul 10 1989 00:00:00,
 * //   Sun Jul 02 1995 00:00:00
 * // ]
 */
function compareAsc (dirtyDateLeft, dirtyDateRight) {
  var dateLeft = parse_1(dirtyDateLeft);
  var timeLeft = dateLeft.getTime();
  var dateRight = parse_1(dirtyDateRight);
  var timeRight = dateRight.getTime();

  if (timeLeft < timeRight) {
    return -1
  } else if (timeLeft > timeRight) {
    return 1
  } else {
    return 0
  }
}

var compare_asc = compareAsc;

/**
 * @category Common Helpers
 * @summary Compare the two dates reverse chronologically and return -1, 0 or 1.
 *
 * @description
 * Compare the two dates and return -1 if the first date is after the second,
 * 1 if the first date is before the second or 0 if dates are equal.
 *
 * @param {Date|String|Number} dateLeft - the first date to compare
 * @param {Date|String|Number} dateRight - the second date to compare
 * @returns {Number} the result of the comparison
 *
 * @example
 * // Compare 11 February 1987 and 10 July 1989 reverse chronologically:
 * var result = compareDesc(
 *   new Date(1987, 1, 11),
 *   new Date(1989, 6, 10)
 * )
 * //=> 1
 *
 * @example
 * // Sort the array of dates in reverse chronological order:
 * var result = [
 *   new Date(1995, 6, 2),
 *   new Date(1987, 1, 11),
 *   new Date(1989, 6, 10)
 * ].sort(compareDesc)
 * //=> [
 * //   Sun Jul 02 1995 00:00:00,
 * //   Mon Jul 10 1989 00:00:00,
 * //   Wed Feb 11 1987 00:00:00
 * // ]
 */
function compareDesc (dirtyDateLeft, dirtyDateRight) {
  var dateLeft = parse_1(dirtyDateLeft);
  var timeLeft = dateLeft.getTime();
  var dateRight = parse_1(dirtyDateRight);
  var timeRight = dateRight.getTime();

  if (timeLeft > timeRight) {
    return -1
  } else if (timeLeft < timeRight) {
    return 1
  } else {
    return 0
  }
}

var compare_desc = compareDesc;

var MILLISECONDS_IN_MINUTE$3 = 60000;
var MILLISECONDS_IN_WEEK = 604800000;

/**
 * @category ISO Week Helpers
 * @summary Get the number of calendar ISO weeks between the given dates.
 *
 * @description
 * Get the number of calendar ISO weeks between the given dates.
 *
 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
 *
 * @param {Date|String|Number} dateLeft - the later date
 * @param {Date|String|Number} dateRight - the earlier date
 * @returns {Number} the number of calendar ISO weeks
 *
 * @example
 * // How many calendar ISO weeks are between 6 July 2014 and 21 July 2014?
 * var result = differenceInCalendarISOWeeks(
 *   new Date(2014, 6, 21),
 *   new Date(2014, 6, 6)
 * )
 * //=> 3
 */
function differenceInCalendarISOWeeks (dirtyDateLeft, dirtyDateRight) {
  var startOfISOWeekLeft = start_of_iso_week(dirtyDateLeft);
  var startOfISOWeekRight = start_of_iso_week(dirtyDateRight);

  var timestampLeft = startOfISOWeekLeft.getTime() -
    startOfISOWeekLeft.getTimezoneOffset() * MILLISECONDS_IN_MINUTE$3;
  var timestampRight = startOfISOWeekRight.getTime() -
    startOfISOWeekRight.getTimezoneOffset() * MILLISECONDS_IN_MINUTE$3;

  // Round the number of days to the nearest integer
  // because the number of milliseconds in a week is not constant
  // (e.g. it's different in the week of the daylight saving time clock shift)
  return Math.round((timestampLeft - timestampRight) / MILLISECONDS_IN_WEEK)
}

var difference_in_calendar_iso_weeks = differenceInCalendarISOWeeks;

/**
 * @category ISO Week-Numbering Year Helpers
 * @summary Get the number of calendar ISO week-numbering years between the given dates.
 *
 * @description
 * Get the number of calendar ISO week-numbering years between the given dates.
 *
 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
 *
 * @param {Date|String|Number} dateLeft - the later date
 * @param {Date|String|Number} dateRight - the earlier date
 * @returns {Number} the number of calendar ISO week-numbering years
 *
 * @example
 * // How many calendar ISO week-numbering years are 1 January 2010 and 1 January 2012?
 * var result = differenceInCalendarISOYears(
 *   new Date(2012, 0, 1),
 *   new Date(2010, 0, 1)
 * )
 * //=> 2
 */
function differenceInCalendarISOYears (dirtyDateLeft, dirtyDateRight) {
  return get_iso_year(dirtyDateLeft) - get_iso_year(dirtyDateRight)
}

var difference_in_calendar_iso_years = differenceInCalendarISOYears;

/**
 * @category Month Helpers
 * @summary Get the number of calendar months between the given dates.
 *
 * @description
 * Get the number of calendar months between the given dates.
 *
 * @param {Date|String|Number} dateLeft - the later date
 * @param {Date|String|Number} dateRight - the earlier date
 * @returns {Number} the number of calendar months
 *
 * @example
 * // How many calendar months are between 31 January 2014 and 1 September 2014?
 * var result = differenceInCalendarMonths(
 *   new Date(2014, 8, 1),
 *   new Date(2014, 0, 31)
 * )
 * //=> 8
 */
function differenceInCalendarMonths (dirtyDateLeft, dirtyDateRight) {
  var dateLeft = parse_1(dirtyDateLeft);
  var dateRight = parse_1(dirtyDateRight);

  var yearDiff = dateLeft.getFullYear() - dateRight.getFullYear();
  var monthDiff = dateLeft.getMonth() - dateRight.getMonth();

  return yearDiff * 12 + monthDiff
}

var difference_in_calendar_months = differenceInCalendarMonths;

/**
 * @category Quarter Helpers
 * @summary Get the year quarter of the given date.
 *
 * @description
 * Get the year quarter of the given date.
 *
 * @param {Date|String|Number} date - the given date
 * @returns {Number} the quarter
 *
 * @example
 * // Which quarter is 2 July 2014?
 * var result = getQuarter(new Date(2014, 6, 2))
 * //=> 3
 */
function getQuarter (dirtyDate) {
  var date = parse_1(dirtyDate);
  var quarter = Math.floor(date.getMonth() / 3) + 1;
  return quarter
}

var get_quarter = getQuarter;

/**
 * @category Quarter Helpers
 * @summary Get the number of calendar quarters between the given dates.
 *
 * @description
 * Get the number of calendar quarters between the given dates.
 *
 * @param {Date|String|Number} dateLeft - the later date
 * @param {Date|String|Number} dateRight - the earlier date
 * @returns {Number} the number of calendar quarters
 *
 * @example
 * // How many calendar quarters are between 31 December 2013 and 2 July 2014?
 * var result = differenceInCalendarQuarters(
 *   new Date(2014, 6, 2),
 *   new Date(2013, 11, 31)
 * )
 * //=> 3
 */
function differenceInCalendarQuarters (dirtyDateLeft, dirtyDateRight) {
  var dateLeft = parse_1(dirtyDateLeft);
  var dateRight = parse_1(dirtyDateRight);

  var yearDiff = dateLeft.getFullYear() - dateRight.getFullYear();
  var quarterDiff = get_quarter(dateLeft) - get_quarter(dateRight);

  return yearDiff * 4 + quarterDiff
}

var difference_in_calendar_quarters = differenceInCalendarQuarters;

var MILLISECONDS_IN_MINUTE$4 = 60000;
var MILLISECONDS_IN_WEEK$1 = 604800000;

/**
 * @category Week Helpers
 * @summary Get the number of calendar weeks between the given dates.
 *
 * @description
 * Get the number of calendar weeks between the given dates.
 *
 * @param {Date|String|Number} dateLeft - the later date
 * @param {Date|String|Number} dateRight - the earlier date
 * @param {Object} [options] - the object with options
 * @param {Number} [options.weekStartsOn=0] - the index of the first day of the week (0 - Sunday)
 * @returns {Number} the number of calendar weeks
 *
 * @example
 * // How many calendar weeks are between 5 July 2014 and 20 July 2014?
 * var result = differenceInCalendarWeeks(
 *   new Date(2014, 6, 20),
 *   new Date(2014, 6, 5)
 * )
 * //=> 3
 *
 * @example
 * // If the week starts on Monday,
 * // how many calendar weeks are between 5 July 2014 and 20 July 2014?
 * var result = differenceInCalendarWeeks(
 *   new Date(2014, 6, 20),
 *   new Date(2014, 6, 5),
 *   {weekStartsOn: 1}
 * )
 * //=> 2
 */
function differenceInCalendarWeeks (dirtyDateLeft, dirtyDateRight, dirtyOptions) {
  var startOfWeekLeft = start_of_week(dirtyDateLeft, dirtyOptions);
  var startOfWeekRight = start_of_week(dirtyDateRight, dirtyOptions);

  var timestampLeft = startOfWeekLeft.getTime() -
    startOfWeekLeft.getTimezoneOffset() * MILLISECONDS_IN_MINUTE$4;
  var timestampRight = startOfWeekRight.getTime() -
    startOfWeekRight.getTimezoneOffset() * MILLISECONDS_IN_MINUTE$4;

  // Round the number of days to the nearest integer
  // because the number of milliseconds in a week is not constant
  // (e.g. it's different in the week of the daylight saving time clock shift)
  return Math.round((timestampLeft - timestampRight) / MILLISECONDS_IN_WEEK$1)
}

var difference_in_calendar_weeks = differenceInCalendarWeeks;

/**
 * @category Year Helpers
 * @summary Get the number of calendar years between the given dates.
 *
 * @description
 * Get the number of calendar years between the given dates.
 *
 * @param {Date|String|Number} dateLeft - the later date
 * @param {Date|String|Number} dateRight - the earlier date
 * @returns {Number} the number of calendar years
 *
 * @example
 * // How many calendar years are between 31 December 2013 and 11 February 2015?
 * var result = differenceInCalendarYears(
 *   new Date(2015, 1, 11),
 *   new Date(2013, 11, 31)
 * )
 * //=> 2
 */
function differenceInCalendarYears (dirtyDateLeft, dirtyDateRight) {
  var dateLeft = parse_1(dirtyDateLeft);
  var dateRight = parse_1(dirtyDateRight);

  return dateLeft.getFullYear() - dateRight.getFullYear()
}

var difference_in_calendar_years = differenceInCalendarYears;

/**
 * @category Day Helpers
 * @summary Get the number of full days between the given dates.
 *
 * @description
 * Get the number of full days between the given dates.
 *
 * @param {Date|String|Number} dateLeft - the later date
 * @param {Date|String|Number} dateRight - the earlier date
 * @returns {Number} the number of full days
 *
 * @example
 * // How many full days are between
 * // 2 July 2011 23:00:00 and 2 July 2012 00:00:00?
 * var result = differenceInDays(
 *   new Date(2012, 6, 2, 0, 0),
 *   new Date(2011, 6, 2, 23, 0)
 * )
 * //=> 365
 */
function differenceInDays (dirtyDateLeft, dirtyDateRight) {
  var dateLeft = parse_1(dirtyDateLeft);
  var dateRight = parse_1(dirtyDateRight);

  var sign = compare_asc(dateLeft, dateRight);
  var difference = Math.abs(difference_in_calendar_days(dateLeft, dateRight));
  dateLeft.setDate(dateLeft.getDate() - sign * difference);

  // Math.abs(diff in full days - diff in calendar days) === 1 if last calendar day is not full
  // If so, result must be decreased by 1 in absolute value
  var isLastDayNotFull = compare_asc(dateLeft, dateRight) === -sign;
  return sign * (difference - isLastDayNotFull)
}

var difference_in_days = differenceInDays;

/**
 * @category Millisecond Helpers
 * @summary Get the number of milliseconds between the given dates.
 *
 * @description
 * Get the number of milliseconds between the given dates.
 *
 * @param {Date|String|Number} dateLeft - the later date
 * @param {Date|String|Number} dateRight - the earlier date
 * @returns {Number} the number of milliseconds
 *
 * @example
 * // How many milliseconds are between
 * // 2 July 2014 12:30:20.600 and 2 July 2014 12:30:21.700?
 * var result = differenceInMilliseconds(
 *   new Date(2014, 6, 2, 12, 30, 21, 700),
 *   new Date(2014, 6, 2, 12, 30, 20, 600)
 * )
 * //=> 1100
 */
function differenceInMilliseconds (dirtyDateLeft, dirtyDateRight) {
  var dateLeft = parse_1(dirtyDateLeft);
  var dateRight = parse_1(dirtyDateRight);
  return dateLeft.getTime() - dateRight.getTime()
}

var difference_in_milliseconds = differenceInMilliseconds;

var MILLISECONDS_IN_HOUR$2 = 3600000;

/**
 * @category Hour Helpers
 * @summary Get the number of hours between the given dates.
 *
 * @description
 * Get the number of hours between the given dates.
 *
 * @param {Date|String|Number} dateLeft - the later date
 * @param {Date|String|Number} dateRight - the earlier date
 * @returns {Number} the number of hours
 *
 * @example
 * // How many hours are between 2 July 2014 06:50:00 and 2 July 2014 19:00:00?
 * var result = differenceInHours(
 *   new Date(2014, 6, 2, 19, 0),
 *   new Date(2014, 6, 2, 6, 50)
 * )
 * //=> 12
 */
function differenceInHours (dirtyDateLeft, dirtyDateRight) {
  var diff = difference_in_milliseconds(dirtyDateLeft, dirtyDateRight) / MILLISECONDS_IN_HOUR$2;
  return diff > 0 ? Math.floor(diff) : Math.ceil(diff)
}

var difference_in_hours = differenceInHours;

/**
 * @category ISO Week-Numbering Year Helpers
 * @summary Subtract the specified number of ISO week-numbering years from the given date.
 *
 * @description
 * Subtract the specified number of ISO week-numbering years from the given date.
 *
 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} amount - the amount of ISO week-numbering years to be subtracted
 * @returns {Date} the new date with the ISO week-numbering years subtracted
 *
 * @example
 * // Subtract 5 ISO week-numbering years from 1 September 2014:
 * var result = subISOYears(new Date(2014, 8, 1), 5)
 * //=> Mon Aug 31 2009 00:00:00
 */
function subISOYears (dirtyDate, dirtyAmount) {
  var amount = Number(dirtyAmount);
  return add_iso_years(dirtyDate, -amount)
}

var sub_iso_years = subISOYears;

/**
 * @category ISO Week-Numbering Year Helpers
 * @summary Get the number of full ISO week-numbering years between the given dates.
 *
 * @description
 * Get the number of full ISO week-numbering years between the given dates.
 *
 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
 *
 * @param {Date|String|Number} dateLeft - the later date
 * @param {Date|String|Number} dateRight - the earlier date
 * @returns {Number} the number of full ISO week-numbering years
 *
 * @example
 * // How many full ISO week-numbering years are between 1 January 2010 and 1 January 2012?
 * var result = differenceInISOYears(
 *   new Date(2012, 0, 1),
 *   new Date(2010, 0, 1)
 * )
 * //=> 1
 */
function differenceInISOYears (dirtyDateLeft, dirtyDateRight) {
  var dateLeft = parse_1(dirtyDateLeft);
  var dateRight = parse_1(dirtyDateRight);

  var sign = compare_asc(dateLeft, dateRight);
  var difference = Math.abs(difference_in_calendar_iso_years(dateLeft, dateRight));
  dateLeft = sub_iso_years(dateLeft, sign * difference);

  // Math.abs(diff in full ISO years - diff in calendar ISO years) === 1
  // if last calendar ISO year is not full
  // If so, result must be decreased by 1 in absolute value
  var isLastISOYearNotFull = compare_asc(dateLeft, dateRight) === -sign;
  return sign * (difference - isLastISOYearNotFull)
}

var difference_in_iso_years = differenceInISOYears;

var MILLISECONDS_IN_MINUTE$5 = 60000;

/**
 * @category Minute Helpers
 * @summary Get the number of minutes between the given dates.
 *
 * @description
 * Get the number of minutes between the given dates.
 *
 * @param {Date|String|Number} dateLeft - the later date
 * @param {Date|String|Number} dateRight - the earlier date
 * @returns {Number} the number of minutes
 *
 * @example
 * // How many minutes are between 2 July 2014 12:07:59 and 2 July 2014 12:20:00?
 * var result = differenceInMinutes(
 *   new Date(2014, 6, 2, 12, 20, 0),
 *   new Date(2014, 6, 2, 12, 7, 59)
 * )
 * //=> 12
 */
function differenceInMinutes (dirtyDateLeft, dirtyDateRight) {
  var diff = difference_in_milliseconds(dirtyDateLeft, dirtyDateRight) / MILLISECONDS_IN_MINUTE$5;
  return diff > 0 ? Math.floor(diff) : Math.ceil(diff)
}

var difference_in_minutes = differenceInMinutes;

/**
 * @category Month Helpers
 * @summary Get the number of full months between the given dates.
 *
 * @description
 * Get the number of full months between the given dates.
 *
 * @param {Date|String|Number} dateLeft - the later date
 * @param {Date|String|Number} dateRight - the earlier date
 * @returns {Number} the number of full months
 *
 * @example
 * // How many full months are between 31 January 2014 and 1 September 2014?
 * var result = differenceInMonths(
 *   new Date(2014, 8, 1),
 *   new Date(2014, 0, 31)
 * )
 * //=> 7
 */
function differenceInMonths (dirtyDateLeft, dirtyDateRight) {
  var dateLeft = parse_1(dirtyDateLeft);
  var dateRight = parse_1(dirtyDateRight);

  var sign = compare_asc(dateLeft, dateRight);
  var difference = Math.abs(difference_in_calendar_months(dateLeft, dateRight));
  dateLeft.setMonth(dateLeft.getMonth() - sign * difference);

  // Math.abs(diff in full months - diff in calendar months) === 1 if last calendar month is not full
  // If so, result must be decreased by 1 in absolute value
  var isLastMonthNotFull = compare_asc(dateLeft, dateRight) === -sign;
  return sign * (difference - isLastMonthNotFull)
}

var difference_in_months = differenceInMonths;

/**
 * @category Quarter Helpers
 * @summary Get the number of full quarters between the given dates.
 *
 * @description
 * Get the number of full quarters between the given dates.
 *
 * @param {Date|String|Number} dateLeft - the later date
 * @param {Date|String|Number} dateRight - the earlier date
 * @returns {Number} the number of full quarters
 *
 * @example
 * // How many full quarters are between 31 December 2013 and 2 July 2014?
 * var result = differenceInQuarters(
 *   new Date(2014, 6, 2),
 *   new Date(2013, 11, 31)
 * )
 * //=> 2
 */
function differenceInQuarters (dirtyDateLeft, dirtyDateRight) {
  var diff = difference_in_months(dirtyDateLeft, dirtyDateRight) / 3;
  return diff > 0 ? Math.floor(diff) : Math.ceil(diff)
}

var difference_in_quarters = differenceInQuarters;

/**
 * @category Second Helpers
 * @summary Get the number of seconds between the given dates.
 *
 * @description
 * Get the number of seconds between the given dates.
 *
 * @param {Date|String|Number} dateLeft - the later date
 * @param {Date|String|Number} dateRight - the earlier date
 * @returns {Number} the number of seconds
 *
 * @example
 * // How many seconds are between
 * // 2 July 2014 12:30:07.999 and 2 July 2014 12:30:20.000?
 * var result = differenceInSeconds(
 *   new Date(2014, 6, 2, 12, 30, 20, 0),
 *   new Date(2014, 6, 2, 12, 30, 7, 999)
 * )
 * //=> 12
 */
function differenceInSeconds (dirtyDateLeft, dirtyDateRight) {
  var diff = difference_in_milliseconds(dirtyDateLeft, dirtyDateRight) / 1000;
  return diff > 0 ? Math.floor(diff) : Math.ceil(diff)
}

var difference_in_seconds = differenceInSeconds;

/**
 * @category Week Helpers
 * @summary Get the number of full weeks between the given dates.
 *
 * @description
 * Get the number of full weeks between the given dates.
 *
 * @param {Date|String|Number} dateLeft - the later date
 * @param {Date|String|Number} dateRight - the earlier date
 * @returns {Number} the number of full weeks
 *
 * @example
 * // How many full weeks are between 5 July 2014 and 20 July 2014?
 * var result = differenceInWeeks(
 *   new Date(2014, 6, 20),
 *   new Date(2014, 6, 5)
 * )
 * //=> 2
 */
function differenceInWeeks (dirtyDateLeft, dirtyDateRight) {
  var diff = difference_in_days(dirtyDateLeft, dirtyDateRight) / 7;
  return diff > 0 ? Math.floor(diff) : Math.ceil(diff)
}

var difference_in_weeks = differenceInWeeks;

/**
 * @category Year Helpers
 * @summary Get the number of full years between the given dates.
 *
 * @description
 * Get the number of full years between the given dates.
 *
 * @param {Date|String|Number} dateLeft - the later date
 * @param {Date|String|Number} dateRight - the earlier date
 * @returns {Number} the number of full years
 *
 * @example
 * // How many full years are between 31 December 2013 and 11 February 2015?
 * var result = differenceInYears(
 *   new Date(2015, 1, 11),
 *   new Date(2013, 11, 31)
 * )
 * //=> 1
 */
function differenceInYears (dirtyDateLeft, dirtyDateRight) {
  var dateLeft = parse_1(dirtyDateLeft);
  var dateRight = parse_1(dirtyDateRight);

  var sign = compare_asc(dateLeft, dateRight);
  var difference = Math.abs(difference_in_calendar_years(dateLeft, dateRight));
  dateLeft.setFullYear(dateLeft.getFullYear() - sign * difference);

  // Math.abs(diff in full years - diff in calendar years) === 1 if last calendar year is not full
  // If so, result must be decreased by 1 in absolute value
  var isLastYearNotFull = compare_asc(dateLeft, dateRight) === -sign;
  return sign * (difference - isLastYearNotFull)
}

var difference_in_years = differenceInYears;

function buildDistanceInWordsLocale () {
  var distanceInWordsLocale = {
    lessThanXSeconds: {
      one: 'less than a second',
      other: 'less than {{count}} seconds'
    },

    xSeconds: {
      one: '1 second',
      other: '{{count}} seconds'
    },

    halfAMinute: 'half a minute',

    lessThanXMinutes: {
      one: 'less than a minute',
      other: 'less than {{count}} minutes'
    },

    xMinutes: {
      one: '1 minute',
      other: '{{count}} minutes'
    },

    aboutXHours: {
      one: 'about 1 hour',
      other: 'about {{count}} hours'
    },

    xHours: {
      one: '1 hour',
      other: '{{count}} hours'
    },

    xDays: {
      one: '1 day',
      other: '{{count}} days'
    },

    aboutXMonths: {
      one: 'about 1 month',
      other: 'about {{count}} months'
    },

    xMonths: {
      one: '1 month',
      other: '{{count}} months'
    },

    aboutXYears: {
      one: 'about 1 year',
      other: 'about {{count}} years'
    },

    xYears: {
      one: '1 year',
      other: '{{count}} years'
    },

    overXYears: {
      one: 'over 1 year',
      other: 'over {{count}} years'
    },

    almostXYears: {
      one: 'almost 1 year',
      other: 'almost {{count}} years'
    }
  };

  function localize (token, count, options) {
    options = options || {};

    var result;
    if (typeof distanceInWordsLocale[token] === 'string') {
      result = distanceInWordsLocale[token];
    } else if (count === 1) {
      result = distanceInWordsLocale[token].one;
    } else {
      result = distanceInWordsLocale[token].other.replace('{{count}}', count);
    }

    if (options.addSuffix) {
      if (options.comparison > 0) {
        return 'in ' + result
      } else {
        return result + ' ago'
      }
    }

    return result
  }

  return {
    localize: localize
  }
}

var build_distance_in_words_locale = buildDistanceInWordsLocale;

var commonFormatterKeys = [
  'M', 'MM', 'Q', 'D', 'DD', 'DDD', 'DDDD', 'd',
  'E', 'W', 'WW', 'YY', 'YYYY', 'GG', 'GGGG',
  'H', 'HH', 'h', 'hh', 'm', 'mm',
  's', 'ss', 'S', 'SS', 'SSS',
  'Z', 'ZZ', 'X', 'x'
];

function buildFormattingTokensRegExp (formatters) {
  var formatterKeys = [];
  for (var key in formatters) {
    if (formatters.hasOwnProperty(key)) {
      formatterKeys.push(key);
    }
  }

  var formattingTokens = commonFormatterKeys
    .concat(formatterKeys)
    .sort()
    .reverse();
  var formattingTokensRegExp = new RegExp(
    '(\\[[^\\[]*\\])|(\\\\)?' + '(' + formattingTokens.join('|') + '|.)', 'g'
  );

  return formattingTokensRegExp
}

var build_formatting_tokens_reg_exp = buildFormattingTokensRegExp;

function buildFormatLocale () {
  // Note: in English, the names of days of the week and months are capitalized.
  // If you are making a new locale based on this one, check if the same is true for the language you're working on.
  // Generally, formatted dates should look like they are in the middle of a sentence,
  // e.g. in Spanish language the weekdays and months should be in the lowercase.
  var months3char = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  var monthsFull = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  var weekdays2char = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];
  var weekdays3char = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  var weekdaysFull = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  var meridiemUppercase = ['AM', 'PM'];
  var meridiemLowercase = ['am', 'pm'];
  var meridiemFull = ['a.m.', 'p.m.'];

  var formatters = {
    // Month: Jan, Feb, ..., Dec
    'MMM': function (date) {
      return months3char[date.getMonth()]
    },

    // Month: January, February, ..., December
    'MMMM': function (date) {
      return monthsFull[date.getMonth()]
    },

    // Day of week: Su, Mo, ..., Sa
    'dd': function (date) {
      return weekdays2char[date.getDay()]
    },

    // Day of week: Sun, Mon, ..., Sat
    'ddd': function (date) {
      return weekdays3char[date.getDay()]
    },

    // Day of week: Sunday, Monday, ..., Saturday
    'dddd': function (date) {
      return weekdaysFull[date.getDay()]
    },

    // AM, PM
    'A': function (date) {
      return (date.getHours() / 12) >= 1 ? meridiemUppercase[1] : meridiemUppercase[0]
    },

    // am, pm
    'a': function (date) {
      return (date.getHours() / 12) >= 1 ? meridiemLowercase[1] : meridiemLowercase[0]
    },

    // a.m., p.m.
    'aa': function (date) {
      return (date.getHours() / 12) >= 1 ? meridiemFull[1] : meridiemFull[0]
    }
  };

  // Generate ordinal version of formatters: M -> Mo, D -> Do, etc.
  var ordinalFormatters = ['M', 'D', 'DDD', 'd', 'Q', 'W'];
  ordinalFormatters.forEach(function (formatterToken) {
    formatters[formatterToken + 'o'] = function (date, formatters) {
      return ordinal(formatters[formatterToken](date))
    };
  });

  return {
    formatters: formatters,
    formattingTokensRegExp: build_formatting_tokens_reg_exp(formatters)
  }
}

function ordinal (number) {
  var rem100 = number % 100;
  if (rem100 > 20 || rem100 < 10) {
    switch (rem100 % 10) {
      case 1:
        return number + 'st'
      case 2:
        return number + 'nd'
      case 3:
        return number + 'rd'
    }
  }
  return number + 'th'
}

var build_format_locale = buildFormatLocale;

/**
 * @category Locales
 * @summary English locale.
 */
var en = {
  distanceInWords: build_distance_in_words_locale(),
  format: build_format_locale()
};

var MINUTES_IN_DAY = 1440;
var MINUTES_IN_ALMOST_TWO_DAYS = 2520;
var MINUTES_IN_MONTH = 43200;
var MINUTES_IN_TWO_MONTHS = 86400;

/**
 * @category Common Helpers
 * @summary Return the distance between the given dates in words.
 *
 * @description
 * Return the distance between the given dates in words.
 *
 * | Distance between dates                                            | Result              |
 * |-------------------------------------------------------------------|---------------------|
 * | 0 ... 30 secs                                                     | less than a minute  |
 * | 30 secs ... 1 min 30 secs                                         | 1 minute            |
 * | 1 min 30 secs ... 44 mins 30 secs                                 | [2..44] minutes     |
 * | 44 mins ... 30 secs ... 89 mins 30 secs                           | about 1 hour        |
 * | 89 mins 30 secs ... 23 hrs 59 mins 30 secs                        | about [2..24] hours |
 * | 23 hrs 59 mins 30 secs ... 41 hrs 59 mins 30 secs                 | 1 day               |
 * | 41 hrs 59 mins 30 secs ... 29 days 23 hrs 59 mins 30 secs         | [2..30] days        |
 * | 29 days 23 hrs 59 mins 30 secs ... 44 days 23 hrs 59 mins 30 secs | about 1 month       |
 * | 44 days 23 hrs 59 mins 30 secs ... 59 days 23 hrs 59 mins 30 secs | about 2 months      |
 * | 59 days 23 hrs 59 mins 30 secs ... 1 yr                           | [2..12] months      |
 * | 1 yr ... 1 yr 3 months                                            | about 1 year        |
 * | 1 yr 3 months ... 1 yr 9 month s                                  | over 1 year         |
 * | 1 yr 9 months ... 2 yrs                                           | almost 2 years      |
 * | N yrs ... N yrs 3 months                                          | about N years       |
 * | N yrs 3 months ... N yrs 9 months                                 | over N years        |
 * | N yrs 9 months ... N+1 yrs                                        | almost N+1 years    |
 *
 * With `options.includeSeconds == true`:
 * | Distance between dates | Result               |
 * |------------------------|----------------------|
 * | 0 secs ... 5 secs      | less than 5 seconds  |
 * | 5 secs ... 10 secs     | less than 10 seconds |
 * | 10 secs ... 20 secs    | less than 20 seconds |
 * | 20 secs ... 40 secs    | half a minute        |
 * | 40 secs ... 60 secs    | less than a minute   |
 * | 60 secs ... 90 secs    | 1 minute             |
 *
 * @param {Date|String|Number} dateToCompare - the date to compare with
 * @param {Date|String|Number} date - the other date
 * @param {Object} [options] - the object with options
 * @param {Boolean} [options.includeSeconds=false] - distances less than a minute are more detailed
 * @param {Boolean} [options.addSuffix=false] - result indicates if the second date is earlier or later than the first
 * @param {Object} [options.locale=enLocale] - the locale object
 * @returns {String} the distance in words
 *
 * @example
 * // What is the distance between 2 July 2014 and 1 January 2015?
 * var result = distanceInWords(
 *   new Date(2014, 6, 2),
 *   new Date(2015, 0, 1)
 * )
 * //=> '6 months'
 *
 * @example
 * // What is the distance between 1 January 2015 00:00:15
 * // and 1 January 2015 00:00:00, including seconds?
 * var result = distanceInWords(
 *   new Date(2015, 0, 1, 0, 0, 15),
 *   new Date(2015, 0, 1, 0, 0, 0),
 *   {includeSeconds: true}
 * )
 * //=> 'less than 20 seconds'
 *
 * @example
 * // What is the distance from 1 January 2016
 * // to 1 January 2015, with a suffix?
 * var result = distanceInWords(
 *   new Date(2016, 0, 1),
 *   new Date(2015, 0, 1),
 *   {addSuffix: true}
 * )
 * //=> 'about 1 year ago'
 *
 * @example
 * // What is the distance between 1 August 2016 and 1 January 2015 in Esperanto?
 * var eoLocale = require('date-fns/locale/eo')
 * var result = distanceInWords(
 *   new Date(2016, 7, 1),
 *   new Date(2015, 0, 1),
 *   {locale: eoLocale}
 * )
 * //=> 'pli ol 1 jaro'
 */
function distanceInWords (dirtyDateToCompare, dirtyDate, dirtyOptions) {
  var options = dirtyOptions || {};

  var comparison = compare_desc(dirtyDateToCompare, dirtyDate);

  var locale = options.locale;
  var localize = en.distanceInWords.localize;
  if (locale && locale.distanceInWords && locale.distanceInWords.localize) {
    localize = locale.distanceInWords.localize;
  }

  var localizeOptions = {
    addSuffix: Boolean(options.addSuffix),
    comparison: comparison
  };

  var dateLeft, dateRight;
  if (comparison > 0) {
    dateLeft = parse_1(dirtyDateToCompare);
    dateRight = parse_1(dirtyDate);
  } else {
    dateLeft = parse_1(dirtyDate);
    dateRight = parse_1(dirtyDateToCompare);
  }

  var seconds = difference_in_seconds(dateRight, dateLeft);
  var offset = dateRight.getTimezoneOffset() - dateLeft.getTimezoneOffset();
  var minutes = Math.round(seconds / 60) - offset;
  var months;

  // 0 up to 2 mins
  if (minutes < 2) {
    if (options.includeSeconds) {
      if (seconds < 5) {
        return localize('lessThanXSeconds', 5, localizeOptions)
      } else if (seconds < 10) {
        return localize('lessThanXSeconds', 10, localizeOptions)
      } else if (seconds < 20) {
        return localize('lessThanXSeconds', 20, localizeOptions)
      } else if (seconds < 40) {
        return localize('halfAMinute', null, localizeOptions)
      } else if (seconds < 60) {
        return localize('lessThanXMinutes', 1, localizeOptions)
      } else {
        return localize('xMinutes', 1, localizeOptions)
      }
    } else {
      if (minutes === 0) {
        return localize('lessThanXMinutes', 1, localizeOptions)
      } else {
        return localize('xMinutes', minutes, localizeOptions)
      }
    }

  // 2 mins up to 0.75 hrs
  } else if (minutes < 45) {
    return localize('xMinutes', minutes, localizeOptions)

  // 0.75 hrs up to 1.5 hrs
  } else if (minutes < 90) {
    return localize('aboutXHours', 1, localizeOptions)

  // 1.5 hrs up to 24 hrs
  } else if (minutes < MINUTES_IN_DAY) {
    var hours = Math.round(minutes / 60);
    return localize('aboutXHours', hours, localizeOptions)

  // 1 day up to 1.75 days
  } else if (minutes < MINUTES_IN_ALMOST_TWO_DAYS) {
    return localize('xDays', 1, localizeOptions)

  // 1.75 days up to 30 days
  } else if (minutes < MINUTES_IN_MONTH) {
    var days = Math.round(minutes / MINUTES_IN_DAY);
    return localize('xDays', days, localizeOptions)

  // 1 month up to 2 months
  } else if (minutes < MINUTES_IN_TWO_MONTHS) {
    months = Math.round(minutes / MINUTES_IN_MONTH);
    return localize('aboutXMonths', months, localizeOptions)
  }

  months = difference_in_months(dateRight, dateLeft);

  // 2 months up to 12 months
  if (months < 12) {
    var nearestMonth = Math.round(minutes / MINUTES_IN_MONTH);
    return localize('xMonths', nearestMonth, localizeOptions)

  // 1 year up to max Date
  } else {
    var monthsSinceStartOfYear = months % 12;
    var years = Math.floor(months / 12);

    // N years up to 1 years 3 months
    if (monthsSinceStartOfYear < 3) {
      return localize('aboutXYears', years, localizeOptions)

    // N years 3 months up to N years 9 months
    } else if (monthsSinceStartOfYear < 9) {
      return localize('overXYears', years, localizeOptions)

    // N years 9 months up to N year 12 months
    } else {
      return localize('almostXYears', years + 1, localizeOptions)
    }
  }
}

var distance_in_words = distanceInWords;

var MINUTES_IN_DAY$1 = 1440;
var MINUTES_IN_MONTH$1 = 43200;
var MINUTES_IN_YEAR = 525600;

/**
 * @category Common Helpers
 * @summary Return the distance between the given dates in words.
 *
 * @description
 * Return the distance between the given dates in words, using strict units.
 * This is like `distanceInWords`, but does not use helpers like 'almost', 'over',
 * 'less than' and the like.
 *
 * | Distance between dates | Result              |
 * |------------------------|---------------------|
 * | 0 ... 59 secs          | [0..59] seconds     |
 * | 1 ... 59 mins          | [1..59] minutes     |
 * | 1 ... 23 hrs           | [1..23] hours       |
 * | 1 ... 29 days          | [1..29] days        |
 * | 1 ... 11 months        | [1..11] months      |
 * | 1 ... N years          | [1..N]  years       |
 *
 * @param {Date|String|Number} dateToCompare - the date to compare with
 * @param {Date|String|Number} date - the other date
 * @param {Object} [options] - the object with options
 * @param {Boolean} [options.addSuffix=false] - result indicates if the second date is earlier or later than the first
 * @param {'s'|'m'|'h'|'d'|'M'|'Y'} [options.unit] - if specified, will force a unit
 * @param {'floor'|'ceil'|'round'} [options.partialMethod='floor'] - which way to round partial units
 * @param {Object} [options.locale=enLocale] - the locale object
 * @returns {String} the distance in words
 *
 * @example
 * // What is the distance between 2 July 2014 and 1 January 2015?
 * var result = distanceInWordsStrict(
 *   new Date(2014, 6, 2),
 *   new Date(2015, 0, 2)
 * )
 * //=> '6 months'
 *
 * @example
 * // What is the distance between 1 January 2015 00:00:15
 * // and 1 January 2015 00:00:00?
 * var result = distanceInWordsStrict(
 *   new Date(2015, 0, 1, 0, 0, 15),
 *   new Date(2015, 0, 1, 0, 0, 0),
 * )
 * //=> '15 seconds'
 *
 * @example
 * // What is the distance from 1 January 2016
 * // to 1 January 2015, with a suffix?
 * var result = distanceInWordsStrict(
 *   new Date(2016, 0, 1),
 *   new Date(2015, 0, 1),
 *   {addSuffix: true}
 * )
 * //=> '1 year ago'
 *
 * @example
 * // What is the distance from 1 January 2016
 * // to 1 January 2015, in minutes?
 * var result = distanceInWordsStrict(
 *   new Date(2016, 0, 1),
 *   new Date(2015, 0, 1),
 *   {unit: 'm'}
 * )
 * //=> '525600 minutes'
 *
 * @example
 * // What is the distance from 1 January 2016
 * // to 28 January 2015, in months, rounded up?
 * var result = distanceInWordsStrict(
 *   new Date(2015, 0, 28),
 *   new Date(2015, 0, 1),
 *   {unit: 'M', partialMethod: 'ceil'}
 * )
 * //=> '1 month'
 *
 * @example
 * // What is the distance between 1 August 2016 and 1 January 2015 in Esperanto?
 * var eoLocale = require('date-fns/locale/eo')
 * var result = distanceInWordsStrict(
 *   new Date(2016, 7, 1),
 *   new Date(2015, 0, 1),
 *   {locale: eoLocale}
 * )
 * //=> '1 jaro'
 */
function distanceInWordsStrict (dirtyDateToCompare, dirtyDate, dirtyOptions) {
  var options = dirtyOptions || {};

  var comparison = compare_desc(dirtyDateToCompare, dirtyDate);

  var locale = options.locale;
  var localize = en.distanceInWords.localize;
  if (locale && locale.distanceInWords && locale.distanceInWords.localize) {
    localize = locale.distanceInWords.localize;
  }

  var localizeOptions = {
    addSuffix: Boolean(options.addSuffix),
    comparison: comparison
  };

  var dateLeft, dateRight;
  if (comparison > 0) {
    dateLeft = parse_1(dirtyDateToCompare);
    dateRight = parse_1(dirtyDate);
  } else {
    dateLeft = parse_1(dirtyDate);
    dateRight = parse_1(dirtyDateToCompare);
  }

  var unit;
  var mathPartial = Math[options.partialMethod ? String(options.partialMethod) : 'floor'];
  var seconds = difference_in_seconds(dateRight, dateLeft);
  var offset = dateRight.getTimezoneOffset() - dateLeft.getTimezoneOffset();
  var minutes = mathPartial(seconds / 60) - offset;
  var hours, days, months, years;

  if (options.unit) {
    unit = String(options.unit);
  } else {
    if (minutes < 1) {
      unit = 's';
    } else if (minutes < 60) {
      unit = 'm';
    } else if (minutes < MINUTES_IN_DAY$1) {
      unit = 'h';
    } else if (minutes < MINUTES_IN_MONTH$1) {
      unit = 'd';
    } else if (minutes < MINUTES_IN_YEAR) {
      unit = 'M';
    } else {
      unit = 'Y';
    }
  }

  // 0 up to 60 seconds
  if (unit === 's') {
    return localize('xSeconds', seconds, localizeOptions)

  // 1 up to 60 mins
  } else if (unit === 'm') {
    return localize('xMinutes', minutes, localizeOptions)

  // 1 up to 24 hours
  } else if (unit === 'h') {
    hours = mathPartial(minutes / 60);
    return localize('xHours', hours, localizeOptions)

  // 1 up to 30 days
  } else if (unit === 'd') {
    days = mathPartial(minutes / MINUTES_IN_DAY$1);
    return localize('xDays', days, localizeOptions)

  // 1 up to 12 months
  } else if (unit === 'M') {
    months = mathPartial(minutes / MINUTES_IN_MONTH$1);
    return localize('xMonths', months, localizeOptions)

  // 1 year up to max Date
  } else if (unit === 'Y') {
    years = mathPartial(minutes / MINUTES_IN_YEAR);
    return localize('xYears', years, localizeOptions)
  }

  throw new Error('Unknown unit: ' + unit)
}

var distance_in_words_strict = distanceInWordsStrict;

/**
 * @category Common Helpers
 * @summary Return the distance between the given date and now in words.
 *
 * @description
 * Return the distance between the given date and now in words.
 *
 * | Distance to now                                                   | Result              |
 * |-------------------------------------------------------------------|---------------------|
 * | 0 ... 30 secs                                                     | less than a minute  |
 * | 30 secs ... 1 min 30 secs                                         | 1 minute            |
 * | 1 min 30 secs ... 44 mins 30 secs                                 | [2..44] minutes     |
 * | 44 mins ... 30 secs ... 89 mins 30 secs                           | about 1 hour        |
 * | 89 mins 30 secs ... 23 hrs 59 mins 30 secs                        | about [2..24] hours |
 * | 23 hrs 59 mins 30 secs ... 41 hrs 59 mins 30 secs                 | 1 day               |
 * | 41 hrs 59 mins 30 secs ... 29 days 23 hrs 59 mins 30 secs         | [2..30] days        |
 * | 29 days 23 hrs 59 mins 30 secs ... 44 days 23 hrs 59 mins 30 secs | about 1 month       |
 * | 44 days 23 hrs 59 mins 30 secs ... 59 days 23 hrs 59 mins 30 secs | about 2 months      |
 * | 59 days 23 hrs 59 mins 30 secs ... 1 yr                           | [2..12] months      |
 * | 1 yr ... 1 yr 3 months                                            | about 1 year        |
 * | 1 yr 3 months ... 1 yr 9 month s                                  | over 1 year         |
 * | 1 yr 9 months ... 2 yrs                                           | almost 2 years      |
 * | N yrs ... N yrs 3 months                                          | about N years       |
 * | N yrs 3 months ... N yrs 9 months                                 | over N years        |
 * | N yrs 9 months ... N+1 yrs                                        | almost N+1 years    |
 *
 * With `options.includeSeconds == true`:
 * | Distance to now     | Result               |
 * |---------------------|----------------------|
 * | 0 secs ... 5 secs   | less than 5 seconds  |
 * | 5 secs ... 10 secs  | less than 10 seconds |
 * | 10 secs ... 20 secs | less than 20 seconds |
 * | 20 secs ... 40 secs | half a minute        |
 * | 40 secs ... 60 secs | less than a minute   |
 * | 60 secs ... 90 secs | 1 minute             |
 *
 * @param {Date|String|Number} date - the given date
 * @param {Object} [options] - the object with options
 * @param {Boolean} [options.includeSeconds=false] - distances less than a minute are more detailed
 * @param {Boolean} [options.addSuffix=false] - result specifies if the second date is earlier or later than the first
 * @param {Object} [options.locale=enLocale] - the locale object
 * @returns {String} the distance in words
 *
 * @example
 * // If today is 1 January 2015, what is the distance to 2 July 2014?
 * var result = distanceInWordsToNow(
 *   new Date(2014, 6, 2)
 * )
 * //=> '6 months'
 *
 * @example
 * // If now is 1 January 2015 00:00:00,
 * // what is the distance to 1 January 2015 00:00:15, including seconds?
 * var result = distanceInWordsToNow(
 *   new Date(2015, 0, 1, 0, 0, 15),
 *   {includeSeconds: true}
 * )
 * //=> 'less than 20 seconds'
 *
 * @example
 * // If today is 1 January 2015,
 * // what is the distance to 1 January 2016, with a suffix?
 * var result = distanceInWordsToNow(
 *   new Date(2016, 0, 1),
 *   {addSuffix: true}
 * )
 * //=> 'in about 1 year'
 *
 * @example
 * // If today is 1 January 2015,
 * // what is the distance to 1 August 2016 in Esperanto?
 * var eoLocale = require('date-fns/locale/eo')
 * var result = distanceInWordsToNow(
 *   new Date(2016, 7, 1),
 *   {locale: eoLocale}
 * )
 * //=> 'pli ol 1 jaro'
 */
function distanceInWordsToNow (dirtyDate, dirtyOptions) {
  return distance_in_words(Date.now(), dirtyDate, dirtyOptions)
}

var distance_in_words_to_now = distanceInWordsToNow;

/**
 * @category Day Helpers
 * @summary Return the array of dates within the specified range.
 *
 * @description
 * Return the array of dates within the specified range.
 *
 * @param {Date|String|Number} startDate - the first date
 * @param {Date|String|Number} endDate - the last date
 * @param {Number} [step=1] - the step between each day
 * @returns {Date[]} the array with starts of days from the day of startDate to the day of endDate
 * @throws {Error} startDate cannot be after endDate
 *
 * @example
 * // Each day between 6 October 2014 and 10 October 2014:
 * var result = eachDay(
 *   new Date(2014, 9, 6),
 *   new Date(2014, 9, 10)
 * )
 * //=> [
 * //   Mon Oct 06 2014 00:00:00,
 * //   Tue Oct 07 2014 00:00:00,
 * //   Wed Oct 08 2014 00:00:00,
 * //   Thu Oct 09 2014 00:00:00,
 * //   Fri Oct 10 2014 00:00:00
 * // ]
 */
function eachDay (dirtyStartDate, dirtyEndDate, dirtyStep) {
  var startDate = parse_1(dirtyStartDate);
  var endDate = parse_1(dirtyEndDate);
  var step = dirtyStep !== undefined ? dirtyStep : 1;

  var endTime = endDate.getTime();

  if (startDate.getTime() > endTime) {
    throw new Error('The first date cannot be after the second date')
  }

  var dates = [];

  var currentDate = startDate;
  currentDate.setHours(0, 0, 0, 0);

  while (currentDate.getTime() <= endTime) {
    dates.push(parse_1(currentDate));
    currentDate.setDate(currentDate.getDate() + step);
  }

  return dates
}

var each_day = eachDay;

/**
 * @category Day Helpers
 * @summary Return the end of a day for the given date.
 *
 * @description
 * Return the end of a day for the given date.
 * The result will be in the local timezone.
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the end of a day
 *
 * @example
 * // The end of a day for 2 September 2014 11:55:00:
 * var result = endOfDay(new Date(2014, 8, 2, 11, 55, 0))
 * //=> Tue Sep 02 2014 23:59:59.999
 */
function endOfDay (dirtyDate) {
  var date = parse_1(dirtyDate);
  date.setHours(23, 59, 59, 999);
  return date
}

var end_of_day = endOfDay;

/**
 * @category Hour Helpers
 * @summary Return the end of an hour for the given date.
 *
 * @description
 * Return the end of an hour for the given date.
 * The result will be in the local timezone.
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the end of an hour
 *
 * @example
 * // The end of an hour for 2 September 2014 11:55:00:
 * var result = endOfHour(new Date(2014, 8, 2, 11, 55))
 * //=> Tue Sep 02 2014 11:59:59.999
 */
function endOfHour (dirtyDate) {
  var date = parse_1(dirtyDate);
  date.setMinutes(59, 59, 999);
  return date
}

var end_of_hour = endOfHour;

/**
 * @category Week Helpers
 * @summary Return the end of a week for the given date.
 *
 * @description
 * Return the end of a week for the given date.
 * The result will be in the local timezone.
 *
 * @param {Date|String|Number} date - the original date
 * @param {Object} [options] - the object with options
 * @param {Number} [options.weekStartsOn=0] - the index of the first day of the week (0 - Sunday)
 * @returns {Date} the end of a week
 *
 * @example
 * // The end of a week for 2 September 2014 11:55:00:
 * var result = endOfWeek(new Date(2014, 8, 2, 11, 55, 0))
 * //=> Sat Sep 06 2014 23:59:59.999
 *
 * @example
 * // If the week starts on Monday, the end of the week for 2 September 2014 11:55:00:
 * var result = endOfWeek(new Date(2014, 8, 2, 11, 55, 0), {weekStartsOn: 1})
 * //=> Sun Sep 07 2014 23:59:59.999
 */
function endOfWeek (dirtyDate, dirtyOptions) {
  var weekStartsOn = dirtyOptions ? (Number(dirtyOptions.weekStartsOn) || 0) : 0;

  var date = parse_1(dirtyDate);
  var day = date.getDay();
  var diff = (day < weekStartsOn ? -7 : 0) + 6 - (day - weekStartsOn);

  date.setDate(date.getDate() + diff);
  date.setHours(23, 59, 59, 999);
  return date
}

var end_of_week = endOfWeek;

/**
 * @category ISO Week Helpers
 * @summary Return the end of an ISO week for the given date.
 *
 * @description
 * Return the end of an ISO week for the given date.
 * The result will be in the local timezone.
 *
 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the end of an ISO week
 *
 * @example
 * // The end of an ISO week for 2 September 2014 11:55:00:
 * var result = endOfISOWeek(new Date(2014, 8, 2, 11, 55, 0))
 * //=> Sun Sep 07 2014 23:59:59.999
 */
function endOfISOWeek (dirtyDate) {
  return end_of_week(dirtyDate, {weekStartsOn: 1})
}

var end_of_iso_week = endOfISOWeek;

/**
 * @category ISO Week-Numbering Year Helpers
 * @summary Return the end of an ISO week-numbering year for the given date.
 *
 * @description
 * Return the end of an ISO week-numbering year,
 * which always starts 3 days before the year's first Thursday.
 * The result will be in the local timezone.
 *
 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the end of an ISO week-numbering year
 *
 * @example
 * // The end of an ISO week-numbering year for 2 July 2005:
 * var result = endOfISOYear(new Date(2005, 6, 2))
 * //=> Sun Jan 01 2006 23:59:59.999
 */
function endOfISOYear (dirtyDate) {
  var year = get_iso_year(dirtyDate);
  var fourthOfJanuaryOfNextYear = new Date(0);
  fourthOfJanuaryOfNextYear.setFullYear(year + 1, 0, 4);
  fourthOfJanuaryOfNextYear.setHours(0, 0, 0, 0);
  var date = start_of_iso_week(fourthOfJanuaryOfNextYear);
  date.setMilliseconds(date.getMilliseconds() - 1);
  return date
}

var end_of_iso_year = endOfISOYear;

/**
 * @category Minute Helpers
 * @summary Return the end of a minute for the given date.
 *
 * @description
 * Return the end of a minute for the given date.
 * The result will be in the local timezone.
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the end of a minute
 *
 * @example
 * // The end of a minute for 1 December 2014 22:15:45.400:
 * var result = endOfMinute(new Date(2014, 11, 1, 22, 15, 45, 400))
 * //=> Mon Dec 01 2014 22:15:59.999
 */
function endOfMinute (dirtyDate) {
  var date = parse_1(dirtyDate);
  date.setSeconds(59, 999);
  return date
}

var end_of_minute = endOfMinute;

/**
 * @category Month Helpers
 * @summary Return the end of a month for the given date.
 *
 * @description
 * Return the end of a month for the given date.
 * The result will be in the local timezone.
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the end of a month
 *
 * @example
 * // The end of a month for 2 September 2014 11:55:00:
 * var result = endOfMonth(new Date(2014, 8, 2, 11, 55, 0))
 * //=> Tue Sep 30 2014 23:59:59.999
 */
function endOfMonth (dirtyDate) {
  var date = parse_1(dirtyDate);
  var month = date.getMonth();
  date.setFullYear(date.getFullYear(), month + 1, 0);
  date.setHours(23, 59, 59, 999);
  return date
}

var end_of_month = endOfMonth;

/**
 * @category Quarter Helpers
 * @summary Return the end of a year quarter for the given date.
 *
 * @description
 * Return the end of a year quarter for the given date.
 * The result will be in the local timezone.
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the end of a quarter
 *
 * @example
 * // The end of a quarter for 2 September 2014 11:55:00:
 * var result = endOfQuarter(new Date(2014, 8, 2, 11, 55, 0))
 * //=> Tue Sep 30 2014 23:59:59.999
 */
function endOfQuarter (dirtyDate) {
  var date = parse_1(dirtyDate);
  var currentMonth = date.getMonth();
  var month = currentMonth - currentMonth % 3 + 3;
  date.setMonth(month, 0);
  date.setHours(23, 59, 59, 999);
  return date
}

var end_of_quarter = endOfQuarter;

/**
 * @category Second Helpers
 * @summary Return the end of a second for the given date.
 *
 * @description
 * Return the end of a second for the given date.
 * The result will be in the local timezone.
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the end of a second
 *
 * @example
 * // The end of a second for 1 December 2014 22:15:45.400:
 * var result = endOfSecond(new Date(2014, 11, 1, 22, 15, 45, 400))
 * //=> Mon Dec 01 2014 22:15:45.999
 */
function endOfSecond (dirtyDate) {
  var date = parse_1(dirtyDate);
  date.setMilliseconds(999);
  return date
}

var end_of_second = endOfSecond;

/**
 * @category Day Helpers
 * @summary Return the end of today.
 *
 * @description
 * Return the end of today.
 *
 * @returns {Date} the end of today
 *
 * @example
 * // If today is 6 October 2014:
 * var result = endOfToday()
 * //=> Mon Oct 6 2014 23:59:59.999
 */
function endOfToday () {
  return end_of_day(new Date())
}

var end_of_today = endOfToday;

/**
 * @category Day Helpers
 * @summary Return the end of tomorrow.
 *
 * @description
 * Return the end of tomorrow.
 *
 * @returns {Date} the end of tomorrow
 *
 * @example
 * // If today is 6 October 2014:
 * var result = endOfTomorrow()
 * //=> Tue Oct 7 2014 23:59:59.999
 */
function endOfTomorrow () {
  var now = new Date();
  var year = now.getFullYear();
  var month = now.getMonth();
  var day = now.getDate();

  var date = new Date(0);
  date.setFullYear(year, month, day + 1);
  date.setHours(23, 59, 59, 999);
  return date
}

var end_of_tomorrow = endOfTomorrow;

/**
 * @category Year Helpers
 * @summary Return the end of a year for the given date.
 *
 * @description
 * Return the end of a year for the given date.
 * The result will be in the local timezone.
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the end of a year
 *
 * @example
 * // The end of a year for 2 September 2014 11:55:00:
 * var result = endOfYear(new Date(2014, 8, 2, 11, 55, 00))
 * //=> Wed Dec 31 2014 23:59:59.999
 */
function endOfYear (dirtyDate) {
  var date = parse_1(dirtyDate);
  var year = date.getFullYear();
  date.setFullYear(year + 1, 0, 0);
  date.setHours(23, 59, 59, 999);
  return date
}

var end_of_year = endOfYear;

/**
 * @category Day Helpers
 * @summary Return the end of yesterday.
 *
 * @description
 * Return the end of yesterday.
 *
 * @returns {Date} the end of yesterday
 *
 * @example
 * // If today is 6 October 2014:
 * var result = endOfYesterday()
 * //=> Sun Oct 5 2014 23:59:59.999
 */
function endOfYesterday () {
  var now = new Date();
  var year = now.getFullYear();
  var month = now.getMonth();
  var day = now.getDate();

  var date = new Date(0);
  date.setFullYear(year, month, day - 1);
  date.setHours(23, 59, 59, 999);
  return date
}

var end_of_yesterday = endOfYesterday;

/**
 * @category Year Helpers
 * @summary Return the start of a year for the given date.
 *
 * @description
 * Return the start of a year for the given date.
 * The result will be in the local timezone.
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the start of a year
 *
 * @example
 * // The start of a year for 2 September 2014 11:55:00:
 * var result = startOfYear(new Date(2014, 8, 2, 11, 55, 00))
 * //=> Wed Jan 01 2014 00:00:00
 */
function startOfYear (dirtyDate) {
  var cleanDate = parse_1(dirtyDate);
  var date = new Date(0);
  date.setFullYear(cleanDate.getFullYear(), 0, 1);
  date.setHours(0, 0, 0, 0);
  return date
}

var start_of_year = startOfYear;

/**
 * @category Day Helpers
 * @summary Get the day of the year of the given date.
 *
 * @description
 * Get the day of the year of the given date.
 *
 * @param {Date|String|Number} date - the given date
 * @returns {Number} the day of year
 *
 * @example
 * // Which day of the year is 2 July 2014?
 * var result = getDayOfYear(new Date(2014, 6, 2))
 * //=> 183
 */
function getDayOfYear (dirtyDate) {
  var date = parse_1(dirtyDate);
  var diff = difference_in_calendar_days(date, start_of_year(date));
  var dayOfYear = diff + 1;
  return dayOfYear
}

var get_day_of_year = getDayOfYear;

var MILLISECONDS_IN_WEEK$2 = 604800000;

/**
 * @category ISO Week Helpers
 * @summary Get the ISO week of the given date.
 *
 * @description
 * Get the ISO week of the given date.
 *
 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
 *
 * @param {Date|String|Number} date - the given date
 * @returns {Number} the ISO week
 *
 * @example
 * // Which week of the ISO-week numbering year is 2 January 2005?
 * var result = getISOWeek(new Date(2005, 0, 2))
 * //=> 53
 */
function getISOWeek (dirtyDate) {
  var date = parse_1(dirtyDate);
  var diff = start_of_iso_week(date).getTime() - start_of_iso_year(date).getTime();

  // Round the number of days to the nearest integer
  // because the number of milliseconds in a week is not constant
  // (e.g. it's different in the week of the daylight saving time clock shift)
  return Math.round(diff / MILLISECONDS_IN_WEEK$2) + 1
}

var get_iso_week = getISOWeek;

/**
 * @category Common Helpers
 * @summary Is the given date valid?
 *
 * @description
 * Returns false if argument is Invalid Date and true otherwise.
 * Invalid Date is a Date, whose time value is NaN.
 *
 * Time value of Date: http://es5.github.io/#x15.9.1.1
 *
 * @param {Date} date - the date to check
 * @returns {Boolean} the date is valid
 * @throws {TypeError} argument must be an instance of Date
 *
 * @example
 * // For the valid date:
 * var result = isValid(new Date(2014, 1, 31))
 * //=> true
 *
 * @example
 * // For the invalid date:
 * var result = isValid(new Date(''))
 * //=> false
 */
function isValid (dirtyDate) {
  if (is_date(dirtyDate)) {
    return !isNaN(dirtyDate)
  } else {
    throw new TypeError(toString.call(dirtyDate) + ' is not an instance of Date')
  }
}

var is_valid = isValid;

/**
 * @category Common Helpers
 * @summary Format the date.
 *
 * @description
 * Return the formatted date string in the given format.
 *
 * Accepted tokens:
 * | Unit                    | Token | Result examples                  |
 * |-------------------------|-------|----------------------------------|
 * | Month                   | M     | 1, 2, ..., 12                    |
 * |                         | Mo    | 1st, 2nd, ..., 12th              |
 * |                         | MM    | 01, 02, ..., 12                  |
 * |                         | MMM   | Jan, Feb, ..., Dec               |
 * |                         | MMMM  | January, February, ..., December |
 * | Quarter                 | Q     | 1, 2, 3, 4                       |
 * |                         | Qo    | 1st, 2nd, 3rd, 4th               |
 * | Day of month            | D     | 1, 2, ..., 31                    |
 * |                         | Do    | 1st, 2nd, ..., 31st              |
 * |                         | DD    | 01, 02, ..., 31                  |
 * | Day of year             | DDD   | 1, 2, ..., 366                   |
 * |                         | DDDo  | 1st, 2nd, ..., 366th             |
 * |                         | DDDD  | 001, 002, ..., 366               |
 * | Day of week             | d     | 0, 1, ..., 6                     |
 * |                         | do    | 0th, 1st, ..., 6th               |
 * |                         | dd    | Su, Mo, ..., Sa                  |
 * |                         | ddd   | Sun, Mon, ..., Sat               |
 * |                         | dddd  | Sunday, Monday, ..., Saturday    |
 * | Day of ISO week         | E     | 1, 2, ..., 7                     |
 * | ISO week                | W     | 1, 2, ..., 53                    |
 * |                         | Wo    | 1st, 2nd, ..., 53rd              |
 * |                         | WW    | 01, 02, ..., 53                  |
 * | Year                    | YY    | 00, 01, ..., 99                  |
 * |                         | YYYY  | 1900, 1901, ..., 2099            |
 * | ISO week-numbering year | GG    | 00, 01, ..., 99                  |
 * |                         | GGGG  | 1900, 1901, ..., 2099            |
 * | AM/PM                   | A     | AM, PM                           |
 * |                         | a     | am, pm                           |
 * |                         | aa    | a.m., p.m.                       |
 * | Hour                    | H     | 0, 1, ... 23                     |
 * |                         | HH    | 00, 01, ... 23                   |
 * |                         | h     | 1, 2, ..., 12                    |
 * |                         | hh    | 01, 02, ..., 12                  |
 * | Minute                  | m     | 0, 1, ..., 59                    |
 * |                         | mm    | 00, 01, ..., 59                  |
 * | Second                  | s     | 0, 1, ..., 59                    |
 * |                         | ss    | 00, 01, ..., 59                  |
 * | 1/10 of second          | S     | 0, 1, ..., 9                     |
 * | 1/100 of second         | SS    | 00, 01, ..., 99                  |
 * | Millisecond             | SSS   | 000, 001, ..., 999               |
 * | Timezone                | Z     | -01:00, +00:00, ... +12:00       |
 * |                         | ZZ    | -0100, +0000, ..., +1200         |
 * | Seconds timestamp       | X     | 512969520                        |
 * | Milliseconds timestamp  | x     | 512969520900                     |
 *
 * The characters wrapped in square brackets are escaped.
 *
 * The result may vary by locale.
 *
 * @param {Date|String|Number} date - the original date
 * @param {String} [format='YYYY-MM-DDTHH:mm:ss.SSSZ'] - the string of tokens
 * @param {Object} [options] - the object with options
 * @param {Object} [options.locale=enLocale] - the locale object
 * @returns {String} the formatted date string
 *
 * @example
 * // Represent 11 February 2014 in middle-endian format:
 * var result = format(
 *   new Date(2014, 1, 11),
 *   'MM/DD/YYYY'
 * )
 * //=> '02/11/2014'
 *
 * @example
 * // Represent 2 July 2014 in Esperanto:
 * var eoLocale = require('date-fns/locale/eo')
 * var result = format(
 *   new Date(2014, 6, 2),
 *   'Do [de] MMMM YYYY',
 *   {locale: eoLocale}
 * )
 * //=> '2-a de julio 2014'
 */
function format (dirtyDate, dirtyFormatStr, dirtyOptions) {
  var formatStr = dirtyFormatStr ? String(dirtyFormatStr) : 'YYYY-MM-DDTHH:mm:ss.SSSZ';
  var options = dirtyOptions || {};

  var locale = options.locale;
  var localeFormatters = en.format.formatters;
  var formattingTokensRegExp = en.format.formattingTokensRegExp;
  if (locale && locale.format && locale.format.formatters) {
    localeFormatters = locale.format.formatters;

    if (locale.format.formattingTokensRegExp) {
      formattingTokensRegExp = locale.format.formattingTokensRegExp;
    }
  }

  var date = parse_1(dirtyDate);

  if (!is_valid(date)) {
    return 'Invalid Date'
  }

  var formatFn = buildFormatFn(formatStr, localeFormatters, formattingTokensRegExp);

  return formatFn(date)
}

var formatters = {
  // Month: 1, 2, ..., 12
  'M': function (date) {
    return date.getMonth() + 1
  },

  // Month: 01, 02, ..., 12
  'MM': function (date) {
    return addLeadingZeros(date.getMonth() + 1, 2)
  },

  // Quarter: 1, 2, 3, 4
  'Q': function (date) {
    return Math.ceil((date.getMonth() + 1) / 3)
  },

  // Day of month: 1, 2, ..., 31
  'D': function (date) {
    return date.getDate()
  },

  // Day of month: 01, 02, ..., 31
  'DD': function (date) {
    return addLeadingZeros(date.getDate(), 2)
  },

  // Day of year: 1, 2, ..., 366
  'DDD': function (date) {
    return get_day_of_year(date)
  },

  // Day of year: 001, 002, ..., 366
  'DDDD': function (date) {
    return addLeadingZeros(get_day_of_year(date), 3)
  },

  // Day of week: 0, 1, ..., 6
  'd': function (date) {
    return date.getDay()
  },

  // Day of ISO week: 1, 2, ..., 7
  'E': function (date) {
    return date.getDay() || 7
  },

  // ISO week: 1, 2, ..., 53
  'W': function (date) {
    return get_iso_week(date)
  },

  // ISO week: 01, 02, ..., 53
  'WW': function (date) {
    return addLeadingZeros(get_iso_week(date), 2)
  },

  // Year: 00, 01, ..., 99
  'YY': function (date) {
    return addLeadingZeros(date.getFullYear(), 4).substr(2)
  },

  // Year: 1900, 1901, ..., 2099
  'YYYY': function (date) {
    return addLeadingZeros(date.getFullYear(), 4)
  },

  // ISO week-numbering year: 00, 01, ..., 99
  'GG': function (date) {
    return String(get_iso_year(date)).substr(2)
  },

  // ISO week-numbering year: 1900, 1901, ..., 2099
  'GGGG': function (date) {
    return get_iso_year(date)
  },

  // Hour: 0, 1, ... 23
  'H': function (date) {
    return date.getHours()
  },

  // Hour: 00, 01, ..., 23
  'HH': function (date) {
    return addLeadingZeros(date.getHours(), 2)
  },

  // Hour: 1, 2, ..., 12
  'h': function (date) {
    var hours = date.getHours();
    if (hours === 0) {
      return 12
    } else if (hours > 12) {
      return hours % 12
    } else {
      return hours
    }
  },

  // Hour: 01, 02, ..., 12
  'hh': function (date) {
    return addLeadingZeros(formatters['h'](date), 2)
  },

  // Minute: 0, 1, ..., 59
  'm': function (date) {
    return date.getMinutes()
  },

  // Minute: 00, 01, ..., 59
  'mm': function (date) {
    return addLeadingZeros(date.getMinutes(), 2)
  },

  // Second: 0, 1, ..., 59
  's': function (date) {
    return date.getSeconds()
  },

  // Second: 00, 01, ..., 59
  'ss': function (date) {
    return addLeadingZeros(date.getSeconds(), 2)
  },

  // 1/10 of second: 0, 1, ..., 9
  'S': function (date) {
    return Math.floor(date.getMilliseconds() / 100)
  },

  // 1/100 of second: 00, 01, ..., 99
  'SS': function (date) {
    return addLeadingZeros(Math.floor(date.getMilliseconds() / 10), 2)
  },

  // Millisecond: 000, 001, ..., 999
  'SSS': function (date) {
    return addLeadingZeros(date.getMilliseconds(), 3)
  },

  // Timezone: -01:00, +00:00, ... +12:00
  'Z': function (date) {
    return formatTimezone(date.getTimezoneOffset(), ':')
  },

  // Timezone: -0100, +0000, ... +1200
  'ZZ': function (date) {
    return formatTimezone(date.getTimezoneOffset())
  },

  // Seconds timestamp: 512969520
  'X': function (date) {
    return Math.floor(date.getTime() / 1000)
  },

  // Milliseconds timestamp: 512969520900
  'x': function (date) {
    return date.getTime()
  }
};

function buildFormatFn (formatStr, localeFormatters, formattingTokensRegExp) {
  var array = formatStr.match(formattingTokensRegExp);
  var length = array.length;

  var i;
  var formatter;
  for (i = 0; i < length; i++) {
    formatter = localeFormatters[array[i]] || formatters[array[i]];
    if (formatter) {
      array[i] = formatter;
    } else {
      array[i] = removeFormattingTokens(array[i]);
    }
  }

  return function (date) {
    var output = '';
    for (var i = 0; i < length; i++) {
      if (array[i] instanceof Function) {
        output += array[i](date, formatters);
      } else {
        output += array[i];
      }
    }
    return output
  }
}

function removeFormattingTokens (input) {
  if (input.match(/\[[\s\S]/)) {
    return input.replace(/^\[|]$/g, '')
  }
  return input.replace(/\\/g, '')
}

function formatTimezone (offset, delimeter) {
  delimeter = delimeter || '';
  var sign = offset > 0 ? '-' : '+';
  var absOffset = Math.abs(offset);
  var hours = Math.floor(absOffset / 60);
  var minutes = absOffset % 60;
  return sign + addLeadingZeros(hours, 2) + delimeter + addLeadingZeros(minutes, 2)
}

function addLeadingZeros (number, targetLength) {
  var output = Math.abs(number).toString();
  while (output.length < targetLength) {
    output = '0' + output;
  }
  return output
}

var format_1 = format;

/**
 * @category Day Helpers
 * @summary Get the day of the month of the given date.
 *
 * @description
 * Get the day of the month of the given date.
 *
 * @param {Date|String|Number} date - the given date
 * @returns {Number} the day of month
 *
 * @example
 * // Which day of the month is 29 February 2012?
 * var result = getDate(new Date(2012, 1, 29))
 * //=> 29
 */
function getDate (dirtyDate) {
  var date = parse_1(dirtyDate);
  var dayOfMonth = date.getDate();
  return dayOfMonth
}

var get_date = getDate;

/**
 * @category Weekday Helpers
 * @summary Get the day of the week of the given date.
 *
 * @description
 * Get the day of the week of the given date.
 *
 * @param {Date|String|Number} date - the given date
 * @returns {Number} the day of week
 *
 * @example
 * // Which day of the week is 29 February 2012?
 * var result = getDay(new Date(2012, 1, 29))
 * //=> 3
 */
function getDay (dirtyDate) {
  var date = parse_1(dirtyDate);
  var day = date.getDay();
  return day
}

var get_day = getDay;

/**
 * @category Year Helpers
 * @summary Is the given date in the leap year?
 *
 * @description
 * Is the given date in the leap year?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is in the leap year
 *
 * @example
 * // Is 1 September 2012 in the leap year?
 * var result = isLeapYear(new Date(2012, 8, 1))
 * //=> true
 */
function isLeapYear (dirtyDate) {
  var date = parse_1(dirtyDate);
  var year = date.getFullYear();
  return year % 400 === 0 || year % 4 === 0 && year % 100 !== 0
}

var is_leap_year = isLeapYear;

/**
 * @category Year Helpers
 * @summary Get the number of days in a year of the given date.
 *
 * @description
 * Get the number of days in a year of the given date.
 *
 * @param {Date|String|Number} date - the given date
 * @returns {Number} the number of days in a year
 *
 * @example
 * // How many days are in 2012?
 * var result = getDaysInYear(new Date(2012, 0, 1))
 * //=> 366
 */
function getDaysInYear (dirtyDate) {
  return is_leap_year(dirtyDate) ? 366 : 365
}

var get_days_in_year = getDaysInYear;

/**
 * @category Hour Helpers
 * @summary Get the hours of the given date.
 *
 * @description
 * Get the hours of the given date.
 *
 * @param {Date|String|Number} date - the given date
 * @returns {Number} the hours
 *
 * @example
 * // Get the hours of 29 February 2012 11:45:00:
 * var result = getHours(new Date(2012, 1, 29, 11, 45))
 * //=> 11
 */
function getHours (dirtyDate) {
  var date = parse_1(dirtyDate);
  var hours = date.getHours();
  return hours
}

var get_hours = getHours;

/**
 * @category Weekday Helpers
 * @summary Get the day of the ISO week of the given date.
 *
 * @description
 * Get the day of the ISO week of the given date,
 * which is 7 for Sunday, 1 for Monday etc.
 *
 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
 *
 * @param {Date|String|Number} date - the given date
 * @returns {Number} the day of ISO week
 *
 * @example
 * // Which day of the ISO week is 26 February 2012?
 * var result = getISODay(new Date(2012, 1, 26))
 * //=> 7
 */
function getISODay (dirtyDate) {
  var date = parse_1(dirtyDate);
  var day = date.getDay();

  if (day === 0) {
    day = 7;
  }

  return day
}

var get_iso_day = getISODay;

var MILLISECONDS_IN_WEEK$3 = 604800000;

/**
 * @category ISO Week-Numbering Year Helpers
 * @summary Get the number of weeks in an ISO week-numbering year of the given date.
 *
 * @description
 * Get the number of weeks in an ISO week-numbering year of the given date.
 *
 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
 *
 * @param {Date|String|Number} date - the given date
 * @returns {Number} the number of ISO weeks in a year
 *
 * @example
 * // How many weeks are in ISO week-numbering year 2015?
 * var result = getISOWeeksInYear(new Date(2015, 1, 11))
 * //=> 53
 */
function getISOWeeksInYear (dirtyDate) {
  var thisYear = start_of_iso_year(dirtyDate);
  var nextYear = start_of_iso_year(add_weeks(thisYear, 60));
  var diff = nextYear.valueOf() - thisYear.valueOf();
  // Round the number of weeks to the nearest integer
  // because the number of milliseconds in a week is not constant
  // (e.g. it's different in the week of the daylight saving time clock shift)
  return Math.round(diff / MILLISECONDS_IN_WEEK$3)
}

var get_iso_weeks_in_year = getISOWeeksInYear;

/**
 * @category Millisecond Helpers
 * @summary Get the milliseconds of the given date.
 *
 * @description
 * Get the milliseconds of the given date.
 *
 * @param {Date|String|Number} date - the given date
 * @returns {Number} the milliseconds
 *
 * @example
 * // Get the milliseconds of 29 February 2012 11:45:05.123:
 * var result = getMilliseconds(new Date(2012, 1, 29, 11, 45, 5, 123))
 * //=> 123
 */
function getMilliseconds (dirtyDate) {
  var date = parse_1(dirtyDate);
  var milliseconds = date.getMilliseconds();
  return milliseconds
}

var get_milliseconds = getMilliseconds;

/**
 * @category Minute Helpers
 * @summary Get the minutes of the given date.
 *
 * @description
 * Get the minutes of the given date.
 *
 * @param {Date|String|Number} date - the given date
 * @returns {Number} the minutes
 *
 * @example
 * // Get the minutes of 29 February 2012 11:45:05:
 * var result = getMinutes(new Date(2012, 1, 29, 11, 45, 5))
 * //=> 45
 */
function getMinutes (dirtyDate) {
  var date = parse_1(dirtyDate);
  var minutes = date.getMinutes();
  return minutes
}

var get_minutes = getMinutes;

/**
 * @category Month Helpers
 * @summary Get the month of the given date.
 *
 * @description
 * Get the month of the given date.
 *
 * @param {Date|String|Number} date - the given date
 * @returns {Number} the month
 *
 * @example
 * // Which month is 29 February 2012?
 * var result = getMonth(new Date(2012, 1, 29))
 * //=> 1
 */
function getMonth (dirtyDate) {
  var date = parse_1(dirtyDate);
  var month = date.getMonth();
  return month
}

var get_month = getMonth;

var MILLISECONDS_IN_DAY$1 = 24 * 60 * 60 * 1000;

/**
 * @category Range Helpers
 * @summary Get the number of days that overlap in two date ranges
 *
 * @description
 * Get the number of days that overlap in two date ranges
 *
 * @param {Date|String|Number} initialRangeStartDate - the start of the initial range
 * @param {Date|String|Number} initialRangeEndDate - the end of the initial range
 * @param {Date|String|Number} comparedRangeStartDate - the start of the range to compare it with
 * @param {Date|String|Number} comparedRangeEndDate - the end of the range to compare it with
 * @returns {Number} the number of days that overlap in two date ranges
 * @throws {Error} startDate of a date range cannot be after its endDate
 *
 * @example
 * // For overlapping date ranges adds 1 for each started overlapping day:
 * getOverlappingDaysInRanges(
 *   new Date(2014, 0, 10), new Date(2014, 0, 20), new Date(2014, 0, 17), new Date(2014, 0, 21)
 * )
 * //=> 3
 *
 * @example
 * // For non-overlapping date ranges returns 0:
 * getOverlappingDaysInRanges(
 *   new Date(2014, 0, 10), new Date(2014, 0, 20), new Date(2014, 0, 21), new Date(2014, 0, 22)
 * )
 * //=> 0
 */
function getOverlappingDaysInRanges (dirtyInitialRangeStartDate, dirtyInitialRangeEndDate, dirtyComparedRangeStartDate, dirtyComparedRangeEndDate) {
  var initialStartTime = parse_1(dirtyInitialRangeStartDate).getTime();
  var initialEndTime = parse_1(dirtyInitialRangeEndDate).getTime();
  var comparedStartTime = parse_1(dirtyComparedRangeStartDate).getTime();
  var comparedEndTime = parse_1(dirtyComparedRangeEndDate).getTime();

  if (initialStartTime > initialEndTime || comparedStartTime > comparedEndTime) {
    throw new Error('The start of the range cannot be after the end of the range')
  }

  var isOverlapping = initialStartTime < comparedEndTime && comparedStartTime < initialEndTime;

  if (!isOverlapping) {
    return 0
  }

  var overlapStartDate = comparedStartTime < initialStartTime
    ? initialStartTime
    : comparedStartTime;

  var overlapEndDate = comparedEndTime > initialEndTime
    ? initialEndTime
    : comparedEndTime;

  var differenceInMs = overlapEndDate - overlapStartDate;

  return Math.ceil(differenceInMs / MILLISECONDS_IN_DAY$1)
}

var get_overlapping_days_in_ranges = getOverlappingDaysInRanges;

/**
 * @category Second Helpers
 * @summary Get the seconds of the given date.
 *
 * @description
 * Get the seconds of the given date.
 *
 * @param {Date|String|Number} date - the given date
 * @returns {Number} the seconds
 *
 * @example
 * // Get the seconds of 29 February 2012 11:45:05.123:
 * var result = getSeconds(new Date(2012, 1, 29, 11, 45, 5, 123))
 * //=> 5
 */
function getSeconds (dirtyDate) {
  var date = parse_1(dirtyDate);
  var seconds = date.getSeconds();
  return seconds
}

var get_seconds = getSeconds;

/**
 * @category Timestamp Helpers
 * @summary Get the milliseconds timestamp of the given date.
 *
 * @description
 * Get the milliseconds timestamp of the given date.
 *
 * @param {Date|String|Number} date - the given date
 * @returns {Number} the timestamp
 *
 * @example
 * // Get the timestamp of 29 February 2012 11:45:05.123:
 * var result = getTime(new Date(2012, 1, 29, 11, 45, 5, 123))
 * //=> 1330515905123
 */
function getTime (dirtyDate) {
  var date = parse_1(dirtyDate);
  var timestamp = date.getTime();
  return timestamp
}

var get_time = getTime;

/**
 * @category Year Helpers
 * @summary Get the year of the given date.
 *
 * @description
 * Get the year of the given date.
 *
 * @param {Date|String|Number} date - the given date
 * @returns {Number} the year
 *
 * @example
 * // Which year is 2 July 2014?
 * var result = getYear(new Date(2014, 6, 2))
 * //=> 2014
 */
function getYear (dirtyDate) {
  var date = parse_1(dirtyDate);
  var year = date.getFullYear();
  return year
}

var get_year = getYear;

/**
 * @category Common Helpers
 * @summary Is the first date after the second one?
 *
 * @description
 * Is the first date after the second one?
 *
 * @param {Date|String|Number} date - the date that should be after the other one to return true
 * @param {Date|String|Number} dateToCompare - the date to compare with
 * @returns {Boolean} the first date is after the second date
 *
 * @example
 * // Is 10 July 1989 after 11 February 1987?
 * var result = isAfter(new Date(1989, 6, 10), new Date(1987, 1, 11))
 * //=> true
 */
function isAfter (dirtyDate, dirtyDateToCompare) {
  var date = parse_1(dirtyDate);
  var dateToCompare = parse_1(dirtyDateToCompare);
  return date.getTime() > dateToCompare.getTime()
}

var is_after = isAfter;

/**
 * @category Common Helpers
 * @summary Is the first date before the second one?
 *
 * @description
 * Is the first date before the second one?
 *
 * @param {Date|String|Number} date - the date that should be before the other one to return true
 * @param {Date|String|Number} dateToCompare - the date to compare with
 * @returns {Boolean} the first date is before the second date
 *
 * @example
 * // Is 10 July 1989 before 11 February 1987?
 * var result = isBefore(new Date(1989, 6, 10), new Date(1987, 1, 11))
 * //=> false
 */
function isBefore (dirtyDate, dirtyDateToCompare) {
  var date = parse_1(dirtyDate);
  var dateToCompare = parse_1(dirtyDateToCompare);
  return date.getTime() < dateToCompare.getTime()
}

var is_before = isBefore;

/**
 * @category Common Helpers
 * @summary Are the given dates equal?
 *
 * @description
 * Are the given dates equal?
 *
 * @param {Date|String|Number} dateLeft - the first date to compare
 * @param {Date|String|Number} dateRight - the second date to compare
 * @returns {Boolean} the dates are equal
 *
 * @example
 * // Are 2 July 2014 06:30:45.000 and 2 July 2014 06:30:45.500 equal?
 * var result = isEqual(
 *   new Date(2014, 6, 2, 6, 30, 45, 0)
 *   new Date(2014, 6, 2, 6, 30, 45, 500)
 * )
 * //=> false
 */
function isEqual (dirtyLeftDate, dirtyRightDate) {
  var dateLeft = parse_1(dirtyLeftDate);
  var dateRight = parse_1(dirtyRightDate);
  return dateLeft.getTime() === dateRight.getTime()
}

var is_equal = isEqual;

/**
 * @category Month Helpers
 * @summary Is the given date the first day of a month?
 *
 * @description
 * Is the given date the first day of a month?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is the first day of a month
 *
 * @example
 * // Is 1 September 2014 the first day of a month?
 * var result = isFirstDayOfMonth(new Date(2014, 8, 1))
 * //=> true
 */
function isFirstDayOfMonth (dirtyDate) {
  return parse_1(dirtyDate).getDate() === 1
}

var is_first_day_of_month = isFirstDayOfMonth;

/**
 * @category Weekday Helpers
 * @summary Is the given date Friday?
 *
 * @description
 * Is the given date Friday?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is Friday
 *
 * @example
 * // Is 26 September 2014 Friday?
 * var result = isFriday(new Date(2014, 8, 26))
 * //=> true
 */
function isFriday (dirtyDate) {
  return parse_1(dirtyDate).getDay() === 5
}

var is_friday = isFriday;

/**
 * @category Common Helpers
 * @summary Is the given date in the future?
 *
 * @description
 * Is the given date in the future?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is in the future
 *
 * @example
 * // If today is 6 October 2014, is 31 December 2014 in the future?
 * var result = isFuture(new Date(2014, 11, 31))
 * //=> true
 */
function isFuture (dirtyDate) {
  return parse_1(dirtyDate).getTime() > new Date().getTime()
}

var is_future = isFuture;

/**
 * @category Month Helpers
 * @summary Is the given date the last day of a month?
 *
 * @description
 * Is the given date the last day of a month?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is the last day of a month
 *
 * @example
 * // Is 28 February 2014 the last day of a month?
 * var result = isLastDayOfMonth(new Date(2014, 1, 28))
 * //=> true
 */
function isLastDayOfMonth (dirtyDate) {
  var date = parse_1(dirtyDate);
  return end_of_day(date).getTime() === end_of_month(date).getTime()
}

var is_last_day_of_month = isLastDayOfMonth;

/**
 * @category Weekday Helpers
 * @summary Is the given date Monday?
 *
 * @description
 * Is the given date Monday?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is Monday
 *
 * @example
 * // Is 22 September 2014 Monday?
 * var result = isMonday(new Date(2014, 8, 22))
 * //=> true
 */
function isMonday (dirtyDate) {
  return parse_1(dirtyDate).getDay() === 1
}

var is_monday = isMonday;

/**
 * @category Common Helpers
 * @summary Is the given date in the past?
 *
 * @description
 * Is the given date in the past?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is in the past
 *
 * @example
 * // If today is 6 October 2014, is 2 July 2014 in the past?
 * var result = isPast(new Date(2014, 6, 2))
 * //=> true
 */
function isPast (dirtyDate) {
  return parse_1(dirtyDate).getTime() < new Date().getTime()
}

var is_past = isPast;

/**
 * @category Day Helpers
 * @summary Are the given dates in the same day?
 *
 * @description
 * Are the given dates in the same day?
 *
 * @param {Date|String|Number} dateLeft - the first date to check
 * @param {Date|String|Number} dateRight - the second date to check
 * @returns {Boolean} the dates are in the same day
 *
 * @example
 * // Are 4 September 06:00:00 and 4 September 18:00:00 in the same day?
 * var result = isSameDay(
 *   new Date(2014, 8, 4, 6, 0),
 *   new Date(2014, 8, 4, 18, 0)
 * )
 * //=> true
 */
function isSameDay (dirtyDateLeft, dirtyDateRight) {
  var dateLeftStartOfDay = start_of_day(dirtyDateLeft);
  var dateRightStartOfDay = start_of_day(dirtyDateRight);

  return dateLeftStartOfDay.getTime() === dateRightStartOfDay.getTime()
}

var is_same_day = isSameDay;

/**
 * @category Hour Helpers
 * @summary Return the start of an hour for the given date.
 *
 * @description
 * Return the start of an hour for the given date.
 * The result will be in the local timezone.
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the start of an hour
 *
 * @example
 * // The start of an hour for 2 September 2014 11:55:00:
 * var result = startOfHour(new Date(2014, 8, 2, 11, 55))
 * //=> Tue Sep 02 2014 11:00:00
 */
function startOfHour (dirtyDate) {
  var date = parse_1(dirtyDate);
  date.setMinutes(0, 0, 0);
  return date
}

var start_of_hour = startOfHour;

/**
 * @category Hour Helpers
 * @summary Are the given dates in the same hour?
 *
 * @description
 * Are the given dates in the same hour?
 *
 * @param {Date|String|Number} dateLeft - the first date to check
 * @param {Date|String|Number} dateRight - the second date to check
 * @returns {Boolean} the dates are in the same hour
 *
 * @example
 * // Are 4 September 2014 06:00:00 and 4 September 06:30:00 in the same hour?
 * var result = isSameHour(
 *   new Date(2014, 8, 4, 6, 0),
 *   new Date(2014, 8, 4, 6, 30)
 * )
 * //=> true
 */
function isSameHour (dirtyDateLeft, dirtyDateRight) {
  var dateLeftStartOfHour = start_of_hour(dirtyDateLeft);
  var dateRightStartOfHour = start_of_hour(dirtyDateRight);

  return dateLeftStartOfHour.getTime() === dateRightStartOfHour.getTime()
}

var is_same_hour = isSameHour;

/**
 * @category Week Helpers
 * @summary Are the given dates in the same week?
 *
 * @description
 * Are the given dates in the same week?
 *
 * @param {Date|String|Number} dateLeft - the first date to check
 * @param {Date|String|Number} dateRight - the second date to check
 * @param {Object} [options] - the object with options
 * @param {Number} [options.weekStartsOn=0] - the index of the first day of the week (0 - Sunday)
 * @returns {Boolean} the dates are in the same week
 *
 * @example
 * // Are 31 August 2014 and 4 September 2014 in the same week?
 * var result = isSameWeek(
 *   new Date(2014, 7, 31),
 *   new Date(2014, 8, 4)
 * )
 * //=> true
 *
 * @example
 * // If week starts with Monday,
 * // are 31 August 2014 and 4 September 2014 in the same week?
 * var result = isSameWeek(
 *   new Date(2014, 7, 31),
 *   new Date(2014, 8, 4),
 *   {weekStartsOn: 1}
 * )
 * //=> false
 */
function isSameWeek (dirtyDateLeft, dirtyDateRight, dirtyOptions) {
  var dateLeftStartOfWeek = start_of_week(dirtyDateLeft, dirtyOptions);
  var dateRightStartOfWeek = start_of_week(dirtyDateRight, dirtyOptions);

  return dateLeftStartOfWeek.getTime() === dateRightStartOfWeek.getTime()
}

var is_same_week = isSameWeek;

/**
 * @category ISO Week Helpers
 * @summary Are the given dates in the same ISO week?
 *
 * @description
 * Are the given dates in the same ISO week?
 *
 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
 *
 * @param {Date|String|Number} dateLeft - the first date to check
 * @param {Date|String|Number} dateRight - the second date to check
 * @returns {Boolean} the dates are in the same ISO week
 *
 * @example
 * // Are 1 September 2014 and 7 September 2014 in the same ISO week?
 * var result = isSameISOWeek(
 *   new Date(2014, 8, 1),
 *   new Date(2014, 8, 7)
 * )
 * //=> true
 */
function isSameISOWeek (dirtyDateLeft, dirtyDateRight) {
  return is_same_week(dirtyDateLeft, dirtyDateRight, {weekStartsOn: 1})
}

var is_same_iso_week = isSameISOWeek;

/**
 * @category ISO Week-Numbering Year Helpers
 * @summary Are the given dates in the same ISO week-numbering year?
 *
 * @description
 * Are the given dates in the same ISO week-numbering year?
 *
 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
 *
 * @param {Date|String|Number} dateLeft - the first date to check
 * @param {Date|String|Number} dateRight - the second date to check
 * @returns {Boolean} the dates are in the same ISO week-numbering year
 *
 * @example
 * // Are 29 December 2003 and 2 January 2005 in the same ISO week-numbering year?
 * var result = isSameISOYear(
 *   new Date(2003, 11, 29),
 *   new Date(2005, 0, 2)
 * )
 * //=> true
 */
function isSameISOYear (dirtyDateLeft, dirtyDateRight) {
  var dateLeftStartOfYear = start_of_iso_year(dirtyDateLeft);
  var dateRightStartOfYear = start_of_iso_year(dirtyDateRight);

  return dateLeftStartOfYear.getTime() === dateRightStartOfYear.getTime()
}

var is_same_iso_year = isSameISOYear;

/**
 * @category Minute Helpers
 * @summary Return the start of a minute for the given date.
 *
 * @description
 * Return the start of a minute for the given date.
 * The result will be in the local timezone.
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the start of a minute
 *
 * @example
 * // The start of a minute for 1 December 2014 22:15:45.400:
 * var result = startOfMinute(new Date(2014, 11, 1, 22, 15, 45, 400))
 * //=> Mon Dec 01 2014 22:15:00
 */
function startOfMinute (dirtyDate) {
  var date = parse_1(dirtyDate);
  date.setSeconds(0, 0);
  return date
}

var start_of_minute = startOfMinute;

/**
 * @category Minute Helpers
 * @summary Are the given dates in the same minute?
 *
 * @description
 * Are the given dates in the same minute?
 *
 * @param {Date|String|Number} dateLeft - the first date to check
 * @param {Date|String|Number} dateRight - the second date to check
 * @returns {Boolean} the dates are in the same minute
 *
 * @example
 * // Are 4 September 2014 06:30:00 and 4 September 2014 06:30:15
 * // in the same minute?
 * var result = isSameMinute(
 *   new Date(2014, 8, 4, 6, 30),
 *   new Date(2014, 8, 4, 6, 30, 15)
 * )
 * //=> true
 */
function isSameMinute (dirtyDateLeft, dirtyDateRight) {
  var dateLeftStartOfMinute = start_of_minute(dirtyDateLeft);
  var dateRightStartOfMinute = start_of_minute(dirtyDateRight);

  return dateLeftStartOfMinute.getTime() === dateRightStartOfMinute.getTime()
}

var is_same_minute = isSameMinute;

/**
 * @category Month Helpers
 * @summary Are the given dates in the same month?
 *
 * @description
 * Are the given dates in the same month?
 *
 * @param {Date|String|Number} dateLeft - the first date to check
 * @param {Date|String|Number} dateRight - the second date to check
 * @returns {Boolean} the dates are in the same month
 *
 * @example
 * // Are 2 September 2014 and 25 September 2014 in the same month?
 * var result = isSameMonth(
 *   new Date(2014, 8, 2),
 *   new Date(2014, 8, 25)
 * )
 * //=> true
 */
function isSameMonth (dirtyDateLeft, dirtyDateRight) {
  var dateLeft = parse_1(dirtyDateLeft);
  var dateRight = parse_1(dirtyDateRight);
  return dateLeft.getFullYear() === dateRight.getFullYear() &&
    dateLeft.getMonth() === dateRight.getMonth()
}

var is_same_month = isSameMonth;

/**
 * @category Quarter Helpers
 * @summary Return the start of a year quarter for the given date.
 *
 * @description
 * Return the start of a year quarter for the given date.
 * The result will be in the local timezone.
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the start of a quarter
 *
 * @example
 * // The start of a quarter for 2 September 2014 11:55:00:
 * var result = startOfQuarter(new Date(2014, 8, 2, 11, 55, 0))
 * //=> Tue Jul 01 2014 00:00:00
 */
function startOfQuarter (dirtyDate) {
  var date = parse_1(dirtyDate);
  var currentMonth = date.getMonth();
  var month = currentMonth - currentMonth % 3;
  date.setMonth(month, 1);
  date.setHours(0, 0, 0, 0);
  return date
}

var start_of_quarter = startOfQuarter;

/**
 * @category Quarter Helpers
 * @summary Are the given dates in the same year quarter?
 *
 * @description
 * Are the given dates in the same year quarter?
 *
 * @param {Date|String|Number} dateLeft - the first date to check
 * @param {Date|String|Number} dateRight - the second date to check
 * @returns {Boolean} the dates are in the same quarter
 *
 * @example
 * // Are 1 January 2014 and 8 March 2014 in the same quarter?
 * var result = isSameQuarter(
 *   new Date(2014, 0, 1),
 *   new Date(2014, 2, 8)
 * )
 * //=> true
 */
function isSameQuarter (dirtyDateLeft, dirtyDateRight) {
  var dateLeftStartOfQuarter = start_of_quarter(dirtyDateLeft);
  var dateRightStartOfQuarter = start_of_quarter(dirtyDateRight);

  return dateLeftStartOfQuarter.getTime() === dateRightStartOfQuarter.getTime()
}

var is_same_quarter = isSameQuarter;

/**
 * @category Second Helpers
 * @summary Return the start of a second for the given date.
 *
 * @description
 * Return the start of a second for the given date.
 * The result will be in the local timezone.
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the start of a second
 *
 * @example
 * // The start of a second for 1 December 2014 22:15:45.400:
 * var result = startOfSecond(new Date(2014, 11, 1, 22, 15, 45, 400))
 * //=> Mon Dec 01 2014 22:15:45.000
 */
function startOfSecond (dirtyDate) {
  var date = parse_1(dirtyDate);
  date.setMilliseconds(0);
  return date
}

var start_of_second = startOfSecond;

/**
 * @category Second Helpers
 * @summary Are the given dates in the same second?
 *
 * @description
 * Are the given dates in the same second?
 *
 * @param {Date|String|Number} dateLeft - the first date to check
 * @param {Date|String|Number} dateRight - the second date to check
 * @returns {Boolean} the dates are in the same second
 *
 * @example
 * // Are 4 September 2014 06:30:15.000 and 4 September 2014 06:30.15.500
 * // in the same second?
 * var result = isSameSecond(
 *   new Date(2014, 8, 4, 6, 30, 15),
 *   new Date(2014, 8, 4, 6, 30, 15, 500)
 * )
 * //=> true
 */
function isSameSecond (dirtyDateLeft, dirtyDateRight) {
  var dateLeftStartOfSecond = start_of_second(dirtyDateLeft);
  var dateRightStartOfSecond = start_of_second(dirtyDateRight);

  return dateLeftStartOfSecond.getTime() === dateRightStartOfSecond.getTime()
}

var is_same_second = isSameSecond;

/**
 * @category Year Helpers
 * @summary Are the given dates in the same year?
 *
 * @description
 * Are the given dates in the same year?
 *
 * @param {Date|String|Number} dateLeft - the first date to check
 * @param {Date|String|Number} dateRight - the second date to check
 * @returns {Boolean} the dates are in the same year
 *
 * @example
 * // Are 2 September 2014 and 25 September 2014 in the same year?
 * var result = isSameYear(
 *   new Date(2014, 8, 2),
 *   new Date(2014, 8, 25)
 * )
 * //=> true
 */
function isSameYear (dirtyDateLeft, dirtyDateRight) {
  var dateLeft = parse_1(dirtyDateLeft);
  var dateRight = parse_1(dirtyDateRight);
  return dateLeft.getFullYear() === dateRight.getFullYear()
}

var is_same_year = isSameYear;

/**
 * @category Weekday Helpers
 * @summary Is the given date Saturday?
 *
 * @description
 * Is the given date Saturday?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is Saturday
 *
 * @example
 * // Is 27 September 2014 Saturday?
 * var result = isSaturday(new Date(2014, 8, 27))
 * //=> true
 */
function isSaturday (dirtyDate) {
  return parse_1(dirtyDate).getDay() === 6
}

var is_saturday = isSaturday;

/**
 * @category Weekday Helpers
 * @summary Is the given date Sunday?
 *
 * @description
 * Is the given date Sunday?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is Sunday
 *
 * @example
 * // Is 21 September 2014 Sunday?
 * var result = isSunday(new Date(2014, 8, 21))
 * //=> true
 */
function isSunday (dirtyDate) {
  return parse_1(dirtyDate).getDay() === 0
}

var is_sunday = isSunday;

/**
 * @category Hour Helpers
 * @summary Is the given date in the same hour as the current date?
 *
 * @description
 * Is the given date in the same hour as the current date?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is in this hour
 *
 * @example
 * // If now is 25 September 2014 18:30:15.500,
 * // is 25 September 2014 18:00:00 in this hour?
 * var result = isThisHour(new Date(2014, 8, 25, 18))
 * //=> true
 */
function isThisHour (dirtyDate) {
  return is_same_hour(new Date(), dirtyDate)
}

var is_this_hour = isThisHour;

/**
 * @category ISO Week Helpers
 * @summary Is the given date in the same ISO week as the current date?
 *
 * @description
 * Is the given date in the same ISO week as the current date?
 *
 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is in this ISO week
 *
 * @example
 * // If today is 25 September 2014, is 22 September 2014 in this ISO week?
 * var result = isThisISOWeek(new Date(2014, 8, 22))
 * //=> true
 */
function isThisISOWeek (dirtyDate) {
  return is_same_iso_week(new Date(), dirtyDate)
}

var is_this_iso_week = isThisISOWeek;

/**
 * @category ISO Week-Numbering Year Helpers
 * @summary Is the given date in the same ISO week-numbering year as the current date?
 *
 * @description
 * Is the given date in the same ISO week-numbering year as the current date?
 *
 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is in this ISO week-numbering year
 *
 * @example
 * // If today is 25 September 2014,
 * // is 30 December 2013 in this ISO week-numbering year?
 * var result = isThisISOYear(new Date(2013, 11, 30))
 * //=> true
 */
function isThisISOYear (dirtyDate) {
  return is_same_iso_year(new Date(), dirtyDate)
}

var is_this_iso_year = isThisISOYear;

/**
 * @category Minute Helpers
 * @summary Is the given date in the same minute as the current date?
 *
 * @description
 * Is the given date in the same minute as the current date?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is in this minute
 *
 * @example
 * // If now is 25 September 2014 18:30:15.500,
 * // is 25 September 2014 18:30:00 in this minute?
 * var result = isThisMinute(new Date(2014, 8, 25, 18, 30))
 * //=> true
 */
function isThisMinute (dirtyDate) {
  return is_same_minute(new Date(), dirtyDate)
}

var is_this_minute = isThisMinute;

/**
 * @category Month Helpers
 * @summary Is the given date in the same month as the current date?
 *
 * @description
 * Is the given date in the same month as the current date?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is in this month
 *
 * @example
 * // If today is 25 September 2014, is 15 September 2014 in this month?
 * var result = isThisMonth(new Date(2014, 8, 15))
 * //=> true
 */
function isThisMonth (dirtyDate) {
  return is_same_month(new Date(), dirtyDate)
}

var is_this_month = isThisMonth;

/**
 * @category Quarter Helpers
 * @summary Is the given date in the same quarter as the current date?
 *
 * @description
 * Is the given date in the same quarter as the current date?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is in this quarter
 *
 * @example
 * // If today is 25 September 2014, is 2 July 2014 in this quarter?
 * var result = isThisQuarter(new Date(2014, 6, 2))
 * //=> true
 */
function isThisQuarter (dirtyDate) {
  return is_same_quarter(new Date(), dirtyDate)
}

var is_this_quarter = isThisQuarter;

/**
 * @category Second Helpers
 * @summary Is the given date in the same second as the current date?
 *
 * @description
 * Is the given date in the same second as the current date?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is in this second
 *
 * @example
 * // If now is 25 September 2014 18:30:15.500,
 * // is 25 September 2014 18:30:15.000 in this second?
 * var result = isThisSecond(new Date(2014, 8, 25, 18, 30, 15))
 * //=> true
 */
function isThisSecond (dirtyDate) {
  return is_same_second(new Date(), dirtyDate)
}

var is_this_second = isThisSecond;

/**
 * @category Week Helpers
 * @summary Is the given date in the same week as the current date?
 *
 * @description
 * Is the given date in the same week as the current date?
 *
 * @param {Date|String|Number} date - the date to check
 * @param {Object} [options] - the object with options
 * @param {Number} [options.weekStartsOn=0] - the index of the first day of the week (0 - Sunday)
 * @returns {Boolean} the date is in this week
 *
 * @example
 * // If today is 25 September 2014, is 21 September 2014 in this week?
 * var result = isThisWeek(new Date(2014, 8, 21))
 * //=> true
 *
 * @example
 * // If today is 25 September 2014 and week starts with Monday
 * // is 21 September 2014 in this week?
 * var result = isThisWeek(new Date(2014, 8, 21), {weekStartsOn: 1})
 * //=> false
 */
function isThisWeek (dirtyDate, dirtyOptions) {
  return is_same_week(new Date(), dirtyDate, dirtyOptions)
}

var is_this_week = isThisWeek;

/**
 * @category Year Helpers
 * @summary Is the given date in the same year as the current date?
 *
 * @description
 * Is the given date in the same year as the current date?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is in this year
 *
 * @example
 * // If today is 25 September 2014, is 2 July 2014 in this year?
 * var result = isThisYear(new Date(2014, 6, 2))
 * //=> true
 */
function isThisYear (dirtyDate) {
  return is_same_year(new Date(), dirtyDate)
}

var is_this_year = isThisYear;

/**
 * @category Weekday Helpers
 * @summary Is the given date Thursday?
 *
 * @description
 * Is the given date Thursday?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is Thursday
 *
 * @example
 * // Is 25 September 2014 Thursday?
 * var result = isThursday(new Date(2014, 8, 25))
 * //=> true
 */
function isThursday (dirtyDate) {
  return parse_1(dirtyDate).getDay() === 4
}

var is_thursday = isThursday;

/**
 * @category Day Helpers
 * @summary Is the given date today?
 *
 * @description
 * Is the given date today?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is today
 *
 * @example
 * // If today is 6 October 2014, is 6 October 14:00:00 today?
 * var result = isToday(new Date(2014, 9, 6, 14, 0))
 * //=> true
 */
function isToday (dirtyDate) {
  return start_of_day(dirtyDate).getTime() === start_of_day(new Date()).getTime()
}

var is_today = isToday;

/**
 * @category Day Helpers
 * @summary Is the given date tomorrow?
 *
 * @description
 * Is the given date tomorrow?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is tomorrow
 *
 * @example
 * // If today is 6 October 2014, is 7 October 14:00:00 tomorrow?
 * var result = isTomorrow(new Date(2014, 9, 7, 14, 0))
 * //=> true
 */
function isTomorrow (dirtyDate) {
  var tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  return start_of_day(dirtyDate).getTime() === start_of_day(tomorrow).getTime()
}

var is_tomorrow = isTomorrow;

/**
 * @category Weekday Helpers
 * @summary Is the given date Tuesday?
 *
 * @description
 * Is the given date Tuesday?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is Tuesday
 *
 * @example
 * // Is 23 September 2014 Tuesday?
 * var result = isTuesday(new Date(2014, 8, 23))
 * //=> true
 */
function isTuesday (dirtyDate) {
  return parse_1(dirtyDate).getDay() === 2
}

var is_tuesday = isTuesday;

/**
 * @category Weekday Helpers
 * @summary Is the given date Wednesday?
 *
 * @description
 * Is the given date Wednesday?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is Wednesday
 *
 * @example
 * // Is 24 September 2014 Wednesday?
 * var result = isWednesday(new Date(2014, 8, 24))
 * //=> true
 */
function isWednesday (dirtyDate) {
  return parse_1(dirtyDate).getDay() === 3
}

var is_wednesday = isWednesday;

/**
 * @category Weekday Helpers
 * @summary Does the given date fall on a weekend?
 *
 * @description
 * Does the given date fall on a weekend?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date falls on a weekend
 *
 * @example
 * // Does 5 October 2014 fall on a weekend?
 * var result = isWeekend(new Date(2014, 9, 5))
 * //=> true
 */
function isWeekend (dirtyDate) {
  var date = parse_1(dirtyDate);
  var day = date.getDay();
  return day === 0 || day === 6
}

var is_weekend = isWeekend;

/**
 * @category Range Helpers
 * @summary Is the given date within the range?
 *
 * @description
 * Is the given date within the range?
 *
 * @param {Date|String|Number} date - the date to check
 * @param {Date|String|Number} startDate - the start of range
 * @param {Date|String|Number} endDate - the end of range
 * @returns {Boolean} the date is within the range
 * @throws {Error} startDate cannot be after endDate
 *
 * @example
 * // For the date within the range:
 * isWithinRange(
 *   new Date(2014, 0, 3), new Date(2014, 0, 1), new Date(2014, 0, 7)
 * )
 * //=> true
 *
 * @example
 * // For the date outside of the range:
 * isWithinRange(
 *   new Date(2014, 0, 10), new Date(2014, 0, 1), new Date(2014, 0, 7)
 * )
 * //=> false
 */
function isWithinRange (dirtyDate, dirtyStartDate, dirtyEndDate) {
  var time = parse_1(dirtyDate).getTime();
  var startTime = parse_1(dirtyStartDate).getTime();
  var endTime = parse_1(dirtyEndDate).getTime();

  if (startTime > endTime) {
    throw new Error('The start of the range cannot be after the end of the range')
  }

  return time >= startTime && time <= endTime
}

var is_within_range = isWithinRange;

/**
 * @category Day Helpers
 * @summary Is the given date yesterday?
 *
 * @description
 * Is the given date yesterday?
 *
 * @param {Date|String|Number} date - the date to check
 * @returns {Boolean} the date is yesterday
 *
 * @example
 * // If today is 6 October 2014, is 5 October 14:00:00 yesterday?
 * var result = isYesterday(new Date(2014, 9, 5, 14, 0))
 * //=> true
 */
function isYesterday (dirtyDate) {
  var yesterday = new Date();
  yesterday.setDate(yesterday.getDate() - 1);
  return start_of_day(dirtyDate).getTime() === start_of_day(yesterday).getTime()
}

var is_yesterday = isYesterday;

/**
 * @category Week Helpers
 * @summary Return the last day of a week for the given date.
 *
 * @description
 * Return the last day of a week for the given date.
 * The result will be in the local timezone.
 *
 * @param {Date|String|Number} date - the original date
 * @param {Object} [options] - the object with options
 * @param {Number} [options.weekStartsOn=0] - the index of the first day of the week (0 - Sunday)
 * @returns {Date} the last day of a week
 *
 * @example
 * // The last day of a week for 2 September 2014 11:55:00:
 * var result = lastDayOfWeek(new Date(2014, 8, 2, 11, 55, 0))
 * //=> Sat Sep 06 2014 00:00:00
 *
 * @example
 * // If the week starts on Monday, the last day of the week for 2 September 2014 11:55:00:
 * var result = lastDayOfWeek(new Date(2014, 8, 2, 11, 55, 0), {weekStartsOn: 1})
 * //=> Sun Sep 07 2014 00:00:00
 */
function lastDayOfWeek (dirtyDate, dirtyOptions) {
  var weekStartsOn = dirtyOptions ? (Number(dirtyOptions.weekStartsOn) || 0) : 0;

  var date = parse_1(dirtyDate);
  var day = date.getDay();
  var diff = (day < weekStartsOn ? -7 : 0) + 6 - (day - weekStartsOn);

  date.setHours(0, 0, 0, 0);
  date.setDate(date.getDate() + diff);
  return date
}

var last_day_of_week = lastDayOfWeek;

/**
 * @category ISO Week Helpers
 * @summary Return the last day of an ISO week for the given date.
 *
 * @description
 * Return the last day of an ISO week for the given date.
 * The result will be in the local timezone.
 *
 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the last day of an ISO week
 *
 * @example
 * // The last day of an ISO week for 2 September 2014 11:55:00:
 * var result = lastDayOfISOWeek(new Date(2014, 8, 2, 11, 55, 0))
 * //=> Sun Sep 07 2014 00:00:00
 */
function lastDayOfISOWeek (dirtyDate) {
  return last_day_of_week(dirtyDate, {weekStartsOn: 1})
}

var last_day_of_iso_week = lastDayOfISOWeek;

/**
 * @category ISO Week-Numbering Year Helpers
 * @summary Return the last day of an ISO week-numbering year for the given date.
 *
 * @description
 * Return the last day of an ISO week-numbering year,
 * which always starts 3 days before the year's first Thursday.
 * The result will be in the local timezone.
 *
 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the end of an ISO week-numbering year
 *
 * @example
 * // The last day of an ISO week-numbering year for 2 July 2005:
 * var result = lastDayOfISOYear(new Date(2005, 6, 2))
 * //=> Sun Jan 01 2006 00:00:00
 */
function lastDayOfISOYear (dirtyDate) {
  var year = get_iso_year(dirtyDate);
  var fourthOfJanuary = new Date(0);
  fourthOfJanuary.setFullYear(year + 1, 0, 4);
  fourthOfJanuary.setHours(0, 0, 0, 0);
  var date = start_of_iso_week(fourthOfJanuary);
  date.setDate(date.getDate() - 1);
  return date
}

var last_day_of_iso_year = lastDayOfISOYear;

/**
 * @category Month Helpers
 * @summary Return the last day of a month for the given date.
 *
 * @description
 * Return the last day of a month for the given date.
 * The result will be in the local timezone.
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the last day of a month
 *
 * @example
 * // The last day of a month for 2 September 2014 11:55:00:
 * var result = lastDayOfMonth(new Date(2014, 8, 2, 11, 55, 0))
 * //=> Tue Sep 30 2014 00:00:00
 */
function lastDayOfMonth (dirtyDate) {
  var date = parse_1(dirtyDate);
  var month = date.getMonth();
  date.setFullYear(date.getFullYear(), month + 1, 0);
  date.setHours(0, 0, 0, 0);
  return date
}

var last_day_of_month = lastDayOfMonth;

/**
 * @category Quarter Helpers
 * @summary Return the last day of a year quarter for the given date.
 *
 * @description
 * Return the last day of a year quarter for the given date.
 * The result will be in the local timezone.
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the last day of a quarter
 *
 * @example
 * // The last day of a quarter for 2 September 2014 11:55:00:
 * var result = lastDayOfQuarter(new Date(2014, 8, 2, 11, 55, 0))
 * //=> Tue Sep 30 2014 00:00:00
 */
function lastDayOfQuarter (dirtyDate) {
  var date = parse_1(dirtyDate);
  var currentMonth = date.getMonth();
  var month = currentMonth - currentMonth % 3 + 3;
  date.setMonth(month, 0);
  date.setHours(0, 0, 0, 0);
  return date
}

var last_day_of_quarter = lastDayOfQuarter;

/**
 * @category Year Helpers
 * @summary Return the last day of a year for the given date.
 *
 * @description
 * Return the last day of a year for the given date.
 * The result will be in the local timezone.
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the last day of a year
 *
 * @example
 * // The last day of a year for 2 September 2014 11:55:00:
 * var result = lastDayOfYear(new Date(2014, 8, 2, 11, 55, 00))
 * //=> Wed Dec 31 2014 00:00:00
 */
function lastDayOfYear (dirtyDate) {
  var date = parse_1(dirtyDate);
  var year = date.getFullYear();
  date.setFullYear(year + 1, 0, 0);
  date.setHours(0, 0, 0, 0);
  return date
}

var last_day_of_year = lastDayOfYear;

/**
 * @category Common Helpers
 * @summary Return the latest of the given dates.
 *
 * @description
 * Return the latest of the given dates.
 *
 * @param {...(Date|String|Number)} dates - the dates to compare
 * @returns {Date} the latest of the dates
 *
 * @example
 * // Which of these dates is the latest?
 * var result = max(
 *   new Date(1989, 6, 10),
 *   new Date(1987, 1, 11),
 *   new Date(1995, 6, 2),
 *   new Date(1990, 0, 1)
 * )
 * //=> Sun Jul 02 1995 00:00:00
 */
function max () {
  var dirtyDates = Array.prototype.slice.call(arguments);
  var dates = dirtyDates.map(function (dirtyDate) {
    return parse_1(dirtyDate)
  });
  var latestTimestamp = Math.max.apply(null, dates);
  return new Date(latestTimestamp)
}

var max_1 = max;

/**
 * @category Common Helpers
 * @summary Return the earliest of the given dates.
 *
 * @description
 * Return the earliest of the given dates.
 *
 * @param {...(Date|String|Number)} dates - the dates to compare
 * @returns {Date} the earliest of the dates
 *
 * @example
 * // Which of these dates is the earliest?
 * var result = min(
 *   new Date(1989, 6, 10),
 *   new Date(1987, 1, 11),
 *   new Date(1995, 6, 2),
 *   new Date(1990, 0, 1)
 * )
 * //=> Wed Feb 11 1987 00:00:00
 */
function min () {
  var dirtyDates = Array.prototype.slice.call(arguments);
  var dates = dirtyDates.map(function (dirtyDate) {
    return parse_1(dirtyDate)
  });
  var earliestTimestamp = Math.min.apply(null, dates);
  return new Date(earliestTimestamp)
}

var min_1 = min;

/**
 * @category Day Helpers
 * @summary Set the day of the month to the given date.
 *
 * @description
 * Set the day of the month to the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} dayOfMonth - the day of the month of the new date
 * @returns {Date} the new date with the day of the month setted
 *
 * @example
 * // Set the 30th day of the month to 1 September 2014:
 * var result = setDate(new Date(2014, 8, 1), 30)
 * //=> Tue Sep 30 2014 00:00:00
 */
function setDate (dirtyDate, dirtyDayOfMonth) {
  var date = parse_1(dirtyDate);
  var dayOfMonth = Number(dirtyDayOfMonth);
  date.setDate(dayOfMonth);
  return date
}

var set_date = setDate;

/**
 * @category Weekday Helpers
 * @summary Set the day of the week to the given date.
 *
 * @description
 * Set the day of the week to the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} day - the day of the week of the new date
 * @param {Object} [options] - the object with options
 * @param {Number} [options.weekStartsOn=0] - the index of the first day of the week (0 - Sunday)
 * @returns {Date} the new date with the day of the week setted
 *
 * @example
 * // Set Sunday to 1 September 2014:
 * var result = setDay(new Date(2014, 8, 1), 0)
 * //=> Sun Aug 31 2014 00:00:00
 *
 * @example
 * // If week starts with Monday, set Sunday to 1 September 2014:
 * var result = setDay(new Date(2014, 8, 1), 0, {weekStartsOn: 1})
 * //=> Sun Sep 07 2014 00:00:00
 */
function setDay (dirtyDate, dirtyDay, dirtyOptions) {
  var weekStartsOn = dirtyOptions ? (Number(dirtyOptions.weekStartsOn) || 0) : 0;
  var date = parse_1(dirtyDate);
  var day = Number(dirtyDay);
  var currentDay = date.getDay();

  var remainder = day % 7;
  var dayIndex = (remainder + 7) % 7;

  var diff = (dayIndex < weekStartsOn ? 7 : 0) + day - currentDay;
  return add_days(date, diff)
}

var set_day = setDay;

/**
 * @category Day Helpers
 * @summary Set the day of the year to the given date.
 *
 * @description
 * Set the day of the year to the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} dayOfYear - the day of the year of the new date
 * @returns {Date} the new date with the day of the year setted
 *
 * @example
 * // Set the 2nd day of the year to 2 July 2014:
 * var result = setDayOfYear(new Date(2014, 6, 2), 2)
 * //=> Thu Jan 02 2014 00:00:00
 */
function setDayOfYear (dirtyDate, dirtyDayOfYear) {
  var date = parse_1(dirtyDate);
  var dayOfYear = Number(dirtyDayOfYear);
  date.setMonth(0);
  date.setDate(dayOfYear);
  return date
}

var set_day_of_year = setDayOfYear;

/**
 * @category Hour Helpers
 * @summary Set the hours to the given date.
 *
 * @description
 * Set the hours to the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} hours - the hours of the new date
 * @returns {Date} the new date with the hours setted
 *
 * @example
 * // Set 4 hours to 1 September 2014 11:30:00:
 * var result = setHours(new Date(2014, 8, 1, 11, 30), 4)
 * //=> Mon Sep 01 2014 04:30:00
 */
function setHours (dirtyDate, dirtyHours) {
  var date = parse_1(dirtyDate);
  var hours = Number(dirtyHours);
  date.setHours(hours);
  return date
}

var set_hours = setHours;

/**
 * @category Weekday Helpers
 * @summary Set the day of the ISO week to the given date.
 *
 * @description
 * Set the day of the ISO week to the given date.
 * ISO week starts with Monday.
 * 7 is the index of Sunday, 1 is the index of Monday etc.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} day - the day of the ISO week of the new date
 * @returns {Date} the new date with the day of the ISO week setted
 *
 * @example
 * // Set Sunday to 1 September 2014:
 * var result = setISODay(new Date(2014, 8, 1), 7)
 * //=> Sun Sep 07 2014 00:00:00
 */
function setISODay (dirtyDate, dirtyDay) {
  var date = parse_1(dirtyDate);
  var day = Number(dirtyDay);
  var currentDay = get_iso_day(date);
  var diff = day - currentDay;
  return add_days(date, diff)
}

var set_iso_day = setISODay;

/**
 * @category ISO Week Helpers
 * @summary Set the ISO week to the given date.
 *
 * @description
 * Set the ISO week to the given date, saving the weekday number.
 *
 * ISO week-numbering year: http://en.wikipedia.org/wiki/ISO_week_date
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} isoWeek - the ISO week of the new date
 * @returns {Date} the new date with the ISO week setted
 *
 * @example
 * // Set the 53rd ISO week to 7 August 2004:
 * var result = setISOWeek(new Date(2004, 7, 7), 53)
 * //=> Sat Jan 01 2005 00:00:00
 */
function setISOWeek (dirtyDate, dirtyISOWeek) {
  var date = parse_1(dirtyDate);
  var isoWeek = Number(dirtyISOWeek);
  var diff = get_iso_week(date) - isoWeek;
  date.setDate(date.getDate() - diff * 7);
  return date
}

var set_iso_week = setISOWeek;

/**
 * @category Millisecond Helpers
 * @summary Set the milliseconds to the given date.
 *
 * @description
 * Set the milliseconds to the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} milliseconds - the milliseconds of the new date
 * @returns {Date} the new date with the milliseconds setted
 *
 * @example
 * // Set 300 milliseconds to 1 September 2014 11:30:40.500:
 * var result = setMilliseconds(new Date(2014, 8, 1, 11, 30, 40, 500), 300)
 * //=> Mon Sep 01 2014 11:30:40.300
 */
function setMilliseconds (dirtyDate, dirtyMilliseconds) {
  var date = parse_1(dirtyDate);
  var milliseconds = Number(dirtyMilliseconds);
  date.setMilliseconds(milliseconds);
  return date
}

var set_milliseconds = setMilliseconds;

/**
 * @category Minute Helpers
 * @summary Set the minutes to the given date.
 *
 * @description
 * Set the minutes to the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} minutes - the minutes of the new date
 * @returns {Date} the new date with the minutes setted
 *
 * @example
 * // Set 45 minutes to 1 September 2014 11:30:40:
 * var result = setMinutes(new Date(2014, 8, 1, 11, 30, 40), 45)
 * //=> Mon Sep 01 2014 11:45:40
 */
function setMinutes (dirtyDate, dirtyMinutes) {
  var date = parse_1(dirtyDate);
  var minutes = Number(dirtyMinutes);
  date.setMinutes(minutes);
  return date
}

var set_minutes = setMinutes;

/**
 * @category Month Helpers
 * @summary Set the month to the given date.
 *
 * @description
 * Set the month to the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} month - the month of the new date
 * @returns {Date} the new date with the month setted
 *
 * @example
 * // Set February to 1 September 2014:
 * var result = setMonth(new Date(2014, 8, 1), 1)
 * //=> Sat Feb 01 2014 00:00:00
 */
function setMonth (dirtyDate, dirtyMonth) {
  var date = parse_1(dirtyDate);
  var month = Number(dirtyMonth);
  var year = date.getFullYear();
  var day = date.getDate();

  var dateWithDesiredMonth = new Date(0);
  dateWithDesiredMonth.setFullYear(year, month, 15);
  dateWithDesiredMonth.setHours(0, 0, 0, 0);
  var daysInMonth = get_days_in_month(dateWithDesiredMonth);
  // Set the last day of the new month
  // if the original date was the last day of the longer month
  date.setMonth(month, Math.min(day, daysInMonth));
  return date
}

var set_month = setMonth;

/**
 * @category Quarter Helpers
 * @summary Set the year quarter to the given date.
 *
 * @description
 * Set the year quarter to the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} quarter - the quarter of the new date
 * @returns {Date} the new date with the quarter setted
 *
 * @example
 * // Set the 2nd quarter to 2 July 2014:
 * var result = setQuarter(new Date(2014, 6, 2), 2)
 * //=> Wed Apr 02 2014 00:00:00
 */
function setQuarter (dirtyDate, dirtyQuarter) {
  var date = parse_1(dirtyDate);
  var quarter = Number(dirtyQuarter);
  var oldQuarter = Math.floor(date.getMonth() / 3) + 1;
  var diff = quarter - oldQuarter;
  return set_month(date, date.getMonth() + diff * 3)
}

var set_quarter = setQuarter;

/**
 * @category Second Helpers
 * @summary Set the seconds to the given date.
 *
 * @description
 * Set the seconds to the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} seconds - the seconds of the new date
 * @returns {Date} the new date with the seconds setted
 *
 * @example
 * // Set 45 seconds to 1 September 2014 11:30:40:
 * var result = setSeconds(new Date(2014, 8, 1, 11, 30, 40), 45)
 * //=> Mon Sep 01 2014 11:30:45
 */
function setSeconds (dirtyDate, dirtySeconds) {
  var date = parse_1(dirtyDate);
  var seconds = Number(dirtySeconds);
  date.setSeconds(seconds);
  return date
}

var set_seconds = setSeconds;

/**
 * @category Year Helpers
 * @summary Set the year to the given date.
 *
 * @description
 * Set the year to the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} year - the year of the new date
 * @returns {Date} the new date with the year setted
 *
 * @example
 * // Set year 2013 to 1 September 2014:
 * var result = setYear(new Date(2014, 8, 1), 2013)
 * //=> Sun Sep 01 2013 00:00:00
 */
function setYear (dirtyDate, dirtyYear) {
  var date = parse_1(dirtyDate);
  var year = Number(dirtyYear);
  date.setFullYear(year);
  return date
}

var set_year = setYear;

/**
 * @category Month Helpers
 * @summary Return the start of a month for the given date.
 *
 * @description
 * Return the start of a month for the given date.
 * The result will be in the local timezone.
 *
 * @param {Date|String|Number} date - the original date
 * @returns {Date} the start of a month
 *
 * @example
 * // The start of a month for 2 September 2014 11:55:00:
 * var result = startOfMonth(new Date(2014, 8, 2, 11, 55, 0))
 * //=> Mon Sep 01 2014 00:00:00
 */
function startOfMonth (dirtyDate) {
  var date = parse_1(dirtyDate);
  date.setDate(1);
  date.setHours(0, 0, 0, 0);
  return date
}

var start_of_month = startOfMonth;

/**
 * @category Day Helpers
 * @summary Return the start of today.
 *
 * @description
 * Return the start of today.
 *
 * @returns {Date} the start of today
 *
 * @example
 * // If today is 6 October 2014:
 * var result = startOfToday()
 * //=> Mon Oct 6 2014 00:00:00
 */
function startOfToday () {
  return start_of_day(new Date())
}

var start_of_today = startOfToday;

/**
 * @category Day Helpers
 * @summary Return the start of tomorrow.
 *
 * @description
 * Return the start of tomorrow.
 *
 * @returns {Date} the start of tomorrow
 *
 * @example
 * // If today is 6 October 2014:
 * var result = startOfTomorrow()
 * //=> Tue Oct 7 2014 00:00:00
 */
function startOfTomorrow () {
  var now = new Date();
  var year = now.getFullYear();
  var month = now.getMonth();
  var day = now.getDate();

  var date = new Date(0);
  date.setFullYear(year, month, day + 1);
  date.setHours(0, 0, 0, 0);
  return date
}

var start_of_tomorrow = startOfTomorrow;

/**
 * @category Day Helpers
 * @summary Return the start of yesterday.
 *
 * @description
 * Return the start of yesterday.
 *
 * @returns {Date} the start of yesterday
 *
 * @example
 * // If today is 6 October 2014:
 * var result = startOfYesterday()
 * //=> Sun Oct 5 2014 00:00:00
 */
function startOfYesterday () {
  var now = new Date();
  var year = now.getFullYear();
  var month = now.getMonth();
  var day = now.getDate();

  var date = new Date(0);
  date.setFullYear(year, month, day - 1);
  date.setHours(0, 0, 0, 0);
  return date
}

var start_of_yesterday = startOfYesterday;

/**
 * @category Day Helpers
 * @summary Subtract the specified number of days from the given date.
 *
 * @description
 * Subtract the specified number of days from the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} amount - the amount of days to be subtracted
 * @returns {Date} the new date with the days subtracted
 *
 * @example
 * // Subtract 10 days from 1 September 2014:
 * var result = subDays(new Date(2014, 8, 1), 10)
 * //=> Fri Aug 22 2014 00:00:00
 */
function subDays (dirtyDate, dirtyAmount) {
  var amount = Number(dirtyAmount);
  return add_days(dirtyDate, -amount)
}

var sub_days = subDays;

/**
 * @category Hour Helpers
 * @summary Subtract the specified number of hours from the given date.
 *
 * @description
 * Subtract the specified number of hours from the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} amount - the amount of hours to be subtracted
 * @returns {Date} the new date with the hours subtracted
 *
 * @example
 * // Subtract 2 hours from 11 July 2014 01:00:00:
 * var result = subHours(new Date(2014, 6, 11, 1, 0), 2)
 * //=> Thu Jul 10 2014 23:00:00
 */
function subHours (dirtyDate, dirtyAmount) {
  var amount = Number(dirtyAmount);
  return add_hours(dirtyDate, -amount)
}

var sub_hours = subHours;

/**
 * @category Millisecond Helpers
 * @summary Subtract the specified number of milliseconds from the given date.
 *
 * @description
 * Subtract the specified number of milliseconds from the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} amount - the amount of milliseconds to be subtracted
 * @returns {Date} the new date with the milliseconds subtracted
 *
 * @example
 * // Subtract 750 milliseconds from 10 July 2014 12:45:30.000:
 * var result = subMilliseconds(new Date(2014, 6, 10, 12, 45, 30, 0), 750)
 * //=> Thu Jul 10 2014 12:45:29.250
 */
function subMilliseconds (dirtyDate, dirtyAmount) {
  var amount = Number(dirtyAmount);
  return add_milliseconds(dirtyDate, -amount)
}

var sub_milliseconds = subMilliseconds;

/**
 * @category Minute Helpers
 * @summary Subtract the specified number of minutes from the given date.
 *
 * @description
 * Subtract the specified number of minutes from the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} amount - the amount of minutes to be subtracted
 * @returns {Date} the new date with the mintues subtracted
 *
 * @example
 * // Subtract 30 minutes from 10 July 2014 12:00:00:
 * var result = subMinutes(new Date(2014, 6, 10, 12, 0), 30)
 * //=> Thu Jul 10 2014 11:30:00
 */
function subMinutes (dirtyDate, dirtyAmount) {
  var amount = Number(dirtyAmount);
  return add_minutes(dirtyDate, -amount)
}

var sub_minutes = subMinutes;

/**
 * @category Month Helpers
 * @summary Subtract the specified number of months from the given date.
 *
 * @description
 * Subtract the specified number of months from the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} amount - the amount of months to be subtracted
 * @returns {Date} the new date with the months subtracted
 *
 * @example
 * // Subtract 5 months from 1 February 2015:
 * var result = subMonths(new Date(2015, 1, 1), 5)
 * //=> Mon Sep 01 2014 00:00:00
 */
function subMonths (dirtyDate, dirtyAmount) {
  var amount = Number(dirtyAmount);
  return add_months(dirtyDate, -amount)
}

var sub_months = subMonths;

/**
 * @category Quarter Helpers
 * @summary Subtract the specified number of year quarters from the given date.
 *
 * @description
 * Subtract the specified number of year quarters from the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} amount - the amount of quarters to be subtracted
 * @returns {Date} the new date with the quarters subtracted
 *
 * @example
 * // Subtract 3 quarters from 1 September 2014:
 * var result = subQuarters(new Date(2014, 8, 1), 3)
 * //=> Sun Dec 01 2013 00:00:00
 */
function subQuarters (dirtyDate, dirtyAmount) {
  var amount = Number(dirtyAmount);
  return add_quarters(dirtyDate, -amount)
}

var sub_quarters = subQuarters;

/**
 * @category Second Helpers
 * @summary Subtract the specified number of seconds from the given date.
 *
 * @description
 * Subtract the specified number of seconds from the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} amount - the amount of seconds to be subtracted
 * @returns {Date} the new date with the seconds subtracted
 *
 * @example
 * // Subtract 30 seconds from 10 July 2014 12:45:00:
 * var result = subSeconds(new Date(2014, 6, 10, 12, 45, 0), 30)
 * //=> Thu Jul 10 2014 12:44:30
 */
function subSeconds (dirtyDate, dirtyAmount) {
  var amount = Number(dirtyAmount);
  return add_seconds(dirtyDate, -amount)
}

var sub_seconds = subSeconds;

/**
 * @category Week Helpers
 * @summary Subtract the specified number of weeks from the given date.
 *
 * @description
 * Subtract the specified number of weeks from the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} amount - the amount of weeks to be subtracted
 * @returns {Date} the new date with the weeks subtracted
 *
 * @example
 * // Subtract 4 weeks from 1 September 2014:
 * var result = subWeeks(new Date(2014, 8, 1), 4)
 * //=> Mon Aug 04 2014 00:00:00
 */
function subWeeks (dirtyDate, dirtyAmount) {
  var amount = Number(dirtyAmount);
  return add_weeks(dirtyDate, -amount)
}

var sub_weeks = subWeeks;

/**
 * @category Year Helpers
 * @summary Subtract the specified number of years from the given date.
 *
 * @description
 * Subtract the specified number of years from the given date.
 *
 * @param {Date|String|Number} date - the date to be changed
 * @param {Number} amount - the amount of years to be subtracted
 * @returns {Date} the new date with the years subtracted
 *
 * @example
 * // Subtract 5 years from 1 September 2014:
 * var result = subYears(new Date(2014, 8, 1), 5)
 * //=> Tue Sep 01 2009 00:00:00
 */
function subYears (dirtyDate, dirtyAmount) {
  var amount = Number(dirtyAmount);
  return add_years(dirtyDate, -amount)
}

var sub_years = subYears;

var dateFns = {
  addDays: add_days,
  addHours: add_hours,
  addISOYears: add_iso_years,
  addMilliseconds: add_milliseconds,
  addMinutes: add_minutes,
  addMonths: add_months,
  addQuarters: add_quarters,
  addSeconds: add_seconds,
  addWeeks: add_weeks,
  addYears: add_years,
  areRangesOverlapping: are_ranges_overlapping,
  closestIndexTo: closest_index_to,
  closestTo: closest_to,
  compareAsc: compare_asc,
  compareDesc: compare_desc,
  differenceInCalendarDays: difference_in_calendar_days,
  differenceInCalendarISOWeeks: difference_in_calendar_iso_weeks,
  differenceInCalendarISOYears: difference_in_calendar_iso_years,
  differenceInCalendarMonths: difference_in_calendar_months,
  differenceInCalendarQuarters: difference_in_calendar_quarters,
  differenceInCalendarWeeks: difference_in_calendar_weeks,
  differenceInCalendarYears: difference_in_calendar_years,
  differenceInDays: difference_in_days,
  differenceInHours: difference_in_hours,
  differenceInISOYears: difference_in_iso_years,
  differenceInMilliseconds: difference_in_milliseconds,
  differenceInMinutes: difference_in_minutes,
  differenceInMonths: difference_in_months,
  differenceInQuarters: difference_in_quarters,
  differenceInSeconds: difference_in_seconds,
  differenceInWeeks: difference_in_weeks,
  differenceInYears: difference_in_years,
  distanceInWords: distance_in_words,
  distanceInWordsStrict: distance_in_words_strict,
  distanceInWordsToNow: distance_in_words_to_now,
  eachDay: each_day,
  endOfDay: end_of_day,
  endOfHour: end_of_hour,
  endOfISOWeek: end_of_iso_week,
  endOfISOYear: end_of_iso_year,
  endOfMinute: end_of_minute,
  endOfMonth: end_of_month,
  endOfQuarter: end_of_quarter,
  endOfSecond: end_of_second,
  endOfToday: end_of_today,
  endOfTomorrow: end_of_tomorrow,
  endOfWeek: end_of_week,
  endOfYear: end_of_year,
  endOfYesterday: end_of_yesterday,
  format: format_1,
  getDate: get_date,
  getDay: get_day,
  getDayOfYear: get_day_of_year,
  getDaysInMonth: get_days_in_month,
  getDaysInYear: get_days_in_year,
  getHours: get_hours,
  getISODay: get_iso_day,
  getISOWeek: get_iso_week,
  getISOWeeksInYear: get_iso_weeks_in_year,
  getISOYear: get_iso_year,
  getMilliseconds: get_milliseconds,
  getMinutes: get_minutes,
  getMonth: get_month,
  getOverlappingDaysInRanges: get_overlapping_days_in_ranges,
  getQuarter: get_quarter,
  getSeconds: get_seconds,
  getTime: get_time,
  getYear: get_year,
  isAfter: is_after,
  isBefore: is_before,
  isDate: is_date,
  isEqual: is_equal,
  isFirstDayOfMonth: is_first_day_of_month,
  isFriday: is_friday,
  isFuture: is_future,
  isLastDayOfMonth: is_last_day_of_month,
  isLeapYear: is_leap_year,
  isMonday: is_monday,
  isPast: is_past,
  isSameDay: is_same_day,
  isSameHour: is_same_hour,
  isSameISOWeek: is_same_iso_week,
  isSameISOYear: is_same_iso_year,
  isSameMinute: is_same_minute,
  isSameMonth: is_same_month,
  isSameQuarter: is_same_quarter,
  isSameSecond: is_same_second,
  isSameWeek: is_same_week,
  isSameYear: is_same_year,
  isSaturday: is_saturday,
  isSunday: is_sunday,
  isThisHour: is_this_hour,
  isThisISOWeek: is_this_iso_week,
  isThisISOYear: is_this_iso_year,
  isThisMinute: is_this_minute,
  isThisMonth: is_this_month,
  isThisQuarter: is_this_quarter,
  isThisSecond: is_this_second,
  isThisWeek: is_this_week,
  isThisYear: is_this_year,
  isThursday: is_thursday,
  isToday: is_today,
  isTomorrow: is_tomorrow,
  isTuesday: is_tuesday,
  isValid: is_valid,
  isWednesday: is_wednesday,
  isWeekend: is_weekend,
  isWithinRange: is_within_range,
  isYesterday: is_yesterday,
  lastDayOfISOWeek: last_day_of_iso_week,
  lastDayOfISOYear: last_day_of_iso_year,
  lastDayOfMonth: last_day_of_month,
  lastDayOfQuarter: last_day_of_quarter,
  lastDayOfWeek: last_day_of_week,
  lastDayOfYear: last_day_of_year,
  max: max_1,
  min: min_1,
  parse: parse_1,
  setDate: set_date,
  setDay: set_day,
  setDayOfYear: set_day_of_year,
  setHours: set_hours,
  setISODay: set_iso_day,
  setISOWeek: set_iso_week,
  setISOYear: set_iso_year,
  setMilliseconds: set_milliseconds,
  setMinutes: set_minutes,
  setMonth: set_month,
  setQuarter: set_quarter,
  setSeconds: set_seconds,
  setYear: set_year,
  startOfDay: start_of_day,
  startOfHour: start_of_hour,
  startOfISOWeek: start_of_iso_week,
  startOfISOYear: start_of_iso_year,
  startOfMinute: start_of_minute,
  startOfMonth: start_of_month,
  startOfQuarter: start_of_quarter,
  startOfSecond: start_of_second,
  startOfToday: start_of_today,
  startOfTomorrow: start_of_tomorrow,
  startOfWeek: start_of_week,
  startOfYear: start_of_year,
  startOfYesterday: start_of_yesterday,
  subDays: sub_days,
  subHours: sub_hours,
  subISOYears: sub_iso_years,
  subMilliseconds: sub_milliseconds,
  subMinutes: sub_minutes,
  subMonths: sub_months,
  subQuarters: sub_quarters,
  subSeconds: sub_seconds,
  subWeeks: sub_weeks,
  subYears: sub_years
};
var dateFns_15 = dateFns.compareDesc;
var dateFns_33 = dateFns.distanceInWords;
var dateFns_50 = dateFns.format;
var dateFns_71 = dateFns.isDate;

function buildDistanceInWordsLocale$1 () {
  var distanceInWordsLocale = {
    lessThanXSeconds: {
      one: 'mindre end et sekund',
      other: 'mindre end {{count}} sekunder'
    },

    xSeconds: {
      one: '1 sekund',
      other: '{{count}} sekunder'
    },

    halfAMinute: 'et halvt minut',

    lessThanXMinutes: {
      one: 'mindre end et minut',
      other: 'mindre end {{count}} minutter'
    },

    xMinutes: {
      one: '1 minut',
      other: '{{count}} minutter'
    },

    aboutXHours: {
      one: 'cirka 1 time',
      other: 'cirka {{count}} timer'
    },

    xHours: {
      one: '1 time',
      other: '{{count}} timer'
    },

    xDays: {
      one: '1 dag',
      other: '{{count}} dage'
    },

    aboutXMonths: {
      one: 'cirka 1 måned',
      other: 'cirka {{count}} måneder'
    },

    xMonths: {
      one: '1 måned',
      other: '{{count}} måneder'
    },

    aboutXYears: {
      one: 'cirka 1 år',
      other: 'cirka {{count}} år'
    },

    xYears: {
      one: '1 år',
      other: '{{count}} år'
    },

    overXYears: {
      one: 'over 1 år',
      other: 'over {{count}} år'
    },

    almostXYears: {
      one: 'næsten 1 år',
      other: 'næsten {{count}} år'
    }
  };

  function localize (token, count, options) {
    options = options || {};

    var result;
    if (typeof distanceInWordsLocale[token] === 'string') {
      result = distanceInWordsLocale[token];
    } else if (count === 1) {
      result = distanceInWordsLocale[token].one;
    } else {
      result = distanceInWordsLocale[token].other.replace('{{count}}', count);
    }

    if (options.addSuffix) {
      if (options.comparison > 0) {
        return 'om ' + result
      } else {
        return result + ' siden'
      }
    }

    return result
  }

  return {
    localize: localize
  }
}

var build_distance_in_words_locale$1 = buildDistanceInWordsLocale$1;

function buildFormatLocale$1 () {
  var months3char = ['jan', 'feb', 'mar', 'apr', 'maj', 'jun', 'jul', 'aug', 'sep', 'okt', 'nov', 'dec'];
  var monthsFull = ['januar', 'februar', 'marts', 'april', 'maj', 'juni', 'juli', 'august', 'september', 'oktober', 'november', 'december'];
  var weekdays2char = ['sø', 'ma', 'ti', 'on', 'to', 'fr', 'lø'];
  var weekdays3char = ['søn', 'man', 'tir', 'ons', 'tor', 'fre', 'lør'];
  var weekdaysFull = ['søndag', 'mandag', 'tirsdag', 'onsdag', 'torsdag', 'fredag', 'lørdag'];
  var meridiemUppercase = ['AM', 'PM'];
  var meridiemLowercase = ['am', 'pm'];
  var meridiemFull = ['a.m.', 'p.m.'];

  var formatters = {
    // Month: Jan, Feb, ..., Dec
    'MMM': function (date) {
      return months3char[date.getMonth()]
    },

    // Month: January, February, ..., December
    'MMMM': function (date) {
      return monthsFull[date.getMonth()]
    },

    // Day of week: Su, Mo, ..., Sa
    'dd': function (date) {
      return weekdays2char[date.getDay()]
    },

    // Day of week: Sun, Mon, ..., Sat
    'ddd': function (date) {
      return weekdays3char[date.getDay()]
    },

    // Day of week: Sunday, Monday, ..., Saturday
    'dddd': function (date) {
      return weekdaysFull[date.getDay()]
    },

    // AM, PM
    'A': function (date) {
      return (date.getHours() / 12) >= 1 ? meridiemUppercase[1] : meridiemUppercase[0]
    },

    // am, pm
    'a': function (date) {
      return (date.getHours() / 12) >= 1 ? meridiemLowercase[1] : meridiemLowercase[0]
    },

    // a.m., p.m.
    'aa': function (date) {
      return (date.getHours() / 12) >= 1 ? meridiemFull[1] : meridiemFull[0]
    }
  };

  // Generate ordinal version of formatters: M -> Mo, D -> Do, etc.
  var ordinalFormatters = ['M', 'D', 'DDD', 'd', 'Q', 'W'];
  ordinalFormatters.forEach(function (formatterToken) {
    formatters[formatterToken + 'o'] = function (date, formatters) {
      return ordinal$1(formatters[formatterToken](date))
    };
  });

  return {
    formatters: formatters,
    formattingTokensRegExp: build_formatting_tokens_reg_exp(formatters)
  }
}

function ordinal$1 (number) {
  return number + '.'
}

var build_format_locale$1 = buildFormatLocale$1;

/**
 * @category Locales
 * @summary Danish locale.
 * @author Anders B. Hansen [@Andersbiha]{@link https://github.com/Andersbiha}
 * @author [@kgram]{@link https://github.com/kgram}
 */
var da = {
  distanceInWords: build_distance_in_words_locale$1(),
  format: build_format_locale$1()
};

/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return value != null && (type == 'object' || type == 'function');
}

var isObject_1 = isObject;

var commonjsGlobal = typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof commonjsGlobal == 'object' && commonjsGlobal && commonjsGlobal.Object === Object && commonjsGlobal;

var _freeGlobal = freeGlobal;

/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = _freeGlobal || freeSelf || Function('return this')();

var _root = root;

/** Built-in value references. */
var Symbol$1 = _root.Symbol;

var _Symbol = Symbol$1;

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/** Built-in value references. */
var symToStringTag = _Symbol ? _Symbol.toStringTag : undefined;

/**
 * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the raw `toStringTag`.
 */
function getRawTag(value) {
  var isOwn = hasOwnProperty.call(value, symToStringTag),
      tag = value[symToStringTag];

  try {
    value[symToStringTag] = undefined;
  } catch (e) {}

  var result = nativeObjectToString.call(value);
  {
    if (isOwn) {
      value[symToStringTag] = tag;
    } else {
      delete value[symToStringTag];
    }
  }
  return result;
}

var _getRawTag = getRawTag;

/** Used for built-in method references. */
var objectProto$1 = Object.prototype;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString$1 = objectProto$1.toString;

/**
 * Converts `value` to a string using `Object.prototype.toString`.
 *
 * @private
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 */
function objectToString(value) {
  return nativeObjectToString$1.call(value);
}

var _objectToString = objectToString;

/** `Object#toString` result references. */
var nullTag = '[object Null]',
    undefinedTag = '[object Undefined]';

/** Built-in value references. */
var symToStringTag$1 = _Symbol ? _Symbol.toStringTag : undefined;

/**
 * The base implementation of `getTag` without fallbacks for buggy environments.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
function baseGetTag(value) {
  if (value == null) {
    return value === undefined ? undefinedTag : nullTag;
  }
  return (symToStringTag$1 && symToStringTag$1 in Object(value))
    ? _getRawTag(value)
    : _objectToString(value);
}

var _baseGetTag = baseGetTag;

/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return value != null && typeof value == 'object';
}

var isObjectLike_1 = isObjectLike;

/** `Object#toString` result references. */
var numberTag = '[object Number]';

/**
 * Checks if `value` is classified as a `Number` primitive or object.
 *
 * **Note:** To exclude `Infinity`, `-Infinity`, and `NaN`, which are
 * classified as numbers, use the `_.isFinite` method.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a number, else `false`.
 * @example
 *
 * _.isNumber(3);
 * // => true
 *
 * _.isNumber(Number.MIN_VALUE);
 * // => true
 *
 * _.isNumber(Infinity);
 * // => true
 *
 * _.isNumber('3');
 * // => false
 */
function isNumber(value) {
  return typeof value == 'number' ||
    (isObjectLike_1(value) && _baseGetTag(value) == numberTag);
}

var isNumber_1 = isNumber;

var isString = function isString(s) {
  return typeof s === 'string' || s instanceof String;
};
var isDefined = function isDefined(n) {
  return n !== undefined && n !== null;
};
var isNumber$1 = function isNumber(n) {
  return isNumber_1(n);
};
var isArray = function isArray(n) {
  return Array.isArray(n);
};
var isObject$1 = function isObject(obj) {
  return isObject_1(obj);
};
var toString$1 = function toString(s) {
  return isString(s) ? s : s.toString();
};

/**
 * Returns a new Date based on the input
 */
var toDate = function toDate(date) {
  if (!date) return new Date();
  if (dateFns_71(date)) return date;
  if (isString(date)) return toDate(new Date(date));

  console.warn('[newDate]: could not create date object from:', date);
  return new Date();
};

/**
 * Checks wether a date is after a specified date
 */
var isDateAfter = function isDateAfter(dateA, dateB) {
  var result = dateFns_15(dateA, dateB);

  if (result === 1) return true;
  return false;
};

/**
 * returns a new date
 */
var now = function now() {
  return new Date();
};

/**
 * Formats a date or time to a standardized format
 * @param {*} param0
 */
var format$1 = function format$$1(_ref) {
  var value = _ref.value,
      customFormat = _ref.customFormat,
      _ref$compact = _ref.compact,
      compact = _ref$compact === undefined ? false : _ref$compact,
      _ref$mode = _ref.mode,
      mode = _ref$mode === undefined ? 'dateTime' : _ref$mode;

  var getDisplayFormat = function getDisplayFormat(m) {
    if (customFormat && isString(customFormat)) return customFormat;

    switch (m) {
      case 'date':
        if (compact) return 'MM-D-YY';
        return 'MMMM Mo YYYY';
      case 'time':
        return 'HH:mm a';
      case 'dateTime':
        return getDisplayFormat('date') + ', ' + getDisplayFormat('time');
      default:
        return '';
    }
  };

  if (mode === 'timeAgo') {
    var dateToCompare = toDate(value);
    var date = now();

    return dateFns_33(dateToCompare, date);
  }

  return dateFns_50(toDate(value), getDisplayFormat(mode), { locale: en });
};

var dispatch = function dispatch(log) {

  var type = log.type,
      body = log.body;


  switch (type) {
    case 'warning':
      console.warn(body);
      break;
    case 'info':
      console.info(body);
      break;
    default:
      console.log(body);
      break;
  }
};

var logFromMethod = function logFromMethod(_ref) {
  var className = _ref.className,
      method = _ref.method,
      type = _ref.type,
      message = _ref.message;

  var classString = className ? className + ': ' : '';
  var body = '[TILE] [' + classString + method + '] ' + message;

  dispatch({
    type: type,
    body: body
  });
};

/**
 * Checks if `value` is classified as an `Array` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array, else `false`.
 * @example
 *
 * _.isArray([1, 2, 3]);
 * // => true
 *
 * _.isArray(document.body.children);
 * // => false
 *
 * _.isArray('abc');
 * // => false
 *
 * _.isArray(_.noop);
 * // => false
 */
var isArray$1 = Array.isArray;

var isArray_1 = isArray$1;

/** `Object#toString` result references. */
var symbolTag = '[object Symbol]';

/**
 * Checks if `value` is classified as a `Symbol` primitive or object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
 * @example
 *
 * _.isSymbol(Symbol.iterator);
 * // => true
 *
 * _.isSymbol('abc');
 * // => false
 */
function isSymbol(value) {
  return typeof value == 'symbol' ||
    (isObjectLike_1(value) && _baseGetTag(value) == symbolTag);
}

var isSymbol_1 = isSymbol;

/** Used to match property names within property paths. */
var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
    reIsPlainProp = /^\w*$/;

/**
 * Checks if `value` is a property name and not a property path.
 *
 * @private
 * @param {*} value The value to check.
 * @param {Object} [object] The object to query keys on.
 * @returns {boolean} Returns `true` if `value` is a property name, else `false`.
 */
function isKey(value, object) {
  if (isArray_1(value)) {
    return false;
  }
  var type = typeof value;
  if (type == 'number' || type == 'symbol' || type == 'boolean' ||
      value == null || isSymbol_1(value)) {
    return true;
  }
  return reIsPlainProp.test(value) || !reIsDeepProp.test(value) ||
    (object != null && value in Object(object));
}

var _isKey = isKey;

/** `Object#toString` result references. */
var asyncTag = '[object AsyncFunction]',
    funcTag = '[object Function]',
    genTag = '[object GeneratorFunction]',
    proxyTag = '[object Proxy]';

/**
 * Checks if `value` is classified as a `Function` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a function, else `false`.
 * @example
 *
 * _.isFunction(_);
 * // => true
 *
 * _.isFunction(/abc/);
 * // => false
 */
function isFunction(value) {
  if (!isObject_1(value)) {
    return false;
  }
  // The use of `Object#toString` avoids issues with the `typeof` operator
  // in Safari 9 which returns 'object' for typed arrays and other constructors.
  var tag = _baseGetTag(value);
  return tag == funcTag || tag == genTag || tag == asyncTag || tag == proxyTag;
}

var isFunction_1 = isFunction;

/** Used to detect overreaching core-js shims. */
var coreJsData = _root['__core-js_shared__'];

var _coreJsData = coreJsData;

/** Used to detect methods masquerading as native. */
var maskSrcKey = (function() {
  var uid = /[^.]+$/.exec(_coreJsData && _coreJsData.keys && _coreJsData.keys.IE_PROTO || '');
  return uid ? ('Symbol(src)_1.' + uid) : '';
}());

/**
 * Checks if `func` has its source masked.
 *
 * @private
 * @param {Function} func The function to check.
 * @returns {boolean} Returns `true` if `func` is masked, else `false`.
 */
function isMasked(func) {
  return !!maskSrcKey && (maskSrcKey in func);
}

var _isMasked = isMasked;

/** Used for built-in method references. */
var funcProto = Function.prototype;

/** Used to resolve the decompiled source of functions. */
var funcToString = funcProto.toString;

/**
 * Converts `func` to its source code.
 *
 * @private
 * @param {Function} func The function to convert.
 * @returns {string} Returns the source code.
 */
function toSource(func) {
  if (func != null) {
    try {
      return funcToString.call(func);
    } catch (e) {}
    try {
      return (func + '');
    } catch (e) {}
  }
  return '';
}

var _toSource = toSource;

/**
 * Used to match `RegExp`
 * [syntax characters](http://ecma-international.org/ecma-262/7.0/#sec-patterns).
 */
var reRegExpChar = /[\\^$.*+?()[\]{}|]/g;

/** Used to detect host constructors (Safari). */
var reIsHostCtor = /^\[object .+?Constructor\]$/;

/** Used for built-in method references. */
var funcProto$1 = Function.prototype,
    objectProto$2 = Object.prototype;

/** Used to resolve the decompiled source of functions. */
var funcToString$1 = funcProto$1.toString;

/** Used to check objects for own properties. */
var hasOwnProperty$1 = objectProto$2.hasOwnProperty;

/** Used to detect if a method is native. */
var reIsNative = RegExp('^' +
  funcToString$1.call(hasOwnProperty$1).replace(reRegExpChar, '\\$&')
  .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
);

/**
 * The base implementation of `_.isNative` without bad shim checks.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a native function,
 *  else `false`.
 */
function baseIsNative(value) {
  if (!isObject_1(value) || _isMasked(value)) {
    return false;
  }
  var pattern = isFunction_1(value) ? reIsNative : reIsHostCtor;
  return pattern.test(_toSource(value));
}

var _baseIsNative = baseIsNative;

/**
 * Gets the value at `key` of `object`.
 *
 * @private
 * @param {Object} [object] The object to query.
 * @param {string} key The key of the property to get.
 * @returns {*} Returns the property value.
 */
function getValue(object, key) {
  return object == null ? undefined : object[key];
}

var _getValue = getValue;

/**
 * Gets the native function at `key` of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {string} key The key of the method to get.
 * @returns {*} Returns the function if it's native, else `undefined`.
 */
function getNative(object, key) {
  var value = _getValue(object, key);
  return _baseIsNative(value) ? value : undefined;
}

var _getNative = getNative;

/* Built-in method references that are verified to be native. */
var nativeCreate = _getNative(Object, 'create');

var _nativeCreate = nativeCreate;

/**
 * Removes all key-value entries from the hash.
 *
 * @private
 * @name clear
 * @memberOf Hash
 */
function hashClear() {
  this.__data__ = _nativeCreate ? _nativeCreate(null) : {};
  this.size = 0;
}

var _hashClear = hashClear;

/**
 * Removes `key` and its value from the hash.
 *
 * @private
 * @name delete
 * @memberOf Hash
 * @param {Object} hash The hash to modify.
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function hashDelete(key) {
  var result = this.has(key) && delete this.__data__[key];
  this.size -= result ? 1 : 0;
  return result;
}

var _hashDelete = hashDelete;

/** Used to stand-in for `undefined` hash values. */
var HASH_UNDEFINED = '__lodash_hash_undefined__';

/** Used for built-in method references. */
var objectProto$3 = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty$2 = objectProto$3.hasOwnProperty;

/**
 * Gets the hash value for `key`.
 *
 * @private
 * @name get
 * @memberOf Hash
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function hashGet(key) {
  var data = this.__data__;
  if (_nativeCreate) {
    var result = data[key];
    return result === HASH_UNDEFINED ? undefined : result;
  }
  return hasOwnProperty$2.call(data, key) ? data[key] : undefined;
}

var _hashGet = hashGet;

/** Used for built-in method references. */
var objectProto$4 = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty$3 = objectProto$4.hasOwnProperty;

/**
 * Checks if a hash value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf Hash
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function hashHas(key) {
  var data = this.__data__;
  return _nativeCreate ? (data[key] !== undefined) : hasOwnProperty$3.call(data, key);
}

var _hashHas = hashHas;

/** Used to stand-in for `undefined` hash values. */
var HASH_UNDEFINED$1 = '__lodash_hash_undefined__';

/**
 * Sets the hash `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf Hash
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the hash instance.
 */
function hashSet(key, value) {
  var data = this.__data__;
  this.size += this.has(key) ? 0 : 1;
  data[key] = (_nativeCreate && value === undefined) ? HASH_UNDEFINED$1 : value;
  return this;
}

var _hashSet = hashSet;

/**
 * Creates a hash object.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function Hash(entries) {
  var index = -1,
      length = entries == null ? 0 : entries.length;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

// Add methods to `Hash`.
Hash.prototype.clear = _hashClear;
Hash.prototype['delete'] = _hashDelete;
Hash.prototype.get = _hashGet;
Hash.prototype.has = _hashHas;
Hash.prototype.set = _hashSet;

var _Hash = Hash;

/**
 * Removes all key-value entries from the list cache.
 *
 * @private
 * @name clear
 * @memberOf ListCache
 */
function listCacheClear() {
  this.__data__ = [];
  this.size = 0;
}

var _listCacheClear = listCacheClear;

/**
 * Performs a
 * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * comparison between two values to determine if they are equivalent.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 * @example
 *
 * var object = { 'a': 1 };
 * var other = { 'a': 1 };
 *
 * _.eq(object, object);
 * // => true
 *
 * _.eq(object, other);
 * // => false
 *
 * _.eq('a', 'a');
 * // => true
 *
 * _.eq('a', Object('a'));
 * // => false
 *
 * _.eq(NaN, NaN);
 * // => true
 */
function eq(value, other) {
  return value === other || (value !== value && other !== other);
}

var eq_1 = eq;

/**
 * Gets the index at which the `key` is found in `array` of key-value pairs.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {*} key The key to search for.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function assocIndexOf(array, key) {
  var length = array.length;
  while (length--) {
    if (eq_1(array[length][0], key)) {
      return length;
    }
  }
  return -1;
}

var _assocIndexOf = assocIndexOf;

/** Used for built-in method references. */
var arrayProto = Array.prototype;

/** Built-in value references. */
var splice = arrayProto.splice;

/**
 * Removes `key` and its value from the list cache.
 *
 * @private
 * @name delete
 * @memberOf ListCache
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function listCacheDelete(key) {
  var data = this.__data__,
      index = _assocIndexOf(data, key);

  if (index < 0) {
    return false;
  }
  var lastIndex = data.length - 1;
  if (index == lastIndex) {
    data.pop();
  } else {
    splice.call(data, index, 1);
  }
  --this.size;
  return true;
}

var _listCacheDelete = listCacheDelete;

/**
 * Gets the list cache value for `key`.
 *
 * @private
 * @name get
 * @memberOf ListCache
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function listCacheGet(key) {
  var data = this.__data__,
      index = _assocIndexOf(data, key);

  return index < 0 ? undefined : data[index][1];
}

var _listCacheGet = listCacheGet;

/**
 * Checks if a list cache value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf ListCache
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function listCacheHas(key) {
  return _assocIndexOf(this.__data__, key) > -1;
}

var _listCacheHas = listCacheHas;

/**
 * Sets the list cache `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf ListCache
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the list cache instance.
 */
function listCacheSet(key, value) {
  var data = this.__data__,
      index = _assocIndexOf(data, key);

  if (index < 0) {
    ++this.size;
    data.push([key, value]);
  } else {
    data[index][1] = value;
  }
  return this;
}

var _listCacheSet = listCacheSet;

/**
 * Creates an list cache object.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function ListCache(entries) {
  var index = -1,
      length = entries == null ? 0 : entries.length;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

// Add methods to `ListCache`.
ListCache.prototype.clear = _listCacheClear;
ListCache.prototype['delete'] = _listCacheDelete;
ListCache.prototype.get = _listCacheGet;
ListCache.prototype.has = _listCacheHas;
ListCache.prototype.set = _listCacheSet;

var _ListCache = ListCache;

/* Built-in method references that are verified to be native. */
var Map = _getNative(_root, 'Map');

var _Map = Map;

/**
 * Removes all key-value entries from the map.
 *
 * @private
 * @name clear
 * @memberOf MapCache
 */
function mapCacheClear() {
  this.size = 0;
  this.__data__ = {
    'hash': new _Hash,
    'map': new (_Map || _ListCache),
    'string': new _Hash
  };
}

var _mapCacheClear = mapCacheClear;

/**
 * Checks if `value` is suitable for use as unique object key.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is suitable, else `false`.
 */
function isKeyable(value) {
  var type = typeof value;
  return (type == 'string' || type == 'number' || type == 'symbol' || type == 'boolean')
    ? (value !== '__proto__')
    : (value === null);
}

var _isKeyable = isKeyable;

/**
 * Gets the data for `map`.
 *
 * @private
 * @param {Object} map The map to query.
 * @param {string} key The reference key.
 * @returns {*} Returns the map data.
 */
function getMapData(map, key) {
  var data = map.__data__;
  return _isKeyable(key)
    ? data[typeof key == 'string' ? 'string' : 'hash']
    : data.map;
}

var _getMapData = getMapData;

/**
 * Removes `key` and its value from the map.
 *
 * @private
 * @name delete
 * @memberOf MapCache
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function mapCacheDelete(key) {
  var result = _getMapData(this, key)['delete'](key);
  this.size -= result ? 1 : 0;
  return result;
}

var _mapCacheDelete = mapCacheDelete;

/**
 * Gets the map value for `key`.
 *
 * @private
 * @name get
 * @memberOf MapCache
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function mapCacheGet(key) {
  return _getMapData(this, key).get(key);
}

var _mapCacheGet = mapCacheGet;

/**
 * Checks if a map value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf MapCache
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function mapCacheHas(key) {
  return _getMapData(this, key).has(key);
}

var _mapCacheHas = mapCacheHas;

/**
 * Sets the map `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf MapCache
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the map cache instance.
 */
function mapCacheSet(key, value) {
  var data = _getMapData(this, key),
      size = data.size;

  data.set(key, value);
  this.size += data.size == size ? 0 : 1;
  return this;
}

var _mapCacheSet = mapCacheSet;

/**
 * Creates a map cache object to store key-value pairs.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function MapCache(entries) {
  var index = -1,
      length = entries == null ? 0 : entries.length;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

// Add methods to `MapCache`.
MapCache.prototype.clear = _mapCacheClear;
MapCache.prototype['delete'] = _mapCacheDelete;
MapCache.prototype.get = _mapCacheGet;
MapCache.prototype.has = _mapCacheHas;
MapCache.prototype.set = _mapCacheSet;

var _MapCache = MapCache;

/** Error message constants. */
var FUNC_ERROR_TEXT = 'Expected a function';

/**
 * Creates a function that memoizes the result of `func`. If `resolver` is
 * provided, it determines the cache key for storing the result based on the
 * arguments provided to the memoized function. By default, the first argument
 * provided to the memoized function is used as the map cache key. The `func`
 * is invoked with the `this` binding of the memoized function.
 *
 * **Note:** The cache is exposed as the `cache` property on the memoized
 * function. Its creation may be customized by replacing the `_.memoize.Cache`
 * constructor with one whose instances implement the
 * [`Map`](http://ecma-international.org/ecma-262/7.0/#sec-properties-of-the-map-prototype-object)
 * method interface of `clear`, `delete`, `get`, `has`, and `set`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to have its output memoized.
 * @param {Function} [resolver] The function to resolve the cache key.
 * @returns {Function} Returns the new memoized function.
 * @example
 *
 * var object = { 'a': 1, 'b': 2 };
 * var other = { 'c': 3, 'd': 4 };
 *
 * var values = _.memoize(_.values);
 * values(object);
 * // => [1, 2]
 *
 * values(other);
 * // => [3, 4]
 *
 * object.a = 2;
 * values(object);
 * // => [1, 2]
 *
 * // Modify the result cache.
 * values.cache.set(object, ['a', 'b']);
 * values(object);
 * // => ['a', 'b']
 *
 * // Replace `_.memoize.Cache`.
 * _.memoize.Cache = WeakMap;
 */
function memoize(func, resolver) {
  if (typeof func != 'function' || (resolver != null && typeof resolver != 'function')) {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  var memoized = function() {
    var args = arguments,
        key = resolver ? resolver.apply(this, args) : args[0],
        cache = memoized.cache;

    if (cache.has(key)) {
      return cache.get(key);
    }
    var result = func.apply(this, args);
    memoized.cache = cache.set(key, result) || cache;
    return result;
  };
  memoized.cache = new (memoize.Cache || _MapCache);
  return memoized;
}

// Expose `MapCache`.
memoize.Cache = _MapCache;

var memoize_1 = memoize;

/** Used as the maximum memoize cache size. */
var MAX_MEMOIZE_SIZE = 500;

/**
 * A specialized version of `_.memoize` which clears the memoized function's
 * cache when it exceeds `MAX_MEMOIZE_SIZE`.
 *
 * @private
 * @param {Function} func The function to have its output memoized.
 * @returns {Function} Returns the new memoized function.
 */
function memoizeCapped(func) {
  var result = memoize_1(func, function(key) {
    if (cache.size === MAX_MEMOIZE_SIZE) {
      cache.clear();
    }
    return key;
  });

  var cache = result.cache;
  return result;
}

var _memoizeCapped = memoizeCapped;

/** Used to match property names within property paths. */
var rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g;

/** Used to match backslashes in property paths. */
var reEscapeChar = /\\(\\)?/g;

/**
 * Converts `string` to a property path array.
 *
 * @private
 * @param {string} string The string to convert.
 * @returns {Array} Returns the property path array.
 */
var stringToPath = _memoizeCapped(function(string) {
  var result = [];
  if (string.charCodeAt(0) === 46 /* . */) {
    result.push('');
  }
  string.replace(rePropName, function(match, number, quote, subString) {
    result.push(quote ? subString.replace(reEscapeChar, '$1') : (number || match));
  });
  return result;
});

var _stringToPath = stringToPath;

/**
 * A specialized version of `_.map` for arrays without support for iteratee
 * shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the new mapped array.
 */
function arrayMap(array, iteratee) {
  var index = -1,
      length = array == null ? 0 : array.length,
      result = Array(length);

  while (++index < length) {
    result[index] = iteratee(array[index], index, array);
  }
  return result;
}

var _arrayMap = arrayMap;

/** Used as references for various `Number` constants. */
var INFINITY = 1 / 0;

/** Used to convert symbols to primitives and strings. */
var symbolProto = _Symbol ? _Symbol.prototype : undefined,
    symbolToString = symbolProto ? symbolProto.toString : undefined;

/**
 * The base implementation of `_.toString` which doesn't convert nullish
 * values to empty strings.
 *
 * @private
 * @param {*} value The value to process.
 * @returns {string} Returns the string.
 */
function baseToString(value) {
  // Exit early for strings to avoid a performance hit in some environments.
  if (typeof value == 'string') {
    return value;
  }
  if (isArray_1(value)) {
    // Recursively convert values (susceptible to call stack limits).
    return _arrayMap(value, baseToString) + '';
  }
  if (isSymbol_1(value)) {
    return symbolToString ? symbolToString.call(value) : '';
  }
  var result = (value + '');
  return (result == '0' && (1 / value) == -INFINITY) ? '-0' : result;
}

var _baseToString = baseToString;

/**
 * Converts `value` to a string. An empty string is returned for `null`
 * and `undefined` values. The sign of `-0` is preserved.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 * @example
 *
 * _.toString(null);
 * // => ''
 *
 * _.toString(-0);
 * // => '-0'
 *
 * _.toString([1, 2, 3]);
 * // => '1,2,3'
 */
function toString$2(value) {
  return value == null ? '' : _baseToString(value);
}

var toString_1 = toString$2;

/**
 * Casts `value` to a path array if it's not one.
 *
 * @private
 * @param {*} value The value to inspect.
 * @param {Object} [object] The object to query keys on.
 * @returns {Array} Returns the cast property path array.
 */
function castPath(value, object) {
  if (isArray_1(value)) {
    return value;
  }
  return _isKey(value, object) ? [value] : _stringToPath(toString_1(value));
}

var _castPath = castPath;

/** Used as references for various `Number` constants. */
var INFINITY$1 = 1 / 0;

/**
 * Converts `value` to a string key if it's not a string or symbol.
 *
 * @private
 * @param {*} value The value to inspect.
 * @returns {string|symbol} Returns the key.
 */
function toKey(value) {
  if (typeof value == 'string' || isSymbol_1(value)) {
    return value;
  }
  var result = (value + '');
  return (result == '0' && (1 / value) == -INFINITY$1) ? '-0' : result;
}

var _toKey = toKey;

/**
 * The base implementation of `_.get` without support for default values.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Array|string} path The path of the property to get.
 * @returns {*} Returns the resolved value.
 */
function baseGet(object, path) {
  path = _castPath(path, object);

  var index = 0,
      length = path.length;

  while (object != null && index < length) {
    object = object[_toKey(path[index++])];
  }
  return (index && index == length) ? object : undefined;
}

var _baseGet = baseGet;

/**
 * Gets the value at `path` of `object`. If the resolved value is
 * `undefined`, the `defaultValue` is returned in its place.
 *
 * @static
 * @memberOf _
 * @since 3.7.0
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path of the property to get.
 * @param {*} [defaultValue] The value returned for `undefined` resolved values.
 * @returns {*} Returns the resolved value.
 * @example
 *
 * var object = { 'a': [{ 'b': { 'c': 3 } }] };
 *
 * _.get(object, 'a[0].b.c');
 * // => 3
 *
 * _.get(object, ['a', '0', 'b', 'c']);
 * // => 3
 *
 * _.get(object, 'a.b.c', 'default');
 * // => 'default'
 */
function get(object, path, defaultValue) {
  var result = object == null ? undefined : _baseGet(object, path);
  return result === undefined ? defaultValue : result;
}

var get_1 = get;

/**
 * Removes all key-value entries from the stack.
 *
 * @private
 * @name clear
 * @memberOf Stack
 */
function stackClear() {
  this.__data__ = new _ListCache;
  this.size = 0;
}

var _stackClear = stackClear;

/**
 * Removes `key` and its value from the stack.
 *
 * @private
 * @name delete
 * @memberOf Stack
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function stackDelete(key) {
  var data = this.__data__,
      result = data['delete'](key);

  this.size = data.size;
  return result;
}

var _stackDelete = stackDelete;

/**
 * Gets the stack value for `key`.
 *
 * @private
 * @name get
 * @memberOf Stack
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function stackGet(key) {
  return this.__data__.get(key);
}

var _stackGet = stackGet;

/**
 * Checks if a stack value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf Stack
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function stackHas(key) {
  return this.__data__.has(key);
}

var _stackHas = stackHas;

/** Used as the size to enable large array optimizations. */
var LARGE_ARRAY_SIZE = 200;

/**
 * Sets the stack `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf Stack
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the stack cache instance.
 */
function stackSet(key, value) {
  var data = this.__data__;
  if (data instanceof _ListCache) {
    var pairs = data.__data__;
    if (!_Map || (pairs.length < LARGE_ARRAY_SIZE - 1)) {
      pairs.push([key, value]);
      this.size = ++data.size;
      return this;
    }
    data = this.__data__ = new _MapCache(pairs);
  }
  data.set(key, value);
  this.size = data.size;
  return this;
}

var _stackSet = stackSet;

/**
 * Creates a stack cache object to store key-value pairs.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function Stack(entries) {
  var data = this.__data__ = new _ListCache(entries);
  this.size = data.size;
}

// Add methods to `Stack`.
Stack.prototype.clear = _stackClear;
Stack.prototype['delete'] = _stackDelete;
Stack.prototype.get = _stackGet;
Stack.prototype.has = _stackHas;
Stack.prototype.set = _stackSet;

var _Stack = Stack;

/** Used to stand-in for `undefined` hash values. */
var HASH_UNDEFINED$2 = '__lodash_hash_undefined__';

/**
 * Adds `value` to the array cache.
 *
 * @private
 * @name add
 * @memberOf SetCache
 * @alias push
 * @param {*} value The value to cache.
 * @returns {Object} Returns the cache instance.
 */
function setCacheAdd(value) {
  this.__data__.set(value, HASH_UNDEFINED$2);
  return this;
}

var _setCacheAdd = setCacheAdd;

/**
 * Checks if `value` is in the array cache.
 *
 * @private
 * @name has
 * @memberOf SetCache
 * @param {*} value The value to search for.
 * @returns {number} Returns `true` if `value` is found, else `false`.
 */
function setCacheHas(value) {
  return this.__data__.has(value);
}

var _setCacheHas = setCacheHas;

/**
 *
 * Creates an array cache object to store unique values.
 *
 * @private
 * @constructor
 * @param {Array} [values] The values to cache.
 */
function SetCache(values) {
  var index = -1,
      length = values == null ? 0 : values.length;

  this.__data__ = new _MapCache;
  while (++index < length) {
    this.add(values[index]);
  }
}

// Add methods to `SetCache`.
SetCache.prototype.add = SetCache.prototype.push = _setCacheAdd;
SetCache.prototype.has = _setCacheHas;

var _SetCache = SetCache;

/**
 * A specialized version of `_.some` for arrays without support for iteratee
 * shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {boolean} Returns `true` if any element passes the predicate check,
 *  else `false`.
 */
function arraySome(array, predicate) {
  var index = -1,
      length = array == null ? 0 : array.length;

  while (++index < length) {
    if (predicate(array[index], index, array)) {
      return true;
    }
  }
  return false;
}

var _arraySome = arraySome;

/**
 * Checks if a `cache` value for `key` exists.
 *
 * @private
 * @param {Object} cache The cache to query.
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function cacheHas(cache, key) {
  return cache.has(key);
}

var _cacheHas = cacheHas;

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;

/**
 * A specialized version of `baseIsEqualDeep` for arrays with support for
 * partial deep comparisons.
 *
 * @private
 * @param {Array} array The array to compare.
 * @param {Array} other The other array to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `array` and `other` objects.
 * @returns {boolean} Returns `true` if the arrays are equivalent, else `false`.
 */
function equalArrays(array, other, bitmask, customizer, equalFunc, stack) {
  var isPartial = bitmask & COMPARE_PARTIAL_FLAG,
      arrLength = array.length,
      othLength = other.length;

  if (arrLength != othLength && !(isPartial && othLength > arrLength)) {
    return false;
  }
  // Assume cyclic values are equal.
  var stacked = stack.get(array);
  if (stacked && stack.get(other)) {
    return stacked == other;
  }
  var index = -1,
      result = true,
      seen = (bitmask & COMPARE_UNORDERED_FLAG) ? new _SetCache : undefined;

  stack.set(array, other);
  stack.set(other, array);

  // Ignore non-index properties.
  while (++index < arrLength) {
    var arrValue = array[index],
        othValue = other[index];

    if (customizer) {
      var compared = isPartial
        ? customizer(othValue, arrValue, index, other, array, stack)
        : customizer(arrValue, othValue, index, array, other, stack);
    }
    if (compared !== undefined) {
      if (compared) {
        continue;
      }
      result = false;
      break;
    }
    // Recursively compare arrays (susceptible to call stack limits).
    if (seen) {
      if (!_arraySome(other, function(othValue, othIndex) {
            if (!_cacheHas(seen, othIndex) &&
                (arrValue === othValue || equalFunc(arrValue, othValue, bitmask, customizer, stack))) {
              return seen.push(othIndex);
            }
          })) {
        result = false;
        break;
      }
    } else if (!(
          arrValue === othValue ||
            equalFunc(arrValue, othValue, bitmask, customizer, stack)
        )) {
      result = false;
      break;
    }
  }
  stack['delete'](array);
  stack['delete'](other);
  return result;
}

var _equalArrays = equalArrays;

/** Built-in value references. */
var Uint8Array = _root.Uint8Array;

var _Uint8Array = Uint8Array;

/**
 * Converts `map` to its key-value pairs.
 *
 * @private
 * @param {Object} map The map to convert.
 * @returns {Array} Returns the key-value pairs.
 */
function mapToArray(map) {
  var index = -1,
      result = Array(map.size);

  map.forEach(function(value, key) {
    result[++index] = [key, value];
  });
  return result;
}

var _mapToArray = mapToArray;

/**
 * Converts `set` to an array of its values.
 *
 * @private
 * @param {Object} set The set to convert.
 * @returns {Array} Returns the values.
 */
function setToArray(set) {
  var index = -1,
      result = Array(set.size);

  set.forEach(function(value) {
    result[++index] = value;
  });
  return result;
}

var _setToArray = setToArray;

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG$1 = 1,
    COMPARE_UNORDERED_FLAG$1 = 2;

/** `Object#toString` result references. */
var boolTag = '[object Boolean]',
    dateTag = '[object Date]',
    errorTag = '[object Error]',
    mapTag = '[object Map]',
    numberTag$1 = '[object Number]',
    regexpTag = '[object RegExp]',
    setTag = '[object Set]',
    stringTag = '[object String]',
    symbolTag$1 = '[object Symbol]';

var arrayBufferTag = '[object ArrayBuffer]',
    dataViewTag = '[object DataView]';

/** Used to convert symbols to primitives and strings. */
var symbolProto$1 = _Symbol ? _Symbol.prototype : undefined,
    symbolValueOf = symbolProto$1 ? symbolProto$1.valueOf : undefined;

/**
 * A specialized version of `baseIsEqualDeep` for comparing objects of
 * the same `toStringTag`.
 *
 * **Note:** This function only supports comparing values with tags of
 * `Boolean`, `Date`, `Error`, `Number`, `RegExp`, or `String`.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {string} tag The `toStringTag` of the objects to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function equalByTag(object, other, tag, bitmask, customizer, equalFunc, stack) {
  switch (tag) {
    case dataViewTag:
      if ((object.byteLength != other.byteLength) ||
          (object.byteOffset != other.byteOffset)) {
        return false;
      }
      object = object.buffer;
      other = other.buffer;

    case arrayBufferTag:
      if ((object.byteLength != other.byteLength) ||
          !equalFunc(new _Uint8Array(object), new _Uint8Array(other))) {
        return false;
      }
      return true;

    case boolTag:
    case dateTag:
    case numberTag$1:
      // Coerce booleans to `1` or `0` and dates to milliseconds.
      // Invalid dates are coerced to `NaN`.
      return eq_1(+object, +other);

    case errorTag:
      return object.name == other.name && object.message == other.message;

    case regexpTag:
    case stringTag:
      // Coerce regexes to strings and treat strings, primitives and objects,
      // as equal. See http://www.ecma-international.org/ecma-262/7.0/#sec-regexp.prototype.tostring
      // for more details.
      return object == (other + '');

    case mapTag:
      var convert = _mapToArray;

    case setTag:
      var isPartial = bitmask & COMPARE_PARTIAL_FLAG$1;
      convert || (convert = _setToArray);

      if (object.size != other.size && !isPartial) {
        return false;
      }
      // Assume cyclic values are equal.
      var stacked = stack.get(object);
      if (stacked) {
        return stacked == other;
      }
      bitmask |= COMPARE_UNORDERED_FLAG$1;

      // Recursively compare objects (susceptible to call stack limits).
      stack.set(object, other);
      var result = _equalArrays(convert(object), convert(other), bitmask, customizer, equalFunc, stack);
      stack['delete'](object);
      return result;

    case symbolTag$1:
      if (symbolValueOf) {
        return symbolValueOf.call(object) == symbolValueOf.call(other);
      }
  }
  return false;
}

var _equalByTag = equalByTag;

/**
 * Appends the elements of `values` to `array`.
 *
 * @private
 * @param {Array} array The array to modify.
 * @param {Array} values The values to append.
 * @returns {Array} Returns `array`.
 */
function arrayPush(array, values) {
  var index = -1,
      length = values.length,
      offset = array.length;

  while (++index < length) {
    array[offset + index] = values[index];
  }
  return array;
}

var _arrayPush = arrayPush;

/**
 * The base implementation of `getAllKeys` and `getAllKeysIn` which uses
 * `keysFunc` and `symbolsFunc` to get the enumerable property names and
 * symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Function} keysFunc The function to get the keys of `object`.
 * @param {Function} symbolsFunc The function to get the symbols of `object`.
 * @returns {Array} Returns the array of property names and symbols.
 */
function baseGetAllKeys(object, keysFunc, symbolsFunc) {
  var result = keysFunc(object);
  return isArray_1(object) ? result : _arrayPush(result, symbolsFunc(object));
}

var _baseGetAllKeys = baseGetAllKeys;

/**
 * A specialized version of `_.filter` for arrays without support for
 * iteratee shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {Array} Returns the new filtered array.
 */
function arrayFilter(array, predicate) {
  var index = -1,
      length = array == null ? 0 : array.length,
      resIndex = 0,
      result = [];

  while (++index < length) {
    var value = array[index];
    if (predicate(value, index, array)) {
      result[resIndex++] = value;
    }
  }
  return result;
}

var _arrayFilter = arrayFilter;

/**
 * This method returns a new empty array.
 *
 * @static
 * @memberOf _
 * @since 4.13.0
 * @category Util
 * @returns {Array} Returns the new empty array.
 * @example
 *
 * var arrays = _.times(2, _.stubArray);
 *
 * console.log(arrays);
 * // => [[], []]
 *
 * console.log(arrays[0] === arrays[1]);
 * // => false
 */
function stubArray() {
  return [];
}

var stubArray_1 = stubArray;

/** Used for built-in method references. */
var objectProto$5 = Object.prototype;

/** Built-in value references. */
var propertyIsEnumerable = objectProto$5.propertyIsEnumerable;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeGetSymbols = Object.getOwnPropertySymbols;

/**
 * Creates an array of the own enumerable symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of symbols.
 */
var getSymbols = !nativeGetSymbols ? stubArray_1 : function(object) {
  if (object == null) {
    return [];
  }
  object = Object(object);
  return _arrayFilter(nativeGetSymbols(object), function(symbol) {
    return propertyIsEnumerable.call(object, symbol);
  });
};

var _getSymbols = getSymbols;

/**
 * The base implementation of `_.times` without support for iteratee shorthands
 * or max array length checks.
 *
 * @private
 * @param {number} n The number of times to invoke `iteratee`.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the array of results.
 */
function baseTimes(n, iteratee) {
  var index = -1,
      result = Array(n);

  while (++index < n) {
    result[index] = iteratee(index);
  }
  return result;
}

var _baseTimes = baseTimes;

/** `Object#toString` result references. */
var argsTag = '[object Arguments]';

/**
 * The base implementation of `_.isArguments`.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 */
function baseIsArguments(value) {
  return isObjectLike_1(value) && _baseGetTag(value) == argsTag;
}

var _baseIsArguments = baseIsArguments;

/** Used for built-in method references. */
var objectProto$6 = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty$4 = objectProto$6.hasOwnProperty;

/** Built-in value references. */
var propertyIsEnumerable$1 = objectProto$6.propertyIsEnumerable;

/**
 * Checks if `value` is likely an `arguments` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 *  else `false`.
 * @example
 *
 * _.isArguments(function() { return arguments; }());
 * // => true
 *
 * _.isArguments([1, 2, 3]);
 * // => false
 */
var isArguments = _baseIsArguments(function() { return arguments; }()) ? _baseIsArguments : function(value) {
  return isObjectLike_1(value) && hasOwnProperty$4.call(value, 'callee') &&
    !propertyIsEnumerable$1.call(value, 'callee');
};

var isArguments_1 = isArguments;

/**
 * This method returns `false`.
 *
 * @static
 * @memberOf _
 * @since 4.13.0
 * @category Util
 * @returns {boolean} Returns `false`.
 * @example
 *
 * _.times(2, _.stubFalse);
 * // => [false, false]
 */
function stubFalse() {
  return false;
}

var stubFalse_1 = stubFalse;

var isBuffer_1 = createCommonjsModule(function (module, exports) {
/** Detect free variable `exports`. */
var freeExports = exports && !exports.nodeType && exports;

/** Detect free variable `module`. */
var freeModule = freeExports && 'object' == 'object' && module && !module.nodeType && module;

/** Detect the popular CommonJS extension `module.exports`. */
var moduleExports = freeModule && freeModule.exports === freeExports;

/** Built-in value references. */
var Buffer = moduleExports ? _root.Buffer : undefined;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeIsBuffer = Buffer ? Buffer.isBuffer : undefined;

/**
 * Checks if `value` is a buffer.
 *
 * @static
 * @memberOf _
 * @since 4.3.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a buffer, else `false`.
 * @example
 *
 * _.isBuffer(new Buffer(2));
 * // => true
 *
 * _.isBuffer(new Uint8Array(2));
 * // => false
 */
var isBuffer = nativeIsBuffer || stubFalse_1;

module.exports = isBuffer;
});

/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER = 9007199254740991;

/** Used to detect unsigned integer values. */
var reIsUint = /^(?:0|[1-9]\d*)$/;

/**
 * Checks if `value` is a valid array-like index.
 *
 * @private
 * @param {*} value The value to check.
 * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
 * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
 */
function isIndex(value, length) {
  var type = typeof value;
  length = length == null ? MAX_SAFE_INTEGER : length;

  return !!length &&
    (type == 'number' ||
      (type != 'symbol' && reIsUint.test(value))) &&
        (value > -1 && value % 1 == 0 && value < length);
}

var _isIndex = isIndex;

/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER$1 = 9007199254740991;

/**
 * Checks if `value` is a valid array-like length.
 *
 * **Note:** This method is loosely based on
 * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
 * @example
 *
 * _.isLength(3);
 * // => true
 *
 * _.isLength(Number.MIN_VALUE);
 * // => false
 *
 * _.isLength(Infinity);
 * // => false
 *
 * _.isLength('3');
 * // => false
 */
function isLength(value) {
  return typeof value == 'number' &&
    value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER$1;
}

var isLength_1 = isLength;

/** `Object#toString` result references. */
var argsTag$1 = '[object Arguments]',
    arrayTag = '[object Array]',
    boolTag$1 = '[object Boolean]',
    dateTag$1 = '[object Date]',
    errorTag$1 = '[object Error]',
    funcTag$1 = '[object Function]',
    mapTag$1 = '[object Map]',
    numberTag$2 = '[object Number]',
    objectTag = '[object Object]',
    regexpTag$1 = '[object RegExp]',
    setTag$1 = '[object Set]',
    stringTag$1 = '[object String]',
    weakMapTag = '[object WeakMap]';

var arrayBufferTag$1 = '[object ArrayBuffer]',
    dataViewTag$1 = '[object DataView]',
    float32Tag = '[object Float32Array]',
    float64Tag = '[object Float64Array]',
    int8Tag = '[object Int8Array]',
    int16Tag = '[object Int16Array]',
    int32Tag = '[object Int32Array]',
    uint8Tag = '[object Uint8Array]',
    uint8ClampedTag = '[object Uint8ClampedArray]',
    uint16Tag = '[object Uint16Array]',
    uint32Tag = '[object Uint32Array]';

/** Used to identify `toStringTag` values of typed arrays. */
var typedArrayTags = {};
typedArrayTags[float32Tag] = typedArrayTags[float64Tag] =
typedArrayTags[int8Tag] = typedArrayTags[int16Tag] =
typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] =
typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] =
typedArrayTags[uint32Tag] = true;
typedArrayTags[argsTag$1] = typedArrayTags[arrayTag] =
typedArrayTags[arrayBufferTag$1] = typedArrayTags[boolTag$1] =
typedArrayTags[dataViewTag$1] = typedArrayTags[dateTag$1] =
typedArrayTags[errorTag$1] = typedArrayTags[funcTag$1] =
typedArrayTags[mapTag$1] = typedArrayTags[numberTag$2] =
typedArrayTags[objectTag] = typedArrayTags[regexpTag$1] =
typedArrayTags[setTag$1] = typedArrayTags[stringTag$1] =
typedArrayTags[weakMapTag] = false;

/**
 * The base implementation of `_.isTypedArray` without Node.js optimizations.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 */
function baseIsTypedArray(value) {
  return isObjectLike_1(value) &&
    isLength_1(value.length) && !!typedArrayTags[_baseGetTag(value)];
}

var _baseIsTypedArray = baseIsTypedArray;

/**
 * The base implementation of `_.unary` without support for storing metadata.
 *
 * @private
 * @param {Function} func The function to cap arguments for.
 * @returns {Function} Returns the new capped function.
 */
function baseUnary(func) {
  return function(value) {
    return func(value);
  };
}

var _baseUnary = baseUnary;

var _nodeUtil = createCommonjsModule(function (module, exports) {
/** Detect free variable `exports`. */
var freeExports = exports && !exports.nodeType && exports;

/** Detect free variable `module`. */
var freeModule = freeExports && 'object' == 'object' && module && !module.nodeType && module;

/** Detect the popular CommonJS extension `module.exports`. */
var moduleExports = freeModule && freeModule.exports === freeExports;

/** Detect free variable `process` from Node.js. */
var freeProcess = moduleExports && _freeGlobal.process;

/** Used to access faster Node.js helpers. */
var nodeUtil = (function() {
  try {
    // Use `util.types` for Node.js 10+.
    var types = freeModule && freeModule.require && freeModule.require('util').types;

    if (types) {
      return types;
    }

    // Legacy `process.binding('util')` for Node.js < 10.
    return freeProcess && freeProcess.binding && freeProcess.binding('util');
  } catch (e) {}
}());

module.exports = nodeUtil;
});

/* Node.js helper references. */
var nodeIsTypedArray = _nodeUtil && _nodeUtil.isTypedArray;

/**
 * Checks if `value` is classified as a typed array.
 *
 * @static
 * @memberOf _
 * @since 3.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 * @example
 *
 * _.isTypedArray(new Uint8Array);
 * // => true
 *
 * _.isTypedArray([]);
 * // => false
 */
var isTypedArray = nodeIsTypedArray ? _baseUnary(nodeIsTypedArray) : _baseIsTypedArray;

var isTypedArray_1 = isTypedArray;

/** Used for built-in method references. */
var objectProto$7 = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty$5 = objectProto$7.hasOwnProperty;

/**
 * Creates an array of the enumerable property names of the array-like `value`.
 *
 * @private
 * @param {*} value The value to query.
 * @param {boolean} inherited Specify returning inherited property names.
 * @returns {Array} Returns the array of property names.
 */
function arrayLikeKeys(value, inherited) {
  var isArr = isArray_1(value),
      isArg = !isArr && isArguments_1(value),
      isBuff = !isArr && !isArg && isBuffer_1(value),
      isType = !isArr && !isArg && !isBuff && isTypedArray_1(value),
      skipIndexes = isArr || isArg || isBuff || isType,
      result = skipIndexes ? _baseTimes(value.length, String) : [],
      length = result.length;

  for (var key in value) {
    if ((inherited || hasOwnProperty$5.call(value, key)) &&
        !(skipIndexes && (
           // Safari 9 has enumerable `arguments.length` in strict mode.
           key == 'length' ||
           // Node.js 0.10 has enumerable non-index properties on buffers.
           (isBuff && (key == 'offset' || key == 'parent')) ||
           // PhantomJS 2 has enumerable non-index properties on typed arrays.
           (isType && (key == 'buffer' || key == 'byteLength' || key == 'byteOffset')) ||
           // Skip index properties.
           _isIndex(key, length)
        ))) {
      result.push(key);
    }
  }
  return result;
}

var _arrayLikeKeys = arrayLikeKeys;

/** Used for built-in method references. */
var objectProto$8 = Object.prototype;

/**
 * Checks if `value` is likely a prototype object.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a prototype, else `false`.
 */
function isPrototype(value) {
  var Ctor = value && value.constructor,
      proto = (typeof Ctor == 'function' && Ctor.prototype) || objectProto$8;

  return value === proto;
}

var _isPrototype = isPrototype;

/**
 * Creates a unary function that invokes `func` with its argument transformed.
 *
 * @private
 * @param {Function} func The function to wrap.
 * @param {Function} transform The argument transform.
 * @returns {Function} Returns the new function.
 */
function overArg(func, transform) {
  return function(arg) {
    return func(transform(arg));
  };
}

var _overArg = overArg;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeKeys = _overArg(Object.keys, Object);

var _nativeKeys = nativeKeys;

/** Used for built-in method references. */
var objectProto$9 = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty$6 = objectProto$9.hasOwnProperty;

/**
 * The base implementation of `_.keys` which doesn't treat sparse arrays as dense.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 */
function baseKeys(object) {
  if (!_isPrototype(object)) {
    return _nativeKeys(object);
  }
  var result = [];
  for (var key in Object(object)) {
    if (hasOwnProperty$6.call(object, key) && key != 'constructor') {
      result.push(key);
    }
  }
  return result;
}

var _baseKeys = baseKeys;

/**
 * Checks if `value` is array-like. A value is considered array-like if it's
 * not a function and has a `value.length` that's an integer greater than or
 * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
 * @example
 *
 * _.isArrayLike([1, 2, 3]);
 * // => true
 *
 * _.isArrayLike(document.body.children);
 * // => true
 *
 * _.isArrayLike('abc');
 * // => true
 *
 * _.isArrayLike(_.noop);
 * // => false
 */
function isArrayLike(value) {
  return value != null && isLength_1(value.length) && !isFunction_1(value);
}

var isArrayLike_1 = isArrayLike;

/**
 * Creates an array of the own enumerable property names of `object`.
 *
 * **Note:** Non-object values are coerced to objects. See the
 * [ES spec](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
 * for more details.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.keys(new Foo);
 * // => ['a', 'b'] (iteration order is not guaranteed)
 *
 * _.keys('hi');
 * // => ['0', '1']
 */
function keys(object) {
  return isArrayLike_1(object) ? _arrayLikeKeys(object) : _baseKeys(object);
}

var keys_1 = keys;

/**
 * Creates an array of own enumerable property names and symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names and symbols.
 */
function getAllKeys(object) {
  return _baseGetAllKeys(object, keys_1, _getSymbols);
}

var _getAllKeys = getAllKeys;

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG$2 = 1;

/** Used for built-in method references. */
var objectProto$a = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty$7 = objectProto$a.hasOwnProperty;

/**
 * A specialized version of `baseIsEqualDeep` for objects with support for
 * partial deep comparisons.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function equalObjects(object, other, bitmask, customizer, equalFunc, stack) {
  var isPartial = bitmask & COMPARE_PARTIAL_FLAG$2,
      objProps = _getAllKeys(object),
      objLength = objProps.length,
      othProps = _getAllKeys(other),
      othLength = othProps.length;

  if (objLength != othLength && !isPartial) {
    return false;
  }
  var index = objLength;
  while (index--) {
    var key = objProps[index];
    if (!(isPartial ? key in other : hasOwnProperty$7.call(other, key))) {
      return false;
    }
  }
  // Assume cyclic values are equal.
  var stacked = stack.get(object);
  if (stacked && stack.get(other)) {
    return stacked == other;
  }
  var result = true;
  stack.set(object, other);
  stack.set(other, object);

  var skipCtor = isPartial;
  while (++index < objLength) {
    key = objProps[index];
    var objValue = object[key],
        othValue = other[key];

    if (customizer) {
      var compared = isPartial
        ? customizer(othValue, objValue, key, other, object, stack)
        : customizer(objValue, othValue, key, object, other, stack);
    }
    // Recursively compare objects (susceptible to call stack limits).
    if (!(compared === undefined
          ? (objValue === othValue || equalFunc(objValue, othValue, bitmask, customizer, stack))
          : compared
        )) {
      result = false;
      break;
    }
    skipCtor || (skipCtor = key == 'constructor');
  }
  if (result && !skipCtor) {
    var objCtor = object.constructor,
        othCtor = other.constructor;

    // Non `Object` object instances with different constructors are not equal.
    if (objCtor != othCtor &&
        ('constructor' in object && 'constructor' in other) &&
        !(typeof objCtor == 'function' && objCtor instanceof objCtor &&
          typeof othCtor == 'function' && othCtor instanceof othCtor)) {
      result = false;
    }
  }
  stack['delete'](object);
  stack['delete'](other);
  return result;
}

var _equalObjects = equalObjects;

/* Built-in method references that are verified to be native. */
var DataView = _getNative(_root, 'DataView');

var _DataView = DataView;

/* Built-in method references that are verified to be native. */
var Promise$1 = _getNative(_root, 'Promise');

var _Promise = Promise$1;

/* Built-in method references that are verified to be native. */
var Set = _getNative(_root, 'Set');

var _Set = Set;

/* Built-in method references that are verified to be native. */
var WeakMap = _getNative(_root, 'WeakMap');

var _WeakMap = WeakMap;

/** `Object#toString` result references. */
var mapTag$2 = '[object Map]',
    objectTag$1 = '[object Object]',
    promiseTag = '[object Promise]',
    setTag$2 = '[object Set]',
    weakMapTag$1 = '[object WeakMap]';

var dataViewTag$2 = '[object DataView]';

/** Used to detect maps, sets, and weakmaps. */
var dataViewCtorString = _toSource(_DataView),
    mapCtorString = _toSource(_Map),
    promiseCtorString = _toSource(_Promise),
    setCtorString = _toSource(_Set),
    weakMapCtorString = _toSource(_WeakMap);

/**
 * Gets the `toStringTag` of `value`.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
var getTag = _baseGetTag;

// Fallback for data views, maps, sets, and weak maps in IE 11 and promises in Node.js < 6.
if ((_DataView && getTag(new _DataView(new ArrayBuffer(1))) != dataViewTag$2) ||
    (_Map && getTag(new _Map) != mapTag$2) ||
    (_Promise && getTag(_Promise.resolve()) != promiseTag) ||
    (_Set && getTag(new _Set) != setTag$2) ||
    (_WeakMap && getTag(new _WeakMap) != weakMapTag$1)) {
  getTag = function(value) {
    var result = _baseGetTag(value),
        Ctor = result == objectTag$1 ? value.constructor : undefined,
        ctorString = Ctor ? _toSource(Ctor) : '';

    if (ctorString) {
      switch (ctorString) {
        case dataViewCtorString: return dataViewTag$2;
        case mapCtorString: return mapTag$2;
        case promiseCtorString: return promiseTag;
        case setCtorString: return setTag$2;
        case weakMapCtorString: return weakMapTag$1;
      }
    }
    return result;
  };
}

var _getTag = getTag;

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG$3 = 1;

/** `Object#toString` result references. */
var argsTag$2 = '[object Arguments]',
    arrayTag$1 = '[object Array]',
    objectTag$2 = '[object Object]';

/** Used for built-in method references. */
var objectProto$b = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty$8 = objectProto$b.hasOwnProperty;

/**
 * A specialized version of `baseIsEqual` for arrays and objects which performs
 * deep comparisons and tracks traversed objects enabling objects with circular
 * references to be compared.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} [stack] Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function baseIsEqualDeep(object, other, bitmask, customizer, equalFunc, stack) {
  var objIsArr = isArray_1(object),
      othIsArr = isArray_1(other),
      objTag = objIsArr ? arrayTag$1 : _getTag(object),
      othTag = othIsArr ? arrayTag$1 : _getTag(other);

  objTag = objTag == argsTag$2 ? objectTag$2 : objTag;
  othTag = othTag == argsTag$2 ? objectTag$2 : othTag;

  var objIsObj = objTag == objectTag$2,
      othIsObj = othTag == objectTag$2,
      isSameTag = objTag == othTag;

  if (isSameTag && isBuffer_1(object)) {
    if (!isBuffer_1(other)) {
      return false;
    }
    objIsArr = true;
    objIsObj = false;
  }
  if (isSameTag && !objIsObj) {
    stack || (stack = new _Stack);
    return (objIsArr || isTypedArray_1(object))
      ? _equalArrays(object, other, bitmask, customizer, equalFunc, stack)
      : _equalByTag(object, other, objTag, bitmask, customizer, equalFunc, stack);
  }
  if (!(bitmask & COMPARE_PARTIAL_FLAG$3)) {
    var objIsWrapped = objIsObj && hasOwnProperty$8.call(object, '__wrapped__'),
        othIsWrapped = othIsObj && hasOwnProperty$8.call(other, '__wrapped__');

    if (objIsWrapped || othIsWrapped) {
      var objUnwrapped = objIsWrapped ? object.value() : object,
          othUnwrapped = othIsWrapped ? other.value() : other;

      stack || (stack = new _Stack);
      return equalFunc(objUnwrapped, othUnwrapped, bitmask, customizer, stack);
    }
  }
  if (!isSameTag) {
    return false;
  }
  stack || (stack = new _Stack);
  return _equalObjects(object, other, bitmask, customizer, equalFunc, stack);
}

var _baseIsEqualDeep = baseIsEqualDeep;

/**
 * The base implementation of `_.isEqual` which supports partial comparisons
 * and tracks traversed objects.
 *
 * @private
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @param {boolean} bitmask The bitmask flags.
 *  1 - Unordered comparison
 *  2 - Partial comparison
 * @param {Function} [customizer] The function to customize comparisons.
 * @param {Object} [stack] Tracks traversed `value` and `other` objects.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 */
function baseIsEqual(value, other, bitmask, customizer, stack) {
  if (value === other) {
    return true;
  }
  if (value == null || other == null || (!isObjectLike_1(value) && !isObjectLike_1(other))) {
    return value !== value && other !== other;
  }
  return _baseIsEqualDeep(value, other, bitmask, customizer, baseIsEqual, stack);
}

var _baseIsEqual = baseIsEqual;

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG$4 = 1,
    COMPARE_UNORDERED_FLAG$2 = 2;

/**
 * The base implementation of `_.isMatch` without support for iteratee shorthands.
 *
 * @private
 * @param {Object} object The object to inspect.
 * @param {Object} source The object of property values to match.
 * @param {Array} matchData The property names, values, and compare flags to match.
 * @param {Function} [customizer] The function to customize comparisons.
 * @returns {boolean} Returns `true` if `object` is a match, else `false`.
 */
function baseIsMatch(object, source, matchData, customizer) {
  var index = matchData.length,
      length = index,
      noCustomizer = !customizer;

  if (object == null) {
    return !length;
  }
  object = Object(object);
  while (index--) {
    var data = matchData[index];
    if ((noCustomizer && data[2])
          ? data[1] !== object[data[0]]
          : !(data[0] in object)
        ) {
      return false;
    }
  }
  while (++index < length) {
    data = matchData[index];
    var key = data[0],
        objValue = object[key],
        srcValue = data[1];

    if (noCustomizer && data[2]) {
      if (objValue === undefined && !(key in object)) {
        return false;
      }
    } else {
      var stack = new _Stack;
      if (customizer) {
        var result = customizer(objValue, srcValue, key, object, source, stack);
      }
      if (!(result === undefined
            ? _baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG$4 | COMPARE_UNORDERED_FLAG$2, customizer, stack)
            : result
          )) {
        return false;
      }
    }
  }
  return true;
}

var _baseIsMatch = baseIsMatch;

/**
 * Checks if `value` is suitable for strict equality comparisons, i.e. `===`.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` if suitable for strict
 *  equality comparisons, else `false`.
 */
function isStrictComparable(value) {
  return value === value && !isObject_1(value);
}

var _isStrictComparable = isStrictComparable;

/**
 * Gets the property names, values, and compare flags of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the match data of `object`.
 */
function getMatchData(object) {
  var result = keys_1(object),
      length = result.length;

  while (length--) {
    var key = result[length],
        value = object[key];

    result[length] = [key, value, _isStrictComparable(value)];
  }
  return result;
}

var _getMatchData = getMatchData;

/**
 * A specialized version of `matchesProperty` for source values suitable
 * for strict equality comparisons, i.e. `===`.
 *
 * @private
 * @param {string} key The key of the property to get.
 * @param {*} srcValue The value to match.
 * @returns {Function} Returns the new spec function.
 */
function matchesStrictComparable(key, srcValue) {
  return function(object) {
    if (object == null) {
      return false;
    }
    return object[key] === srcValue &&
      (srcValue !== undefined || (key in Object(object)));
  };
}

var _matchesStrictComparable = matchesStrictComparable;

/**
 * The base implementation of `_.matches` which doesn't clone `source`.
 *
 * @private
 * @param {Object} source The object of property values to match.
 * @returns {Function} Returns the new spec function.
 */
function baseMatches(source) {
  var matchData = _getMatchData(source);
  if (matchData.length == 1 && matchData[0][2]) {
    return _matchesStrictComparable(matchData[0][0], matchData[0][1]);
  }
  return function(object) {
    return object === source || _baseIsMatch(object, source, matchData);
  };
}

var _baseMatches = baseMatches;

/**
 * The base implementation of `_.hasIn` without support for deep paths.
 *
 * @private
 * @param {Object} [object] The object to query.
 * @param {Array|string} key The key to check.
 * @returns {boolean} Returns `true` if `key` exists, else `false`.
 */
function baseHasIn(object, key) {
  return object != null && key in Object(object);
}

var _baseHasIn = baseHasIn;

/**
 * Checks if `path` exists on `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Array|string} path The path to check.
 * @param {Function} hasFunc The function to check properties.
 * @returns {boolean} Returns `true` if `path` exists, else `false`.
 */
function hasPath(object, path, hasFunc) {
  path = _castPath(path, object);

  var index = -1,
      length = path.length,
      result = false;

  while (++index < length) {
    var key = _toKey(path[index]);
    if (!(result = object != null && hasFunc(object, key))) {
      break;
    }
    object = object[key];
  }
  if (result || ++index != length) {
    return result;
  }
  length = object == null ? 0 : object.length;
  return !!length && isLength_1(length) && _isIndex(key, length) &&
    (isArray_1(object) || isArguments_1(object));
}

var _hasPath = hasPath;

/**
 * Checks if `path` is a direct or inherited property of `object`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path to check.
 * @returns {boolean} Returns `true` if `path` exists, else `false`.
 * @example
 *
 * var object = _.create({ 'a': _.create({ 'b': 2 }) });
 *
 * _.hasIn(object, 'a');
 * // => true
 *
 * _.hasIn(object, 'a.b');
 * // => true
 *
 * _.hasIn(object, ['a', 'b']);
 * // => true
 *
 * _.hasIn(object, 'b');
 * // => false
 */
function hasIn(object, path) {
  return object != null && _hasPath(object, path, _baseHasIn);
}

var hasIn_1 = hasIn;

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG$5 = 1,
    COMPARE_UNORDERED_FLAG$3 = 2;

/**
 * The base implementation of `_.matchesProperty` which doesn't clone `srcValue`.
 *
 * @private
 * @param {string} path The path of the property to get.
 * @param {*} srcValue The value to match.
 * @returns {Function} Returns the new spec function.
 */
function baseMatchesProperty(path, srcValue) {
  if (_isKey(path) && _isStrictComparable(srcValue)) {
    return _matchesStrictComparable(_toKey(path), srcValue);
  }
  return function(object) {
    var objValue = get_1(object, path);
    return (objValue === undefined && objValue === srcValue)
      ? hasIn_1(object, path)
      : _baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG$5 | COMPARE_UNORDERED_FLAG$3);
  };
}

var _baseMatchesProperty = baseMatchesProperty;

/**
 * This method returns the first argument it receives.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Util
 * @param {*} value Any value.
 * @returns {*} Returns `value`.
 * @example
 *
 * var object = { 'a': 1 };
 *
 * console.log(_.identity(object) === object);
 * // => true
 */
function identity(value) {
  return value;
}

var identity_1 = identity;

/**
 * The base implementation of `_.property` without support for deep paths.
 *
 * @private
 * @param {string} key The key of the property to get.
 * @returns {Function} Returns the new accessor function.
 */
function baseProperty(key) {
  return function(object) {
    return object == null ? undefined : object[key];
  };
}

var _baseProperty = baseProperty;

/**
 * A specialized version of `baseProperty` which supports deep paths.
 *
 * @private
 * @param {Array|string} path The path of the property to get.
 * @returns {Function} Returns the new accessor function.
 */
function basePropertyDeep(path) {
  return function(object) {
    return _baseGet(object, path);
  };
}

var _basePropertyDeep = basePropertyDeep;

/**
 * Creates a function that returns the value at `path` of a given object.
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Util
 * @param {Array|string} path The path of the property to get.
 * @returns {Function} Returns the new accessor function.
 * @example
 *
 * var objects = [
 *   { 'a': { 'b': 2 } },
 *   { 'a': { 'b': 1 } }
 * ];
 *
 * _.map(objects, _.property('a.b'));
 * // => [2, 1]
 *
 * _.map(_.sortBy(objects, _.property(['a', 'b'])), 'a.b');
 * // => [1, 2]
 */
function property(path) {
  return _isKey(path) ? _baseProperty(_toKey(path)) : _basePropertyDeep(path);
}

var property_1 = property;

/**
 * The base implementation of `_.iteratee`.
 *
 * @private
 * @param {*} [value=_.identity] The value to convert to an iteratee.
 * @returns {Function} Returns the iteratee.
 */
function baseIteratee(value) {
  // Don't store the `typeof` result in a variable to avoid a JIT bug in Safari 9.
  // See https://bugs.webkit.org/show_bug.cgi?id=156034 for more details.
  if (typeof value == 'function') {
    return value;
  }
  if (value == null) {
    return identity_1;
  }
  if (typeof value == 'object') {
    return isArray_1(value)
      ? _baseMatchesProperty(value[0], value[1])
      : _baseMatches(value);
  }
  return property_1(value);
}

var _baseIteratee = baseIteratee;

/**
 * Creates a base function for methods like `_.forIn` and `_.forOwn`.
 *
 * @private
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {Function} Returns the new base function.
 */
function createBaseFor(fromRight) {
  return function(object, iteratee, keysFunc) {
    var index = -1,
        iterable = Object(object),
        props = keysFunc(object),
        length = props.length;

    while (length--) {
      var key = props[fromRight ? length : ++index];
      if (iteratee(iterable[key], key, iterable) === false) {
        break;
      }
    }
    return object;
  };
}

var _createBaseFor = createBaseFor;

/**
 * The base implementation of `baseForOwn` which iterates over `object`
 * properties returned by `keysFunc` and invokes `iteratee` for each property.
 * Iteratee functions may exit iteration early by explicitly returning `false`.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @param {Function} keysFunc The function to get the keys of `object`.
 * @returns {Object} Returns `object`.
 */
var baseFor = _createBaseFor();

var _baseFor = baseFor;

/**
 * The base implementation of `_.forOwn` without support for iteratee shorthands.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Object} Returns `object`.
 */
function baseForOwn(object, iteratee) {
  return object && _baseFor(object, iteratee, keys_1);
}

var _baseForOwn = baseForOwn;

/**
 * Creates a `baseEach` or `baseEachRight` function.
 *
 * @private
 * @param {Function} eachFunc The function to iterate over a collection.
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {Function} Returns the new base function.
 */
function createBaseEach(eachFunc, fromRight) {
  return function(collection, iteratee) {
    if (collection == null) {
      return collection;
    }
    if (!isArrayLike_1(collection)) {
      return eachFunc(collection, iteratee);
    }
    var length = collection.length,
        index = fromRight ? length : -1,
        iterable = Object(collection);

    while ((fromRight ? index-- : ++index < length)) {
      if (iteratee(iterable[index], index, iterable) === false) {
        break;
      }
    }
    return collection;
  };
}

var _createBaseEach = createBaseEach;

/**
 * The base implementation of `_.forEach` without support for iteratee shorthands.
 *
 * @private
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array|Object} Returns `collection`.
 */
var baseEach = _createBaseEach(_baseForOwn);

var _baseEach = baseEach;

/**
 * The base implementation of `_.map` without support for iteratee shorthands.
 *
 * @private
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the new mapped array.
 */
function baseMap(collection, iteratee) {
  var index = -1,
      result = isArrayLike_1(collection) ? Array(collection.length) : [];

  _baseEach(collection, function(value, key, collection) {
    result[++index] = iteratee(value, key, collection);
  });
  return result;
}

var _baseMap = baseMap;

/**
 * Creates an array of values by running each element in `collection` thru
 * `iteratee`. The iteratee is invoked with three arguments:
 * (value, index|key, collection).
 *
 * Many lodash methods are guarded to work as iteratees for methods like
 * `_.every`, `_.filter`, `_.map`, `_.mapValues`, `_.reject`, and `_.some`.
 *
 * The guarded methods are:
 * `ary`, `chunk`, `curry`, `curryRight`, `drop`, `dropRight`, `every`,
 * `fill`, `invert`, `parseInt`, `random`, `range`, `rangeRight`, `repeat`,
 * `sampleSize`, `slice`, `some`, `sortBy`, `split`, `take`, `takeRight`,
 * `template`, `trim`, `trimEnd`, `trimStart`, and `words`
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Collection
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function} [iteratee=_.identity] The function invoked per iteration.
 * @returns {Array} Returns the new mapped array.
 * @example
 *
 * function square(n) {
 *   return n * n;
 * }
 *
 * _.map([4, 8], square);
 * // => [16, 64]
 *
 * _.map({ 'a': 4, 'b': 8 }, square);
 * // => [16, 64] (iteration order is not guaranteed)
 *
 * var users = [
 *   { 'user': 'barney' },
 *   { 'user': 'fred' }
 * ];
 *
 * // The `_.property` iteratee shorthand.
 * _.map(users, 'user');
 * // => ['barney', 'fred']
 */
function map(collection, iteratee) {
  var func = isArray_1(collection) ? _arrayMap : _baseMap;
  return func(collection, _baseIteratee(iteratee, 3));
}

var map_1 = map;

/**
 * Validations
 */

/**
 * isEmailValid
 * @param {*} email
 */
var isEmail = function isEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

var isDateInFuture = function isDateInFuture() {
  var date = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : new Date();
  var dateToCompare = arguments[1];

  if (!date || !dateToCompare) return false;

  var dateA = toDate(date);
  var dateB = toDate(dateToCompare);

  return isDateAfter(dateA, dateB);
};

/**
 * Utils
 */


var mapForSelect = function mapForSelect(array, properties) {
  if (!isArray(array)) return [];

  var idPath = properties.idPath,
      valuePath = properties.valuePath,
      labelPath = properties.labelPath;


  return map_1(array, function (obj, index) {
    return {
      id: get_1(obj, idPath, index),
      value: get_1(obj, valuePath, index),
      label: get_1(obj, labelPath, get_1(obj, 'idPath', index))
    };
  });
};

// dateTime

var commonUtils = /*#__PURE__*/Object.freeze({
  toDate: toDate,
  isDateAfter: isDateAfter,
  now: now,
  format: format$1,
  isString: isString,
  isDefined: isDefined,
  isNumber: isNumber$1,
  isArray: isArray,
  isObject: isObject$1,
  toString: toString$1,
  logFromMethod: logFromMethod,
  isEmail: isEmail,
  isDateInFuture: isDateInFuture,
  mapForSelect: mapForSelect
});

/*
object-assign
(c) Sindre Sorhus
@license MIT
*/
/* eslint-disable no-unused-vars */
var getOwnPropertySymbols = Object.getOwnPropertySymbols;
var hasOwnProperty$9 = Object.prototype.hasOwnProperty;
var propIsEnumerable = Object.prototype.propertyIsEnumerable;

function toObject(val) {
	if (val === null || val === undefined) {
		throw new TypeError('Object.assign cannot be called with null or undefined');
	}

	return Object(val);
}

function shouldUseNative() {
	try {
		if (!Object.assign) {
			return false;
		}

		// Detect buggy property enumeration order in older V8 versions.

		// https://bugs.chromium.org/p/v8/issues/detail?id=4118
		var test1 = new String('abc');  // eslint-disable-line no-new-wrappers
		test1[5] = 'de';
		if (Object.getOwnPropertyNames(test1)[0] === '5') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test2 = {};
		for (var i = 0; i < 10; i++) {
			test2['_' + String.fromCharCode(i)] = i;
		}
		var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
			return test2[n];
		});
		if (order2.join('') !== '0123456789') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test3 = {};
		'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
			test3[letter] = letter;
		});
		if (Object.keys(Object.assign({}, test3)).join('') !==
				'abcdefghijklmnopqrst') {
			return false;
		}

		return true;
	} catch (err) {
		// We don't expect any of the above to throw, but better to be safe.
		return false;
	}
}

var objectAssign = shouldUseNative() ? Object.assign : function (target, source) {
	var from;
	var to = toObject(target);
	var symbols;

	for (var s = 1; s < arguments.length; s++) {
		from = Object(arguments[s]);

		for (var key in from) {
			if (hasOwnProperty$9.call(from, key)) {
				to[key] = from[key];
			}
		}

		if (getOwnPropertySymbols) {
			symbols = getOwnPropertySymbols(from);
			for (var i = 0; i < symbols.length; i++) {
				if (propIsEnumerable.call(from, symbols[i])) {
					to[symbols[i]] = from[symbols[i]];
				}
			}
		}
	}

	return to;
};

/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var ReactPropTypesSecret = 'SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED';

var ReactPropTypesSecret_1 = ReactPropTypesSecret;

var printWarning = function() {};

if (process.env.NODE_ENV !== 'production') {
  var ReactPropTypesSecret$1 = ReactPropTypesSecret_1;
  var loggedTypeFailures = {};

  printWarning = function(text) {
    var message = 'Warning: ' + text;
    if (typeof console !== 'undefined') {
      console.error(message);
    }
    try {
      // --- Welcome to debugging React ---
      // This error was thrown as a convenience so that you can use this stack
      // to find the callsite that caused this warning to fire.
      throw new Error(message);
    } catch (x) {}
  };
}

/**
 * Assert that the values match with the type specs.
 * Error messages are memorized and will only be shown once.
 *
 * @param {object} typeSpecs Map of name to a ReactPropType
 * @param {object} values Runtime values that need to be type-checked
 * @param {string} location e.g. "prop", "context", "child context"
 * @param {string} componentName Name of the component for error messages.
 * @param {?Function} getStack Returns the component stack.
 * @private
 */
function checkPropTypes(typeSpecs, values, location, componentName, getStack) {
  if (process.env.NODE_ENV !== 'production') {
    for (var typeSpecName in typeSpecs) {
      if (typeSpecs.hasOwnProperty(typeSpecName)) {
        var error;
        // Prop type validation may throw. In case they do, we don't want to
        // fail the render phase where it didn't fail before. So we log it.
        // After these have been cleaned up, we'll let them throw.
        try {
          // This is intentionally an invariant that gets caught. It's the same
          // behavior as without this statement except with a better message.
          if (typeof typeSpecs[typeSpecName] !== 'function') {
            var err = Error(
              (componentName || 'React class') + ': ' + location + ' type `' + typeSpecName + '` is invalid; ' +
              'it must be a function, usually from the `prop-types` package, but received `' + typeof typeSpecs[typeSpecName] + '`.'
            );
            err.name = 'Invariant Violation';
            throw err;
          }
          error = typeSpecs[typeSpecName](values, typeSpecName, componentName, location, null, ReactPropTypesSecret$1);
        } catch (ex) {
          error = ex;
        }
        if (error && !(error instanceof Error)) {
          printWarning(
            (componentName || 'React class') + ': type specification of ' +
            location + ' `' + typeSpecName + '` is invalid; the type checker ' +
            'function must return `null` or an `Error` but returned a ' + typeof error + '. ' +
            'You may have forgotten to pass an argument to the type checker ' +
            'creator (arrayOf, instanceOf, objectOf, oneOf, oneOfType, and ' +
            'shape all require an argument).'
          );

        }
        if (error instanceof Error && !(error.message in loggedTypeFailures)) {
          // Only monitor this failure once because there tends to be a lot of the
          // same error.
          loggedTypeFailures[error.message] = true;

          var stack = getStack ? getStack() : '';

          printWarning(
            'Failed ' + location + ' type: ' + error.message + (stack != null ? stack : '')
          );
        }
      }
    }
  }
}

var checkPropTypes_1 = checkPropTypes;

var printWarning$1 = function() {};

if (process.env.NODE_ENV !== 'production') {
  printWarning$1 = function(text) {
    var message = 'Warning: ' + text;
    if (typeof console !== 'undefined') {
      console.error(message);
    }
    try {
      // --- Welcome to debugging React ---
      // This error was thrown as a convenience so that you can use this stack
      // to find the callsite that caused this warning to fire.
      throw new Error(message);
    } catch (x) {}
  };
}

function emptyFunctionThatReturnsNull() {
  return null;
}

var factoryWithTypeCheckers = function(isValidElement, throwOnDirectAccess) {
  /* global Symbol */
  var ITERATOR_SYMBOL = typeof Symbol === 'function' && Symbol.iterator;
  var FAUX_ITERATOR_SYMBOL = '@@iterator'; // Before Symbol spec.

  /**
   * Returns the iterator method function contained on the iterable object.
   *
   * Be sure to invoke the function with the iterable as context:
   *
   *     var iteratorFn = getIteratorFn(myIterable);
   *     if (iteratorFn) {
   *       var iterator = iteratorFn.call(myIterable);
   *       ...
   *     }
   *
   * @param {?object} maybeIterable
   * @return {?function}
   */
  function getIteratorFn(maybeIterable) {
    var iteratorFn = maybeIterable && (ITERATOR_SYMBOL && maybeIterable[ITERATOR_SYMBOL] || maybeIterable[FAUX_ITERATOR_SYMBOL]);
    if (typeof iteratorFn === 'function') {
      return iteratorFn;
    }
  }

  /**
   * Collection of methods that allow declaration and validation of props that are
   * supplied to React components. Example usage:
   *
   *   var Props = require('ReactPropTypes');
   *   var MyArticle = React.createClass({
   *     propTypes: {
   *       // An optional string prop named "description".
   *       description: Props.string,
   *
   *       // A required enum prop named "category".
   *       category: Props.oneOf(['News','Photos']).isRequired,
   *
   *       // A prop named "dialog" that requires an instance of Dialog.
   *       dialog: Props.instanceOf(Dialog).isRequired
   *     },
   *     render: function() { ... }
   *   });
   *
   * A more formal specification of how these methods are used:
   *
   *   type := array|bool|func|object|number|string|oneOf([...])|instanceOf(...)
   *   decl := ReactPropTypes.{type}(.isRequired)?
   *
   * Each and every declaration produces a function with the same signature. This
   * allows the creation of custom validation functions. For example:
   *
   *  var MyLink = React.createClass({
   *    propTypes: {
   *      // An optional string or URI prop named "href".
   *      href: function(props, propName, componentName) {
   *        var propValue = props[propName];
   *        if (propValue != null && typeof propValue !== 'string' &&
   *            !(propValue instanceof URI)) {
   *          return new Error(
   *            'Expected a string or an URI for ' + propName + ' in ' +
   *            componentName
   *          );
   *        }
   *      }
   *    },
   *    render: function() {...}
   *  });
   *
   * @internal
   */

  var ANONYMOUS = '<<anonymous>>';

  // Important!
  // Keep this list in sync with production version in `./factoryWithThrowingShims.js`.
  var ReactPropTypes = {
    array: createPrimitiveTypeChecker('array'),
    bool: createPrimitiveTypeChecker('boolean'),
    func: createPrimitiveTypeChecker('function'),
    number: createPrimitiveTypeChecker('number'),
    object: createPrimitiveTypeChecker('object'),
    string: createPrimitiveTypeChecker('string'),
    symbol: createPrimitiveTypeChecker('symbol'),

    any: createAnyTypeChecker(),
    arrayOf: createArrayOfTypeChecker,
    element: createElementTypeChecker(),
    instanceOf: createInstanceTypeChecker,
    node: createNodeChecker(),
    objectOf: createObjectOfTypeChecker,
    oneOf: createEnumTypeChecker,
    oneOfType: createUnionTypeChecker,
    shape: createShapeTypeChecker,
    exact: createStrictShapeTypeChecker,
  };

  /**
   * inlined Object.is polyfill to avoid requiring consumers ship their own
   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/is
   */
  /*eslint-disable no-self-compare*/
  function is(x, y) {
    // SameValue algorithm
    if (x === y) {
      // Steps 1-5, 7-10
      // Steps 6.b-6.e: +0 != -0
      return x !== 0 || 1 / x === 1 / y;
    } else {
      // Step 6.a: NaN == NaN
      return x !== x && y !== y;
    }
  }
  /*eslint-enable no-self-compare*/

  /**
   * We use an Error-like object for backward compatibility as people may call
   * PropTypes directly and inspect their output. However, we don't use real
   * Errors anymore. We don't inspect their stack anyway, and creating them
   * is prohibitively expensive if they are created too often, such as what
   * happens in oneOfType() for any type before the one that matched.
   */
  function PropTypeError(message) {
    this.message = message;
    this.stack = '';
  }
  // Make `instanceof Error` still work for returned errors.
  PropTypeError.prototype = Error.prototype;

  function createChainableTypeChecker(validate) {
    if (process.env.NODE_ENV !== 'production') {
      var manualPropTypeCallCache = {};
      var manualPropTypeWarningCount = 0;
    }
    function checkType(isRequired, props, propName, componentName, location, propFullName, secret) {
      componentName = componentName || ANONYMOUS;
      propFullName = propFullName || propName;

      if (secret !== ReactPropTypesSecret_1) {
        if (throwOnDirectAccess) {
          // New behavior only for users of `prop-types` package
          var err = new Error(
            'Calling PropTypes validators directly is not supported by the `prop-types` package. ' +
            'Use `PropTypes.checkPropTypes()` to call them. ' +
            'Read more at http://fb.me/use-check-prop-types'
          );
          err.name = 'Invariant Violation';
          throw err;
        } else if (process.env.NODE_ENV !== 'production' && typeof console !== 'undefined') {
          // Old behavior for people using React.PropTypes
          var cacheKey = componentName + ':' + propName;
          if (
            !manualPropTypeCallCache[cacheKey] &&
            // Avoid spamming the console because they are often not actionable except for lib authors
            manualPropTypeWarningCount < 3
          ) {
            printWarning$1(
              'You are manually calling a React.PropTypes validation ' +
              'function for the `' + propFullName + '` prop on `' + componentName  + '`. This is deprecated ' +
              'and will throw in the standalone `prop-types` package. ' +
              'You may be seeing this warning due to a third-party PropTypes ' +
              'library. See https://fb.me/react-warning-dont-call-proptypes ' + 'for details.'
            );
            manualPropTypeCallCache[cacheKey] = true;
            manualPropTypeWarningCount++;
          }
        }
      }
      if (props[propName] == null) {
        if (isRequired) {
          if (props[propName] === null) {
            return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required ' + ('in `' + componentName + '`, but its value is `null`.'));
          }
          return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required in ' + ('`' + componentName + '`, but its value is `undefined`.'));
        }
        return null;
      } else {
        return validate(props, propName, componentName, location, propFullName);
      }
    }

    var chainedCheckType = checkType.bind(null, false);
    chainedCheckType.isRequired = checkType.bind(null, true);

    return chainedCheckType;
  }

  function createPrimitiveTypeChecker(expectedType) {
    function validate(props, propName, componentName, location, propFullName, secret) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== expectedType) {
        // `propValue` being instance of, say, date/regexp, pass the 'object'
        // check, but we can offer a more precise error message here rather than
        // 'of type `object`'.
        var preciseType = getPreciseType(propValue);

        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + preciseType + '` supplied to `' + componentName + '`, expected ') + ('`' + expectedType + '`.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createAnyTypeChecker() {
    return createChainableTypeChecker(emptyFunctionThatReturnsNull);
  }

  function createArrayOfTypeChecker(typeChecker) {
    function validate(props, propName, componentName, location, propFullName) {
      if (typeof typeChecker !== 'function') {
        return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside arrayOf.');
      }
      var propValue = props[propName];
      if (!Array.isArray(propValue)) {
        var propType = getPropType(propValue);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an array.'));
      }
      for (var i = 0; i < propValue.length; i++) {
        var error = typeChecker(propValue, i, componentName, location, propFullName + '[' + i + ']', ReactPropTypesSecret_1);
        if (error instanceof Error) {
          return error;
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createElementTypeChecker() {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      if (!isValidElement(propValue)) {
        var propType = getPropType(propValue);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected a single ReactElement.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createInstanceTypeChecker(expectedClass) {
    function validate(props, propName, componentName, location, propFullName) {
      if (!(props[propName] instanceof expectedClass)) {
        var expectedClassName = expectedClass.name || ANONYMOUS;
        var actualClassName = getClassName(props[propName]);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + actualClassName + '` supplied to `' + componentName + '`, expected ') + ('instance of `' + expectedClassName + '`.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createEnumTypeChecker(expectedValues) {
    if (!Array.isArray(expectedValues)) {
      process.env.NODE_ENV !== 'production' ? printWarning$1('Invalid argument supplied to oneOf, expected an instance of array.') : void 0;
      return emptyFunctionThatReturnsNull;
    }

    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      for (var i = 0; i < expectedValues.length; i++) {
        if (is(propValue, expectedValues[i])) {
          return null;
        }
      }

      var valuesString = JSON.stringify(expectedValues);
      return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of value `' + propValue + '` ' + ('supplied to `' + componentName + '`, expected one of ' + valuesString + '.'));
    }
    return createChainableTypeChecker(validate);
  }

  function createObjectOfTypeChecker(typeChecker) {
    function validate(props, propName, componentName, location, propFullName) {
      if (typeof typeChecker !== 'function') {
        return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside objectOf.');
      }
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an object.'));
      }
      for (var key in propValue) {
        if (propValue.hasOwnProperty(key)) {
          var error = typeChecker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret_1);
          if (error instanceof Error) {
            return error;
          }
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createUnionTypeChecker(arrayOfTypeCheckers) {
    if (!Array.isArray(arrayOfTypeCheckers)) {
      process.env.NODE_ENV !== 'production' ? printWarning$1('Invalid argument supplied to oneOfType, expected an instance of array.') : void 0;
      return emptyFunctionThatReturnsNull;
    }

    for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
      var checker = arrayOfTypeCheckers[i];
      if (typeof checker !== 'function') {
        printWarning$1(
          'Invalid argument supplied to oneOfType. Expected an array of check functions, but ' +
          'received ' + getPostfixForTypeWarning(checker) + ' at index ' + i + '.'
        );
        return emptyFunctionThatReturnsNull;
      }
    }

    function validate(props, propName, componentName, location, propFullName) {
      for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
        var checker = arrayOfTypeCheckers[i];
        if (checker(props, propName, componentName, location, propFullName, ReactPropTypesSecret_1) == null) {
          return null;
        }
      }

      return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`.'));
    }
    return createChainableTypeChecker(validate);
  }

  function createNodeChecker() {
    function validate(props, propName, componentName, location, propFullName) {
      if (!isNode(props[propName])) {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`, expected a ReactNode.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createShapeTypeChecker(shapeTypes) {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
      }
      for (var key in shapeTypes) {
        var checker = shapeTypes[key];
        if (!checker) {
          continue;
        }
        var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret_1);
        if (error) {
          return error;
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createStrictShapeTypeChecker(shapeTypes) {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
      }
      // We need to check all keys in case some are required but missing from
      // props.
      var allKeys = objectAssign({}, props[propName], shapeTypes);
      for (var key in allKeys) {
        var checker = shapeTypes[key];
        if (!checker) {
          return new PropTypeError(
            'Invalid ' + location + ' `' + propFullName + '` key `' + key + '` supplied to `' + componentName + '`.' +
            '\nBad object: ' + JSON.stringify(props[propName], null, '  ') +
            '\nValid keys: ' +  JSON.stringify(Object.keys(shapeTypes), null, '  ')
          );
        }
        var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret_1);
        if (error) {
          return error;
        }
      }
      return null;
    }

    return createChainableTypeChecker(validate);
  }

  function isNode(propValue) {
    switch (typeof propValue) {
      case 'number':
      case 'string':
      case 'undefined':
        return true;
      case 'boolean':
        return !propValue;
      case 'object':
        if (Array.isArray(propValue)) {
          return propValue.every(isNode);
        }
        if (propValue === null || isValidElement(propValue)) {
          return true;
        }

        var iteratorFn = getIteratorFn(propValue);
        if (iteratorFn) {
          var iterator = iteratorFn.call(propValue);
          var step;
          if (iteratorFn !== propValue.entries) {
            while (!(step = iterator.next()).done) {
              if (!isNode(step.value)) {
                return false;
              }
            }
          } else {
            // Iterator will provide entry [k,v] tuples rather than values.
            while (!(step = iterator.next()).done) {
              var entry = step.value;
              if (entry) {
                if (!isNode(entry[1])) {
                  return false;
                }
              }
            }
          }
        } else {
          return false;
        }

        return true;
      default:
        return false;
    }
  }

  function isSymbol(propType, propValue) {
    // Native Symbol.
    if (propType === 'symbol') {
      return true;
    }

    // 19.4.3.5 Symbol.prototype[@@toStringTag] === 'Symbol'
    if (propValue['@@toStringTag'] === 'Symbol') {
      return true;
    }

    // Fallback for non-spec compliant Symbols which are polyfilled.
    if (typeof Symbol === 'function' && propValue instanceof Symbol) {
      return true;
    }

    return false;
  }

  // Equivalent of `typeof` but with special handling for array and regexp.
  function getPropType(propValue) {
    var propType = typeof propValue;
    if (Array.isArray(propValue)) {
      return 'array';
    }
    if (propValue instanceof RegExp) {
      // Old webkits (at least until Android 4.0) return 'function' rather than
      // 'object' for typeof a RegExp. We'll normalize this here so that /bla/
      // passes PropTypes.object.
      return 'object';
    }
    if (isSymbol(propType, propValue)) {
      return 'symbol';
    }
    return propType;
  }

  // This handles more types than `getPropType`. Only used for error messages.
  // See `createPrimitiveTypeChecker`.
  function getPreciseType(propValue) {
    if (typeof propValue === 'undefined' || propValue === null) {
      return '' + propValue;
    }
    var propType = getPropType(propValue);
    if (propType === 'object') {
      if (propValue instanceof Date) {
        return 'date';
      } else if (propValue instanceof RegExp) {
        return 'regexp';
      }
    }
    return propType;
  }

  // Returns a string that is postfixed to a warning about an invalid type.
  // For example, "undefined" or "of type array"
  function getPostfixForTypeWarning(value) {
    var type = getPreciseType(value);
    switch (type) {
      case 'array':
      case 'object':
        return 'an ' + type;
      case 'boolean':
      case 'date':
      case 'regexp':
        return 'a ' + type;
      default:
        return type;
    }
  }

  // Returns class name of the object, if any.
  function getClassName(propValue) {
    if (!propValue.constructor || !propValue.constructor.name) {
      return ANONYMOUS;
    }
    return propValue.constructor.name;
  }

  ReactPropTypes.checkPropTypes = checkPropTypes_1;
  ReactPropTypes.PropTypes = ReactPropTypes;

  return ReactPropTypes;
};

function emptyFunction() {}

var factoryWithThrowingShims = function() {
  function shim(props, propName, componentName, location, propFullName, secret) {
    if (secret === ReactPropTypesSecret_1) {
      // It is still safe when called from React.
      return;
    }
    var err = new Error(
      'Calling PropTypes validators directly is not supported by the `prop-types` package. ' +
      'Use PropTypes.checkPropTypes() to call them. ' +
      'Read more at http://fb.me/use-check-prop-types'
    );
    err.name = 'Invariant Violation';
    throw err;
  }  shim.isRequired = shim;
  function getShim() {
    return shim;
  }  // Important!
  // Keep this list in sync with production version in `./factoryWithTypeCheckers.js`.
  var ReactPropTypes = {
    array: shim,
    bool: shim,
    func: shim,
    number: shim,
    object: shim,
    string: shim,
    symbol: shim,

    any: shim,
    arrayOf: getShim,
    element: shim,
    instanceOf: getShim,
    node: shim,
    objectOf: getShim,
    oneOf: getShim,
    oneOfType: getShim,
    shape: getShim,
    exact: getShim
  };

  ReactPropTypes.checkPropTypes = emptyFunction;
  ReactPropTypes.PropTypes = ReactPropTypes;

  return ReactPropTypes;
};

var propTypes = createCommonjsModule(function (module) {
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

if (process.env.NODE_ENV !== 'production') {
  var REACT_ELEMENT_TYPE = (typeof Symbol === 'function' &&
    Symbol.for &&
    Symbol.for('react.element')) ||
    0xeac7;

  var isValidElement = function(object) {
    return typeof object === 'object' &&
      object !== null &&
      object.$$typeof === REACT_ELEMENT_TYPE;
  };

  // By explicitly using `prop-types` you are opting into new development behavior.
  // http://fb.me/prop-types-in-prod
  var throwOnDirectAccess = true;
  module.exports = factoryWithTypeCheckers(isValidElement, throwOnDirectAccess);
} else {
  // By explicitly using `prop-types` you are opting into new production behavior.
  // http://fb.me/prop-types-in-prod
  module.exports = factoryWithThrowingShims();
}
});

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var noop = function noop(n) {
  return n;
};

var propTypes$1 = {
  numberOrString: propTypes.oneOfType([propTypes.number, propTypes.string]),
  responsive: propTypes.oneOfType([propTypes.number, propTypes.string, propTypes.array])
};
var defaultBreakpoints = [40, 52, 64].map(function (n) {
  return n + 'em';
});
var is = function is(n) {
  return n !== undefined && n !== null;
};
var num = function num(n) {
  return typeof n === 'number' && !isNaN(n);
};
var px = function px(n) {
  return num(n) ? n + 'px' : n;
};
var get$1 = function get(obj) {
  for (var _len = arguments.length, paths = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    paths[_key - 1] = arguments[_key];
  }

  return paths.join('.').split('.').reduce(function (a, b) {
    return a && a[b] ? a[b] : null;
  }, obj);
};
var themeGet = function themeGet(paths, fallback) {
  return function (props) {
    return get$1(props.theme, paths) || fallback;
  };
};
var cloneFunc = function cloneFunc(fn) {
  return function () {
    return fn.apply(void 0, arguments);
  };
};
var merge = function merge(a, b) {
  return Object.assign({}, a, b, Object.keys(b || {}).reduce(function (obj, key) {
    var _Object$assign;

    return Object.assign(obj, (_Object$assign = {}, _Object$assign[key] = a[key] !== null && typeof a[key] === 'object' ? merge(a[key], b[key]) : b[key], _Object$assign));
  }, {}));
};
var compose = function compose() {
  for (var _len2 = arguments.length, funcs = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
    funcs[_key2] = arguments[_key2];
  }

  var fn = function fn(props) {
    return funcs.map(function (fn) {
      return fn(props);
    }).filter(Boolean).reduce(merge, {});
  };

  fn.propTypes = funcs.map(function (fn) {
    return fn.propTypes;
  }).reduce(merge, {});
  return fn;
};
var createMediaQuery = function createMediaQuery(n) {
  return "@media screen and (min-width: " + n + ")";
};
var style = function style(_ref) {
  var _fn$propTypes;

  var prop = _ref.prop,
      cssProperty = _ref.cssProperty,
      key = _ref.key,
      getter = _ref.getter,
      transformValue = _ref.transformValue,
      _ref$scale = _ref.scale,
      defaultScale = _ref$scale === void 0 ? {} : _ref$scale;
  var css = cssProperty || prop;
  var transform = transformValue || getter || noop;

  var fn = function fn(props) {
    var val = props[prop];
    if (!is(val)) return null;
    var scale = get$1(props.theme, key) || defaultScale;

    var style = function style(n) {
      var _ref2;

      return is(n) ? (_ref2 = {}, _ref2[css] = transform(get$1(scale, n) || n), _ref2) : null;
    };

    if (!Array.isArray(val)) {
      return style(val);
    } // how to hoist this up??


    var breakpoints = [null].concat((get$1(props.theme, 'breakpoints') || defaultBreakpoints).map(createMediaQuery));
    var styles = {};

    for (var i = 0; i < val.length; i++) {
      var media = breakpoints[i];

      if (!media) {
        styles = style(val[i]) || {};
        continue;
      }

      var rule = style(val[i]);
      if (!rule) continue;
      styles[media] = rule;
    }

    return styles;
  };

  fn.propTypes = (_fn$propTypes = {}, _fn$propTypes[prop] = cloneFunc(propTypes$1.responsive), _fn$propTypes);
  fn.propTypes[prop].meta = {
    prop: prop,
    themeKey: key,
    styleType: 'responsive'
  };
  return fn;
};
var getWidth = function getWidth(n) {
  return !num(n) || n > 1 ? px(n) : n * 100 + '%';
}; // variant

var variant = function variant(_ref3) {
  var _fn$propTypes2;

  var key = _ref3.key,
      _ref3$prop = _ref3.prop,
      prop = _ref3$prop === void 0 ? 'variant' : _ref3$prop;

  var fn = function fn(props) {
    return get$1(props.theme, key, props[prop]) || null;
  };

  fn.propTypes = (_fn$propTypes2 = {}, _fn$propTypes2[prop] = propTypes$1.numberOrString, _fn$propTypes2);
  return fn;
};

var isNegative = function isNegative(n) {
  return n < 0;
};

var REG = /^[mp][trblxy]?$/;
var properties = {
  m: 'margin',
  p: 'padding'
};
var directions = {
  t: 'Top',
  r: 'Right',
  b: 'Bottom',
  l: 'Left',
  x: ['Left', 'Right'],
  y: ['Top', 'Bottom']
};

var getProperties = function getProperties(key) {
  var _key$split = key.split(''),
      a = _key$split[0],
      b = _key$split[1];

  var property = properties[a];
  var direction = directions[b] || '';
  return Array.isArray(direction) ? direction.map(function (dir) {
    return property + dir;
  }) : [property + direction];
};

var getValue$1 = function getValue(scale) {
  return function (n) {
    if (!num(n)) {
      return scale[n] || n;
    }

    var abs = Math.abs(n);
    var neg = isNegative(n);
    var value = scale[abs] || abs;

    if (!num(value)) {
      return neg ? '-' + value : value;
    }

    return px(value * (neg ? -1 : 1));
  };
};

var defaultScale = [0, 4, 8, 16, 32, 64, 128, 256, 512];
var space = function space(props) {
  var keys = Object.keys(props).filter(function (key) {
    return REG.test(key);
  }).sort();
  var scale = get$1(props.theme, 'space') || defaultScale;
  var getStyle = getValue$1(scale);
  return keys.map(function (key) {
    var value = props[key];
    var properties = getProperties(key);

    var style = function style(n) {
      return is(n) ? properties.reduce(function (a, prop) {
        var _extends2;

        return _extends({}, a, (_extends2 = {}, _extends2[prop] = getStyle(n), _extends2));
      }, {}) : null;
    };

    if (!Array.isArray(value)) {
      return style(value);
    }

    var breakpoints = [null].concat((get$1(props.theme, 'breakpoints') || defaultBreakpoints).map(createMediaQuery));
    var styles = {};

    for (var i = 0; i < value.length; i++) {
      var media = breakpoints[i];

      if (!media) {
        styles = style(value[i]) || {};
        continue;
      }

      var rule = style(value[i]);
      if (!rule) continue;
      styles[media] = rule;
    }

    return styles;
  }).reduce(merge, {});
};
space.propTypes = {
  m: cloneFunc(propTypes$1.responsive),
  mt: cloneFunc(propTypes$1.responsive),
  mr: cloneFunc(propTypes$1.responsive),
  mb: cloneFunc(propTypes$1.responsive),
  ml: cloneFunc(propTypes$1.responsive),
  mx: cloneFunc(propTypes$1.responsive),
  my: cloneFunc(propTypes$1.responsive),
  p: cloneFunc(propTypes$1.responsive),
  pt: cloneFunc(propTypes$1.responsive),
  pr: cloneFunc(propTypes$1.responsive),
  pb: cloneFunc(propTypes$1.responsive),
  pl: cloneFunc(propTypes$1.responsive),
  px: cloneFunc(propTypes$1.responsive),
  py: cloneFunc(propTypes$1.responsive)
};

var meta = function meta(prop) {
  return {
    prop: prop,
    themeKey: 'space',
    styleType: 'responsive'
  };
};

Object.keys(space.propTypes).forEach(function (prop) {
  space.propTypes[prop].meta = meta(prop);
}); // styles

var width = style({
  prop: 'width',
  transformValue: getWidth
});
var fontSize = style({
  prop: 'fontSize',
  key: 'fontSizes',
  transformValue: px,
  scale: [12, 14, 16, 20, 24, 32, 48, 64, 72]
});
var textColor = style({
  prop: 'color',
  key: 'colors'
});
var bgColor = style({
  prop: 'bg',
  cssProperty: 'backgroundColor',
  key: 'colors'
});
var color = compose(textColor, bgColor); // typography

var fontFamily = style({
  prop: 'fontFamily',
  key: 'fonts'
});
var textAlign = style({
  prop: 'textAlign'
});
var lineHeight = style({
  prop: 'lineHeight',
  key: 'lineHeights'
});
var fontWeight = style({
  prop: 'fontWeight',
  key: 'fontWeights'
});
var fontStyle = style({
  prop: 'fontStyle'
});
var letterSpacing = style({
  prop: 'letterSpacing',
  key: 'letterSpacings',
  transformValue: px
}); // layout

var display = style({
  prop: 'display'
});
var maxWidth = style({
  prop: 'maxWidth',
  key: 'maxWidths',
  transformValue: px
});
var minWidth = style({
  prop: 'minWidth',
  key: 'minWidths',
  transformValue: px
});
var height = style({
  prop: 'height',
  key: 'heights',
  transformValue: px
});
var maxHeight = style({
  prop: 'maxHeight',
  key: 'maxHeights',
  transformValue: px
});
var minHeight = style({
  prop: 'minHeight',
  key: 'minHeights',
  transformValue: px
});
var sizeWidth = style({
  prop: 'size',
  cssProperty: 'width',
  transformValue: px
});
var sizeHeight = style({
  prop: 'size',
  cssProperty: 'height',
  transformValue: px
});
var size = compose(sizeHeight, sizeWidth);
var ratioPadding = style({
  prop: 'ratio',
  cssProperty: 'paddingBottom',
  transformValue: function transformValue(n) {
    return n * 100 + '%';
  }
});
var ratio = function ratio(props) {
  return props.ratio ? _extends({
    height: 0
  }, ratioPadding(props)) : null;
};
ratio.propTypes = _extends({}, ratioPadding.propTypes);
var verticalAlign = style({
  prop: 'verticalAlign'
}); // flexbox

var alignItems = style({
  prop: 'alignItems'
});
var alignContent = style({
  prop: 'alignContent'
});
var justifyItems = style({
  prop: 'justifyItems'
});
var justifyContent = style({
  prop: 'justifyContent'
});
var flexWrap = style({
  prop: 'flexWrap'
});
var flexBasis = style({
  prop: 'flexBasis',
  transformValue: getWidth
});
var flexDirection = style({
  prop: 'flexDirection'
});
var flex = style({
  prop: 'flex'
});
var justifySelf = style({
  prop: 'justifySelf'
});
var alignSelf = style({
  prop: 'alignSelf'
});
var order = style({
  prop: 'order'
}); // grid

var gridGap = style({
  prop: 'gridGap',
  transformValue: px,
  key: 'space'
});
var gridColumnGap = style({
  prop: 'gridColumnGap',
  transformValue: px,
  key: 'space'
});
var gridRowGap = style({
  prop: 'gridRowGap',
  transformValue: px,
  key: 'space'
});
var gridColumn = style({
  prop: 'gridColumn'
});
var gridRow = style({
  prop: 'gridRow'
});
var gridAutoFlow = style({
  prop: 'gridAutoFlow'
});
var gridAutoColumns = style({
  prop: 'gridAutoColumns'
});
var gridAutoRows = style({
  prop: 'gridAutoRows'
});
var gridTemplateColumns = style({
  prop: 'gridTemplateColumns'
});
var gridTemplateRows = style({
  prop: 'gridTemplateRows'
});
var gridTemplateAreas = style({
  prop: 'gridTemplateAreas'
});
var gridArea = style({
  prop: 'gridArea'
}); // borders

var getBorder = function getBorder(n) {
  return num(n) && n > 0 ? n + 'px solid' : n;
};

var border = style({
  prop: 'border',
  key: 'borders',
  transformValue: getBorder
});
var borderTop = style({
  prop: 'borderTop',
  key: 'borders',
  transformValue: getBorder
});
var borderRight = style({
  prop: 'borderRight',
  key: 'borders',
  transformValue: getBorder
});
var borderBottom = style({
  prop: 'borderBottom',
  key: 'borders',
  transformValue: getBorder
});
var borderLeft = style({
  prop: 'borderLeft',
  key: 'borders',
  transformValue: getBorder
});
var borders = compose(border, borderTop, borderRight, borderBottom, borderLeft);
var borderColor = style({
  prop: 'borderColor',
  key: 'colors'
});
var borderRadius = style({
  prop: 'borderRadius',
  key: 'radii',
  transformValue: px
});
var boxShadow = style({
  prop: 'boxShadow',
  key: 'shadows'
});
var opacity = style({
  prop: 'opacity'
});
var overflow = style({
  prop: 'overflow'
}); // backgrounds

var background = style({
  prop: 'background'
});
var backgroundImage = style({
  prop: 'backgroundImage'
});
var backgroundSize = style({
  prop: 'backgroundSize'
});
var backgroundPosition = style({
  prop: 'backgroundPosition'
});
var backgroundRepeat = style({
  prop: 'backgroundRepeat'
}); // position

var position = style({
  prop: 'position'
});
var zIndex = style({
  prop: 'zIndex'
});
var top = style({
  prop: 'top',
  transformValue: px
});
var right = style({
  prop: 'right',
  transformValue: px
});
var bottom = style({
  prop: 'bottom',
  transformValue: px
});
var left = style({
  prop: 'left',
  transformValue: px
});
var textStyle = variant({
  prop: 'textStyle',
  key: 'textStyles'
});
var colorStyle = variant({
  prop: 'colors',
  key: 'colorStyles'
});
var buttonStyle = variant({
  key: 'buttons'
});
var styles = {
  space: space,
  width: width,
  fontSize: fontSize,
  textColor: textColor,
  bgColor: bgColor,
  color: color,
  fontFamily: fontFamily,
  textAlign: textAlign,
  lineHeight: lineHeight,
  fontWeight: fontWeight,
  fontStyle: fontStyle,
  letterSpacing: letterSpacing,
  display: display,
  maxWidth: maxWidth,
  minWidth: minWidth,
  height: height,
  maxHeight: maxHeight,
  minHeight: minHeight,
  sizeWidth: sizeWidth,
  sizeHeight: sizeHeight,
  size: size,
  ratioPadding: ratioPadding,
  ratio: ratio,
  verticalAlign: verticalAlign,
  alignItems: alignItems,
  alignContent: alignContent,
  justifyItems: justifyItems,
  justifyContent: justifyContent,
  flexWrap: flexWrap,
  flexBasis: flexBasis,
  flexDirection: flexDirection,
  flex: flex,
  justifySelf: justifySelf,
  alignSelf: alignSelf,
  order: order,
  gridGap: gridGap,
  gridColumnGap: gridColumnGap,
  gridRowGap: gridRowGap,
  gridColumn: gridColumn,
  gridRow: gridRow,
  gridAutoFlow: gridAutoFlow,
  gridAutoColumns: gridAutoColumns,
  gridAutoRows: gridAutoRows,
  gridTemplateColumns: gridTemplateColumns,
  gridTemplateRows: gridTemplateRows,
  gridTemplateAreas: gridTemplateAreas,
  gridArea: gridArea,
  // borders
  border: border,
  borderTop: borderTop,
  borderRight: borderRight,
  borderBottom: borderBottom,
  borderLeft: borderLeft,
  borders: borders,
  borderColor: borderColor,
  borderRadius: borderRadius,
  boxShadow: boxShadow,
  opacity: opacity,
  overflow: overflow,
  background: background,
  backgroundImage: backgroundImage,
  backgroundPosition: backgroundPosition,
  backgroundRepeat: backgroundRepeat,
  backgroundSize: backgroundSize,
  position: position,
  zIndex: zIndex,
  top: top,
  right: right,
  bottom: bottom,
  left: left,
  textStyle: textStyle,
  colorStyle: colorStyle,
  buttonStyle: buttonStyle // mixed

};

var funcs = Object.keys(styles).map(function (key) {
  return styles[key];
}).filter(function (fn) {
  return typeof fn === 'function';
});
var blacklist = funcs.reduce(function (a, fn) {
  return a.concat(Object.keys(fn.propTypes || {}));
}, ['theme']);

/**
 * constants
 */

/**
 * Transformers
 */
var is$1 = function is$$1(n) {
  return n !== undefined && n !== null;
};
/**
 * Converts a number to string with px
 * @param {*} n
 */
var toPx = function toPx(n) {
  return is$1(n) ? isNumber_1(n) ? n + 'px' : n : '0px';
};

var getWidth$1 = function getWidth$$1(n) {
  return !isNumber_1(n) || n > 1 ? toPx(n) : n * 100 + '%';
};

/**
 * Utils
 */

var whiteSpace = style({
  prop: 'whiteSpace'
});

var elevation = style({
  prop: 'elevation',
  key: 'elevation',
  cssProperty: 'boxShadow'
});

var maxWidth$1 = style({
  prop: 'maxWidth',
  transformValue: getWidth$1
});

var minWidth$1 = style({
  prop: 'minWidth',
  transformValue: getWidth$1
});

var uiSize = variant({
  key: 'uiSizes',
  prop: 'uiSize'
});

/**
 * Style mixins
 */

var inputError = function inputError(props) {
  if (props.hasError) return styled.css(['box-shadow:0 0 0 1px ', ';'], themeGet('colors.danger')(props));
};

var inputFocus = function inputFocus(props) {
  return styled.css(['box-shadow:0 0 0 1px ', ';'], themeGet('colors.primary')(props));
};

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

var defineProperty = function (obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
};

var _extends$1 = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var inherits = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
};

var objectWithoutProperties = function (obj, keys) {
  var target = {};

  for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;
    if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
    target[i] = obj[i];
  }

  return target;
};

var possibleConstructorReturn = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
};

var taggedTemplateLiteral = function (strings, raw) {
  return Object.freeze(Object.defineProperties(strings, {
    raw: {
      value: Object.freeze(raw)
    }
  }));
};

var BoxWrapper = styled__default.div.withConfig({
  displayName: 'Box__BoxWrapper',
  componentId: 'sc-19nus8-0'
})(['box-sizing:border-box;', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ''], height, width, space, fontSize, color, flex, order, alignSelf, elevation, maxWidth$1, minWidth$1, borderRadius);

var Box = function Box(_ref) {
  var children = _ref.children,
      rest = objectWithoutProperties(_ref, ['children']);
  return React.createElement(
    BoxWrapper,
    rest,
    children
  );
};
Box.displayName = 'Box';
Box.defaultProps = {};
Box.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Box',
  'props': {
    'children': {
      'required': true,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    }
  }
};

var FlexWrapper = styled__default(BoxWrapper).withConfig({
  displayName: 'Flex__FlexWrapper',
  componentId: 'sc-16x1734-0'
})(['display:flex;', ' ', ' ', ' ', ''], flexWrap, flexDirection, alignItems, justifyContent);

var Flex = function Flex(_ref) {
  var children = _ref.children,
      rest = objectWithoutProperties(_ref, ['children']);
  return React.createElement(
    FlexWrapper,
    rest,
    children
  );
};
Flex.displayName = 'Flex';
Flex.defaultProps = {};
Flex.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Flex'
};

var Row = function Row(_ref) {
  var children = _ref.children,
      rest = objectWithoutProperties(_ref, ['children']);
  return React.createElement(
    Flex,
    rest,
    children
  );
};
Row.displayName = 'Row';
Row.defaultProps = {
  flexDirection: 'row'
};
Row.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Row',
  'props': {
    'flexDirection': {
      'defaultValue': {
        'value': '\'row\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'flexDirection.propTypes'
      },
      'description': ''
    },
    'children': {
      'required': true,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    }
  }
};

var Col = function Col(_ref) {
  var children = _ref.children,
      rest = objectWithoutProperties(_ref, ['children']);
  return React.createElement(
    Flex,
    rest,
    children
  );
};
Col.displayName = 'Col';
Col.defaultProps = {
  flexDirection: 'column',
  flex: 1
};
Col.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Col',
  'props': {
    'flexDirection': {
      'defaultValue': {
        'value': '\'column\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'flexDirection.propTypes'
      },
      'description': ''
    },
    'flex': {
      'defaultValue': {
        'value': '1',
        'computed': false
      },
      'required': false
    },
    'children': {
      'required': true,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    }
  }
};

var breakpoint = [0, 600, 900, 1200, 1800];

var uiSize$1 = [0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 60, 64, 68, 72, 76, 80, 84, 88, 92, 96, 100, 104, 108, 112, 116, 120, 124, 128, 132, 136, 140, 144, 148, 152, 156, 160, 164];

/**
 * Variants
 */
var uiSizes = {
  small: {
    height: '32px'
  },
  large: {
    height: '40px'
  }
};

/**
 *
 */

var margin = uiSize$1;
var marginBottom = uiSize$1;
var marginRight = uiSize$1;
var marginLeft = uiSize$1;
var marginTop = uiSize$1;

var padding = uiSize$1;
var paddingBottom = uiSize$1;
var paddingRight = uiSize$1;
var paddingLeft = uiSize$1;
var paddingTop = uiSize$1;
var borderRadius$1 = uiSize$1;

var width$1 = uiSize$1;
var height$1 = uiSize$1;

var fontSize$1 = [8, 10, 12, 14, 16, 20, 24, 28, 32, 36, 40, 44, 48, 64, 72, 96];
var fontWeight$1 = [100, 200, 300, 400, 500, 600, 700];
var lineHeight$1 = uiSize$1;
var zIndex$1 = [100, 200, 300, 400];

var textAlign$1 = {
  center: 'center',
  left: 'left',
  right: 'right'
};

var elevation$1 = ['none', '0 0 0 1px rgba(48, 72, 87, 0.1)', '0 2px 4px 0 rgba(48, 72, 87, 0.1)', '0 4px 6px 0 rgba(48, 72, 87, 0.1)', '0 5px 15px 0 rgba(0, 0, 0, 0.05), 0 15px 35px 0 rgba(48, 72, 87, 0.1)', '0 5px 15px 0 rgba(0, 0, 0, 0.01), 0 5px 35px 0 rgba(48, 72, 87, 0.15), 0 50px 100px 0 rgba(48, 72, 87, 0.1)'];

var transitions = ['200ms ease-in-out', '500ms ease-in-out'];

/**
 * Colors
 */
var greys = ['#314756', '#6A7B86', '#87959E', '#BDC5C9', '#D6DBDE', '#EFF1F2', '#F6F7F7', '#FFFFFF'];

var text = greys[0];
var label = greys[1];
var grey = greys[1];
var icon = greys[2];
var muted = greys[3];
var border$1 = greys[4];
var separator = greys[5];
var primaryBackground = greys[6];
var secondaryBackground = greys[7];
var white = greys[7];

var primary = '#49C7BA';
var success = '#3CCF8E';
var danger = '#E25554';
var warning = '#F3951D';
var info = '#6672E5';
var secondary = '#67D4F8';

var color$1 = {
  primary: primary,
  success: success,
  danger: danger,
  warning: warning,
  info: info,
  secondary: secondary,
  text: text,
  grey: grey,
  label: label,
  icon: icon,
  muted: muted,
  border: border$1,
  separator: separator,
  greys: greys,
  primaryBackground: primaryBackground,
  secondaryBackground: secondaryBackground,
  white: white
};

var backgroundColor = _extends$1({}, color$1);

var theme = {
  space: uiSize$1,
  breakpoint: breakpoint,
  uiSize: uiSize$1,
  margin: margin,
  marginBottom: marginBottom,
  marginLeft: marginLeft,
  marginRight: marginRight,
  marginTop: marginTop,
  padding: padding,
  paddingBottom: paddingBottom,
  paddingTop: paddingTop,
  paddingLeft: paddingLeft,
  paddingRight: paddingRight,
  borderRadius: borderRadius$1,
  radii: borderRadius$1,
  fontSizes: fontSize$1,
  fontSize: fontSize$1,
  lineHeight: lineHeight$1,
  fontWeight: fontWeight$1,
  zIndex: zIndex$1,
  elevation: elevation$1,
  transitions: transitions,
  textAlign: textAlign$1,
  color: color$1,
  lineHeights: uiSize$1,
  heights: uiSize$1,
  colors: _extends$1({}, color$1, { greys: greys }),
  backgroundColor: backgroundColor,
  width: width$1,
  height: height$1,
  uiSizes: uiSizes,
  fontWeights: fontWeight$1
};

var _templateObject = taggedTemplateLiteral(['\n\nhtml,\nbody,\ndiv,\nspan,\napplet,\nobject,\niframe,\nh1,\nh2,\nh3,\nh4,\nh5,\nh6,\np,\nblockquote,\npre,\na,\nabbr,\nacronym,\naddress,\nbig,\ncite,\ncode,\ndel,\ndfn,\nem,\nimg,\nins,\nkbd,\nq,\ns,\nsamp,\nsmall,\nstrike,\nstrong,\nsub,\nsup,\ntt,\nvar,\nb,\nu,\ni,\ncenter,\ndl,\ndt,\ndd,\nol,\nul,\nli,\nfieldset,\nform,\nlabel,\nlegend,\ntable,\ncaption,\ntbody,\ntfoot,\nthead,\ntr,\nth,\ntd,\narticle,\naside,\ncanvas,\ndetails,\nembed,\nfigure,\nfigcaption,\nfooter,\nheader,\nhgroup,\nmenu,\nnav,\noutput,\nruby,\nsection,\nsummary,\ntime,\nmark,\naudio,\nvideo {\n  margin: 0;\n  padding: 0;\n  border: 0;\n  vertical-align: baseline;\n}\n/* HTML5 display-role reset for older browsers */\narticle,\naside,\ndetails,\nfigcaption,\nfigure,\nfooter,\nheader,\nhgroup,\nmenu,\nnav,\nsection {\n  display: block;\n}\n\nbody {\n  line-height: 1;\n  font-variant-ligatures: none;\n  text-rendering: optimizeLegibility;\n  -webkit-font-smoothing: antialiased;\n  text-decoration-skip-ink: auto;\n  font-size: 12px;\n  color: #314756;\n}\n\nol,\nul {\n  list-style: none;\n}\n\nblockquote,\nq {\n  quotes: none;\n}\n\na {\n  text-decoration: none;\n}\n\nblockquote:before,\nblockquote:after,\nq:before,\nq:after {\n  content: \'\';\n  content: none;\n}\ntable {\n  border-collapse: collapse;\n  border-spacing: 0;\n}\n\n.ReactVirtualized__Grid {\n  outline: none;\n}\n\n  button {\n      cursor: pointer;\n      outline: none !important;\n  }\n\n  * {\n    font-family: \'Montserrat\', -apple-system, system-ui, Arial, sans-serif;\n    line-height: 1.6;\n    box-sizing: border-box;\n    margin-top: 0;\n  }\n\n'], ['\n\nhtml,\nbody,\ndiv,\nspan,\napplet,\nobject,\niframe,\nh1,\nh2,\nh3,\nh4,\nh5,\nh6,\np,\nblockquote,\npre,\na,\nabbr,\nacronym,\naddress,\nbig,\ncite,\ncode,\ndel,\ndfn,\nem,\nimg,\nins,\nkbd,\nq,\ns,\nsamp,\nsmall,\nstrike,\nstrong,\nsub,\nsup,\ntt,\nvar,\nb,\nu,\ni,\ncenter,\ndl,\ndt,\ndd,\nol,\nul,\nli,\nfieldset,\nform,\nlabel,\nlegend,\ntable,\ncaption,\ntbody,\ntfoot,\nthead,\ntr,\nth,\ntd,\narticle,\naside,\ncanvas,\ndetails,\nembed,\nfigure,\nfigcaption,\nfooter,\nheader,\nhgroup,\nmenu,\nnav,\noutput,\nruby,\nsection,\nsummary,\ntime,\nmark,\naudio,\nvideo {\n  margin: 0;\n  padding: 0;\n  border: 0;\n  vertical-align: baseline;\n}\n/* HTML5 display-role reset for older browsers */\narticle,\naside,\ndetails,\nfigcaption,\nfigure,\nfooter,\nheader,\nhgroup,\nmenu,\nnav,\nsection {\n  display: block;\n}\n\nbody {\n  line-height: 1;\n  font-variant-ligatures: none;\n  text-rendering: optimizeLegibility;\n  -webkit-font-smoothing: antialiased;\n  text-decoration-skip-ink: auto;\n  font-size: 12px;\n  color: #314756;\n}\n\nol,\nul {\n  list-style: none;\n}\n\nblockquote,\nq {\n  quotes: none;\n}\n\na {\n  text-decoration: none;\n}\n\nblockquote:before,\nblockquote:after,\nq:before,\nq:after {\n  content: \'\';\n  content: none;\n}\ntable {\n  border-collapse: collapse;\n  border-spacing: 0;\n}\n\n.ReactVirtualized__Grid {\n  outline: none;\n}\n\n  button {\n      cursor: pointer;\n      outline: none !important;\n  }\n\n  * {\n    font-family: \'Montserrat\', -apple-system, system-ui, Arial, sans-serif;\n    line-height: 1.6;\n    box-sizing: border-box;\n    margin-top: 0;\n  }\n\n']);

/**
 * Global resets
 */
styled.injectGlobal(_templateObject);

var AppContainer = function (_React$Component) {
  inherits(AppContainer, _React$Component);

  function AppContainer() {
    classCallCheck(this, AppContainer);
    return possibleConstructorReturn(this, (AppContainer.__proto__ || Object.getPrototypeOf(AppContainer)).apply(this, arguments));
  }

  createClass(AppContainer, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          theme$$1 = _props.theme;


      return React.createElement(
        styled.ThemeProvider,
        { theme: theme$$1 },
        children
      );
    }
  }]);
  return AppContainer;
}(React.Component);

AppContainer.defaultProps = {
  theme: theme
};
AppContainer.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'AppContainer',
  'props': {
    'theme': {
      'defaultValue': {
        'value': 'defaultTheme',
        'computed': true
      },
      'required': false,
      'flowType': {
        'name': 'signature',
        'type': 'object',
        'raw': '{}',
        'signature': {
          'properties': []
        }
      },
      'description': ''
    },
    'children': {
      'required': true,
      'flowType': {
        'name': 'ReactNode',
        'raw': 'React.Node'
      },
      'description': ''
    }
  }
};

/**
 * A specialized version of `_.forEach` for arrays without support for
 * iteratee shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns `array`.
 */
function arrayEach(array, iteratee) {
  var index = -1,
      length = array == null ? 0 : array.length;

  while (++index < length) {
    if (iteratee(array[index], index, array) === false) {
      break;
    }
  }
  return array;
}

var _arrayEach = arrayEach;

/**
 * Casts `value` to `identity` if it's not a function.
 *
 * @private
 * @param {*} value The value to inspect.
 * @returns {Function} Returns cast function.
 */
function castFunction(value) {
  return typeof value == 'function' ? value : identity_1;
}

var _castFunction = castFunction;

/**
 * Iterates over elements of `collection` and invokes `iteratee` for each element.
 * The iteratee is invoked with three arguments: (value, index|key, collection).
 * Iteratee functions may exit iteration early by explicitly returning `false`.
 *
 * **Note:** As with other "Collections" methods, objects with a "length"
 * property are iterated like arrays. To avoid this behavior use `_.forIn`
 * or `_.forOwn` for object iteration.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @alias each
 * @category Collection
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function} [iteratee=_.identity] The function invoked per iteration.
 * @returns {Array|Object} Returns `collection`.
 * @see _.forEachRight
 * @example
 *
 * _.forEach([1, 2], function(value) {
 *   console.log(value);
 * });
 * // => Logs `1` then `2`.
 *
 * _.forEach({ 'a': 1, 'b': 2 }, function(value, key) {
 *   console.log(key);
 * });
 * // => Logs 'a' then 'b' (iteration order is not guaranteed).
 */
function forEach(collection, iteratee) {
  var func = isArray_1(collection) ? _arrayEach : _baseEach;
  return func(collection, _castFunction(iteratee));
}

var forEach_1 = forEach;

var defineProperty$1 = (function() {
  try {
    var func = _getNative(Object, 'defineProperty');
    func({}, '', {});
    return func;
  } catch (e) {}
}());

var _defineProperty = defineProperty$1;

/**
 * The base implementation of `assignValue` and `assignMergeValue` without
 * value checks.
 *
 * @private
 * @param {Object} object The object to modify.
 * @param {string} key The key of the property to assign.
 * @param {*} value The value to assign.
 */
function baseAssignValue(object, key, value) {
  if (key == '__proto__' && _defineProperty) {
    _defineProperty(object, key, {
      'configurable': true,
      'enumerable': true,
      'value': value,
      'writable': true
    });
  } else {
    object[key] = value;
  }
}

var _baseAssignValue = baseAssignValue;

/** Used for built-in method references. */
var objectProto$c = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty$a = objectProto$c.hasOwnProperty;

/**
 * Assigns `value` to `key` of `object` if the existing value is not equivalent
 * using [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * for equality comparisons.
 *
 * @private
 * @param {Object} object The object to modify.
 * @param {string} key The key of the property to assign.
 * @param {*} value The value to assign.
 */
function assignValue(object, key, value) {
  var objValue = object[key];
  if (!(hasOwnProperty$a.call(object, key) && eq_1(objValue, value)) ||
      (value === undefined && !(key in object))) {
    _baseAssignValue(object, key, value);
  }
}

var _assignValue = assignValue;

/**
 * The base implementation of `_.set`.
 *
 * @private
 * @param {Object} object The object to modify.
 * @param {Array|string} path The path of the property to set.
 * @param {*} value The value to set.
 * @param {Function} [customizer] The function to customize path creation.
 * @returns {Object} Returns `object`.
 */
function baseSet(object, path, value, customizer) {
  if (!isObject_1(object)) {
    return object;
  }
  path = _castPath(path, object);

  var index = -1,
      length = path.length,
      lastIndex = length - 1,
      nested = object;

  while (nested != null && ++index < length) {
    var key = _toKey(path[index]),
        newValue = value;

    if (index != lastIndex) {
      var objValue = nested[key];
      newValue = customizer ? customizer(objValue, key, nested) : undefined;
      if (newValue === undefined) {
        newValue = isObject_1(objValue)
          ? objValue
          : (_isIndex(path[index + 1]) ? [] : {});
      }
    }
    _assignValue(nested, key, newValue);
    nested = nested[key];
  }
  return object;
}

var _baseSet = baseSet;

/**
 * Sets the value at `path` of `object`. If a portion of `path` doesn't exist,
 * it's created. Arrays are created for missing index properties while objects
 * are created for all other missing properties. Use `_.setWith` to customize
 * `path` creation.
 *
 * **Note:** This method mutates `object`.
 *
 * @static
 * @memberOf _
 * @since 3.7.0
 * @category Object
 * @param {Object} object The object to modify.
 * @param {Array|string} path The path of the property to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns `object`.
 * @example
 *
 * var object = { 'a': [{ 'b': { 'c': 3 } }] };
 *
 * _.set(object, 'a[0].b.c', 4);
 * console.log(object.a[0].b.c);
 * // => 4
 *
 * _.set(object, ['x', '0', 'y', 'z'], 5);
 * console.log(object.x[0].y.z);
 * // => 5
 */
function set$1(object, path, value) {
  return object == null ? object : _baseSet(object, path, value);
}

var set_1 = set$1;

// TODO: fix style for component
var AccordionSC = styled__default.div.withConfig({
  displayName: 'styled__AccordionSC',
  componentId: 'pdxbn3-0'
})(['']);

var Title = styled__default.div.withConfig({
  displayName: 'styled__Title',
  componentId: 'pdxbn3-1'
})(['padding:', 'px;border-bottom:1px solid ', ';transition:all 200ms 200ms ease-in-out;cursor:pointer;user-select:none;&:hover{color:#49c9bc;}', ';'], function (props) {
  return props.theme.padding[3];
}, function (props) {
  return props.theme.color.border;
}, function (props) {
  return props.toggled && styled.css(['background:#eef9f8;color:#49c9bc;border-color:#7bc7c7;box-shadow:0px -1px 0px #7bc7c7;']);
});

var AccordionBody = styled__default.div.withConfig({
  displayName: 'styled__AccordionBody',
  componentId: 'pdxbn3-2'
})(['height:0px;transition:height 200ms ease;overflow:hidden;margin-top:-1px;', ';'], function (props) {
  return props.toggled && styled.css(['height:', 'px;'], props.height);
});

var Item = styled__default.div.withConfig({
  displayName: 'styled__Item',
  componentId: 'pdxbn3-3'
})(['border-bottom:1px solid ', ';width:100%;', ';'], function (props) {
  return props.theme.color.border;
}, function (props) {
  return props.last && styled.css(['border-bottom:none;']);
});

var Accordion = function (_React$Component) {
  inherits(Accordion, _React$Component);

  function Accordion() {
    var _ref;

    var _temp, _this, _ret;

    classCallCheck(this, Accordion);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = possibleConstructorReturn(this, (_ref = Accordion.__proto__ || Object.getPrototypeOf(Accordion)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      toggled: {},
      total: 0
    }, _this.buildState = function (items) {
      var toggled = {};

      forEach_1(items, function (obj) {
        var initalValue = obj.startToggled || false;
        set_1(toggled, '' + obj.id, initalValue);
      });

      _this.setState({
        toggled: toggled,
        total: items.length
      });
    }, _this.toggleItem = function (id) {
      var forceSingleToggle = _this.props.forceSingleToggle;


      _this.setState(function (_ref2) {
        var toggled = _ref2.toggled;

        var nextToggled = _extends$1({}, toggled);

        /**
         * If forceSingleToggle is enabled,
         * only allow one item to be toggled open.
         */
        if (forceSingleToggle) {
          forEach_1(Object.keys(nextToggled), function (key, i) {
            nextToggled[key] = false;
          });
        }

        set_1(nextToggled, '' + id, !get_1(toggled, id));

        return {
          toggled: nextToggled
        };
      });
    }, _temp), possibleConstructorReturn(_this, _ret);
  }

  createClass(Accordion, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var items = this.props.items;


      if (items.length) {
        this.buildState(items);
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          items = _props.items,
          bg = _props.bg;
      var _state = this.state,
          toggled = _state.toggled,
          total = _state.total;


      return React.createElement(
        AccordionSC,
        null,
        React.createElement(
          Box,
          { bg: bg, p: 0, elevation: 2 },
          map_1(items, function (obj, i) {
            return React.createElement(
              Item,
              { last: total === i + 1, key: obj.id },
              React.createElement(
                Title,
                { onClick: function onClick() {
                    return _this2.toggleItem(obj.id);
                  }, toggled: toggled[obj.id] },
                obj.title
              ),
              React.createElement(
                AccordionBody,
                { height: obj.height, toggled: toggled[obj.id] },
                obj.render()
              )
            );
          })
        )
      );
    }
  }]);
  return Accordion;
}(React.Component);

Accordion.defaultProps = {
  forceSingleToggle: true,
  bg: '#fff'
};
Accordion.__docgenInfo = {
  'description': '',
  'methods': [{
    'name': 'buildState',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'items',
      'type': {
        'name': 'any'
      }
    }],
    'returns': null
  }, {
    'name': 'toggleItem',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'id',
      'type': {
        'name': 'any'
      }
    }],
    'returns': null
  }],
  'displayName': 'Accordion',
  'props': {
    'forceSingleToggle': {
      'defaultValue': {
        'value': 'true',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'boolean'
      },
      'description': ''
    },
    'bg': {
      'defaultValue': {
        'value': '\'#fff\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'items': {
      'required': true,
      'flowType': {
        'name': 'tuple',
        'raw': '[ItemType]',
        'elements': [{
          'name': 'signature',
          'type': 'object',
          'raw': '{\n  height: number,\n  id: string,\n  title: string,\n  render: () => {},\n  startToggled: boolean,\n}',
          'signature': {
            'properties': [{
              'key': 'height',
              'value': {
                'name': 'number',
                'required': true
              }
            }, {
              'key': 'id',
              'value': {
                'name': 'string',
                'required': true
              }
            }, {
              'key': 'title',
              'value': {
                'name': 'string',
                'required': true
              }
            }, {
              'key': 'render',
              'value': {
                'name': 'signature',
                'type': 'function',
                'raw': '() => {}',
                'signature': {
                  'arguments': [],
                  'return': {
                    'name': 'signature',
                    'type': 'object',
                    'raw': '{}',
                    'signature': {
                      'properties': []
                    }
                  }
                },
                'required': true
              }
            }, {
              'key': 'startToggled',
              'value': {
                'name': 'boolean',
                'required': true
              }
            }]
          }
        }]
      },
      'description': ''
    }
  }
};

//      

var _extends$2 = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};



























var taggedTemplateLiteralLoose$1 = function (strings, raw) {
  strings.raw = raw;
  return strings;
};

var _opinionatedRules;
var _abbrTitle;
var _unopinionatedRules;

//      
var opinionatedRules = (_opinionatedRules = {
  html: {
    fontFamily: 'sans-serif'
  },

  body: {
    margin: '0'
  }

}, _opinionatedRules['a:active,\n  a:hover'] = {
  outlineWidth: '0'
}, _opinionatedRules['button,\n  input,\n  optgroup,\n  select,\n  textarea'] = {
  fontFamily: 'sans-serif',
  fontSize: '100%',
  lineHeight: '1.15'
}, _opinionatedRules);

var unopinionatedRules = (_unopinionatedRules = {
  html: {
    lineHeight: '1.15',
    textSizeAdjust: '100%'
  }

}, _unopinionatedRules['article,\n  aside,\n  footer,\n  header,\n  nav,\n  section'] = {
  display: 'block'
}, _unopinionatedRules.h1 = {
  fontSize: '2em',
  margin: '0.67em 0'
}, _unopinionatedRules['figcaption,\n  figure,\n  main'] = {
  display: 'block'
}, _unopinionatedRules.figure = {
  margin: '1em 40px'
}, _unopinionatedRules.hr = {
  boxSizing: 'content-box',
  height: '0',
  overflow: 'visible'
}, _unopinionatedRules.pre = {
  fontFamily: 'monospace, monospace',
  fontSize: '1em'
}, _unopinionatedRules.a = {
  'background-color': 'transparent',
  '-webkit-text-decoration-skip': 'objects'
}, _unopinionatedRules['abbr[title]'] = (_abbrTitle = {
  borderBottom: 'none',
  textDecoration: 'underline'
}, _abbrTitle['textDecoration'] = 'underline dotted', _abbrTitle), _unopinionatedRules['b,\n  strong'] = {
  fontWeight: 'inherit'
}, _unopinionatedRules['code,\n  kbd,\n  samp'] = {
  fontFamily: 'monospace, monospace',
  fontSize: '1em'
}, _unopinionatedRules.dfn = {
  fontStyle: 'italic'
}, _unopinionatedRules.mark = {
  backgroundColor: '#ff0',
  color: '#000'
}, _unopinionatedRules.small = {
  fontSize: '80%'
}, _unopinionatedRules['sub,\n  sup'] = {
  fontSize: '75%',
  lineHeight: '0',
  position: 'relative',
  verticalAlign: 'baseline'
}, _unopinionatedRules.sub = {
  bottom: '-0.25em'
}, _unopinionatedRules.sup = {
  top: '-0.5em'
}, _unopinionatedRules['audio,\n  video'] = {
  display: 'inline-block'
}, _unopinionatedRules['audio:not([controls])'] = {
  display: 'none',
  height: '0'
}, _unopinionatedRules.img = {
  borderStyle: 'none'
}, _unopinionatedRules['svg:not(:root)'] = {
  overflow: 'hidden'
}, _unopinionatedRules['button,\n  input,\n  optgroup,\n  select,\n  textarea'] = {
  margin: '0'
}, _unopinionatedRules['button,\n  input'] = {
  overflow: 'visible'
}, _unopinionatedRules['button,\n  select'] = {
  textTransform: 'none'
}, _unopinionatedRules['button,\n  html [type="button"],\n  [type="reset"],\n  [type="submit"]'] = {
  '-webkit-appearance': 'button'
}, _unopinionatedRules['button::-moz-focus-inner,\n  [type="button"]::-moz-focus-inner,\n  [type="reset"]::-moz-focus-inner,\n  [type="submit"]::-moz-focus-inner'] = {
  borderStyle: 'none',
  padding: '0'
}, _unopinionatedRules['button:-moz-focusring,\n  [type="button"]:-moz-focusring,\n  [type="reset"]:-moz-focusring,\n  [type="submit"]:-moz-focusring'] = {
  outline: '1px dotted ButtonText'
}, _unopinionatedRules.fieldset = {
  border: '1px solid #c0c0c0',
  margin: '0 2px',
  padding: '0.35em 0.625em 0.75em'
}, _unopinionatedRules.legend = {
  boxSizing: 'border-box',
  color: 'inherit',
  display: 'table',
  maxWidth: '100%',
  padding: '0',
  whiteSpace: 'normal'
}, _unopinionatedRules.progress = {
  display: 'inline-block',
  verticalAlign: 'baseline'
}, _unopinionatedRules.textarea = {
  overflow: 'auto'
}, _unopinionatedRules['[type="checkbox"],\n  [type="radio"]'] = {
  boxSizing: 'border-box',
  padding: '0'
}, _unopinionatedRules['[type="number"]::-webkit-inner-spin-button,\n  [type="number"]::-webkit-outer-spin-button'] = {
  height: 'auto'
}, _unopinionatedRules['[type="search"]'] = {
  '-webkit-appearance': 'textfield',
  outlineOffset: '-2px'
}, _unopinionatedRules['[type="search"]::-webkit-search-cancel-button,\n  [type="search"]::-webkit-search-decoration'] = {
  '-webkit-appearance': 'none'
}, _unopinionatedRules['::-webkit-file-upload-button'] = {
  '-webkit-appearance': 'button',
  font: 'inherit'
}, _unopinionatedRules['details,\n  menu'] = {
  display: 'block'
}, _unopinionatedRules.summary = {
  display: 'list-item'
}, _unopinionatedRules.canvas = {
  display: 'inline-block'
}, _unopinionatedRules.template = {
  display: 'none'
}, _unopinionatedRules['[hidden]'] = {
  display: 'none'
}, _unopinionatedRules);

var _templateObject$1 = /*#__PURE__*/ taggedTemplateLiteralLoose$1(['radial-gradient(', '', '', '', ')'], ['radial-gradient(', '', '', '', ')']);

//      


function colorToInt(color) {
  return Math.round(color * 255);
}

function convertToInt(red, green, blue) {
  return colorToInt(red) + "," + colorToInt(green) + "," + colorToInt(blue);
}

function hslToRgb(hue, saturation, lightness) {
  var convert = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : convertToInt;

  if (saturation === 0) {
    // achromatic
    return convert(lightness, lightness, lightness);
  }

  // formular from https://en.wikipedia.org/wiki/HSL_and_HSV
  var huePrime = hue % 360 / 60;
  var chroma = (1 - Math.abs(2 * lightness - 1)) * saturation;
  var secondComponent = chroma * (1 - Math.abs(huePrime % 2 - 1));

  var red = 0;
  var green = 0;
  var blue = 0;

  if (huePrime >= 0 && huePrime < 1) {
    red = chroma;
    green = secondComponent;
  } else if (huePrime >= 1 && huePrime < 2) {
    red = secondComponent;
    green = chroma;
  } else if (huePrime >= 2 && huePrime < 3) {
    green = chroma;
    blue = secondComponent;
  } else if (huePrime >= 3 && huePrime < 4) {
    green = secondComponent;
    blue = chroma;
  } else if (huePrime >= 4 && huePrime < 5) {
    red = secondComponent;
    blue = chroma;
  } else if (huePrime >= 5 && huePrime < 6) {
    red = chroma;
    blue = secondComponent;
  }

  var lightnessModification = lightness - chroma / 2;
  var finalRed = red + lightnessModification;
  var finalGreen = green + lightnessModification;
  var finalBlue = blue + lightnessModification;
  return convert(finalRed, finalGreen, finalBlue);
}

//      
var namedColorMap = {
  aliceblue: 'f0f8ff',
  antiquewhite: 'faebd7',
  aqua: '00ffff',
  aquamarine: '7fffd4',
  azure: 'f0ffff',
  beige: 'f5f5dc',
  bisque: 'ffe4c4',
  black: '000',
  blanchedalmond: 'ffebcd',
  blue: '0000ff',
  blueviolet: '8a2be2',
  brown: 'a52a2a',
  burlywood: 'deb887',
  cadetblue: '5f9ea0',
  chartreuse: '7fff00',
  chocolate: 'd2691e',
  coral: 'ff7f50',
  cornflowerblue: '6495ed',
  cornsilk: 'fff8dc',
  crimson: 'dc143c',
  cyan: '00ffff',
  darkblue: '00008b',
  darkcyan: '008b8b',
  darkgoldenrod: 'b8860b',
  darkgray: 'a9a9a9',
  darkgreen: '006400',
  darkgrey: 'a9a9a9',
  darkkhaki: 'bdb76b',
  darkmagenta: '8b008b',
  darkolivegreen: '556b2f',
  darkorange: 'ff8c00',
  darkorchid: '9932cc',
  darkred: '8b0000',
  darksalmon: 'e9967a',
  darkseagreen: '8fbc8f',
  darkslateblue: '483d8b',
  darkslategray: '2f4f4f',
  darkslategrey: '2f4f4f',
  darkturquoise: '00ced1',
  darkviolet: '9400d3',
  deeppink: 'ff1493',
  deepskyblue: '00bfff',
  dimgray: '696969',
  dimgrey: '696969',
  dodgerblue: '1e90ff',
  firebrick: 'b22222',
  floralwhite: 'fffaf0',
  forestgreen: '228b22',
  fuchsia: 'ff00ff',
  gainsboro: 'dcdcdc',
  ghostwhite: 'f8f8ff',
  gold: 'ffd700',
  goldenrod: 'daa520',
  gray: '808080',
  green: '008000',
  greenyellow: 'adff2f',
  grey: '808080',
  honeydew: 'f0fff0',
  hotpink: 'ff69b4',
  indianred: 'cd5c5c',
  indigo: '4b0082',
  ivory: 'fffff0',
  khaki: 'f0e68c',
  lavender: 'e6e6fa',
  lavenderblush: 'fff0f5',
  lawngreen: '7cfc00',
  lemonchiffon: 'fffacd',
  lightblue: 'add8e6',
  lightcoral: 'f08080',
  lightcyan: 'e0ffff',
  lightgoldenrodyellow: 'fafad2',
  lightgray: 'd3d3d3',
  lightgreen: '90ee90',
  lightgrey: 'd3d3d3',
  lightpink: 'ffb6c1',
  lightsalmon: 'ffa07a',
  lightseagreen: '20b2aa',
  lightskyblue: '87cefa',
  lightslategray: '789',
  lightslategrey: '789',
  lightsteelblue: 'b0c4de',
  lightyellow: 'ffffe0',
  lime: '0f0',
  limegreen: '32cd32',
  linen: 'faf0e6',
  magenta: 'f0f',
  maroon: '800000',
  mediumaquamarine: '66cdaa',
  mediumblue: '0000cd',
  mediumorchid: 'ba55d3',
  mediumpurple: '9370db',
  mediumseagreen: '3cb371',
  mediumslateblue: '7b68ee',
  mediumspringgreen: '00fa9a',
  mediumturquoise: '48d1cc',
  mediumvioletred: 'c71585',
  midnightblue: '191970',
  mintcream: 'f5fffa',
  mistyrose: 'ffe4e1',
  moccasin: 'ffe4b5',
  navajowhite: 'ffdead',
  navy: '000080',
  oldlace: 'fdf5e6',
  olive: '808000',
  olivedrab: '6b8e23',
  orange: 'ffa500',
  orangered: 'ff4500',
  orchid: 'da70d6',
  palegoldenrod: 'eee8aa',
  palegreen: '98fb98',
  paleturquoise: 'afeeee',
  palevioletred: 'db7093',
  papayawhip: 'ffefd5',
  peachpuff: 'ffdab9',
  peru: 'cd853f',
  pink: 'ffc0cb',
  plum: 'dda0dd',
  powderblue: 'b0e0e6',
  purple: '800080',
  rebeccapurple: '639',
  red: 'f00',
  rosybrown: 'bc8f8f',
  royalblue: '4169e1',
  saddlebrown: '8b4513',
  salmon: 'fa8072',
  sandybrown: 'f4a460',
  seagreen: '2e8b57',
  seashell: 'fff5ee',
  sienna: 'a0522d',
  silver: 'c0c0c0',
  skyblue: '87ceeb',
  slateblue: '6a5acd',
  slategray: '708090',
  slategrey: '708090',
  snow: 'fffafa',
  springgreen: '00ff7f',
  steelblue: '4682b4',
  tan: 'd2b48c',
  teal: '008080',
  thistle: 'd8bfd8',
  tomato: 'ff6347',
  turquoise: '40e0d0',
  violet: 'ee82ee',
  wheat: 'f5deb3',
  white: 'fff',
  whitesmoke: 'f5f5f5',
  yellow: 'ff0',
  yellowgreen: '9acd32'

  /**
   * Checks if a string is a CSS named color and returns its equivalent hex value, otherwise returns the original color.
   * @private
   */
};function nameToHex(color) {
  if (typeof color !== 'string') return color;
  var normalizedColorName = color.toLowerCase();
  return namedColorMap[normalizedColorName] ? '#' + namedColorMap[normalizedColorName] : color;
}

//      
var hexRegex = /^#[a-fA-F0-9]{6}$/;
var reducedHexRegex = /^#[a-fA-F0-9]{3}$/;
var rgbRegex = /^rgb\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)$/;
var rgbaRegex = /^rgba\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*([-+]?[0-9]*[.]?[0-9]+)\s*\)$/;
var hslRegex = /^hsl\(\s*(\d{1,3})\s*,\s*(\d{1,3})%\s*,\s*(\d{1,3})%\s*\)$/;
var hslaRegex = /^hsla\(\s*(\d{1,3})\s*,\s*(\d{1,3})%\s*,\s*(\d{1,3})%\s*,\s*([-+]?[0-9]*[.]?[0-9]+)\s*\)$/;

/**
 * Returns an RgbColor or RgbaColor object. This utility function is only useful
 * if want to extract a color component. With the color util `toColorString` you
 * can convert a RgbColor or RgbaColor object back to a string.
 *
 * @example
 * // Assigns `{ red: 255, green: 0, blue: 0 }` to color1
 * const color1 = 'rgb(255, 0, 0)';
 * // Assigns `{ red: 92, green: 102, blue: 112, alpha: 0.75 }` to color2
 * const color2 = 'hsla(210, 10%, 40%, 0.75)';
 */
function parseToRgb(color) {
  if (typeof color !== 'string') {
    throw new Error('Passed an incorrect argument to a color function, please pass a string representation of a color.');
  }
  var normalizedColor = nameToHex(color);
  if (normalizedColor.match(hexRegex)) {
    return {
      red: parseInt('' + normalizedColor[1] + normalizedColor[2], 16),
      green: parseInt('' + normalizedColor[3] + normalizedColor[4], 16),
      blue: parseInt('' + normalizedColor[5] + normalizedColor[6], 16)
    };
  }
  if (normalizedColor.match(reducedHexRegex)) {
    return {
      red: parseInt('' + normalizedColor[1] + normalizedColor[1], 16),
      green: parseInt('' + normalizedColor[2] + normalizedColor[2], 16),
      blue: parseInt('' + normalizedColor[3] + normalizedColor[3], 16)
    };
  }
  var rgbMatched = rgbRegex.exec(normalizedColor);
  if (rgbMatched) {
    return {
      red: parseInt('' + rgbMatched[1], 10),
      green: parseInt('' + rgbMatched[2], 10),
      blue: parseInt('' + rgbMatched[3], 10)
    };
  }
  var rgbaMatched = rgbaRegex.exec(normalizedColor);
  if (rgbaMatched) {
    return {
      red: parseInt('' + rgbaMatched[1], 10),
      green: parseInt('' + rgbaMatched[2], 10),
      blue: parseInt('' + rgbaMatched[3], 10),
      alpha: parseFloat('' + rgbaMatched[4])
    };
  }
  var hslMatched = hslRegex.exec(normalizedColor);
  if (hslMatched) {
    var hue = parseInt('' + hslMatched[1], 10);
    var saturation = parseInt('' + hslMatched[2], 10) / 100;
    var lightness = parseInt('' + hslMatched[3], 10) / 100;
    var rgbColorString = 'rgb(' + hslToRgb(hue, saturation, lightness) + ')';
    var hslRgbMatched = rgbRegex.exec(rgbColorString);
    if (!hslRgbMatched) {
      throw new Error('Couldn\'t generate valid rgb string from ' + normalizedColor + ', it returned ' + rgbColorString + '.');
    }
    return {
      red: parseInt('' + hslRgbMatched[1], 10),
      green: parseInt('' + hslRgbMatched[2], 10),
      blue: parseInt('' + hslRgbMatched[3], 10)
    };
  }
  var hslaMatched = hslaRegex.exec(normalizedColor);
  if (hslaMatched) {
    var _hue = parseInt('' + hslaMatched[1], 10);
    var _saturation = parseInt('' + hslaMatched[2], 10) / 100;
    var _lightness = parseInt('' + hslaMatched[3], 10) / 100;
    var _rgbColorString = 'rgb(' + hslToRgb(_hue, _saturation, _lightness) + ')';
    var _hslRgbMatched = rgbRegex.exec(_rgbColorString);
    if (!_hslRgbMatched) {
      throw new Error('Couldn\'t generate valid rgb string from ' + normalizedColor + ', it returned ' + _rgbColorString + '.');
    }
    return {
      red: parseInt('' + _hslRgbMatched[1], 10),
      green: parseInt('' + _hslRgbMatched[2], 10),
      blue: parseInt('' + _hslRgbMatched[3], 10),
      alpha: parseFloat('' + hslaMatched[4])
    };
  }
  throw new Error("Couldn't parse the color string. Please provide the color as a string in hex, rgb, rgba, hsl or hsla notation.");
}

//      


function rgbToHsl(color) {
  // make sure rgb are contained in a set of [0, 255]
  var red = color.red / 255;
  var green = color.green / 255;
  var blue = color.blue / 255;

  var max = Math.max(red, green, blue);
  var min = Math.min(red, green, blue);
  var lightness = (max + min) / 2;

  if (max === min) {
    // achromatic
    if (color.alpha !== undefined) {
      return {
        hue: 0,
        saturation: 0,
        lightness: lightness,
        alpha: color.alpha
      };
    } else {
      return { hue: 0, saturation: 0, lightness: lightness };
    }
  }

  var hue = void 0;
  var delta = max - min;
  var saturation = lightness > 0.5 ? delta / (2 - max - min) : delta / (max + min);
  switch (max) {
    case red:
      hue = (green - blue) / delta + (green < blue ? 6 : 0);
      break;
    case green:
      hue = (blue - red) / delta + 2;
      break;
    default:
      // blue case
      hue = (red - green) / delta + 4;
      break;
  }

  hue *= 60;
  if (color.alpha !== undefined) {
    return {
      hue: hue,
      saturation: saturation,
      lightness: lightness,
      alpha: color.alpha
    };
  }
  return { hue: hue, saturation: saturation, lightness: lightness };
}

//      

/**
 * Returns an HslColor or HslaColor object. This utility function is only useful
 * if want to extract a color component. With the color util `toColorString` you
 * can convert a HslColor or HslaColor object back to a string.
 *
 * @example
 * // Assigns `{ red: 255, green: 0, blue: 0 }` to color1
 * const color1 = 'rgb(255, 0, 0)';
 * // Assigns `{ red: 92, green: 102, blue: 112, alpha: 0.75 }` to color2
 * const color2 = 'hsla(210, 10%, 40%, 0.75)';
 */
function parseToHsl(color) {
  // Note: At a later stage we can optimize this function as right now a hsl
  // color would be parsed converted to rgb values and converted back to hsl.
  return rgbToHsl(parseToRgb(color));
}

//      

/**
 * Reduces hex values if possible e.g. #ff8866 to #f86
 * @private
 */
var reduceHexValue = function reduceHexValue(value) {
  if (value.length === 7 && value[1] === value[2] && value[3] === value[4] && value[5] === value[6]) {
    return "#" + value[1] + value[3] + value[5];
  }
  return value;
};

//      
function numberToHex(value) {
  var hex = value.toString(16);
  return hex.length === 1 ? "0" + hex : hex;
}

//      

/**
 * Returns a string value for the color. The returned result is the smallest possible hex notation.
 *
 * @example
 * // Styles as object usage
 * const styles = {
 *   background: rgb(255, 205, 100),
 *   background: rgb({ red: 255, green: 205, blue: 100 }),
 * }
 *
 * // styled-components usage
 * const div = styled.div`
 *   background: ${rgb(255, 205, 100)};
 *   background: ${rgb({ red: 255, green: 205, blue: 100 })};
 * `
 *
 * // CSS in JS Output
 *
 * element {
 *   background: "#ffcd64";
 *   background: "#ffcd64";
 * }
 */
function rgb(value, green, blue) {
  if (typeof value === 'number' && typeof green === 'number' && typeof blue === 'number') {
    return reduceHexValue('#' + numberToHex(value) + numberToHex(green) + numberToHex(blue));
  } else if (typeof value === 'object' && green === undefined && blue === undefined) {
    return reduceHexValue('#' + numberToHex(value.red) + numberToHex(value.green) + numberToHex(value.blue));
  }

  throw new Error('Passed invalid arguments to rgb, please pass multiple numbers e.g. rgb(255, 205, 100) or an object e.g. rgb({ red: 255, green: 205, blue: 100 }).');
}

//      

/**
 * Returns a string value for the color. The returned result is the smallest possible rgba or hex notation.
 *
 * Can also be used to fade a color by passing a hex value or named CSS color along with an alpha value.
 *
 * @example
 * // Styles as object usage
 * const styles = {
 *   background: rgba(255, 205, 100, 0.7),
 *   background: rgba({ red: 255, green: 205, blue: 100, alpha: 0.7 }),
 *   background: rgba(255, 205, 100, 1),
 *   background: rgba('#ffffff', 0.4),
 *   background: rgba('black', 0.7),
 * }
 *
 * // styled-components usage
 * const div = styled.div`
 *   background: ${rgba(255, 205, 100, 0.7)};
 *   background: ${rgba({ red: 255, green: 205, blue: 100, alpha: 0.7 })};
 *   background: ${rgba(255, 205, 100, 1)};
 *   background: ${rgba('#ffffff', 0.4)};
 *   background: ${rgba('black', 0.7)};
 * `
 *
 * // CSS in JS Output
 *
 * element {
 *   background: "rgba(255,205,100,0.7)";
 *   background: "rgba(255,205,100,0.7)";
 *   background: "#ffcd64";
 *   background: "rgba(255,255,255,0.4)";
 *   background: "rgba(0,0,0,0.7)";
 * }
 */
function rgba(firstValue, secondValue, thirdValue, fourthValue) {
  if (typeof firstValue === 'string' && typeof secondValue === 'number') {
    var rgbValue = parseToRgb(firstValue);
    return 'rgba(' + rgbValue.red + ',' + rgbValue.green + ',' + rgbValue.blue + ',' + secondValue + ')';
  } else if (typeof firstValue === 'number' && typeof secondValue === 'number' && typeof thirdValue === 'number' && typeof fourthValue === 'number') {
    return fourthValue >= 1 ? rgb(firstValue, secondValue, thirdValue) : 'rgba(' + firstValue + ',' + secondValue + ',' + thirdValue + ',' + fourthValue + ')';
  } else if (typeof firstValue === 'object' && secondValue === undefined && thirdValue === undefined && fourthValue === undefined) {
    return firstValue.alpha >= 1 ? rgb(firstValue.red, firstValue.green, firstValue.blue) : 'rgba(' + firstValue.red + ',' + firstValue.green + ',' + firstValue.blue + ',' + firstValue.alpha + ')';
  }

  throw new Error('Passed invalid arguments to rgba, please pass multiple numbers e.g. rgb(255, 205, 100, 0.75) or an object e.g. rgb({ red: 255, green: 205, blue: 100, alpha: 0.75 }).');
}

//      
function colorToHex(color) {
  return numberToHex(Math.round(color * 255));
}

function convertToHex(red, green, blue) {
  return reduceHexValue('#' + colorToHex(red) + colorToHex(green) + colorToHex(blue));
}

function hslToHex(hue, saturation, lightness) {
  return hslToRgb(hue, saturation, lightness, convertToHex);
}

//      

/**
 * Returns a string value for the color. The returned result is the smallest possible hex notation.
 *
 * @example
 * // Styles as object usage
 * const styles = {
 *   background: hsl(359, 0.75, 0.4),
 *   background: hsl({ hue: 360, saturation: 0.75, lightness: 0.4 }),
 * }
 *
 * // styled-components usage
 * const div = styled.div`
 *   background: ${hsl(359, 0.75, 0.4)};
 *   background: ${hsl({ hue: 360, saturation: 0.75, lightness: 0.4 })};
 * `
 *
 * // CSS in JS Output
 *
 * element {
 *   background: "#b3191c";
 *   background: "#b3191c";
 * }
 */
function hsl(value, saturation, lightness) {
  if (typeof value === 'number' && typeof saturation === 'number' && typeof lightness === 'number') {
    return hslToHex(value, saturation, lightness);
  } else if (typeof value === 'object' && saturation === undefined && lightness === undefined) {
    return hslToHex(value.hue, value.saturation, value.lightness);
  }

  throw new Error('Passed invalid arguments to hsl, please pass multiple numbers e.g. hsl(360, 0.75, 0.4) or an object e.g. rgb({ hue: 255, saturation: 0.4, lightness: 0.75 }).');
}

//      

/**
 * Returns a string value for the color. The returned result is the smallest possible rgba or hex notation.
 *
 * @example
 * // Styles as object usage
 * const styles = {
 *   background: hsla(359, 0.75, 0.4, 0.7),
 *   background: hsla({ hue: 360, saturation: 0.75, lightness: 0.4, alpha: 0,7 }),
 *   background: hsla(359, 0.75, 0.4, 1),
 * }
 *
 * // styled-components usage
 * const div = styled.div`
 *   background: ${hsla(359, 0.75, 0.4, 0.7)};
 *   background: ${hsla({ hue: 360, saturation: 0.75, lightness: 0.4, alpha: 0,7 })};
 *   background: ${hsla(359, 0.75, 0.4, 1)};
 * `
 *
 * // CSS in JS Output
 *
 * element {
 *   background: "rgba(179,25,28,0.7)";
 *   background: "rgba(179,25,28,0.7)";
 *   background: "#b3191c";
 * }
 */
function hsla(value, saturation, lightness, alpha) {
  if (typeof value === 'number' && typeof saturation === 'number' && typeof lightness === 'number' && typeof alpha === 'number') {
    return alpha >= 1 ? hslToHex(value, saturation, lightness) : 'rgba(' + hslToRgb(value, saturation, lightness) + ',' + alpha + ')';
  } else if (typeof value === 'object' && saturation === undefined && lightness === undefined && alpha === undefined) {
    return value.alpha >= 1 ? hslToHex(value.hue, value.saturation, value.lightness) : 'rgba(' + hslToRgb(value.hue, value.saturation, value.lightness) + ',' + value.alpha + ')';
  }

  throw new Error('Passed invalid arguments to hsla, please pass multiple numbers e.g. hsl(360, 0.75, 0.4, 0.7) or an object e.g. rgb({ hue: 255, saturation: 0.4, lightness: 0.75, alpha: 0.7 }).');
}

//      
var isRgb = function isRgb(color) {
  return typeof color.red === 'number' && typeof color.green === 'number' && typeof color.blue === 'number' && (typeof color.alpha !== 'number' || typeof color.alpha === 'undefined');
};

var isRgba = function isRgba(color) {
  return typeof color.red === 'number' && typeof color.green === 'number' && typeof color.blue === 'number' && typeof color.alpha === 'number';
};

var isHsl = function isHsl(color) {
  return typeof color.hue === 'number' && typeof color.saturation === 'number' && typeof color.lightness === 'number' && (typeof color.alpha !== 'number' || typeof color.alpha === 'undefined');
};

var isHsla = function isHsla(color) {
  return typeof color.hue === 'number' && typeof color.saturation === 'number' && typeof color.lightness === 'number' && typeof color.alpha === 'number';
};

var errMsg = 'Passed invalid argument to toColorString, please pass a RgbColor, RgbaColor, HslColor or HslaColor object.';

/**
 * Converts a RgbColor, RgbaColor, HslColor or HslaColor object to a color string.
 * This util is useful in case you only know on runtime which color object is
 * used. Otherwise we recommend to rely on `rgb`, `rgba`, `hsl` or `hsla`.
 *
 * @example
 * // Styles as object usage
 * const styles = {
 *   background: toColorString({ red: 255, green: 205, blue: 100 }),
 *   background: toColorString({ red: 255, green: 205, blue: 100, alpha: 0.72 }),
 *   background: toColorString({ hue: 240, saturation: 1, lightness: 0.5 }),
 *   background: toColorString({ hue: 360, saturation: 0.75, lightness: 0.4, alpha: 0.72 }),
 * }
 *
 * // styled-components usage
 * const div = styled.div`
 *   background: ${toColorString({ red: 255, green: 205, blue: 100 })};
 *   background: ${toColorString({ red: 255, green: 205, blue: 100, alpha: 0.72 })};
 *   background: ${toColorString({ hue: 240, saturation: 1, lightness: 0.5 })};
 *   background: ${toColorString({ hue: 360, saturation: 0.75, lightness: 0.4, alpha: 0.72 })};
 * `
 *
 * // CSS in JS Output
 * element {
 *   background: "#ffcd64";
 *   background: "rgba(255,205,100,0.72)";
 *   background: "#00f";
 *   background: "rgba(179,25,25,0.72)";
 * }
 */

function toColorString(color) {
  if (typeof color !== 'object') throw new Error(errMsg);
  if (isRgba(color)) return rgba(color);
  if (isRgb(color)) return rgb(color);
  if (isHsla(color)) return hsla(color);
  if (isHsl(color)) return hsl(color);

  throw new Error(errMsg);
}

//      

// Type definitions taken from https://github.com/gcanti/flow-static-land/blob/master/src/Fun.js


// eslint-disable-next-line no-unused-vars


// eslint-disable-next-line no-unused-vars

// eslint-disable-next-line no-redeclare


function curried(f, length, acc) {
  return function fn() {
    // eslint-disable-next-line prefer-rest-params
    var combined = acc.concat(Array.prototype.slice.call(arguments));
    return combined.length >= length ? f.apply(this, combined) : curried(f, length, combined);
  };
}

// eslint-disable-next-line no-redeclare
function curry(f) {
  // eslint-disable-line no-redeclare
  return curried(f, f.length, []);
}

//      

/**
 * Changes the hue of the color. Hue is a number between 0 to 360. The first
 * argument for adjustHue is the amount of degrees the color is rotated along
 * the color wheel.
 *
 * @example
 * // Styles as object usage
 * const styles = {
 *   background: adjustHue(180, '#448'),
 *   background: adjustHue(180, 'rgba(101,100,205,0.7)'),
 * }
 *
 * // styled-components usage
 * const div = styled.div`
 *   background: ${adjustHue(180, '#448')};
 *   background: ${adjustHue(180, 'rgba(101,100,205,0.7)')};
 * `
 *
 * // CSS in JS Output
 * element {
 *   background: "#888844";
 *   background: "rgba(136,136,68,0.7)";
 * }
 */
function adjustHue(degree, color) {
  var hslColor = parseToHsl(color);
  return toColorString(_extends$2({}, hslColor, {
    hue: (hslColor.hue + degree) % 360
  }));
}

var curriedAdjustHue = /*#__PURE__*/curry(adjustHue);

//      

function guard(lowerBoundary, upperBoundary, value) {
  return Math.max(lowerBoundary, Math.min(upperBoundary, value));
}

//      

/**
 * Returns a string value for the darkened color.
 *
 * @example
 * // Styles as object usage
 * const styles = {
 *   background: darken(0.2, '#FFCD64'),
 *   background: darken(0.2, 'rgba(255,205,100,0.7)'),
 * }
 *
 * // styled-components usage
 * const div = styled.div`
 *   background: ${darken(0.2, '#FFCD64')};
 *   background: ${darken(0.2, 'rgba(255,205,100,0.7)')};
 * `
 *
 * // CSS in JS Output
 *
 * element {
 *   background: "#ffbd31";
 *   background: "rgba(255,189,49,0.7)";
 * }
 */
function darken(amount, color) {
  var hslColor = parseToHsl(color);
  return toColorString(_extends$2({}, hslColor, {
    lightness: guard(0, 1, hslColor.lightness - amount)
  }));
}

var curriedDarken = /*#__PURE__*/curry(darken);

//      

/**
 * Decreases the intensity of a color. Its range is between 0 to 1. The first
 * argument of the desaturate function is the amount by how much the color
 * intensity should be decreased.
 *
 * @example
 * // Styles as object usage
 * const styles = {
 *   background: desaturate(0.2, '#CCCD64'),
 *   background: desaturate(0.2, 'rgba(204,205,100,0.7)'),
 * }
 *
 * // styled-components usage
 * const div = styled.div`
 *   background: ${desaturate(0.2, '#CCCD64')};
 *   background: ${desaturate(0.2, 'rgba(204,205,100,0.7)')};
 * `
 *
 * // CSS in JS Output
 * element {
 *   background: "#b8b979";
 *   background: "rgba(184,185,121,0.7)";
 * }
 */
function desaturate(amount, color) {
  var hslColor = parseToHsl(color);
  return toColorString(_extends$2({}, hslColor, {
    saturation: guard(0, 1, hslColor.saturation - amount)
  }));
}

var curriedDesaturate = /*#__PURE__*/curry(desaturate);

//      
/**
 * Returns a number (float) representing the luminance of a color.
 *
 * @example
 * // Styles as object usage
 * const styles = {
 *   background: getLuminance('#CCCD64') >= getLuminance('#0000ff') ? '#CCCD64' : '#0000ff',
 *   background: getLuminance('rgba(58, 133, 255, 1)') >= getLuminance('rgba(255, 57, 149, 1)') ?
 *                             'rgba(58, 133, 255, 1)' :
 *                             'rgba(255, 57, 149, 1)',
 * }
 *
 * // styled-components usage
 * const div = styled.div`
 *   background: ${getLuminance('#CCCD64') >= getLuminance('#0000ff') ? '#CCCD64' : '#0000ff'};
 *   background: ${getLuminance('rgba(58, 133, 255, 1)') >= getLuminance('rgba(255, 57, 149, 1)') ?
 *                             'rgba(58, 133, 255, 1)' :
 *                             'rgba(255, 57, 149, 1)'};
 *
 * // CSS in JS Output
 *
 * div {
 *   background: "#CCCD64";
 *   background: "rgba(58, 133, 255, 1)";
 * }
 */
function getLuminance(color) {
  var rgbColor = parseToRgb(color);

  var _Object$keys$map = Object.keys(rgbColor).map(function (key) {
    var channel = rgbColor[key] / 255;
    return channel <= 0.03928 ? channel / 12.92 : Math.pow((channel + 0.055) / 1.055, 2.4);
  }),
      r = _Object$keys$map[0],
      g = _Object$keys$map[1],
      b = _Object$keys$map[2];

  return 0.2126 * r + 0.7152 * g + 0.0722 * b;
}

//      

/**
 * Returns a string value for the lightened color.
 *
 * @example
 * // Styles as object usage
 * const styles = {
 *   background: lighten(0.2, '#CCCD64'),
 *   background: lighten(0.2, 'rgba(204,205,100,0.7)'),
 * }
 *
 * // styled-components usage
 * const div = styled.div`
 *   background: ${lighten(0.2, '#FFCD64')};
 *   background: ${lighten(0.2, 'rgba(204,205,100,0.7)')};
 * `
 *
 * // CSS in JS Output
 *
 * element {
 *   background: "#e5e6b1";
 *   background: "rgba(229,230,177,0.7)";
 * }
 */
function lighten(amount, color) {
  var hslColor = parseToHsl(color);
  return toColorString(_extends$2({}, hslColor, {
    lightness: guard(0, 1, hslColor.lightness + amount)
  }));
}

var curriedLighten = /*#__PURE__*/curry(lighten);

//      

/**
 * Mixes two colors together by calculating the average of each of the RGB components.
 *
 * By default the weight is 0.5 meaning that half of the first color and half the second
 * color should be used. Optionally the weight can be modified by providing a number
 * as the first argument. 0.25 means that a quarter of the first color and three quarters
 * of the second color should be used.
 *
 * @example
 * // Styles as object usage
 * const styles = {
 *   background: mix(0.5, '#f00', '#00f')
 *   background: mix(0.25, '#f00', '#00f')
 *   background: mix(0.5, 'rgba(255, 0, 0, 0.5)', '#00f')
 * }
 *
 * // styled-components usage
 * const div = styled.div`
 *   background: ${mix(0.5, '#f00', '#00f')};
 *   background: ${mix(0.25, '#f00', '#00f')};
 *   background: ${mix(0.5, 'rgba(255, 0, 0, 0.5)', '#00f')};
 * `
 *
 * // CSS in JS Output
 *
 * element {
 *   background: "#7f007f";
 *   background: "#3f00bf";
 *   background: "rgba(63, 0, 191, 0.75)";
 * }
 */
function mix() {
  var weight = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0.5;
  var color = arguments[1];
  var otherColor = arguments[2];

  var parsedColor1 = parseToRgb(color);
  var color1 = _extends$2({}, parsedColor1, {
    alpha: typeof parsedColor1.alpha === 'number' ? parsedColor1.alpha : 1
  });

  var parsedColor2 = parseToRgb(otherColor);
  var color2 = _extends$2({}, parsedColor2, {
    alpha: typeof parsedColor2.alpha === 'number' ? parsedColor2.alpha : 1

    // The formular is copied from the original Sass implementation:
    // http://sass-lang.com/documentation/Sass/Script/Functions.html#mix-instance_method
  });var alphaDelta = color1.alpha - color2.alpha;
  var x = weight * 2 - 1;
  var y = x * alphaDelta === -1 ? x : x + alphaDelta;
  var z = 1 + x * alphaDelta;
  var weight1 = (y / z + 1) / 2.0;
  var weight2 = 1 - weight1;

  var mixedColor = {
    red: Math.floor(color1.red * weight1 + color2.red * weight2),
    green: Math.floor(color1.green * weight1 + color2.green * weight2),
    blue: Math.floor(color1.blue * weight1 + color2.blue * weight2),
    alpha: color1.alpha + (color2.alpha - color1.alpha) * (weight / 1.0)
  };

  return rgba(mixedColor);
}

var curriedMix = /*#__PURE__*/curry(mix);

//      
/**
 * Increases the opacity of a color. Its range for the amount is between 0 to 1.
 *
 *
 * @example
 * // Styles as object usage
 * const styles = {
 *   background: opacify(0.1, 'rgba(255, 255, 255, 0.9)');
 *   background: opacify(0.2, 'hsla(0, 0%, 100%, 0.5)'),
 *   background: opacify(0.5, 'rgba(255, 0, 0, 0.2)'),
 * }
 *
 * // styled-components usage
 * const div = styled.div`
 *   background: ${opacify(0.1, 'rgba(255, 255, 255, 0.9)')};
 *   background: ${opacify(0.2, 'hsla(0, 0%, 100%, 0.5)')},
 *   background: ${opacify(0.5, 'rgba(255, 0, 0, 0.2)')},
 * `
 *
 * // CSS in JS Output
 *
 * element {
 *   background: "#fff";
 *   background: "rgba(255,255,255,0.7)";
 *   background: "rgba(255,0,0,0.7)";
 * }
 */
function opacify(amount, color) {
  var parsedColor = parseToRgb(color);
  var alpha = typeof parsedColor.alpha === 'number' ? parsedColor.alpha : 1;
  var colorWithAlpha = _extends$2({}, parsedColor, {
    alpha: guard(0, 1, (alpha * 100 + amount * 100) / 100)
  });
  return rgba(colorWithAlpha);
}

var curriedOpacify = /*#__PURE__*/curry(opacify);

//      
/**
 * Selects black or white for best contrast depending on the luminosity of the given color.
 * Follows W3C specs for readability at https://www.w3.org/TR/WCAG20-TECHS/G18.html
 *
 * @example
 * // Styles as object usage
 * const styles = {
 *   color: readableColor('#000'),
 *   color: readableColor('papayawhip'),
 *   color: readableColor('rgb(255,0,0)'),
 * }
 *
 * // styled-components usage
 * const div = styled.div`
 *   color: ${readableColor('#000')};
 *   color: ${readableColor('papayawhip')};
 *   color: ${readableColor('rgb(255,0,0)')};
 * `
 *
 * // CSS in JS Output
 *
 * element {
 *   color: "#fff";
 *   color: "#fff";
 *   color: "#000";
 * }
 */

function readableColor(color) {
  return getLuminance(color) > 0.179 ? '#000' : '#fff';
}

var curriedReadableColor = /*#__PURE__*/curry(readableColor);

//      

/**
 * Increases the intensity of a color. Its range is between 0 to 1. The first
 * argument of the saturate function is the amount by how much the color
 * intensity should be increased.
 *
 * @example
 * // Styles as object usage
 * const styles = {
 *   background: saturate(0.2, '#CCCD64'),
 *   background: saturate(0.2, 'rgba(204,205,100,0.7)'),
 * }
 *
 * // styled-components usage
 * const div = styled.div`
 *   background: ${saturate(0.2, '#FFCD64')};
 *   background: ${saturate(0.2, 'rgba(204,205,100,0.7)')};
 * `
 *
 * // CSS in JS Output
 *
 * element {
 *   background: "#e0e250";
 *   background: "rgba(224,226,80,0.7)";
 * }
 */
function saturate(amount, color) {
  var hslColor = parseToHsl(color);
  return toColorString(_extends$2({}, hslColor, {
    saturation: guard(0, 1, hslColor.saturation + amount)
  }));
}

var curriedSaturate = /*#__PURE__*/curry(saturate);

//      

/**
 * Sets the hue of a color to the provided value. The hue range can be
 * from 0 and 359.
 *
 * @example
 * // Styles as object usage
 * const styles = {
 *   background: setHue(42, '#CCCD64'),
 *   background: setHue(244, 'rgba(204,205,100,0.7)'),
 * }
 *
 * // styled-components usage
 * const div = styled.div`
 *   background: ${setHue(42, '#CCCD64')};
 *   background: ${setHue(244, 'rgba(204,205,100,0.7)')};
 * `
 *
 * // CSS in JS Output
 * element {
 *   background: "#cdae64";
 *   background: "rgba(107,100,205,0.7)";
 * }
 */
function setHue(hue, color) {
  return toColorString(_extends$2({}, parseToHsl(color), {
    hue: hue
  }));
}

var curriedSetHue = /*#__PURE__*/curry(setHue);

//      

/**
 * Sets the lightness of a color to the provided value. The lightness range can be
 * from 0 and 1.
 *
 * @example
 * // Styles as object usage
 * const styles = {
 *   background: setLightness(0.2, '#CCCD64'),
 *   background: setLightness(0.75, 'rgba(204,205,100,0.7)'),
 * }
 *
 * // styled-components usage
 * const div = styled.div`
 *   background: ${setLightness(0.2, '#CCCD64')};
 *   background: ${setLightness(0.75, 'rgba(204,205,100,0.7)')};
 * `
 *
 * // CSS in JS Output
 * element {
 *   background: "#4d4d19";
 *   background: "rgba(223,224,159,0.7)";
 * }
 */
function setLightness(lightness, color) {
  return toColorString(_extends$2({}, parseToHsl(color), {
    lightness: lightness
  }));
}

var curriedSetLightness = /*#__PURE__*/curry(setLightness);

//      

/**
 * Sets the saturation of a color to the provided value. The lightness range can be
 * from 0 and 1.
 *
 * @example
 * // Styles as object usage
 * const styles = {
 *   background: setSaturation(0.2, '#CCCD64'),
 *   background: setSaturation(0.75, 'rgba(204,205,100,0.7)'),
 * }
 *
 * // styled-components usage
 * const div = styled.div`
 *   background: ${setSaturation(0.2, '#CCCD64')};
 *   background: ${setSaturation(0.75, 'rgba(204,205,100,0.7)')};
 * `
 *
 * // CSS in JS Output
 * element {
 *   background: "#adad84";
 *   background: "rgba(228,229,76,0.7)";
 * }
 */
function setSaturation(saturation, color) {
  return toColorString(_extends$2({}, parseToHsl(color), {
    saturation: saturation
  }));
}

var curriedSetSaturation = /*#__PURE__*/curry(setSaturation);

//      

/**
 * Shades a color by mixing it with black. `shade` can produce
 * hue shifts, where as `darken` manipulates the luminance channel and therefore
 * doesn't produce hue shifts.
 *
 * @example
 * // Styles as object usage
 * const styles = {
 *   background: shade(0.25, '#00f')
 * }
 *
 * // styled-components usage
 * const div = styled.div`
 *   background: ${shade(0.25, '#00f')};
 * `
 *
 * // CSS in JS Output
 *
 * element {
 *   background: "#00003f";
 * }
 */

function shade(percentage, color) {
  if (typeof percentage !== 'number' || percentage > 1 || percentage < -1) {
    throw new Error('Passed an incorrect argument to shade, please pass a percentage less than or equal to 1 and larger than or equal to -1.');
  }
  if (typeof color !== 'string') {
    throw new Error('Passed an incorrect argument to a color function, please pass a string representation of a color.');
  }
  return curriedMix(percentage, color, 'rgb(0, 0, 0)');
}

var curriedShade = /*#__PURE__*/curry(shade);

//      

/**
 * Tints a color by mixing it with white. `tint` can produce
 * hue shifts, where as `lighten` manipulates the luminance channel and therefore
 * doesn't produce hue shifts.
 *
 * @example
 * // Styles as object usage
 * const styles = {
 *   background: tint(0.25, '#00f')
 * }
 *
 * // styled-components usage
 * const div = styled.div`
 *   background: ${tint(0.25, '#00f')};
 * `
 *
 * // CSS in JS Output
 *
 * element {
 *   background: "#bfbfff";
 * }
 */

function tint(percentage, color) {
  if (typeof percentage !== 'number' || percentage > 1 || percentage < -1) {
    throw new Error('Passed an incorrect argument to tint, please pass a percentage less than or equal to 1 and larger than or equal to -1.');
  }
  if (typeof color !== 'string') {
    throw new Error('Passed an incorrect argument to a color function, please pass a string representation of a color.');
  }
  return curriedMix(percentage, color, 'rgb(255, 255, 255)');
}

var curriedTint = /*#__PURE__*/curry(tint);

//      
/**
 * Decreases the opacity of a color. Its range for the amount is between 0 to 1.
 *
 *
 * @example
 * // Styles as object usage
 * const styles = {
 *   background: transparentize(0.1, '#fff');
 *   background: transparentize(0.2, 'hsl(0, 0%, 100%)'),
 *   background: transparentize(0.5, 'rgba(255, 0, 0, 0.8)'),
 * }
 *
 * // styled-components usage
 * const div = styled.div`
 *   background: ${transparentize(0.1, '#fff')};
 *   background: ${transparentize(0.2, 'hsl(0, 0%, 100%)')},
 *   background: ${transparentize(0.5, 'rgba(255, 0, 0, 0.8)')},
 * `
 *
 * // CSS in JS Output
 *
 * element {
 *   background: "rgba(255,255,255,0.9)";
 *   background: "rgba(255,255,255,0.8)";
 *   background: "rgba(255,0,0,0.3)";
 * }
 */
function transparentize(amount, color) {
  var parsedColor = parseToRgb(color);
  var alpha = typeof parsedColor.alpha === 'number' ? parsedColor.alpha : 1;
  var colorWithAlpha = _extends$2({}, parsedColor, {
    alpha: guard(0, 1, (alpha * 100 - amount * 100) / 100)
  });
  return rgba(colorWithAlpha);
}

var curriedTransparentize = /*#__PURE__*/curry(transparentize);

var BackdropWrapper = styled__default.div.withConfig({
  displayName: 'Backdrop__BackdropWrapper',
  componentId: 'sc-1g42v95-0'
})(['display:flex;position:fixed;top:0;bottom:0;right:0;left:0;background:', ';overflow:scroll;justify-content:center;align-items:center;', ';transition:all 300ms ease-in-out;'], function (props) {
  return curriedTransparentize(0.6, themeGet('color.greys.1')(props));
}, opacity);

var Backdrop = function Backdrop(_ref) {
  var children = _ref.children,
      rest = objectWithoutProperties(_ref, ['children']);
  return React.createElement(
    BackdropWrapper,
    rest,
    children
  );
};
Backdrop.defaultProps = {
  opacity: 1
};
Backdrop.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Backdrop',
  'props': {
    'opacity': {
      'defaultValue': {
        'value': '1',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': ''
    },
    'children': {
      'required': true,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    }
  }
};

/**
 * Variables
 */
// TODO: as part of theme
var focusShadow = '0px 0px 0px 2px ' + curriedTransparentize(0.3, '#49C7BA') + ';';

/**
 * Common css styles
 */

// disabled component style
var disabled = styled.css(['opacity:0.5;pointer-events:none;']);

// transition
var transition = styled.css(['transition:opacity 0.2s ease-out,background 0.2s ease-out,box-shadow 0.2s cubic-bezier(0.47,0.03,0.49,1.38);']);

// focused component style
var focused = styled.css(['box-shadow:', ';'], focusShadow);

var buttonTypes = {
  raised: 'raised',
  hollow: 'hollow',
  flat: 'flat'
};

var buttonShapes = {
  square: 'square',
  round: 'round'
};

var ContentWrapper = styled__default.div.withConfig({
  displayName: 'styled__ContentWrapper',
  componentId: 'qfzb1h-0'
})(['opacity:1;transition:opacity 200ms ease-in-out;display:flex;align-items:center;justify-content:center;', ';'], function (_ref) {
  var loading = _ref.loading;
  return loading && styled.css(['opacity:0;']);
});

var LoadingWrapper = styled__default.div.withConfig({
  displayName: 'styled__LoadingWrapper',
  componentId: 'qfzb1h-1'
})(['position:absolute;opacity:0;transition:opacity 200ms ease-in-out;width:20px;height:20px;', ';'], function (_ref2) {
  var loading = _ref2.loading;
  return loading && styled.css(['opacity:1;']);
});

var Base = styled__default.button.withConfig({
  displayName: 'styled__Base',
  componentId: 'qfzb1h-2'
})(['border:none;padding:0px ', 'px;text-align:center;outline:none;cursor:pointer;font-size:12px;user-select:none;font-weight:', ';white-space:nowrap;line-height:0;display:flex;align-items:center;justify-content:center;', ';', ';', ';', ';', ';', ';', ';', ';&:focus{', ';}i{margin-right:', 'px;}'], function (props) {
  return !props.onlyIcon ? props.theme.uiSize[5] : 0;
}, function (props) {
  return props.theme.fontWeight[4];
}, function (_ref3) {
  var shape = _ref3.shape,
      theme = _ref3.theme;
  return shape === buttonShapes.round ? styled.css(['border-radius:', 'px;'], theme.borderRadius[10]) : styled.css(['border-radius:', 'px;'], theme.borderRadius[1]);
}, uiSize, space, width, function (props) {
  return props.disabled && disabled;
}, transition, function (props) {
  return props.onlyIcon && styled.css(['width:', ';'], themeGet('uiSizes.' + props.uiSize + '.height')(props));
}, function (props) {
  return props.stretch && styled.css(['width:100%;']);
}, focused, function (props) {
  return !props.onlyIcon && props.theme.uiSize[2];
});

/**
 * Raised
 */

var raisedActive = styled.css(['background:', ';box-shadow:', ' ', ';'], function (_ref4) {
  var theme = _ref4.theme,
      color$$1 = _ref4.color;
  return curriedLighten(0.05, themeGet('colors.' + color$$1)({ theme: theme }));
}, function (props) {
  return props.theme.elevation[2];
}, focusShadow);

var RaisedButton = styled__default(Base).withConfig({
  displayName: 'styled__RaisedButton',
  componentId: 'qfzb1h-3'
})(['background:', ';box-shadow:', ';color:white;&:hover,&:focus{', ';}', ';&:focus{box-shadow:', ';background:', ';}i > svg{fill:white;}'], function (_ref5) {
  var color$$1 = _ref5.color,
      theme = _ref5.theme;
  return themeGet('colors.' + color$$1)({ theme: theme });
}, function (props) {
  return props.theme.elevation[3];
}, raisedActive, function (props) {
  return props.active && raisedActive;
}, focusShadow, function (_ref6) {
  var theme = _ref6.theme,
      color$$1 = _ref6.color;
  return curriedDarken(0.07, themeGet('colors.' + color$$1)({ theme: theme }));
});

/**
 * Hollow
 */
var hollowActive = styled.css(['background:', ';box-shadow:0px 0px 0px 1px ', ';'], function (_ref7) {
  var theme = _ref7.theme,
      color$$1 = _ref7.color;
  return curriedTransparentize(0.85, themeGet('colors.' + color$$1)({ theme: theme }));
}, function (_ref8) {
  var theme = _ref8.theme,
      color$$1 = _ref8.color;
  return curriedTransparentize(0.6, themeGet('colors.' + color$$1)({ theme: theme }));
});

var hollowBackground = function hollowBackground(_ref9) {
  var theme = _ref9.theme,
      color$$1 = _ref9.color;
  return curriedTransparentize(0.95, themeGet('colors.' + color$$1)({ theme: theme }));
};

var HollowButton = styled__default(Base).withConfig({
  displayName: 'styled__HollowButton',
  componentId: 'qfzb1h-4'
})(['color:', ';box-shadow:0px 0px 0px 1px ', ';background:', ';&:hover,&:focus{', ';}&:focus{', ';}', ';i > svg{fill:', ';}'], function (_ref10) {
  var theme = _ref10.theme,
      color$$1 = _ref10.color;
  return themeGet('colors.' + color$$1)({ theme: theme });
}, function (_ref11) {
  var theme = _ref11.theme,
      color$$1 = _ref11.color;
  return curriedTransparentize(0.75, themeGet('colors.' + color$$1)({ theme: theme }));
}, hollowBackground, hollowActive, focused, function (props) {
  return props.active && hollowActive;
}, function (_ref12) {
  var theme = _ref12.theme,
      color$$1 = _ref12.color;
  return themeGet('colors.' + color$$1)({ theme: theme });
});

/**
 * Flat
 */
var flatActive = styled.css(['background:', ';'], function (_ref13) {
  var theme = _ref13.theme,
      color$$1 = _ref13.color;
  return curriedTransparentize(0.9, themeGet('colors.' + color$$1)({ theme: theme }));
});

var FlatButton = styled__default(Base).withConfig({
  displayName: 'styled__FlatButton',
  componentId: 'qfzb1h-5'
})(['color:', ';background:transparent;&:hover,&:focus{', ';}', ';&:focus{', ';}i > svg{fill:', ';}'], function (_ref14) {
  var theme = _ref14.theme,
      color$$1 = _ref14.color;
  return themeGet('colors.' + color$$1)({ theme: theme });
}, flatActive, function (props) {
  return props.active && flatActive;
}, focused, function (_ref15) {
  var theme = _ref15.theme,
      color$$1 = _ref15.color;
  return themeGet('colors.' + color$$1)({ theme: theme });
});

// flow

var addToCart = function addToCart() {
  return React.createElement('path', { d: 'M11,9 L13,9 L13,6 L16,6 L16,4 L13,4 L13,1 L11,1 L11,4 L8,4 L8,6 L11,6 L11,9 L11,9 Z M7,18 C5.9,18 5.01,18.9 5.01,20 C5.01,21.1 5.9,22 7,22 C8.1,22 9,21.1 9,20 C9,18.9 8.1,18 7,18 L7,18 Z M17,18 C15.9,18 15.01,18.9 15.01,20 C15.01,21.1 15.9,22 17,22 C18.1,22 19,21.1 19,20 C19,18.9 18.1,18 17,18 L17,18 Z M7.17,14.75 L7.2,14.63 L8.1,13 L15.55,13 C16.3,13 16.96,12.59 17.3,11.97 L21.16,4.96 L19.42,4 L19.41,4 L18.31,6 L15.55,11 L8.53,11 L8.4,10.73 L6.16,6 L5.21,4 L4.27,2 L1,2 L1,4 L3,4 L6.6,11.59 L5.25,14.04 C5.09,14.32 5,14.65 5,15 C5,16.1 5.9,17 7,17 L19,17 L19,15 L7.42,15 C7.29,15 7.17,14.89 7.17,14.75 L7.17,14.75 Z' });
};

var addCircle = function addCircle() {
  return React.createElement('path', { d: 'M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm5 11h-4v4h-2v-4H7v-2h4V7h2v4h4v2z' });
};

var arrowRight = function arrowRight() {
  return React.createElement('path', { d: 'm12 20-1.41-1.41 5.58-5.59h-12.17v-2h12.17l-5.59-5.58 1.42-1.42 8 8z' });
};

var arrowLeft = function arrowLeft() {
  return React.createElement('path', { d: 'm12 20-8-8 8-8 1.42 1.42-5.59 5.58h12.17v2h-12.17l5.58 5.59z' });
};

var attachment = function attachment() {
  return React.createElement('path', { d: 'm19.0710678 5.42893219c2.1496046 2.14960461 2.1496046 5.62857001 0 7.77817461l-6.7175144 6.7175144-1.4142136-1.4142136 6.7175144-6.7175144c1.3647161-1.3647161 1.3647161-3.58503136 0-4.94974745-1.364716-1.36471609-3.5850313-1.36471609-4.9497474 0l-7.42462122 7.42462125c-.77781746.7778174-.77781746 2.0506096 0 2.8284271.77781746.7778174 2.05060966.7778174 2.82842712 0l6.0740473-6.0740473c.3889087-.3889087-.3181981-1.09601548-.7071068-.7071068l-5.3669405 5.3669405-1.41421356-1.4142135 5.30330086-5.30330091c.9758074-.97580735 2.5597265-.97580735 3.5355339 0 .9758074.97580736.9758074 2.55972651 0 3.53553391l-6.01040763 6.0104076c-1.56270599 1.562706-4.09414827 1.562706-5.65685425 0-1.56270599-1.5627059-1.56270599-4.0941482 0-5.6568542l7.42462118-7.42462121c2.1496046-2.14960462 5.62857-2.14960462 7.7781746 0z' });
};

var caretDown = function caretDown() {
  return React.createElement('path', { d: 'm16.59 8.59-4.59 4.58-4.59-4.58-1.41 1.41 6 6 6-6z' });
};

var checkmark = function checkmark() {
  return React.createElement('path', { d: 'm10 17-5-5.2783109 1.41-1.4884837 3.59 3.7792706 7.59-8.012476 1.41 1.49904031z' });
};

var close = function close() {
  return React.createElement('path', { d: 'm19 6.41-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z' });
};

var done = function done() {
  return React.createElement('path', { d: 'm18 7-1.41-1.41-6.34 6.34 1.41 1.41zm4.24-1.41-10.58 10.58-4.18-4.17-1.41 1.41 5.59 5.59 12-12zm-21.83 7.82 5.59 5.59 1.41-1.41-5.58-5.59z' });
};

var emoticon = function emoticon() {
  return React.createElement('path', { d: 'M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm3.5-9c.83 0 1.5-.67 1.5-1.5S16.33 8 15.5 8 14 8.67 14 9.5s.67 1.5 1.5 1.5zm-7 0c.83 0 1.5-.67 1.5-1.5S9.33 8 8.5 8 7 8.67 7 9.5 7.67 11 8.5 11zm3.5 6.5c2.33 0 4.31-1.46 5.11-3.5H6.89c.8 2.04 2.78 3.5 5.11 3.5z' });
};
var filter = function filter() {
  return React.createElement('path', { d: 'm10 18h4v-2h-4zm-7-12v2h18v-2zm3 7h12v-2h-12z' });
};
var info$1 = function info() {
  return React.createElement('path', { d: 'M11 17h2v-6h-2v6zm1-15C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zM11 9h2V7h-2v2z' });
};

var send = function send() {
  return React.createElement('path', { d: 'm2.01 21-.01-7 15-2-15-2 .01-7 20.99 9z' });
};

var edit = function edit() {
  return React.createElement('path', {
    d: 'm3 17.25v3.75h3.75l11.06-11.06-3.75-3.75zm17.71-10.21c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75z',
    fillRule: 'evenodd'
  });
};

var more = function more() {
  return React.createElement('path', { d: 'm6 10c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm12 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm-6 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z' });
};

var user = function user() {
  return React.createElement('path', { d: 'M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 3c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3zm0 14.2c-2.5 0-4.71-1.28-6-3.22.03-1.99 4-3.08 6-3.08 1.99 0 5.97 1.09 6 3.08-1.29 1.94-3.5 3.22-6 3.22z' });
};

var rotation = function rotation() {
  return React.createElement('path', { d: 'M7.52 21.48C4.25 19.94 1.91 16.76 1.55 13H.05C.56 19.16 5.71 24 12 24l.66-.03-3.81-3.81-1.33 1.32zm.89-6.52c-.19 0-.37-.03-.52-.08-.16-.06-.29-.13-.4-.24-.11-.1-.2-.22-.26-.37-.06-.14-.09-.3-.09-.47h-1.3c0 .36.07.68.21.95.14.27.33.5.56.69.24.18.51.32.82.41.3.1.62.15.96.15.37 0 .72-.05 1.03-.15.32-.1.6-.25.83-.44s.42-.43.55-.72c.13-.29.2-.61.2-.97 0-.19-.02-.38-.07-.56-.05-.18-.12-.35-.23-.51-.1-.16-.24-.3-.4-.43-.17-.13-.37-.23-.61-.31.2-.09.37-.2.52-.33.15-.13.27-.27.37-.42.1-.15.17-.3.22-.46.05-.16.07-.32.07-.48 0-.36-.06-.68-.18-.96-.12-.28-.29-.51-.51-.69-.2-.19-.47-.33-.77-.43C9.1 8.05 8.76 8 8.39 8c-.36 0-.69.05-1 .16-.3.11-.57.26-.79.45-.21.19-.38.41-.51.67-.12.26-.18.54-.18.85h1.3c0-.17.03-.32.09-.45s.14-.25.25-.34c.11-.09.23-.17.38-.22.15-.05.3-.08.48-.08.4 0 .7.1.89.31.19.2.29.49.29.86 0 .18-.03.34-.08.49-.05.15-.14.27-.25.37-.11.1-.25.18-.41.24-.16.06-.36.09-.58.09H7.5v1.03h.77c.22 0 .42.02.6.07s.33.13.45.23c.12.11.22.24.29.4.07.16.1.35.1.57 0 .41-.12.72-.35.93-.23.23-.55.33-.95.33zm8.55-5.92c-.32-.33-.7-.59-1.14-.77-.43-.18-.92-.27-1.46-.27H12v8h2.3c.55 0 1.06-.09 1.51-.27.45-.18.84-.43 1.16-.76.32-.33.57-.73.74-1.19.17-.47.26-.99.26-1.57v-.4c0-.58-.09-1.1-.26-1.57-.18-.47-.43-.87-.75-1.2zm-.39 3.16c0 .42-.05.79-.14 1.13-.1.33-.24.62-.43.85-.19.23-.43.41-.71.53-.29.12-.62.18-.99.18h-.91V9.12h.97c.72 0 1.27.23 1.64.69.38.46.57 1.12.57 1.99v.4zM12 0l-.66.03 3.81 3.81 1.33-1.33c3.27 1.55 5.61 4.72 5.96 8.48h1.5C23.44 4.84 18.29 0 12 0z' });
};

var questionmark = function questionmark() {
  return React.createElement('path', { d: 'M11,18 L13,18 L13,16 L11,16 L11,18 L11,18 Z M12,2 C6.48,2 2,6.48 2,12 C2,17.52 6.48,22 12,22 C17.52,22 22,17.52 22,12 C22,6.48 17.52,2 12,2 L12,2 Z M12,20 C7.59,20 4,16.41 4,12 C4,7.59 7.59,4 12,4 C16.41,4 20,7.59 20,12 C20,16.41 16.41,20 12,20 L12,20 Z M12,6 C9.79,6 8,7.79 8,10 L10,10 C10,8.9 10.9,8 12,8 C13.1,8 14,8.9 14,10 C14,12 11,11.75 11,15 L13,15 C13,12.75 16,12.5 16,10 C16,7.79 14.21,6 12,6 L12,6 Z' });
};

var menu = function menu() {
  return React.createElement('path', { d: 'M3,18 L21,18 L21,16 L3,16 L3,18 L3,18 Z M3,13 L21,13 L21,11 L3,11 L3,13 L3,13 Z M3,6 L3,8 L21,8 L21,6 L3,6 L3,6 Z' });
};

var openInNew = function openInNew() {
  return React.createElement('path', { d: 'M19,19 L5,19 L5,5 L12,5 L12,3 L5,3 C3.89,3 3,3.9 3,5 L3,19 C3,20.1 3.89,21 5,21 L19,21 C20.1,21 21,20.1 21,19 L21,12 L19,12 L19,19 L19,19 Z M14,3 L14,5 L17.59,5 L7.76,14.83 L9.17,16.24 L19,6.41 L19,10 L21,10 L21,3 L14,3 L14,3 Z' });
};

var desktopComputer = function desktopComputer() {
  return React.createElement('path', { d: 'M21,3 L3,3 C1.9,3 1,3.9 1,5 L1,17 C1,18.1 1.9,19 3,19 L8,19 L8,21 L16,21 L16,19 L21,19 C22.1,19 22.99,18.1 22.99,17 L23,5 C23,3.9 22.1,3 21,3 L21,3 Z M21,17 L3,17 L3,5 L21,5 L21,17 L21,17 Z M19,8 L8,8 L8,10 L19,10 L19,8 L19,8 Z M19,12 L8,12 L8,14 L19,14 L19,12 L19,12 Z M7,8 L5,8 L5,10 L7,10 L7,8 L7,8 Z M7,12 L5,12 L5,14 L7,14 L7,12 L7,12 Z' });
};
addToCart.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'addToCart'
};
addCircle.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'addCircle'
};
arrowRight.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'arrowRight'
};
arrowLeft.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'arrowLeft'
};
attachment.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'attachment'
};
caretDown.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'caretDown'
};
checkmark.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'checkmark'
};
close.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'close'
};
done.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'done'
};
emoticon.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'emoticon'
};
filter.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'filter'
};
info$1.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'info'
};
send.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'send'
};
edit.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'edit'
};
more.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'more'
};
user.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'user'
};
rotation.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'rotation'
};
questionmark.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'questionmark'
};
menu.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'menu'
};
openInNew.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'openInNew'
};
desktopComputer.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'desktopComputer'
};

var icons = /*#__PURE__*/Object.freeze({
  addToCart: addToCart,
  addCircle: addCircle,
  arrowRight: arrowRight,
  arrowLeft: arrowLeft,
  attachment: attachment,
  caretDown: caretDown,
  checkmark: checkmark,
  close: close,
  done: done,
  emoticon: emoticon,
  filter: filter,
  info: info$1,
  send: send,
  edit: edit,
  more: more,
  user: user,
  rotation: rotation,
  questionmark: questionmark,
  menu: menu,
  openInNew: openInNew,
  desktopComputer: desktopComputer
});

var IconWrapper = styled__default.i.withConfig({
  displayName: 'Icon__IconWrapper',
  componentId: 'sc-151ev5r-0'
})(['display:inline-flex;', ';'], space);

var SvgWrapper = styled__default.svg.withConfig({
  displayName: 'Icon__SvgWrapper',
  componentId: 'sc-151ev5r-1'
})(['fill:', ';'], function (_ref) {
  var color$$1 = _ref.color,
      rest = objectWithoutProperties(_ref, ['color']);
  return themeGet('colors.' + color$$1)(rest);
});
var Icon = function Icon(_ref2) {
  var _ref2$icon = _ref2.icon,
      icon = _ref2$icon === undefined ? 'questionmark' : _ref2$icon,
      _ref2$size = _ref2.size,
      size$$1 = _ref2$size === undefined ? 20 : _ref2$size,
      color$$1 = _ref2.color,
      rest = objectWithoutProperties(_ref2, ['icon', 'size', 'color']);

  var iconMarkup = icons[icon];

  if (!iconMarkup) return null;

  return React.createElement(
    IconWrapper,
    rest,
    React.createElement(
      SvgWrapper,
      { height: size$$1, viewBox: '0 0 24 24', width: size$$1, color: color$$1, xmlns: 'http://www.w3.org/2000/svg' },
      iconMarkup()
    )
  );
};

Icon.defaultProps = {
  size: 20,
  color: 'grey'
};
Icon.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Icon',
  'props': {
    'icon': {
      'defaultValue': {
        'value': '\'questionmark\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'size': {
      'defaultValue': {
        'value': '20',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': ''
    },
    'color': {
      'defaultValue': {
        'value': '\'grey\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'ThemeColors'
      },
      'description': ''
    }
  }
};

var TextSC = styled__default.span.withConfig({
  displayName: 'styled__TextSC',
  componentId: 'sc-1wt4toq-0'
})(['', ';', ';', ';', ';', ';', ';', ';', ';', ';', ';', ';', ';', ';'], fontSize, textColor, color, fontFamily, textAlign, lineHeight, fontWeight, fontStyle, letterSpacing, bgColor, space, display, whiteSpace);

var Text = function Text(_ref) {
  var children = _ref.children,
      rest = objectWithoutProperties(_ref, ['children']);
  return React.createElement(
    TextSC,
    rest,
    children
  );
};
Text.defaultProps = {
  color: 'inherit',
  fontWeight: 3,
  whiteSpace: 'nowrap',
  fontSize: 'inherit',
  textAlign: 'inherit'
};
Text.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Text',
  'props': {
    'color': {
      'defaultValue': {
        'value': '\'inherit\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'fontWeight': {
      'defaultValue': {
        'value': '3',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': ''
    },
    'whiteSpace': {
      'defaultValue': {
        'value': '\'nowrap\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'union',
        'raw': '\'normal\' | \'nowrap\'',
        'elements': [{
          'name': 'literal',
          'value': '\'normal\''
        }, {
          'name': 'literal',
          'value': '\'nowrap\''
        }]
      },
      'description': ''
    },
    'fontSize': {
      'defaultValue': {
        'value': '\'inherit\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': ''
    },
    'textAlign': {
      'defaultValue': {
        'value': '\'inherit\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'children': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    }
  }
};

var animation$1 = styled.keyframes(['to{transform:rotate(1turn)}']);

var Spinner = styled__default.div.withConfig({
  displayName: 'Spinner',
  componentId: 'sc-1cuysm1-0'
})(['position:absolute;width:', 'px;height:', 'px;border:', 'px solid rgba(189,189,189,0.1);border-left-color:', ';border-radius:50%;transition:opacity 200ms ease;animation:', ' 700ms infinite linear;opacity:0.7;'], function (_ref) {
  var size$$1 = _ref.size,
      rest = objectWithoutProperties(_ref, ['size']);
  return themeGet('space.' + size$$1)(rest);
}, function (_ref2) {
  var size$$1 = _ref2.size,
      rest = objectWithoutProperties(_ref2, ['size']);
  return themeGet('space.' + size$$1)(rest);
}, function (_ref3) {
  var size$$1 = _ref3.size,
      rest = objectWithoutProperties(_ref3, ['size']);
  return themeGet('space.' + size$$1)(rest) * 0.1;
}, function (_ref4) {
  var color$$1 = _ref4.color,
      rest = objectWithoutProperties(_ref4, ['color']);
  return themeGet('colors.' + color$$1)(rest);
}, animation$1);
Spinner.defaultProps = {
  size: 20,
  color: 'primary'
};

var Button = function Button(_ref) {
  var children = _ref.children,
      disabled = _ref.disabled,
      loading = _ref.loading,
      buttonType = _ref.buttonType,
      icon = _ref.icon,
      rest = objectWithoutProperties(_ref, ['children', 'disabled', 'loading', 'buttonType', 'icon']);

  var onlyIcon = icon && !children;

  var onClick = rest.onClick;

  var _disabled = disabled || loading;

  var content = function content() {
    return React.createElement(
      React.Fragment,
      null,
      React.createElement(
        ContentWrapper,
        { loading: loading },
        icon && React.createElement(Icon, { size: 16, icon: icon }),
        children && React.createElement(
          Text,
          { fontWeight: 4, fontSize: 'inherit' },
          children
        )
      ),
      React.createElement(
        LoadingWrapper,
        { loading: loading },
        loading && React.createElement(Spinner, { size: 5, color: 'white' })
      )
    );
  };

  switch (buttonType) {
    case buttonTypes.raised:
      return React.createElement(
        RaisedButton,
        _extends$1({
          onKeyDown: function onKeyDown(e) {
            return e.key === 'Enter' && onClick();
          },
          onlyIcon: onlyIcon
        }, rest, {
          disabled: _disabled
        }),
        content()
      );
    case buttonTypes.hollow:
      return React.createElement(
        HollowButton,
        _extends$1({
          onKeyDown: function onKeyDown(e) {
            return e.key === 'Enter' && onClick();
          },
          onlyIcon: onlyIcon
        }, rest, {
          disabled: _disabled
        }),
        content()
      );
    case buttonTypes.flat:
      return React.createElement(
        FlatButton,
        _extends$1({ onKeyDown: function onKeyDown(e) {
            return e.key === 'Enter' && onClick();
          }, onlyIcon: onlyIcon }, rest, { disabled: _disabled }),
        content()
      );
    default:
      console.log('unknown button type');
      return React.createElement(
        RaisedButton,
        _extends$1({
          onKeyDown: function onKeyDown(e) {
            return e.key === 'Enter' && onClick();
          },
          onlyIcon: onlyIcon
        }, rest, {
          disabled: _disabled
        }),
        content()
      );
  }
};

/**
 * defaultProps
 */
Button.defaultProps = {
  color: 'primary',
  buttonType: 'raised',
  shape: 'round',
  stretch: false,
  width: null,
  uiSize: 'small',
  type: 'button',
  mb: 0,
  mr: 0
};
Button.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Button',
  'props': {
    'color': {
      'defaultValue': {
        'value': '\'primary\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'ThemeColors'
      },
      'description': ''
    },
    'buttonType': {
      'defaultValue': {
        'value': '\'raised\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'union',
        'raw': '\'raised\' | \'hollow\' | \'flat\'',
        'elements': [{
          'name': 'literal',
          'value': '\'raised\''
        }, {
          'name': 'literal',
          'value': '\'hollow\''
        }, {
          'name': 'literal',
          'value': '\'flat\''
        }]
      },
      'description': ''
    },
    'shape': {
      'defaultValue': {
        'value': '\'round\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'union',
        'raw': '\'square\' | \'round\'',
        'elements': [{
          'name': 'literal',
          'value': '\'square\''
        }, {
          'name': 'literal',
          'value': '\'round\''
        }]
      },
      'description': ''
    },
    'stretch': {
      'defaultValue': {
        'value': 'false',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'boolean'
      },
      'description': ''
    },
    'width': {
      'defaultValue': {
        'value': 'null',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    },
    'uiSize': {
      'defaultValue': {
        'value': '\'small\'',
        'computed': false
      },
      'required': false
    },
    'type': {
      'defaultValue': {
        'value': '\'button\'',
        'computed': false
      },
      'required': false
    },
    'mb': {
      'defaultValue': {
        'value': '0',
        'computed': false
      },
      'required': false
    },
    'mr': {
      'defaultValue': {
        'value': '0',
        'computed': false
      },
      'required': false
    },
    'children': {
      'required': false,
      'flowType': {
        'name': 'ReactNode',
        'raw': 'React.Node'
      },
      'description': ''
    },
    'icon': {
      'required': false,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'disabled': {
      'required': false,
      'flowType': {
        'name': 'boolean'
      },
      'description': ''
    },
    'onClick': {
      'required': true,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    },
    'active': {
      'required': false,
      'flowType': {
        'name': 'boolean'
      },
      'description': ''
    },
    'size': {
      'required': false,
      'flowType': {
        'name': 'union',
        'raw': '\'large\' | \'small\'',
        'elements': [{
          'name': 'literal',
          'value': '\'large\''
        }, {
          'name': 'literal',
          'value': '\'small\''
        }]
      },
      'description': ''
    },
    'loading': {
      'required': true,
      'flowType': {
        'name': 'boolean'
      },
      'description': ''
    }
  }
};

var Label = styled__default.label.withConfig({
  displayName: 'styled__Label',
  componentId: 'sc-1yha6dm-0'
})(['font-weight:600;font-size:12px;color:', ';line-height:12px;height:12px;align-items:center;', ';', ';'], themeGet('colors.text'), display, space);

var _Label = function _Label(_ref) {
  var children = _ref.children,
      rest = objectWithoutProperties(_ref, ['children']);
  return React__default.createElement(
    Label,
    rest,
    children
  );
};

/**
 * propTypes
 */
_Label.defaultProps = {
  display: 'flex'
};
_Label.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': '_Label',
  'props': {
    'display': {
      'defaultValue': {
        'value': '\'flex\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    },
    'children': {
      'required': true,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    }
  }
};

var H1 = styled__default.h1.withConfig({
  displayName: 'styled__H1',
  componentId: 'a7bu6w-0'
})(['', ';', ';', ';', ';', ';', ';', ';', ';', ';', ';', ';', ';'], fontSize, textColor, color, fontFamily, textAlign, lineHeight, fontWeight, fontStyle, letterSpacing, bgColor, space, display);

var H1$1 = function H1$$1(_ref) {
  var children = _ref.children,
      rest = objectWithoutProperties(_ref, ['children']);
  return React.createElement(
    H1,
    rest,
    children
  );
};

H1$1.defaultProps = {
  color: 'text',
  fontWeight: 6,
  multiLine: true,
  fontSize: 7
};
H1$1.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'H1',
  'props': {
    'color': {
      'defaultValue': {
        'value': '\'text\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'fontWeight': {
      'defaultValue': {
        'value': '6',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': ''
    },
    'multiLine': {
      'defaultValue': {
        'value': 'true',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'boolean'
      },
      'description': ''
    },
    'fontSize': {
      'defaultValue': {
        'value': '7',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': ''
    },
    'children': {
      'required': true,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    }
  }
};

var H2 = styled__default.h2.withConfig({
  displayName: 'styled__H2',
  componentId: 'bl4b80-0'
})(['', ';', ';', ';', ';', ';', ';', ';', ';', ';', ';', ';', ';'], fontSize, textColor, color, fontFamily, textAlign, lineHeight, fontWeight, fontStyle, letterSpacing, bgColor, space, display);

var H2$1 = function H2$$1(_ref) {
  var children = _ref.children,
      rest = objectWithoutProperties(_ref, ['children']);
  return React.createElement(
    H2,
    rest,
    children
  );
};

/**
 * defaultProps
 */
H2$1.defaultProps = {
  color: 'text',
  fontWeight: 6,
  fontSize: 5
};
H2$1.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'H2',
  'props': {
    'color': {
      'defaultValue': {
        'value': '\'text\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'fontWeight': {
      'defaultValue': {
        'value': '6',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'union',
        'raw': 'number | string',
        'elements': [{
          'name': 'number'
        }, {
          'name': 'string'
        }]
      },
      'description': ''
    },
    'fontSize': {
      'defaultValue': {
        'value': '5',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'union',
        'raw': 'number | string',
        'elements': [{
          'name': 'number'
        }, {
          'name': 'string'
        }]
      },
      'description': ''
    },
    'children': {
      'required': true,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    }
  }
};

var H3 = styled__default.h3.withConfig({
  displayName: 'styled__H3',
  componentId: 'sc-13un9pj-0'
})(['', ';', ';', ';', ';', ';', ';', ';', ';', ';', ';', ';', ';'], fontSize, textColor, color, fontFamily, textAlign, lineHeight, fontWeight, fontStyle, letterSpacing, bgColor, space, display);

var H3$1 = function H3$$1(_ref) {
  var children = _ref.children,
      rest = objectWithoutProperties(_ref, ['children']);
  return React.createElement(
    H3,
    rest,
    children
  );
};
H3$1.defaultProps = {
  color: 'text',
  multiLine: true,
  fontWeight: 5,
  fontSize: 5
};
H3$1.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'H3',
  'props': {
    'color': {
      'defaultValue': {
        'value': '\'text\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'union',
        'raw': '\'white\' | \'default\' | string',
        'elements': [{
          'name': 'literal',
          'value': '\'white\''
        }, {
          'name': 'literal',
          'value': '\'default\''
        }, {
          'name': 'string'
        }]
      },
      'description': ''
    },
    'multiLine': {
      'defaultValue': {
        'value': 'true',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'boolean'
      },
      'description': ''
    },
    'fontWeight': {
      'defaultValue': {
        'value': '5',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'union',
        'raw': 'number | string',
        'elements': [{
          'name': 'number'
        }, {
          'name': 'string'
        }]
      },
      'description': ''
    },
    'fontSize': {
      'defaultValue': {
        'value': '5',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': ''
    },
    'children': {
      'required': true,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    },
    'textAlign': {
      'required': false,
      'flowType': {
        'name': 'union',
        'raw': '\'left\' | \'center\' | \'right\'',
        'elements': [{
          'name': 'literal',
          'value': '\'left\''
        }, {
          'name': 'literal',
          'value': '\'center\''
        }, {
          'name': 'literal',
          'value': '\'right\''
        }]
      },
      'description': ''
    }
  }
};

var LogoSC = styled__default.div.withConfig({
  displayName: 'styled__LogoSC',
  componentId: 'ojzvg2-0'
})(['background-size:contain;height:', 'px;width:', 'px;svg{height:100%;width:100%;padding:', ';}'], function (props) {
  return props.theme.uiSize[props.size];
}, function (props) {
  return props.theme.uiSize[props.size] * props.aspectRatio;
}, function (props) {
  return props.theme.uiSize[props.padding];
});

var LogomarkColored = function LogomarkColored(props) {
  return React.createElement(
    'svg',
    props,
    React.createElement('path', {
      fillRule: 'evenodd',
      clipRule: 'evenodd',
      d: 'M175.2 392.393l-.037 64.62c0 3.049-.435 4.647-1.738 6.187C153.203 463.2.705 383.634.002 232.125.704 79.565 153.203 0 173.425 0c1.303 1.54 1.738 3.138 1.738 6.187l.037 64.599c-38.578 0-120.343 62.242-120.777 160.814.434 98.569 82.199 160.793 120.777 160.793z',
      transform: 'matrix(1 0 0 -1 213.4 471.6)',
      fill: 'url(#a)'
    }),
    React.createElement('path', {
      fillRule: 'evenodd',
      clipRule: 'evenodd',
      d: 'M241.53 70.08c-94.443 0-171.003 76.076-171.003 169.92s76.56 169.92 171.003 169.92c19.547 0 38.566-3.122 56.063-9.124l-.028 66.514c0 3.452-2.371 6.703-5.639 7.458A244.016 244.016 0 0 1 241.53 480C108.137 480 0 372.548 0 240S108.137 0 241.53 0c17.297 0 34.17 1.807 50.438 5.24 3.341.774 5.597 3.913 5.597 7.445l.028 66.504c-17.518-6.018-36.487-9.109-56.063-9.109z',
      transform: 'matrix(1 0 0 -1 91 480)',
      fill: 'url(#b)'
    }),
    React.createElement(
      'defs',
      null,
      React.createElement(
        'linearGradient',
        {
          id: 'a',
          x2: '1',
          gradientUnits: 'userSpaceOnUse',
          gradientTransform: 'matrix(305.055 0 0 806.515 -86.155 231.6)'
        },
        React.createElement('stop', {
          stopColor: '#CEEDEA',
          stopOpacity: '.01'
        }),
        React.createElement('stop', {
          offset: '1',
          stopColor: '#B3E7E2'
        })
      ),
      React.createElement(
        'linearGradient',
        {
          id: 'b',
          x2: '1',
          gradientUnits: 'userSpaceOnUse',
          gradientTransform: 'matrix(308.858 0 0 498.17 -68.728 240)'
        },
        React.createElement('stop', {
          stopColor: '#3DA79D'
        }),
        React.createElement('stop', {
          offset: '1',
          stopColor: '#4CCCBF'
        })
      )
    )
  );
};

LogomarkColored.defaultProps = {
  width: '480',
  height: '480',
  viewBox: '0 0 480 480',
  fill: 'none',
  xmlns: 'http://www.w3.org/2000/svg'
};

var LogomarkInverted = function LogomarkInverted(props) {
  return React.createElement(
    'svg',
    props,
    React.createElement('path', {
      opacity: '.5',
      fillRule: 'evenodd',
      clipRule: 'evenodd',
      d: 'M146 326.994l-.03 53.85c0 2.541-.363 3.873-1.449 5.156C127.67 386 .587 319.695.002 193.438.587 66.305 127.67 0 144.521 0c1.086 1.283 1.449 2.615 1.449 5.156l.03 53.832c-32.148 0-100.286 51.869-100.648 134.012.362 82.141 68.5 133.994 100.648 133.994z',
      transform: 'matrix(1 0 0 -1 102 393)',
      fill: 'url(#a)'
    }),
    React.createElement('path', {
      fillRule: 'evenodd',
      clipRule: 'evenodd',
      d: 'M201.275 341.6c-78.702 0-142.503-63.396-142.503-141.6S122.573 58.4 201.275 58.4c16.289 0 32.138 2.602 46.719 7.604l-.024-55.429c0-2.876-1.975-5.586-4.698-6.215A203.338 203.338 0 0 0 201.275 0C90.114 0 0 89.543 0 200s90.114 200 201.275 200c14.414 0 28.475-1.506 42.031-4.367 2.785-.645 4.664-3.26 4.664-6.204l.024-55.42c-14.598 5.016-30.406 7.591-46.719 7.591z',
      fill: '#fff'
    }),
    React.createElement(
      'defs',
      null,
      React.createElement(
        'linearGradient',
        {
          id: 'a',
          x2: '1',
          gradientUnits: 'userSpaceOnUse',
          gradientTransform: 'matrix(124 0 0 327.836 0 365.13)'
        },
        React.createElement('stop', {
          stopColor: '#fff',
          stopOpacity: '.28'
        }),
        React.createElement('stop', {
          offset: '1',
          stopColor: '#fff'
        })
      )
    )
  );
};

LogomarkInverted.defaultProps = {
  width: '248',
  height: '400',
  viewBox: '0 0 248 400',
  fill: 'none',
  xmlns: 'http://www.w3.org/2000/svg'
};

var LogomarkDark = function LogomarkDark(props) {
  return React.createElement(
    'svg',
    props,
    React.createElement('path', {
      opacity: '.8',
      fillRule: 'evenodd',
      clipRule: 'evenodd',
      d: 'M146 326.994l-.03 53.85c0 2.541-.363 3.873-1.449 5.156C127.669 386 .585 319.695 0 193.438.585 66.305 127.669 0 144.521 0c1.086 1.283 1.449 2.615 1.449 5.156l.03 53.832c-32.149 0-100.287 51.869-100.65 134.012.363 82.141 68.501 133.994 100.65 133.994z',
      transform: 'matrix(1 0 0 -1 101.771 393.615)',
      fill: 'url(#a)',
      fillOpacity: '.9'
    }),
    React.createElement('path', {
      fillRule: 'evenodd',
      clipRule: 'evenodd',
      d: 'M201.28 58.4c-78.704 0-142.506 63.396-142.506 141.6s63.802 141.6 142.506 141.6c16.289 0 32.139-2.602 46.72-7.604l-.024 55.429c0 2.876-1.975 5.586-4.698 6.215A203.348 203.348 0 0 1 201.28 400C90.116 400 0 310.457 0 200S90.116 0 201.28 0a203.33 203.33 0 0 1 42.032 4.367c2.785.645 4.664 3.26 4.664 6.204L248 65.99c-14.598-5.016-30.407-7.591-46.72-7.591z',
      transform: 'matrix(1 0 0 -1 0 400.43)',
      fill: 'url(#b)'
    }),
    React.createElement(
      'defs',
      null,
      React.createElement(
        'linearGradient',
        {
          id: 'a',
          x2: '1',
          gradientUnits: 'userSpaceOnUse',
          gradientTransform: 'matrix(124.002 0 0 327.836 -.002 364.582)'
        },
        React.createElement('stop', {
          stopColor: '#556989',
          stopOpacity: '.6'
        }),
        React.createElement('stop', {
          offset: '1',
          stopColor: '#506380'
        })
      ),
      React.createElement(
        'linearGradient',
        {
          id: 'b',
          x2: '1',
          gradientUnits: 'userSpaceOnUse',
          gradientTransform: 'matrix(223.505 0 0 360.493 13.5 219.754)'
        },
        React.createElement('stop', {
          stopColor: '#2F3A4C'
        }),
        React.createElement('stop', {
          offset: '1',
          stopColor: '#4D5A71'
        })
      )
    )
  );
};

LogomarkDark.defaultProps = {
  width: '248',
  height: '401',
  viewBox: '0 0 248 401',
  fill: 'none',
  xmlns: 'http://www.w3.org/2000/svg'
};

var LogotypeColored = function LogotypeColored(props) {
  return React.createElement(
    'svg',
    props,
    React.createElement(
      'g',
      {
        fillRule: 'evenodd',
        clipRule: 'evenodd'
      },
      React.createElement('path', {
        d: 'M146 326.994l-.03 53.85c0 2.541-.363 3.873-1.449 5.156C127.67 386 .587 319.695.002 193.438.587 66.305 127.67 0 144.521 0c1.086 1.283 1.449 2.615 1.449 5.156l.03 53.832c-32.148 0-100.286 51.869-100.648 134.012.362 82.141 68.5 133.994 100.648 133.994z',
        transform: 'matrix(1 0 0 -1 102 393)',
        fill: 'url(#a)'
      }),
      React.createElement('path', {
        d: 'M201.275 58.4c-78.702 0-142.503 63.396-142.503 141.6s63.801 141.6 142.503 141.6c16.289 0 32.138-2.602 46.719-7.604l-.024 55.429c0 2.876-1.975 5.586-4.698 6.215a203.338 203.338 0 0 1-41.997 4.36C90.114 400 0 310.457 0 200S90.114 0 201.275 0c14.414 0 28.475 1.506 42.031 4.367 2.785.645 4.664 3.26 4.664 6.204l.024 55.42c-14.598-5.016-30.406-7.591-46.719-7.591z',
        transform: 'matrix(1 0 0 -1 0 400)',
        fill: 'url(#b)'
      })
    ),
    React.createElement('path', {
      fillRule: 'evenodd',
      clipRule: 'evenodd',
      d: 'M418.612 281.772c-14.702 0-27.956-3.259-39.76-9.776-11.805-6.518-21.064-15.551-27.78-27.099C344.359 233.349 341 220.42 341 206.109c0-14.31 3.358-27.24 10.073-38.788 6.715-11.548 16.01-20.58 27.885-27.098 11.875-6.518 25.164-9.777 39.866-9.777 11.027 0 21.206 1.842 30.536 5.526 9.331 3.684 17.247 9.068 23.751 16.153l-9.967 9.99c-11.593-11.761-26.224-17.641-43.896-17.641-11.733 0-22.407 2.692-32.02 8.076-9.613 5.384-17.141 12.752-22.584 22.104-5.443 9.352-8.164 19.837-8.164 31.455 0 11.619 2.721 22.104 8.164 31.456 5.443 9.351 12.97 16.719 22.584 22.103 9.613 5.385 20.287 8.077 32.02 8.077 17.813 0 32.445-5.951 43.896-17.853l9.967 9.989c-6.504 7.085-14.456 12.504-23.857 16.259-9.4 3.755-19.615 5.632-30.642 5.632zm178.551-113.07l-55.771 125.185c-4.524 10.485-9.754 17.924-15.692 22.316-5.937 4.393-13.076 6.589-21.417 6.589-5.372 0-10.391-.85-15.056-2.551-4.666-1.7-8.695-4.25-12.087-7.651l6.997-11.264c5.655 5.667 12.441 8.501 20.358 8.501 5.089 0 9.436-1.417 13.041-4.251 3.605-2.834 6.963-7.651 10.073-14.452l4.877-10.84-49.833-111.581h15.692l41.987 95.004 41.987-95.004h14.844zm21.205-45.907h15.056v157.702h-15.056V122.795zm57.043 45.908h15.056v111.794h-15.056V168.703zm7.634-24.442c-3.11 0-5.725-1.062-7.846-3.188-2.12-2.125-3.18-4.676-3.18-7.651 0-2.834 1.06-5.314 3.18-7.439 2.121-2.125 4.736-3.188 7.846-3.188 3.111 0 5.726 1.027 7.846 3.082 2.121 2.054 3.181 4.498 3.181 7.332 0 3.118-1.06 5.739-3.181 7.864-2.12 2.126-4.735 3.188-7.846 3.188zm106.24 23.592c13.996 0 25.129 4.073 33.399 12.22 8.27 8.148 12.405 20.014 12.405 35.6v64.824h-15.056v-63.336c0-11.619-2.898-20.474-8.694-26.567-5.796-6.093-14.066-9.139-24.811-9.139-12.016 0-21.523 3.578-28.521 10.733-6.998 7.156-10.497 17.038-10.497 29.649v58.66h-15.056V168.703h14.42v20.616c4.1-6.801 9.79-12.08 17.071-15.834 7.28-3.755 15.727-5.632 25.34-5.632zm188.517-45.058v157.702h-14.419v-22.104c-4.524 7.51-10.497 13.248-17.919 17.216-7.422 3.967-15.798 5.951-25.129 5.951-10.461 0-19.933-2.409-28.415-7.227-8.482-4.817-15.127-11.547-19.933-20.19-4.807-8.644-7.21-18.491-7.21-29.543s2.403-20.9 7.21-29.543c4.806-8.643 11.451-15.338 19.933-20.084 8.482-4.747 17.954-7.12 28.415-7.12 9.048 0 17.212 1.877 24.493 5.632 7.28 3.755 13.253 9.245 17.918 16.472v-67.162h15.056zM921.608 268.17c7.775 0 14.808-1.807 21.099-5.42s11.239-8.749 14.844-15.409c3.605-6.659 5.408-14.24 5.408-22.741 0-8.502-1.803-16.082-5.408-22.741-3.605-6.66-8.553-11.796-14.844-15.41-6.291-3.612-13.324-5.419-21.099-5.419-7.917 0-15.021 1.807-21.312 5.42s-11.239 8.749-14.844 15.409c-3.605 6.66-5.407 14.24-5.407 22.741s1.802 16.082 5.407 22.741c3.605 6.66 8.553 11.796 14.844 15.409 6.291 3.613 13.395 5.42 21.312 5.42zm143.773 13.39c-10.744 0-20.428-2.444-29.051-7.333-8.624-4.888-15.41-11.654-20.358-20.297s-7.422-18.42-7.422-29.33c0-10.91 2.474-20.687 7.422-29.33 4.948-8.643 11.734-15.374 20.358-20.191 8.623-4.818 18.307-7.226 29.051-7.226s20.428 2.408 29.052 7.226c8.623 4.817 15.374 11.548 20.251 20.19 4.877 8.644 7.316 18.42 7.316 29.331 0 10.91-2.439 20.687-7.316 29.33-4.877 8.643-11.628 15.409-20.251 20.297-8.624 4.889-18.308 7.333-29.052 7.333zm0-13.39c7.917 0 15.021-1.807 21.312-5.42s11.203-8.749 14.738-15.409c3.534-6.659 5.301-14.24 5.301-22.741 0-8.502-1.767-16.082-5.301-22.741-3.535-6.66-8.447-11.796-14.738-15.41-6.291-3.612-13.395-5.419-21.312-5.419-7.917 0-15.02 1.807-21.311 5.42s-11.239 8.749-14.844 15.409c-3.605 6.66-5.408 14.24-5.408 22.741s1.803 16.082 5.408 22.741c3.605 6.66 8.553 11.796 14.844 15.409 6.291 3.613 13.394 5.42 21.311 5.42z',
      fill: '#304857'
    }),
    React.createElement(
      'defs',
      null,
      React.createElement(
        'linearGradient',
        {
          id: 'a',
          x2: '1',
          gradientUnits: 'userSpaceOnUse',
          gradientTransform: 'matrix(254.212 0 0 672.095 -71.796 193)'
        },
        React.createElement('stop', {
          stopColor: '#CEEDEA',
          stopOpacity: '.01'
        }),
        React.createElement('stop', {
          offset: '1',
          stopColor: '#B3E7E2'
        })
      ),
      React.createElement(
        'linearGradient',
        {
          id: 'b',
          x2: '1',
          gradientUnits: 'userSpaceOnUse',
          gradientTransform: 'matrix(257.382 0 0 415.141 -57.273 200)'
        },
        React.createElement('stop', {
          stopColor: '#3DA79D'
        }),
        React.createElement('stop', {
          offset: '1',
          stopColor: '#4CCCBF'
        })
      )
    )
  );
};

LogotypeColored.defaultProps = {
  width: '1122',
  height: '400',
  viewBox: '0 0 1122 400',
  fill: 'none',
  xmlns: 'http://www.w3.org/2000/svg'
};

var LogotypeInverted = function LogotypeInverted(props) {
  return React.createElement(
    'svg',
    props,
    React.createElement(
      'g',
      {
        fillRule: 'evenodd',
        clipRule: 'evenodd'
      },
      React.createElement('path', {
        opacity: '.5',
        d: 'M146 326.994l-.03 53.85c0 2.541-.363 3.873-1.449 5.156C127.67 386 .587 319.695.002 193.438.587 66.305 127.67 0 144.521 0c1.086 1.283 1.449 2.615 1.449 5.156l.03 53.832c-32.148 0-100.286 51.869-100.648 134.012.362 82.141 68.5 133.994 100.648 133.994z',
        transform: 'matrix(1 0 0 -1 102 393)',
        fill: 'url(#a)'
      }),
      React.createElement('path', {
        d: 'M201.275 341.6c-78.702 0-142.503-63.396-142.503-141.6S122.573 58.4 201.275 58.4c16.289 0 32.138 2.602 46.719 7.604l-.024-55.429c0-2.876-1.975-5.586-4.698-6.215A203.338 203.338 0 0 0 201.275 0C90.114 0 0 89.543 0 200s90.114 200 201.275 200c14.414 0 28.475-1.506 42.031-4.367 2.785-.645 4.664-3.26 4.664-6.204l.024-55.42c-14.598 5.016-30.406 7.591-46.719 7.591z',
        fill: '#fff'
      })
    ),
    React.createElement('path', {
      fillRule: 'evenodd',
      clipRule: 'evenodd',
      d: 'M418.612 281.772c-14.702 0-27.956-3.259-39.76-9.776-11.805-6.518-21.064-15.551-27.78-27.099C344.359 233.349 341 220.42 341 206.109c0-14.31 3.358-27.24 10.073-38.788 6.715-11.547 16.01-20.58 27.885-27.098 11.875-6.518 25.164-9.777 39.866-9.777 11.027 0 21.206 1.842 30.536 5.526 9.331 3.684 17.247 9.068 23.751 16.153l-9.967 9.99c-11.593-11.761-26.224-17.641-43.896-17.641-11.733 0-22.407 2.692-32.02 8.076-9.613 5.384-17.141 12.752-22.584 22.104-5.443 9.352-8.164 19.837-8.164 31.455 0 11.619 2.721 22.104 8.164 31.456 5.443 9.351 12.97 16.719 22.584 22.104 9.613 5.384 20.287 8.076 32.02 8.076 17.813 0 32.445-5.951 43.896-17.853l9.967 9.989c-6.504 7.085-14.456 12.504-23.857 16.259-9.4 3.755-19.615 5.632-30.642 5.632zm178.551-113.07l-55.771 125.185c-4.524 10.485-9.754 17.924-15.692 22.316-5.937 4.393-13.076 6.589-21.417 6.589-5.372 0-10.391-.85-15.056-2.551-4.666-1.7-8.695-4.25-12.087-7.651l6.997-11.264c5.655 5.667 12.441 8.501 20.358 8.501 5.089 0 9.436-1.417 13.041-4.251 3.605-2.834 6.963-7.651 10.073-14.452l4.877-10.84-49.833-111.581h15.692l41.987 95.004 41.987-95.004h14.844zm21.205-45.907h15.056v157.702h-15.056V122.795zm57.043 45.908h15.056v111.794h-15.056V168.703zm7.634-24.442c-3.11 0-5.725-1.062-7.846-3.188-2.12-2.125-3.18-4.676-3.18-7.651 0-2.834 1.06-5.314 3.18-7.439 2.121-2.125 4.736-3.188 7.846-3.188 3.111 0 5.726 1.027 7.846 3.082 2.121 2.054 3.181 4.498 3.181 7.332 0 3.118-1.06 5.739-3.181 7.864-2.12 2.126-4.735 3.188-7.846 3.188zm106.24 23.592c13.996 0 25.129 4.073 33.399 12.22 8.27 8.148 12.405 20.014 12.405 35.6v64.824h-15.056v-63.336c0-11.618-2.898-20.474-8.694-26.567-5.796-6.093-14.066-9.139-24.811-9.139-12.016 0-21.523 3.578-28.521 10.733-6.998 7.156-10.497 17.038-10.497 29.649v58.66h-15.056V168.703h14.42v20.616c4.1-6.801 9.79-12.08 17.071-15.834 7.28-3.755 15.727-5.632 25.34-5.632zm188.517-45.058v157.702h-14.419v-22.104c-4.524 7.51-10.497 13.248-17.919 17.216-7.422 3.967-15.798 5.951-25.129 5.951-10.461 0-19.933-2.409-28.415-7.227-8.482-4.817-15.127-11.547-19.933-20.19-4.807-8.644-7.21-18.491-7.21-29.543s2.403-20.9 7.21-29.543c4.806-8.643 11.451-15.338 19.933-20.084 8.482-4.747 17.954-7.12 28.415-7.12 9.048 0 17.212 1.877 24.493 5.632 7.28 3.755 13.253 9.245 17.918 16.472v-67.162h15.056zM921.608 268.17c7.775 0 14.808-1.807 21.099-5.42s11.239-8.749 14.844-15.409c3.605-6.659 5.408-14.24 5.408-22.741s-1.803-16.082-5.408-22.741c-3.605-6.66-8.553-11.796-14.844-15.41-6.291-3.612-13.324-5.419-21.099-5.419-7.917 0-15.021 1.807-21.312 5.42s-11.239 8.749-14.844 15.409c-3.605 6.66-5.407 14.24-5.407 22.741s1.802 16.082 5.407 22.741c3.605 6.66 8.553 11.796 14.844 15.409 6.291 3.613 13.395 5.42 21.312 5.42zm143.773 13.39c-10.744 0-20.428-2.444-29.051-7.333-8.624-4.888-15.41-11.654-20.358-20.297s-7.422-18.42-7.422-29.33c0-10.91 2.474-20.687 7.422-29.33 4.948-8.643 11.734-15.374 20.358-20.191 8.623-4.817 18.307-7.226 29.051-7.226s20.428 2.408 29.052 7.226c8.623 4.818 15.374 11.548 20.251 20.19 4.877 8.644 7.316 18.42 7.316 29.331 0 10.91-2.439 20.687-7.316 29.33-4.877 8.643-11.628 15.409-20.251 20.297-8.624 4.889-18.308 7.333-29.052 7.333zm0-13.39c7.917 0 15.021-1.807 21.312-5.42s11.203-8.749 14.738-15.409c3.534-6.659 5.301-14.24 5.301-22.741s-1.767-16.082-5.301-22.741c-3.535-6.66-8.447-11.796-14.738-15.41-6.291-3.612-13.395-5.419-21.312-5.419-7.917 0-15.02 1.807-21.311 5.42s-11.239 8.749-14.844 15.409c-3.605 6.66-5.408 14.24-5.408 22.741s1.803 16.082 5.408 22.741c3.605 6.66 8.553 11.796 14.844 15.409 6.291 3.613 13.394 5.42 21.311 5.42z',
      fill: '#fff'
    }),
    React.createElement(
      'defs',
      null,
      React.createElement(
        'linearGradient',
        {
          id: 'a',
          x2: '1',
          gradientUnits: 'userSpaceOnUse',
          gradientTransform: 'matrix(124 0 0 327.836 0 365.13)'
        },
        React.createElement('stop', {
          stopColor: '#fff',
          stopOpacity: '.28'
        }),
        React.createElement('stop', {
          offset: '1',
          stopColor: '#fff'
        })
      )
    )
  );
};

LogotypeInverted.defaultProps = {
  width: '1122',
  height: '400',
  viewBox: '0 0 1122 400',
  fill: 'none',
  xmlns: 'http://www.w3.org/2000/svg'
};

var LogotypeDark = function LogotypeDark(props) {
  return React.createElement(
    'svg',
    props,
    React.createElement(
      'g',
      {
        clipPath: 'url(#a)'
      },
      React.createElement(
        'g',
        {
          fillRule: 'evenodd',
          clipRule: 'evenodd'
        },
        React.createElement('path', {
          opacity: '.8',
          d: 'M145.87 326.994l-.03 53.85c0 2.541-.363 3.873-1.448 5.156C127.556 386 .585 319.695 0 193.438.585 66.305 127.556 0 144.392 0c1.085 1.283 1.448 2.615 1.448 5.156l.03 53.832c-32.12 0-100.198 51.869-100.56 134.012.362 82.141 68.44 133.994 100.56 133.994z',
          transform: 'matrix(1 0 0 -1 104.677 393.615)',
          fill: 'url(#b)',
          fillOpacity: '.9'
        }),
        React.createElement('path', {
          d: 'M201.1 58.4c-78.633 0-142.379 63.396-142.379 141.6S122.467 341.6 201.1 341.6c16.276 0 32.111-2.602 46.679-7.604l-.023 55.429c0 2.876-1.975 5.586-4.695 6.215A202.99 202.99 0 0 1 201.1 400C90.036 400 0 310.457 0 200S90.036 0 201.1 0a202.99 202.99 0 0 1 41.996 4.367c2.782.645 4.66 3.26 4.66 6.204l.023 55.42c-14.585-5.016-30.38-7.591-46.679-7.591z',
          transform: 'matrix(1 0 0 -1 2.997 400.43)',
          fill: 'url(#c)'
        })
      ),
      React.createElement('path', {
        fillRule: 'evenodd',
        clipRule: 'evenodd',
        d: 'M419.238 282.767c-14.69 0-27.93-3.259-39.725-9.776-11.794-6.518-21.045-15.551-27.754-27.099-6.71-11.548-10.064-24.477-10.064-38.788 0-14.31 3.355-27.24 10.064-38.788 6.709-11.547 15.995-20.58 27.86-27.098 11.865-6.518 25.141-9.777 39.831-9.777 11.017 0 21.187 1.842 30.509 5.526 9.322 3.684 17.232 9.068 23.729 16.153l-9.958 9.99c-11.582-11.761-26.2-17.641-43.856-17.641-11.724 0-22.387 2.692-31.992 8.076s-17.126 12.752-22.564 22.104c-5.438 9.352-8.157 19.837-8.157 31.455 0 11.619 2.72 22.104 8.157 31.456 5.438 9.351 12.96 16.719 22.564 22.104 9.605 5.384 20.268 8.076 31.992 8.076 17.797 0 32.415-5.951 43.856-17.853l9.958 9.989c-6.497 7.085-14.442 12.504-23.835 16.259-9.393 3.755-19.598 5.632-30.615 5.632zm178.392-113.07l-55.721 125.185c-4.52 10.485-9.746 17.924-15.678 22.316-5.933 4.393-13.065 6.589-21.399 6.589-5.367 0-10.381-.85-15.042-2.551-4.661-1.7-8.687-4.25-12.077-7.651l6.992-11.264c5.65 5.667 12.429 8.501 20.339 8.501 5.085 0 9.428-1.417 13.03-4.251 3.602-2.834 6.956-7.651 10.064-14.452l4.872-10.84-49.788-111.581H498.9l41.95 95.004 41.949-95.004h14.831zm21.186-45.907h15.043v157.702h-15.043V123.79zm56.993 45.908h15.042v111.794h-15.042V169.698zm7.627-24.442c-3.108 0-5.721-1.062-7.839-3.188-2.119-2.125-3.178-4.676-3.178-7.651 0-2.834 1.059-5.314 3.178-7.439 2.118-2.125 4.731-3.188 7.839-3.188 3.107 0 5.72 1.027 7.839 3.082 2.119 2.054 3.178 4.498 3.178 7.332 0 3.118-1.059 5.739-3.178 7.864-2.119 2.126-4.732 3.188-7.839 3.188zm106.145 23.592c13.983 0 25.106 4.073 33.369 12.22 8.263 8.148 12.394 20.014 12.394 35.6v64.824h-15.043v-63.336c0-11.618-2.895-20.474-8.686-26.567s-14.054-9.139-24.788-9.139c-12.006 0-21.505 3.578-28.497 10.733-6.991 7.156-10.487 17.038-10.487 29.649v58.66h-15.042V169.698h14.407v20.616c4.096-6.801 9.781-12.08 17.055-15.834 7.274-3.755 15.713-5.632 25.318-5.632zM977.93 123.79v157.702h-14.407v-22.104c-4.519 7.51-10.487 13.248-17.902 17.216-7.416 3.967-15.784 5.951-25.107 5.951-10.452 0-19.915-2.409-28.39-7.227-8.474-4.817-15.113-11.547-19.915-20.19-4.802-8.644-7.204-18.491-7.204-29.543s2.402-20.9 7.204-29.543 11.441-15.338 19.915-20.084c8.475-4.747 17.938-7.12 28.39-7.12 9.04 0 17.197 1.877 24.471 5.632 7.274 3.755 13.242 9.245 17.903 16.472V123.79h15.042zm-56.144 145.375c7.768 0 14.795-1.807 21.08-5.42 6.286-3.613 11.229-8.749 14.831-15.409 3.602-6.659 5.403-14.24 5.403-22.741s-1.801-16.082-5.403-22.741c-3.602-6.66-8.545-11.796-14.831-15.41-6.285-3.612-13.312-5.419-21.08-5.419-7.91 0-15.008 1.807-21.293 5.42s-11.229 8.749-14.831 15.409c-3.601 6.66-5.402 14.24-5.402 22.741s1.801 16.082 5.402 22.741c3.602 6.66 8.546 11.796 14.831 15.409 6.285 3.613 13.383 5.42 21.293 5.42zm143.645 13.39c-10.734 0-20.41-2.444-29.026-7.333-8.616-4.888-15.395-11.654-20.339-20.297-4.943-8.643-7.415-18.42-7.415-29.33 0-10.91 2.472-20.687 7.415-29.33 4.944-8.643 11.723-15.373 20.339-20.191 8.616-4.817 18.292-7.226 29.026-7.226 10.735 0 20.41 2.408 29.026 7.226s15.36 11.548 20.233 20.19c4.873 8.644 7.31 18.42 7.31 29.331 0 10.91-2.437 20.687-7.31 29.33-4.873 8.643-11.617 15.409-20.233 20.297-8.616 4.889-18.291 7.333-29.026 7.333zm0-13.39c7.91 0 15.007-1.807 21.293-5.42 6.285-3.613 11.193-8.749 14.724-15.409 3.532-6.659 5.297-14.24 5.297-22.741s-1.765-16.082-5.297-22.741c-3.531-6.66-8.439-11.796-14.724-15.41-6.286-3.612-13.383-5.419-21.293-5.419s-15.007 1.807-21.292 5.42c-6.286 3.613-11.229 8.749-14.831 15.409-3.602 6.66-5.403 14.24-5.403 22.741s1.801 16.082 5.403 22.741c3.602 6.66 8.545 11.796 14.831 15.409 6.285 3.613 13.382 5.42 21.292 5.42z',
        fill: '#304857'
      })
    ),
    React.createElement(
      'defs',
      null,
      React.createElement(
        'linearGradient',
        {
          id: 'b',
          x2: '1',
          gradientUnits: 'userSpaceOnUse',
          gradientTransform: 'matrix(123.891 0 0 327.836 -.002 364.582)'
        },
        React.createElement('stop', {
          stopColor: '#556989',
          stopOpacity: '.6'
        }),
        React.createElement('stop', {
          offset: '1',
          stopColor: '#506380'
        })
      ),
      React.createElement(
        'linearGradient',
        {
          id: 'c',
          x2: '1',
          gradientUnits: 'userSpaceOnUse',
          gradientTransform: 'matrix(223.306 0 0 360.493 13.488 219.754)'
        },
        React.createElement('stop', {
          stopColor: '#2F3A4C'
        }),
        React.createElement('stop', {
          offset: '1',
          stopColor: '#4D5A71'
        })
      ),
      React.createElement(
        'clipPath',
        {
          id: 'a'
        },
        React.createElement('path', {
          fill: '#fff',
          d: 'M0 0h1122v400H0z'
        })
      )
    )
  );
};

LogotypeDark.defaultProps = {
  width: '1122',
  height: '400',
  viewBox: '0 0 1122 400',
  fill: 'none',
  xmlns: 'http://www.w3.org/2000/svg'
};

var Logo = function Logo(_ref) {
  var type = _ref.type,
      color = _ref.color,
      rest = objectWithoutProperties(_ref, ['type', 'color']);

  var aspectRatio = void 0;

  switch (type) {
    case 'logotype':
      aspectRatio = 2.8;

      return React.createElement(
        LogoSC,
        _extends$1({ aspectRatio: aspectRatio }, rest),
        color === 'colored' && React.createElement(LogotypeColored, null),
        color === 'inverted' && React.createElement(LogotypeInverted, null),
        color === 'dark' && React.createElement(LogotypeDark, null)
      );

    case 'logomark':
      aspectRatio = 1;

      return React.createElement(
        LogoSC,
        _extends$1({ aspectRatio: aspectRatio }, rest),
        color === 'colored' && React.createElement(LogomarkColored, null),
        color === 'inverted' && React.createElement(LogomarkInverted, null),
        color === 'dark' && React.createElement(LogomarkDark, null)
      );

    default:
      console.log('No type matches');
      break;
  }
};
Logo.defaultProps = {
  size: 10,
  padding: 1,
  type: 'logotype',
  color: 'colored'
};
Logo.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Logo',
  'props': {
    'size': {
      'defaultValue': {
        'value': '10',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': ''
    },
    'padding': {
      'defaultValue': {
        'value': '1',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': ''
    },
    'type': {
      'defaultValue': {
        'value': '\'logotype\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'union',
        'raw': '\'logomark\' | \'logotype\'',
        'elements': [{
          'name': 'literal',
          'value': '\'logomark\''
        }, {
          'name': 'literal',
          'value': '\'logotype\''
        }]
      },
      'description': ''
    },
    'color': {
      'defaultValue': {
        'value': '\'colored\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'union',
        'raw': '\'colored\' | \'inverted\' | \'dark\'',
        'elements': [{
          'name': 'literal',
          'value': '\'colored\''
        }, {
          'name': 'literal',
          'value': '\'inverted\''
        }, {
          'name': 'literal',
          'value': '\'dark\''
        }]
      },
      'description': ''
    }
  }
};

var TagSC = styled__default.span.withConfig({
  displayName: 'styled__TagSC',
  componentId: 'sc-9mrwo7-0'
})(['padding:0 12px;height:20px;border-radius:50px;font-weight:500;line-height:20px;white-space:nowrap;display:inline-block;font-size:10px;letter-spacing:1px;font-weight:500;', ';', ';'], space, function (props) {
  var color$$1 = themeGet('colors.' + props.bg)(props);

  if (!color$$1) return null;

  return styled.css(['background-color:', ';color:', ';'], curriedTransparentize(0.9, color$$1), curriedDarken(0.05, color$$1));
});

var Tag = function Tag(_ref) {
  var children = _ref.children,
      color = _ref.color,
      rest = objectWithoutProperties(_ref, ['children', 'color']);
  return React.createElement(
    TagSC,
    _extends$1({}, rest, { bg: color }),
    children
  );
};
Tag.defaultProps = {
  color: 'info'
};
Tag.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Tag',
  'props': {
    'color': {
      'defaultValue': {
        'value': '\'info\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'union',
        'raw': 'ThemeColors | string',
        'elements': [{
          'name': 'ThemeColors'
        }, {
          'name': 'string'
        }]
      },
      'description': ''
    },
    'children': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    }
  }
};

var LinkSC = styled__default.a.withConfig({
  displayName: 'styled__LinkSC',
  componentId: 'sc-4qk2vh-0'
})(['cursor:pointer;text-decoration:none;transition:all 200ms ease;font-weight:', ';opacity:1;', ';', ';', ';', ';', ';', ';', ';'], function (props) {
  return props.active ? themeGet('fontWeight.5')(props) : themeGet('fontWeight.4')(props);
}, function (props) {
  switch (props.transition) {
    case 'opacity':
      return styled.css(['&:hover{opacity:0.8;}']);
    case 'scale':
      return styled.css(['transform:scale(1);&:hover{transform:scale(1.01);opacity:0.9;}']);
    default:
      return null;
  }
}, lineHeight, height, display, color, fontSize, space);

var Link = function Link(_ref) {
  var children = _ref.children,
      href = _ref.href,
      newPage = _ref.newPage,
      rest = objectWithoutProperties(_ref, ['children', 'href', 'newPage']);
  return React.createElement(
    LinkSC,
    _extends$1({ target: newPage ? '_blank' : '_self', href: href }, rest),
    children
  );
};
Link.defaultProps = {
  height: null,
  transition: 'opacity',
  onClick: null,
  href: '#',
  newPage: false,
  color: 'primary',
  fontSize: 'inherit'
};
Link.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Link',
  'props': {
    'height': {
      'defaultValue': {
        'value': 'null',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': ''
    },
    'transition': {
      'defaultValue': {
        'value': '\'opacity\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'union',
        'raw': '\'opacity\' | \'scale\'',
        'elements': [{
          'name': 'literal',
          'value': '\'opacity\''
        }, {
          'name': 'literal',
          'value': '\'scale\''
        }]
      },
      'description': ''
    },
    'onClick': {
      'defaultValue': {
        'value': 'null',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'signature',
        'type': 'function',
        'raw': '() => {}',
        'signature': {
          'arguments': [],
          'return': {
            'name': 'signature',
            'type': 'object',
            'raw': '{}',
            'signature': {
              'properties': []
            }
          }
        }
      },
      'description': ''
    },
    'href': {
      'defaultValue': {
        'value': '\'#\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'newPage': {
      'defaultValue': {
        'value': 'false',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'boolean'
      },
      'description': ''
    },
    'color': {
      'defaultValue': {
        'value': '\'primary\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'fontSize': {
      'defaultValue': {
        'value': '\'inherit\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'union',
        'raw': 'number | string',
        'elements': [{
          'name': 'number'
        }, {
          'name': 'string'
        }]
      },
      'description': ''
    },
    'children': {
      'required': true,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    },
    'display': {
      'required': false,
      'flowType': {
        'name': 'union',
        'raw': '\'inline-block\' | \'block\'',
        'elements': [{
          'name': 'literal',
          'value': '\'inline-block\''
        }, {
          'name': 'literal',
          'value': '\'block\''
        }]
      },
      'description': ''
    }
  }
};

var DateTimeSC = styled__default.span.withConfig({
  displayName: 'styled__DateTimeSC',
  componentId: 'ab3f7v-0'
})(['']);

var DateTime = function DateTime(_ref) {
  var value = _ref.value,
      customFormat = _ref.customFormat,
      mode = _ref.mode,
      compact = _ref.compact,
      rest = objectWithoutProperties(_ref, ['value', 'customFormat', 'mode', 'compact']);
  return React.createElement(
    DateTimeSC,
    rest,
    format$1({ value: value, customFormat: customFormat, mode: mode, compact: compact })
  );
};
DateTime.defaultProps = {
  compact: false,
  mode: 'dateTime',
  customFormat: null
};
DateTime.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'DateTime',
  'props': {
    'compact': {
      'defaultValue': {
        'value': 'false',
        'computed': false
      },
      'required': false
    },
    'mode': {
      'defaultValue': {
        'value': '\'dateTime\'',
        'computed': false
      },
      'required': false
    },
    'customFormat': {
      'defaultValue': {
        'value': 'null',
        'computed': false
      },
      'required': false
    }
  }
};

var getDimension = function getDimension(props, property) {
  if (!props[property]) return null;

  if (isNumber_1(props[property])) {
    var val = props.theme[property][props[property]] || props[property];
    return val + 'px';
  }
  return props[property];
};

var SpacerSC = styled__default.div.withConfig({
  displayName: 'styled__SpacerSC',
  componentId: 'i61rzu-0'
})(['width:100%;height:', ';pointer-events:none;'], function (props) {
  return getDimension(props, 'height');
});

var Spacer = function Spacer(_ref) {
  var height = _ref.height,
      rest = objectWithoutProperties(_ref, ['height']);
  return React.createElement(SpacerSC, _extends$1({ height: height }, rest));
};
Spacer.defaultProps = {
  height: 20
};
Spacer.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Spacer',
  'props': {
    'height': {
      'defaultValue': {
        'value': '20',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': ''
    }
  }
};

var Psc = styled__default.p.withConfig({
  displayName: 'styled__Psc',
  componentId: 'sc-1a7z4k3-0'
})(['', ';', ';', ';', ';', ';', ';line-height:', ' text-transform:', ';', ';'], textAlign, fontSize, space, size, color, fontStyle, function (_ref) {
  var lineHeight$$1 = _ref.lineHeight,
      rest = objectWithoutProperties(_ref, ['lineHeight']);
  return isString(lineHeight$$1) ? lineHeight$$1 : themeGet('lineHeights.' + lineHeight$$1)(rest);
}, function (_ref2) {
  var textTransform = _ref2.textTransform;
  return textTransform;
}, function (props) {
  return props.overflow && styled.css(['overflow:hidden;display:-webkit-box;-webkit-line-clamp:3;-webkit-box-orient:vertical;text-overflow:ellipsis;']);
});

var P = function P(_ref) {
  var children = _ref.children,
      rest = objectWithoutProperties(_ref, ['children']);
  return React.createElement(
    Psc,
    rest,
    children
  );
};

/**
 * defaultProps
 */
P.defaultProps = {
  color: 'text',
  textAlign: 'left',
  fontWeight: 5,
  fontSize: 2,
  overflow: null,
  mb: 5
};
P.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'P',
  'props': {
    'color': {
      'defaultValue': {
        'value': '\'text\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'textAlign': {
      'defaultValue': {
        'value': '\'left\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'TextAlign'
      },
      'description': ''
    },
    'fontWeight': {
      'defaultValue': {
        'value': '5',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': ''
    },
    'fontSize': {
      'defaultValue': {
        'value': '2',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'union',
        'raw': 'string | number',
        'elements': [{
          'name': 'string'
        }, {
          'name': 'number'
        }]
      },
      'description': ''
    },
    'overflow': {
      'defaultValue': {
        'value': 'null',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    },
    'mb': {
      'defaultValue': {
        'value': '5',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': ''
    },
    'children': {
      'required': true,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    }
  }
};

var HoverTransitionSC = styled__default.div.withConfig({
  displayName: 'styled__HoverTransitionSC',
  componentId: 'sc-1wox4j9-0'
})(['transition:all 200ms ease-in-out;cursor:pointer;', ';'], function (props) {
  switch (props.transition) {
    case 'opacity':
      return styled.css(['&:hover{opacity:0.8;}']);
    case 'scale':
      return styled.css(['transform:scale(1);&:hover{transform:scale(1.01);opacity:0.9;}']);
    default:
      return null;
  }
});

var HoverTransition = function HoverTransition(_ref) {
  var children = _ref.children,
      rest = objectWithoutProperties(_ref, ['children']);
  return React.createElement(
    HoverTransitionSC,
    rest,
    children
  );
};
HoverTransition.defaultProps = {
  transition: 'opacity'
};
HoverTransition.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'HoverTransition',
  'props': {
    'transition': {
      'defaultValue': {
        'value': '\'opacity\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'union',
        'raw': '\'opacity\' | \'scale\'',
        'elements': [{
          'name': 'literal',
          'value': '\'opacity\''
        }, {
          'name': 'literal',
          'value': '\'scale\''
        }]
      },
      'description': ''
    },
    'children': {
      'required': true,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    }
  }
};

var color$2 = themeGet('colors.separator', 'grey');

var SeparatorSC = styled__default.hr.withConfig({
  displayName: 'styled__SeparatorSC',
  componentId: 'sc-4z4dv2-0'
})(['height:1px;width:100%;border:none;color:', ';background-color:', ';', ';'], color$2, color$2, space);

var Separator = function Separator(props) {
  return React.createElement(SeparatorSC, props);
};

Separator.defaultProps = {
  my: 5
};
Separator.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Separator',
  'props': {
    'my': {
      'defaultValue': {
        'value': '5',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': 'Margin top and bottom'
    }
  }
};

var ImageOuter = styled__default.div.withConfig({
  displayName: 'Image__ImageOuter',
  componentId: 'sc-1u4z8re-0'
})(['width:', ';height:', ';overflow:hidden;background-color:', ';', ';', ';'], function (_ref) {
  var width$$1 = _ref.width;
  return toPx(width$$1);
}, function (_ref2) {
  var height$$1 = _ref2.height;
  return toPx(height$$1);
}, themeGet('colors.greys.5'), space, borderRadius);

var ImageWrapper = styled__default.img.withConfig({
  displayName: 'Image__ImageWrapper',
  componentId: 'sc-1u4z8re-1'
})(['display:block;object-fit:cover;']);

// $FlowFixMe
var Image = styled.withTheme(function (_ref3) {
  var src = _ref3.src,
      width$$1 = _ref3.width,
      height$$1 = _ref3.height,
      theme = _ref3.theme,
      rest = objectWithoutProperties(_ref3, ['src', 'width', 'height', 'theme']);

  var w = themeGet('space.' + width$$1)({ theme: theme });
  var h = themeGet('space.' + height$$1)({ theme: theme });

  return React.createElement(
    ImageOuter,
    _extends$1({}, rest, { width: w, height: h }),
    React.createElement(ImageWrapper, { width: w, height: h, src: src })
  );
});
Image.defaultProps = {
  width: 10,
  height: 10
};

var ClickableWrapper = styled__default.div.withConfig({
  displayName: 'Clickable__ClickableWrapper',
  componentId: 'elb8xl-0'
})(['border:none;outline:none;cursor:pointer;user-select:none;padding:0;']);

var Clickable = function Clickable(_ref) {
  var children = _ref.children,
      type = _ref.type,
      tabIndex = _ref.tabIndex,
      rest = objectWithoutProperties(_ref, ['children', 'type', 'tabIndex']);
  return React.createElement(
    ClickableWrapper,
    _extends$1({}, rest, { type: type, role: 'button', tabIndex: tabIndex }),
    children
  );
};
Clickable.defaultProps = {
  tabIndex: 0,
  type: 'button'
};
Clickable.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Clickable',
  'props': {
    'tabIndex': {
      'defaultValue': {
        'value': '0',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    },
    'type': {
      'defaultValue': {
        'value': '\'button\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'union',
        'raw': '\'submit\' | \'reset\' | \'button\'',
        'elements': [{
          'name': 'literal',
          'value': '\'submit\''
        }, {
          'name': 'literal',
          'value': '\'reset\''
        }, {
          'name': 'literal',
          'value': '\'button\''
        }]
      },
      'description': ''
    },
    'children': {
      'required': true,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    }
  }
};

var ActionItem = function ActionItem(_ref) {
  var label = _ref.label;
  return React.createElement(
    P,
    { mb: 0 },
    label
  );
};
ActionItem.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'ActionItem',
  'props': {
    'label': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    }
  }
};

var DropdownWrapper = styled__default.div.withConfig({
  displayName: 'Dropdown__DropdownWrapper',
  componentId: 'sc-1q8dnr5-0'
})(['display:inline-flex;position:relative;']);

var DropdownContainer = styled__default.div.withConfig({
  displayName: 'Dropdown__DropdownContainer',
  componentId: 'sc-1q8dnr5-1'
})(['opacity:0;position:absolute;transition:all 200ms ease;background:white;box-shadow:', ';border-radius:', 'px;overflow:scroll;pointer-events:none;height:0px;width:', ';', ';', ';', ';'], themeGet('elevation.5'), themeGet('borderRadius.2'), function (_ref) {
  var width$$1 = _ref.width;
  return toPx(width$$1);
}, function (_ref2) {
  var posX = _ref2.posX;
  return posX === 'left' ? styled.css(['right:0;']) : styled.css(['left:0;']);
}, function (_ref3) {
  var posY = _ref3.posY,
      elementHeight = _ref3.elementHeight;
  return posY === 'bottom' ? styled.css(['top:', 'px;'], elementHeight) : styled.css(['bottom:', 'px;'], elementHeight);
}, function (_ref4) {
  var isOpen = _ref4.isOpen;
  return isOpen && styled.css(['height:auto;pointer-events:all;opacity:1;z-index:9;max-height:256px;']);
});

var ActionItemList = styled__default.div.withConfig({
  displayName: 'Dropdown__ActionItemList',
  componentId: 'sc-1q8dnr5-2'
})(['display:flex;flex-direction:column;']);

var ActionItemWrapper = styled__default.div.withConfig({
  displayName: 'Dropdown__ActionItemWrapper',
  componentId: 'sc-1q8dnr5-3'
})(['font-weight:400;font-size:17px;line-height:26px;', ';&:hover{background:', ';cursor:pointer;}'], space, function (props) {
  return curriedLighten(0.425, themeGet('colors.primary')(props));
});

var HiddenInput = styled__default.input.withConfig({
  displayName: 'Dropdown__HiddenInput',
  componentId: 'sc-1q8dnr5-4'
})(['pointer-events:none;opacity:0;position:absolute;']);

var Dropdown = function (_React$Component) {
  inherits(Dropdown, _React$Component);

  function Dropdown() {
    var _ref5;

    var _temp, _this, _ret;

    classCallCheck(this, Dropdown);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = possibleConstructorReturn(this, (_ref5 = Dropdown.__proto__ || Object.getPrototypeOf(Dropdown)).call.apply(_ref5, [this].concat(args))), _this), _this.state = {
      isOpen: false,
      isHovering: false,
      isFocused: false
    }, _this.hiddenInputRef = React.createRef(), _this.toggleOpen = function (e) {
      var isOpen = _this.state.isOpen;


      if (e) e.stopPropagation();
      if (!isOpen) _this.focusInput();

      _this.setState({
        isOpen: !isOpen
      });
    }, _this.setFocus = function (value) {
      if (!value) _this.setOpen(false);
      _this.setState({
        isFocused: value
      });
    }, _this.focusInput = function () {
      _this.hiddenInputRef.current.focus();
    }, _this.blurInput = function () {
      _this.hiddenInputRef.current.blur();
    }, _this.setOpen = function (value) {
      _this.setState({
        isOpen: value
      });
    }, _this.onBlur = function () {
      var isHovering = _this.state.isHovering;

      if (!isHovering) {
        _this.setFocus(false);
        _this.blurInput();
      } else {
        _this.focusInput();
      }
    }, _this.onFocus = function () {
      _this.setFocus(true);
    }, _this.setHovering = function (value) {
      _this.setState({
        isHovering: value
      });
    }, _this.onKeyDown = function (e) {
      switch (e.key) {
        case 'Tab':
          _this.setFocus(false);
          break;
        case 'Enter':
        case 'Space':
        case 'ArrowDown':
        case 'ArrowUp':
          _this.toggleOpen();
          break;
        default:
          break;
      }
    }, _this.onAction = function (e, action) {
      var onAction = _this.props.onAction;


      if (e) e.stopPropagation();

      if (onAction) onAction(action);
      _this.setOpen(false);
    }, _temp), possibleConstructorReturn(_this, _ret);
  }

  createClass(Dropdown, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          w = _props.width,
          posX = _props.posX,
          posY = _props.posY,
          children = _props.children,
          actions = _props.actions,
          elementHeight = _props.elementHeight,
          renderItem = _props.renderItem,
          rest = objectWithoutProperties(_props, ['width', 'posX', 'posY', 'children', 'actions', 'elementHeight', 'renderItem']);
      var _state = this.state,
          isOpen = _state.isOpen,
          isFocused = _state.isFocused;


      return React.createElement(
        DropdownWrapper,
        { onMouseEnter: function onMouseEnter() {
            return _this2.setHovering(true);
          }, onMouseLeave: function onMouseLeave() {
            return _this2.setHovering(false);
          } },
        React.createElement(HiddenInput, {
          onKeyDown: this.onKeyDown,
          innerRef: this.hiddenInputRef,
          onFocus: this.onFocus,
          onBlur: this.onBlur
        }),
        React.createElement(
          Clickable,
          { onClick: this.toggleOpen },
          children(_extends$1({
            active: isOpen,
            focused: isFocused
          }, rest))
        ),
        React.createElement(
          DropdownContainer,
          { posX: posX, posY: posY, elementHeight: elementHeight, width: w, isOpen: isOpen },
          React.createElement(
            ActionItemList,
            null,
            map_1(actions, function (a, i) {
              return React.createElement(
                Clickable,
                { tabIndex: isOpen ? 0 : -1, onClick: function onClick(e) {
                    return _this2.onAction(e, a);
                  }, key: i },
                React.createElement(
                  ActionItemWrapper,
                  { px: 3, py: 2 },
                  renderItem(a)
                )
              );
            })
          )
        )
      );
    }
  }]);
  return Dropdown;
}(React.Component);
Dropdown.defaultProps = {
  actions: [],
  renderItem: function renderItem(obj) {
    return React.createElement(ActionItem, { label: obj.label });
  },
  width: 200,
  posX: 'left',
  posY: 'bottom',
  elementHeight: 40
};
Dropdown.__docgenInfo = {
  'description': '',
  'methods': [{
    'name': 'toggleOpen',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'e',
      'type': {
        'name': 'any'
      }
    }],
    'returns': null
  }, {
    'name': 'setFocus',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'value',
      'type': {
        'name': 'boolean'
      }
    }],
    'returns': null
  }, {
    'name': 'focusInput',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'blurInput',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'setOpen',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'value',
      'type': {
        'name': 'boolean'
      }
    }],
    'returns': null
  }, {
    'name': 'onBlur',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'onFocus',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'setHovering',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'value',
      'type': {
        'name': 'boolean'
      }
    }],
    'returns': null
  }, {
    'name': 'onKeyDown',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'e',
      'type': {
        'name': 'any'
      }
    }],
    'returns': null
  }, {
    'name': 'onAction',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'e',
      'type': {
        'name': 'any'
      }
    }, {
      'name': 'action',
      'type': {
        'name': 'any'
      }
    }],
    'returns': null
  }],
  'displayName': 'Dropdown',
  'props': {
    'actions': {
      'defaultValue': {
        'value': '[]',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'Array',
        'elements': [{
          'name': 'any'
        }],
        'raw': 'Array<any>'
      },
      'description': ''
    },
    'renderItem': {
      'defaultValue': {
        'value': '(obj: any) => <ActionItem label={obj.label} />',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'Function'
      },
      'description': ''
    },
    'width': {
      'defaultValue': {
        'value': '200',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': ''
    },
    'posX': {
      'defaultValue': {
        'value': '\'left\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'union',
        'raw': '\'right\' | \'left\'',
        'elements': [{
          'name': 'literal',
          'value': '\'right\''
        }, {
          'name': 'literal',
          'value': '\'left\''
        }]
      },
      'description': ''
    },
    'posY': {
      'defaultValue': {
        'value': '\'bottom\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'union',
        'raw': '\'top\' | \'bottom\'',
        'elements': [{
          'name': 'literal',
          'value': '\'top\''
        }, {
          'name': 'literal',
          'value': '\'bottom\''
        }]
      },
      'description': ''
    },
    'elementHeight': {
      'defaultValue': {
        'value': '40',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': ''
    },
    'children': {
      'required': true,
      'flowType': {
        'name': 'Function'
      },
      'description': ''
    },
    'onAction': {
      'required': true,
      'flowType': {
        'name': 'Function'
      },
      'description': ''
    }
  }
};

var WEEKDAYS_LONG = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

var WEEKDAYS_SHORT = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];

var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

function formatDay(day) {
  return day.toDateString();
}

function formatMonthTitle(d) {
  return MONTHS[d.getMonth()] + ' ' + d.getFullYear();
}

function formatWeekdayShort(i) {
  return WEEKDAYS_SHORT[i];
}

function formatWeekdayLong(i) {
  return WEEKDAYS_LONG[i];
}

function getFirstDayOfWeek() {
  return 0;
}

function getMonths() {
  return MONTHS;
}

var LocaleUtils = {
  formatDay: formatDay,
  formatMonthTitle: formatMonthTitle,
  formatWeekdayShort: formatWeekdayShort,
  formatWeekdayLong: formatWeekdayLong,
  getFirstDayOfWeek: getFirstDayOfWeek,
  getMonths: getMonths
};

var LocaleUtils$1 = /*#__PURE__*/Object.freeze({
  formatDay: formatDay,
  formatMonthTitle: formatMonthTitle,
  formatWeekdayShort: formatWeekdayShort,
  formatWeekdayLong: formatWeekdayLong,
  getFirstDayOfWeek: getFirstDayOfWeek,
  getMonths: getMonths,
  default: LocaleUtils
});

var LEFT = 37;
var UP = 38;
var RIGHT = 39;
var DOWN = 40;
var ENTER = 13;
var SPACE = 32;
var ESC = 27;
var TAB = 9;

var Caption = function (_Component) {
  inherits(Caption, _Component);

  function Caption(props) {
    classCallCheck(this, Caption);

    var _this = possibleConstructorReturn(this, (Caption.__proto__ || Object.getPrototypeOf(Caption)).call(this, props));

    _this.handleKeyUp = _this.handleKeyUp.bind(_this);
    return _this;
  }

  createClass(Caption, [{
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps) {
      return nextProps.locale !== this.props.locale || nextProps.date.getMonth() !== this.props.date.getMonth() || nextProps.date.getFullYear() !== this.props.date.getFullYear();
    }
  }, {
    key: 'handleKeyUp',
    value: function handleKeyUp(e) {
      if (e.keyCode === ENTER) {
        this.props.onClick(e);
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          date = _props.date,
          months = _props.months,
          locale = _props.locale,
          localeUtils = _props.localeUtils,
          onClick = _props.onClick,
          captionDisplay = _props.captionDisplay;


      var CaptionDisplay = captionDisplay;

      return React__default.createElement(
        CaptionDisplay,
        { role: 'heading' },
        React__default.createElement(
          'div',
          { onClick: onClick, onKeyUp: this.handleKeyUp },
          months ? months[date.getMonth()] + ' ' + date.getFullYear() : localeUtils.formatMonthTitle(date, locale)
        )
      );
    }
  }]);
  return Caption;
}(React.Component);

Caption.propTypes = {
  date: propTypes.instanceOf(Date),
  months: propTypes.arrayOf(propTypes.string),
  locale: propTypes.string,
  localeUtils: propTypes.object,
  onClick: propTypes.func
};
Caption.defaultProps = {
  localeUtils: LocaleUtils
};
Caption.__docgenInfo = {
  'description': '',
  'methods': [{
    'name': 'handleKeyUp',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'e',
      'type': null
    }],
    'returns': null
  }],
  'displayName': 'Caption',
  'props': {
    'localeUtils': {
      'defaultValue': {
        'value': 'LocaleUtils',
        'computed': true
      },
      'type': {
        'name': 'object'
      },
      'required': false,
      'description': ''
    },
    'date': {
      'type': {
        'name': 'instanceOf',
        'value': 'Date'
      },
      'required': false,
      'description': ''
    },
    'months': {
      'type': {
        'name': 'arrayOf',
        'value': {
          'name': 'string'
        }
      },
      'required': false,
      'description': ''
    },
    'locale': {
      'type': {
        'name': 'string'
      },
      'required': false,
      'description': ''
    },
    'onClick': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    }
  }
};

// Proxy object to map classnames when css modules are not used

var defaultClassNames = {
  container: 'DayPicker',
  wrapper: 'DayPicker-wrapper',
  interactionDisabled: 'DayPicker--interactionDisabled',
  months: 'DayPicker-Months',
  month: 'DayPicker-Month',

  navBar: 'DayPicker-NavBar',
  navButtonPrev: 'DayPicker-NavButton DayPicker-NavButton--prev',
  navButtonNext: 'DayPicker-NavButton DayPicker-NavButton--next',
  navButtonInteractionDisabled: 'DayPicker-NavButton--interactionDisabled',

  caption: 'DayPicker-Caption',
  weekdays: 'DayPicker-Weekdays',
  weekdaysRow: 'DayPicker-WeekdaysRow',
  weekday: 'DayPicker-Weekday',
  body: 'DayPicker-Body',
  week: 'DayPicker-Week',
  weekNumber: 'DayPicker-WeekNumber',
  day: 'DayPicker-Day',
  footer: 'DayPicker-Footer',
  todayButton: 'DayPicker-TodayButton',

  // default modifiers
  today: 'today',
  selected: 'selected',
  disabled: 'disabled',
  outside: 'outside'
};

var Navbar = function (_Component) {
  inherits(Navbar, _Component);

  function Navbar() {
    var _ref;

    var _temp, _this, _ret;

    classCallCheck(this, Navbar);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = possibleConstructorReturn(this, (_ref = Navbar.__proto__ || Object.getPrototypeOf(Navbar)).call.apply(_ref, [this].concat(args))), _this), _this.handleNextClick = function () {
      if (_this.props.onNextClick) {
        _this.props.onNextClick();
      }
    }, _this.handlePreviousClick = function () {
      if (_this.props.onPreviousClick) {
        _this.props.onPreviousClick();
      }
    }, _this.handleNextKeyDown = function (e) {
      if (e.keyCode !== ENTER && e.keyCode !== SPACE) {
        return;
      }
      e.preventDefault();
      _this.handleNextClick();
    }, _this.handlePreviousKeyDown = function (e) {
      if (e.keyCode !== ENTER && e.keyCode !== SPACE) {
        return;
      }
      e.preventDefault();
      _this.handlePreviousClick();
    }, _temp), possibleConstructorReturn(_this, _ret);
  }

  createClass(Navbar, [{
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps) {
      return nextProps.labels !== this.props.labels || nextProps.dir !== this.props.dir || this.props.showPreviousButton !== nextProps.showPreviousButton || this.props.showNextButton !== nextProps.showNextButton;
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          classNames = _props.classNames,
          className = _props.className,
          showPreviousButton = _props.showPreviousButton,
          showNextButton = _props.showNextButton,
          labels = _props.labels,
          dir = _props.dir,
          navBarDisplay = _props.navBarDisplay,
          navBarNextDisplay = _props.navBarNextDisplay,
          navBarPrevDisplay = _props.navBarPrevDisplay;


      var previousClickHandler = void 0;
      var nextClickHandler = void 0;
      var previousKeyDownHandler = void 0;
      var nextKeyDownHandler = void 0;
      var shouldShowPrevious = void 0;
      var shouldShowNext = void 0;

      if (dir === 'rtl') {
        previousClickHandler = this.handleNextClick;
        nextClickHandler = this.handlePreviousClick;
        previousKeyDownHandler = this.handleNextKeyDown;
        nextKeyDownHandler = this.handlePreviousKeyDown;
        shouldShowNext = showPreviousButton;
        shouldShowPrevious = showNextButton;
      } else {
        previousClickHandler = this.handlePreviousClick;
        nextClickHandler = this.handleNextClick;
        previousKeyDownHandler = this.handlePreviousKeyDown;
        nextKeyDownHandler = this.handleNextKeyDown;
        shouldShowNext = showNextButton;
        shouldShowPrevious = showPreviousButton;
      }

      var previousClassName = shouldShowPrevious ? classNames.navButtonPrev : classNames.navButtonPrev + ' ' + classNames.navButtonInteractionDisabled;

      var nextClassName = shouldShowNext ? classNames.navButtonNext : classNames.navButtonNext + ' ' + classNames.navButtonInteractionDisabled;

      var NavBarPrevDisplay = navBarPrevDisplay;
      var NavBarNextDisplay = navBarNextDisplay;

      var previousButton = React__default.createElement(NavBarPrevDisplay, {
        tabIndex: '0',
        role: 'button',
        'aria-label': labels.previousMonth,
        key: 'previous',
        className: previousClassName,
        onKeyDown: shouldShowPrevious ? previousKeyDownHandler : undefined,
        onClick: shouldShowPrevious ? previousClickHandler : undefined
      });

      var nextButton = React__default.createElement(NavBarNextDisplay, {
        tabIndex: '0',
        role: 'button',
        'aria-label': labels.nextMonth,
        key: 'right',
        className: nextClassName,
        onKeyDown: shouldShowNext ? nextKeyDownHandler : undefined,
        onClick: shouldShowNext ? nextClickHandler : undefined
      });

      var NavBarDisplay = navBarDisplay;

      return React__default.createElement(
        NavBarDisplay,
        { className: className || classNames.navBar },
        dir === 'rtl' ? [nextButton, previousButton] : [previousButton, nextButton]
      );
    }
  }]);
  return Navbar;
}(React.Component);

Navbar.propTypes = {
  classNames: propTypes.shape({
    navBar: propTypes.string.isRequired,
    navButtonPrev: propTypes.string.isRequired,
    navButtonNext: propTypes.string.isRequired
  }),
  className: propTypes.string,
  showPreviousButton: propTypes.bool,
  showNextButton: propTypes.bool,
  onPreviousClick: propTypes.func,
  onNextClick: propTypes.func,
  dir: propTypes.string,
  labels: propTypes.shape({
    previousMonth: propTypes.string.isRequired,
    nextMonth: propTypes.string.isRequired
  })
};
Navbar.defaultProps = {
  classNames: defaultClassNames,
  dir: 'ltr',
  labels: {
    previousMonth: 'Previous Month',
    nextMonth: 'Next Month'
  },
  showPreviousButton: true,
  showNextButton: true
};
Navbar.__docgenInfo = {
  'description': '',
  'methods': [{
    'name': 'handleNextClick',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'handlePreviousClick',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'handleNextKeyDown',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'e',
      'type': null
    }],
    'returns': null
  }, {
    'name': 'handlePreviousKeyDown',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'e',
      'type': null
    }],
    'returns': null
  }],
  'displayName': 'Navbar',
  'props': {
    'classNames': {
      'defaultValue': {
        'value': 'defaultClassNames',
        'computed': true
      },
      'type': {
        'name': 'shape',
        'value': {
          'navBar': {
            'name': 'string',
            'required': true
          },
          'navButtonPrev': {
            'name': 'string',
            'required': true
          },
          'navButtonNext': {
            'name': 'string',
            'required': true
          }
        }
      },
      'required': false,
      'description': ''
    },
    'dir': {
      'defaultValue': {
        'value': '\'ltr\'',
        'computed': false
      },
      'type': {
        'name': 'string'
      },
      'required': false,
      'description': ''
    },
    'labels': {
      'defaultValue': {
        'value': '{\n  previousMonth: \'Previous Month\',\n  nextMonth: \'Next Month\',\n}',
        'computed': false
      },
      'type': {
        'name': 'shape',
        'value': {
          'previousMonth': {
            'name': 'string',
            'required': true
          },
          'nextMonth': {
            'name': 'string',
            'required': true
          }
        }
      },
      'required': false,
      'description': ''
    },
    'showPreviousButton': {
      'defaultValue': {
        'value': 'true',
        'computed': false
      },
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'showNextButton': {
      'defaultValue': {
        'value': 'true',
        'computed': false
      },
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'className': {
      'type': {
        'name': 'string'
      },
      'required': false,
      'description': ''
    },
    'onPreviousClick': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onNextClick': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    }
  }
};

var Weekdays = function (_Component) {
  inherits(Weekdays, _Component);

  function Weekdays() {
    classCallCheck(this, Weekdays);
    return possibleConstructorReturn(this, (Weekdays.__proto__ || Object.getPrototypeOf(Weekdays)).apply(this, arguments));
  }

  createClass(Weekdays, [{
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps) {
      return this.props !== nextProps;
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          classNames = _props.classNames,
          firstDayOfWeek = _props.firstDayOfWeek,
          showWeekNumbers = _props.showWeekNumbers,
          weekdaysLong = _props.weekdaysLong,
          weekdaysShort = _props.weekdaysShort,
          locale = _props.locale,
          localeUtils = _props.localeUtils,
          weekdayElement = _props.weekdayElement,
          weekdayDisplay = _props.weekdayDisplay,
          weekdaysDisplay = _props.weekdaysDisplay,
          weekdaysRowDisplay = _props.weekdaysRowDisplay;

      var days = [];
      for (var i = 0; i < 7; i += 1) {
        var weekday = (i + firstDayOfWeek) % 7;
        var elementProps = {
          key: i,
          className: classNames.weekday,
          weekday: weekday,
          weekdaysLong: weekdaysLong,
          weekdaysShort: weekdaysShort,
          localeUtils: localeUtils,
          locale: locale
        };
        var element = React__default.isValidElement(weekdayElement) ? React__default.cloneElement(weekdayElement, elementProps) : React__default.createElement(weekdayElement, elementProps);
        days.push(element);
      }

      var WeekdaysDisplay = weekdaysDisplay;
      var WeekdaysRowDisplay = weekdaysRowDisplay;

      return React__default.createElement(
        WeekdaysDisplay,
        { role: 'rowgroup' },
        React__default.createElement(
          WeekdaysRowDisplay,
          { className: classNames.weekdaysRow, role: 'row' },
          showWeekNumbers && React__default.createElement('div', { className: classNames.weekday }),
          days
        )
      );
    }
  }]);
  return Weekdays;
}(React.Component);

Weekdays.propTypes = {
  classNames: propTypes.shape({
    weekday: propTypes.string.isRequired,
    weekdays: propTypes.string.isRequired,
    weekdaysRow: propTypes.string.isRequired
  }).isRequired,

  firstDayOfWeek: propTypes.number.isRequired,
  weekdaysLong: propTypes.arrayOf(propTypes.string),
  weekdaysShort: propTypes.arrayOf(propTypes.string),
  showWeekNumbers: propTypes.bool,
  locale: propTypes.string.isRequired,
  localeUtils: propTypes.object.isRequired,
  weekdayElement: propTypes.oneOfType([propTypes.element, propTypes.func, propTypes.instanceOf(React__default.Component)])
};
Weekdays.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Weekdays',
  'props': {
    'classNames': {
      'type': {
        'name': 'shape',
        'value': {
          'weekday': {
            'name': 'string',
            'required': true
          },
          'weekdays': {
            'name': 'string',
            'required': true
          },
          'weekdaysRow': {
            'name': 'string',
            'required': true
          }
        }
      },
      'required': true,
      'description': ''
    },
    'firstDayOfWeek': {
      'type': {
        'name': 'number'
      },
      'required': true,
      'description': ''
    },
    'weekdaysLong': {
      'type': {
        'name': 'arrayOf',
        'value': {
          'name': 'string'
        }
      },
      'required': false,
      'description': ''
    },
    'weekdaysShort': {
      'type': {
        'name': 'arrayOf',
        'value': {
          'name': 'string'
        }
      },
      'required': false,
      'description': ''
    },
    'showWeekNumbers': {
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'locale': {
      'type': {
        'name': 'string'
      },
      'required': true,
      'description': ''
    },
    'localeUtils': {
      'type': {
        'name': 'object'
      },
      'required': true,
      'description': ''
    },
    'weekdayElement': {
      'type': {
        'name': 'union',
        'value': [{
          'name': 'element'
        }, {
          'name': 'func'
        }, {
          'name': 'instanceOf',
          'value': 'React.Component'
        }]
      },
      'required': false,
      'description': ''
    }
  }
};

/**
 * Clone a date object.
 *
 * @export
 * @param  {Date} d The date to clone
 * @return {Date} The cloned date
 */
function clone(d) {
  return new Date(d.getTime());
}

/**
 * Return `true` if the passed value is a valid JavaScript Date object.
 *
 * @export
 * @param {any} value
 * @returns {Boolean}
 */
function isDate$1(value) {
  return value instanceof Date && !isNaN(value.valueOf());
}

/**
 * Return `d` as a new date with `n` months added.
 *
 * @export
 * @param {[type]} d
 * @param {[type]} n
 */
function addMonths$1(d, n) {
  var newDate = clone(d);
  newDate.setMonth(d.getMonth() + n);
  return newDate;
}

/**
 * Return `true` if two dates are the same day, ignoring the time.
 *
 * @export
 * @param  {Date}  d1
 * @param  {Date}  d2
 * @return {Boolean}
 */
function isSameDay$1(d1, d2) {
  if (!d1 || !d2) {
    return false;
  }
  return d1.getDate() === d2.getDate() && d1.getMonth() === d2.getMonth() && d1.getFullYear() === d2.getFullYear();
}

/**
 * Return `true` if two dates fall in the same month.
 *
 * @export
 * @param  {Date}  d1
 * @param  {Date}  d2
 * @return {Boolean}
 */
function isSameMonth$1(d1, d2) {
  if (!d1 || !d2) {
    return false;
  }
  return d1.getMonth() === d2.getMonth() && d1.getFullYear() === d2.getFullYear();
}

/**
 * Returns `true` if the first day is before the second day.
 *
 * @export
 * @param {Date} d1
 * @param {Date} d2
 * @returns {Boolean}
 */
function isDayBefore(d1, d2) {
  var day1 = clone(d1).setHours(0, 0, 0, 0);
  var day2 = clone(d2).setHours(0, 0, 0, 0);
  return day1 < day2;
}

/**
 * Returns `true` if the first day is after the second day.
 *
 * @export
 * @param {Date} d1
 * @param {Date} d2
 * @returns {Boolean}
 */
function isDayAfter(d1, d2) {
  var day1 = clone(d1).setHours(0, 0, 0, 0);
  var day2 = clone(d2).setHours(0, 0, 0, 0);
  return day1 > day2;
}

/**
 * Return `true` if a day is in the past, e.g. yesterday or any day
 * before yesterday.
 *
 * @export
 * @param  {Date}  d
 * @return {Boolean}
 */
function isPastDay(d) {
  var today = new Date();
  today.setHours(0, 0, 0, 0);
  return isDayBefore(d, today);
}

/**
 * Return `true` if a day is in the future, e.g. tomorrow or any day
 * after tomorrow.
 *
 * @export
 * @param  {Date}  d
 * @return {Boolean}
 */
function isFutureDay(d) {
  var tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
  tomorrow.setHours(0, 0, 0, 0);
  return d >= tomorrow;
}

/**
 * Return `true` if day `d` is between days `d1` and `d2`,
 * without including them.
 *
 * @export
 * @param  {Date}  d
 * @param  {Date}  d1
 * @param  {Date}  d2
 * @return {Boolean}
 */
function isDayBetween(d, d1, d2) {
  var date = clone(d);
  date.setHours(0, 0, 0, 0);
  return isDayAfter(date, d1) && isDayBefore(date, d2) || isDayAfter(date, d2) && isDayBefore(date, d1);
}

/**
 * Add a day to a range and return a new range. A range is an object with
 * `from` and `to` days.
 *
 * @export
 * @param {Date} day
 * @param {Object} range
 * @return {Object} Returns a new range object
 */
function addDayToRange(day) {
  var range = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : { from: null, to: null };
  var from = range.from,
      to = range.to;

  if (!from) {
    from = day;
  } else if (from && to && isSameDay$1(from, to) && isSameDay$1(day, from)) {
    from = null;
    to = null;
  } else if (to && isDayBefore(day, from)) {
    from = day;
  } else if (to && isSameDay$1(day, to)) {
    from = day;
    to = day;
  } else {
    to = day;
    if (isDayBefore(to, from)) {
      to = from;
      from = day;
    }
  }

  return { from: from, to: to };
}

/**
 * Return `true` if a day is included in a range of days.
 *
 * @export
 * @param  {Date}  day
 * @param  {Object}  range
 * @return {Boolean}
 */
function isDayInRange(day, range) {
  var from = range.from,
      to = range.to;

  return from && isSameDay$1(day, from) || to && isSameDay$1(day, to) || from && to && isDayBetween(day, from, to);
}

/**
 * Return the year's week number (as per ISO, i.e. with the week starting from monday)
 * for the given day.
 *
 * @export
 * @param {Date} day
 * @returns {Number}
 */
function getWeekNumber(day) {
  var date = clone(day);
  date.setHours(0, 0, 0);
  date.setDate(date.getDate() + 4 - (date.getDay() || 7));
  return Math.ceil(((date - new Date(date.getFullYear(), 0, 1)) / 8.64e7 + 1) / 7);
}

var DateUtils = {
  addDayToRange: addDayToRange,
  addMonths: addMonths$1,
  clone: clone,
  getWeekNumber: getWeekNumber,
  isDate: isDate$1,
  isDayAfter: isDayAfter,
  isDayBefore: isDayBefore,
  isDayBetween: isDayBetween,
  isDayInRange: isDayInRange,
  isFutureDay: isFutureDay,
  isPastDay: isPastDay,
  isSameDay: isSameDay$1,
  isSameMonth: isSameMonth$1
};

var DateUtils$1 = /*#__PURE__*/Object.freeze({
  clone: clone,
  isDate: isDate$1,
  addMonths: addMonths$1,
  isSameDay: isSameDay$1,
  isSameMonth: isSameMonth$1,
  isDayBefore: isDayBefore,
  isDayAfter: isDayAfter,
  isPastDay: isPastDay,
  isFutureDay: isFutureDay,
  isDayBetween: isDayBetween,
  addDayToRange: addDayToRange,
  isDayInRange: isDayInRange,
  getWeekNumber: getWeekNumber,
  default: DateUtils
});

function cancelEvent(e) {
  e.preventDefault();
  e.stopPropagation();
}

function getFirstDayOfMonth(d) {
  return new Date(d.getFullYear(), d.getMonth(), 1, 12);
}

function getDaysInMonth$1(d) {
  var resultDate = getFirstDayOfMonth(d);

  resultDate.setMonth(resultDate.getMonth() + 1);
  resultDate.setDate(resultDate.getDate() - 1);

  return resultDate.getDate();
}

function getModifiersFromProps(props) {
  var modifiers = _extends$1({}, props.modifiers);
  if (props.selectedDays) {
    modifiers[props.classNames.selected] = props.selectedDays;
  }
  if (props.disabledDays) {
    modifiers[props.classNames.disabled] = props.disabledDays;
  }
  return modifiers;
}

function getFirstDayOfWeekFromProps(props) {
  var firstDayOfWeek = props.firstDayOfWeek,
      _props$locale = props.locale,
      locale = _props$locale === undefined ? 'en' : _props$locale,
      _props$localeUtils = props.localeUtils,
      localeUtils = _props$localeUtils === undefined ? {} : _props$localeUtils;

  if (!isNaN(firstDayOfWeek)) {
    return firstDayOfWeek;
  }
  if (localeUtils.getFirstDayOfWeek) {
    return localeUtils.getFirstDayOfWeek(locale);
  }
  return 0;
}

function isRangeOfDates(value) {
  return !!(value && value.from && value.to);
}

function getMonthsDiff(d1, d2) {
  return d2.getMonth() - d1.getMonth() + 12 * (d2.getFullYear() - d1.getFullYear());
}

function getWeekArray(d) {
  var firstDayOfWeek = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : getFirstDayOfWeek();
  var fixedWeeks = arguments[2];

  var daysInMonth = getDaysInMonth$1(d);
  var dayArray = [];

  var week = [];
  var weekArray = [];

  for (var i = 1; i <= daysInMonth; i += 1) {
    dayArray.push(new Date(d.getFullYear(), d.getMonth(), i, 12));
  }

  dayArray.forEach(function (day) {
    if (week.length > 0 && day.getDay() === firstDayOfWeek) {
      weekArray.push(week);
      week = [];
    }
    week.push(day);
    if (dayArray.indexOf(day) === dayArray.length - 1) {
      weekArray.push(week);
    }
  });

  // unshift days to start the first week
  var firstWeek = weekArray[0];
  for (var _i = 7 - firstWeek.length; _i > 0; _i -= 1) {
    var outsideDate = clone(firstWeek[0]);
    outsideDate.setDate(firstWeek[0].getDate() - 1);
    firstWeek.unshift(outsideDate);
  }

  // push days until the end of the last week
  var lastWeek = weekArray[weekArray.length - 1];
  for (var _i2 = lastWeek.length; _i2 < 7; _i2 += 1) {
    var _outsideDate = clone(lastWeek[lastWeek.length - 1]);
    _outsideDate.setDate(lastWeek[lastWeek.length - 1].getDate() + 1);
    lastWeek.push(_outsideDate);
  }

  // add extra weeks to reach 6 weeks
  if (fixedWeeks && weekArray.length < 6) {
    var lastExtraWeek = void 0;

    for (var _i3 = weekArray.length; _i3 < 6; _i3 += 1) {
      lastExtraWeek = weekArray[weekArray.length - 1];
      var lastDay = lastExtraWeek[lastExtraWeek.length - 1];
      var extraWeek = [];

      for (var j = 0; j < 7; j += 1) {
        var _outsideDate2 = clone(lastDay);
        _outsideDate2.setDate(lastDay.getDate() + j + 1);
        extraWeek.push(_outsideDate2);
      }

      weekArray.push(extraWeek);
    }
  }

  return weekArray;
}

function startOfMonth$1(d) {
  var newDate = clone(d);
  newDate.setDate(1);
  newDate.setHours(12, 0, 0, 0); // always set noon to avoid time zone issues
  return newDate;
}

function getDayNodes(node, classNames) {
  var outsideClassName = void 0;
  if (classNames === defaultClassNames) {
    // When using CSS modules prefix the modifier as required by the BEM syntax
    outsideClassName = classNames.day + '--' + classNames.outside;
  } else {
    outsideClassName = '' + classNames.outside;
  }
  var dayQuery = classNames.day.replace(/ /g, '.');
  var outsideDayQuery = outsideClassName.replace(/ /g, '.');
  var selector = '.' + dayQuery + ':not(.' + outsideDayQuery + ')';
  return node.querySelectorAll(selector);
}

function nodeListToArray(nodeList) {
  return Array.prototype.slice.call(nodeList, 0);
}

function hasOwnProp(obj, prop) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}

/* eslint-disable jsx-a11y/no-static-element-interactions, react/forbid-prop-types */

function handleEvent(handler, day, modifiers) {
  if (!handler) {
    return undefined;
  }
  return function (e) {
    e.persist();
    handler(day, modifiers, e);
  };
}

var Day = function (_Component) {
  inherits(Day, _Component);

  function Day() {
    classCallCheck(this, Day);
    return possibleConstructorReturn(this, (Day.__proto__ || Object.getPrototypeOf(Day)).apply(this, arguments));
  }

  createClass(Day, [{
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps) {
      var _this2 = this;

      var propNames = Object.keys(this.props);
      var nextPropNames = Object.keys(nextProps);
      if (propNames.length !== nextPropNames.length) {
        return true;
      }
      return propNames.some(function (name) {
        if (name === 'modifiers' || name === 'modifiersStyles' || name === 'classNames') {
          var prop = _this2.props[name];
          var nextProp = nextProps[name];
          var modifiers = Object.keys(prop);
          var nextModifiers = Object.keys(nextProp);
          if (modifiers.length !== nextModifiers.length) {
            return true;
          }
          return modifiers.some(function (mod) {
            return !hasOwnProp(nextProp, mod) || prop[mod] !== nextProp[mod];
          });
        }
        if (name === 'day') {
          return !isSameDay$1(_this2.props[name], nextProps[name]);
        }
        return !hasOwnProp(nextProps, name) || _this2.props[name] !== nextProps[name];
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          classNames = _props.classNames,
          modifiersStyles = _props.modifiersStyles,
          day = _props.day,
          tabIndex = _props.tabIndex,
          empty = _props.empty,
          modifiers = _props.modifiers,
          onMouseEnter = _props.onMouseEnter,
          onMouseLeave = _props.onMouseLeave,
          onMouseUp = _props.onMouseUp,
          onMouseDown = _props.onMouseDown,
          onClick = _props.onClick,
          onKeyDown = _props.onKeyDown,
          onTouchStart = _props.onTouchStart,
          onTouchEnd = _props.onTouchEnd,
          onFocus = _props.onFocus,
          ariaLabel = _props.ariaLabel,
          ariaDisabled = _props.ariaDisabled,
          ariaSelected = _props.ariaSelected,
          children = _props.children,
          dayCellDisplay = _props.dayCellDisplay,
          dayDisplay = _props.dayDisplay,
          isToday = _props.isToday,
          isOutside = _props.isOutside;


      var className = classNames.day;
      if (classNames !== defaultClassNames) {
        // When using CSS modules prefix the modifier as required by the BEM syntax
        className += ' ' + Object.keys(modifiers).join(' ');
      } else {
        className += Object.keys(modifiers).map(function (modifier) {
          return ' ' + className + '--' + modifier;
        }).join('');
      }

      var style = void 0;
      if (modifiersStyles) {
        Object.keys(modifiers).filter(function (modifier) {
          return !!modifiersStyles[modifier];
        }).forEach(function (modifier) {
          style = _extends$1({}, style, modifiersStyles[modifier]);
        });
      }

      var DayCellDisplay = dayCellDisplay;
      var DayDisplay = dayDisplay;

      if (empty) {
        return React__default.createElement(DayCellDisplay, { 'aria-disabled': true, style: style });
      }

      return React__default.createElement(
        DayCellDisplay,
        {
          tabIndex: tabIndex,
          style: style,
          role: 'gridcell',
          'aria-label': ariaLabel,
          'aria-disabled': ariaDisabled,
          'aria-selected': ariaSelected,
          onClick: handleEvent(onClick, day, modifiers),
          onKeyDown: handleEvent(onKeyDown, day, modifiers),
          onMouseEnter: handleEvent(onMouseEnter, day, modifiers),
          onMouseLeave: handleEvent(onMouseLeave, day, modifiers),
          onMouseUp: handleEvent(onMouseUp, day, modifiers),
          onMouseDown: handleEvent(onMouseDown, day, modifiers),
          onTouchEnd: handleEvent(onTouchEnd, day, modifiers),
          onTouchStart: handleEvent(onTouchStart, day, modifiers),
          onFocus: handleEvent(onFocus, day, modifiers)
        },
        React__default.createElement(
          DayDisplay,
          { isToday: isToday, isOutside: isOutside, isDisabled: false, isSelected: ariaSelected },
          children
        )
      );
    }
  }]);
  return Day;
}(React.Component);

Day.propTypes = {
  classNames: propTypes.shape({
    day: propTypes.string.isRequired
  }).isRequired,

  day: propTypes.instanceOf(Date).isRequired,
  children: propTypes.node.isRequired,

  ariaDisabled: propTypes.bool,
  ariaLabel: propTypes.string,
  ariaSelected: propTypes.bool,
  empty: propTypes.bool,
  modifiers: propTypes.object,
  modifiersStyles: propTypes.object,
  onClick: propTypes.func,
  onKeyDown: propTypes.func,
  onMouseEnter: propTypes.func,
  onMouseLeave: propTypes.func,
  onMouseDown: propTypes.func,
  onMouseUp: propTypes.func,
  onTouchEnd: propTypes.func,
  onTouchStart: propTypes.func,
  onFocus: propTypes.func,
  tabIndex: propTypes.number
};
Day.defaultProps = {
  tabIndex: -1
};
Day.defaultProps = {
  modifiers: {},
  modifiersStyles: {},
  empty: false
};
Day.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Day',
  'props': {
    'tabIndex': {
      'defaultValue': {
        'value': '-1',
        'computed': false
      },
      'type': {
        'name': 'number'
      },
      'required': false,
      'description': ''
    },
    'classNames': {
      'type': {
        'name': 'shape',
        'value': {
          'day': {
            'name': 'string',
            'required': true
          }
        }
      },
      'required': true,
      'description': ''
    },
    'day': {
      'type': {
        'name': 'instanceOf',
        'value': 'Date'
      },
      'required': true,
      'description': ''
    },
    'children': {
      'type': {
        'name': 'node'
      },
      'required': true,
      'description': ''
    },
    'ariaDisabled': {
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'ariaLabel': {
      'type': {
        'name': 'string'
      },
      'required': false,
      'description': ''
    },
    'ariaSelected': {
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'empty': {
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'modifiers': {
      'type': {
        'name': 'object'
      },
      'required': false,
      'description': ''
    },
    'modifiersStyles': {
      'type': {
        'name': 'object'
      },
      'required': false,
      'description': ''
    },
    'onClick': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onKeyDown': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onMouseEnter': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onMouseLeave': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onMouseDown': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onMouseUp': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onTouchEnd': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onTouchStart': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onFocus': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    }
  }
};

/**
 * Return `true` if a date matches the specified modifier.
 *
 * @export
 * @param {Date} day
 * @param {Any} modifier
 * @return {Boolean}
 */
function dayMatchesModifier(day, modifier) {
  if (!modifier) {
    return false;
  }
  var arr = Array.isArray(modifier) ? modifier : [modifier];
  return arr.some(function (mod) {
    if (!mod) {
      return false;
    }
    if (mod instanceof Date) {
      return isSameDay$1(day, mod);
    }
    if (isRangeOfDates(mod)) {
      return isDayInRange(day, mod);
    }
    if (mod.after && mod.before && isDayAfter(mod.before, mod.after)) {
      return isDayAfter(day, mod.after) && isDayBefore(day, mod.before);
    }
    if (mod.after && mod.before && (isDayAfter(mod.after, mod.before) || isSameDay$1(mod.after, mod.before))) {
      return isDayAfter(day, mod.after) || isDayBefore(day, mod.before);
    }
    if (mod.after) {
      return isDayAfter(day, mod.after);
    }
    if (mod.before) {
      return isDayBefore(day, mod.before);
    }
    if (mod.daysOfWeek) {
      return mod.daysOfWeek.some(function (dayOfWeek) {
        return day.getDay() === dayOfWeek;
      });
    }
    if (typeof mod === 'function') {
      return mod(day);
    }
    return false;
  });
}

/**
 * Return the modifiers matching the given day for the given
 * object of modifiers.
 *
 * @export
 * @param {Date} day
 * @param {Object} [modifiersObj={}]
 * @return {Array}
 */
function getModifiersForDay(day) {
  var modifiersObj = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  return Object.keys(modifiersObj).reduce(function (modifiers, modifierName) {
    var value = modifiersObj[modifierName];
    if (dayMatchesModifier(day, value)) {
      modifiers.push(modifierName);
    }
    return modifiers;
  }, []);
}

var ModifiersUtils = { dayMatchesModifier: dayMatchesModifier, getModifiersForDay: getModifiersForDay };

var ModifiersUtils$1 = /*#__PURE__*/Object.freeze({
  dayMatchesModifier: dayMatchesModifier,
  getModifiersForDay: getModifiersForDay,
  default: ModifiersUtils
});

var Month = function (_Component) {
  inherits(Month, _Component);

  function Month() {
    var _ref;

    var _temp, _this, _ret;

    classCallCheck(this, Month);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = possibleConstructorReturn(this, (_ref = Month.__proto__ || Object.getPrototypeOf(Month)).call.apply(_ref, [this].concat(args))), _this), _this.renderDay = function (day) {
      var monthNumber = _this.props.month.getMonth();
      var propModifiers = getModifiersFromProps(_this.props);
      var dayModifiers = getModifiersForDay(day, propModifiers);
      var _this$props = _this.props,
          dayCellDisplay = _this$props.dayCellDisplay,
          dayDisplay = _this$props.dayDisplay;


      var isToday = isSameDay$1(day, new Date());
      var isOutside = day.getMonth() !== monthNumber;

      var tabIndex = -1;
      // Focus on the first day of the month
      if (_this.props.onDayClick && !isOutside && day.getDate() === 1) {
        tabIndex = _this.props.tabIndex; // eslint-disable-line prefer-destructuring
      }
      var key = '' + day.getFullYear() + day.getMonth() + day.getDate();
      var modifiers = {};
      dayModifiers.forEach(function (modifier) {
        modifiers[modifier] = true;
      });

      return React__default.createElement(
        Day,
        {
          key: '' + (isOutside ? 'outside-' : '') + key,
          classNames: _this.props.classNames,
          day: day,
          modifiers: modifiers,
          modifiersStyles: _this.props.modifiersStyles,
          empty: isOutside && !_this.props.showOutsideDays && !_this.props.fixedWeeks,
          tabIndex: tabIndex,
          ariaLabel: _this.props.localeUtils.formatDay(day, _this.props.locale),
          ariaDisabled: isOutside || dayModifiers.indexOf('disabled') > -1,
          ariaSelected: dayModifiers.indexOf('selected') > -1,
          onClick: _this.props.onDayClick,
          onFocus: _this.props.onDayFocus,
          onKeyDown: _this.props.onDayKeyDown,
          onMouseEnter: _this.props.onDayMouseEnter,
          onMouseLeave: _this.props.onDayMouseLeave,
          onMouseDown: _this.props.onDayMouseDown,
          onMouseUp: _this.props.onDayMouseUp,
          onTouchEnd: _this.props.onDayTouchEnd,
          onTouchStart: _this.props.onDayTouchStart,
          dayCellDisplay: dayCellDisplay,
          dayDisplay: dayDisplay,
          isToday: isToday,
          isOutside: isOutside
        },
        _this.props.renderDay(day, modifiers)
      );
    }, _temp), possibleConstructorReturn(_this, _ret);
  }

  createClass(Month, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          classNames = _props.classNames,
          month = _props.month,
          months = _props.months,
          fixedWeeks = _props.fixedWeeks,
          captionElement = _props.captionElement,
          weekdayElement = _props.weekdayElement,
          locale = _props.locale,
          localeUtils = _props.localeUtils,
          weekdaysLong = _props.weekdaysLong,
          weekdaysShort = _props.weekdaysShort,
          firstDayOfWeek = _props.firstDayOfWeek,
          onCaptionClick = _props.onCaptionClick,
          showWeekNumbers = _props.showWeekNumbers,
          showWeekDays = _props.showWeekDays,
          onWeekClick = _props.onWeekClick,
          weekDisplay = _props.weekDisplay,
          weeksDisplay = _props.weeksDisplay,
          monthDisplay = _props.monthDisplay,
          monthsDisplay = _props.monthsDisplay,
          weekdayDisplay = _props.weekdayDisplay,
          weekdaysDisplay = _props.weekdaysDisplay,
          weekdaysRowDisplay = _props.weekdaysRowDisplay;


      var captionProps = {
        date: month,
        classNames: classNames,
        months: months,
        localeUtils: localeUtils,
        locale: locale,
        onClick: onCaptionClick ? function (e) {
          return onCaptionClick(month, e);
        } : undefined
      };
      var caption = React__default.isValidElement(captionElement) ? React__default.cloneElement(captionElement, captionProps) : React__default.createElement(captionElement, captionProps);

      var weeks = getWeekArray(month, firstDayOfWeek, fixedWeeks);

      var WeeksDisplay = weeksDisplay;
      var WeekDisplay = weekDisplay;
      var MonthDisplay = monthDisplay;

      return React__default.createElement(
        MonthDisplay,
        { className: classNames.month, role: 'grid' },
        caption,
        showWeekDays && React__default.createElement(Weekdays, {
          classNames: classNames,
          weekdaysShort: weekdaysShort,
          weekdaysLong: weekdaysLong,
          firstDayOfWeek: firstDayOfWeek,
          showWeekNumbers: showWeekNumbers,
          locale: locale,
          localeUtils: localeUtils,
          weekdayElement: weekdayElement,
          weekdaysDisplay: weekdaysDisplay,
          weekdaysRowDisplay: weekdaysRowDisplay
        }),
        React__default.createElement(
          WeeksDisplay,
          { className: classNames.body, role: 'rowgroup' },
          weeks.map(function (week) {
            var weekNumber = void 0;
            if (showWeekNumbers) {
              weekNumber = getWeekNumber(week[6]);
            }

            return React__default.createElement(
              WeekDisplay,
              { key: week[0].getTime(), className: classNames.week, role: 'row' },
              showWeekNumbers && React__default.createElement(
                'div',
                {
                  className: classNames.weekNumber,
                  tabIndex: 0,
                  role: 'gridcell',
                  onClick: onWeekClick ? function (e) {
                    return onWeekClick(weekNumber, week, e);
                  } : undefined,
                  onKeyUp: onWeekClick ? function (e) {
                    return e.keyCode === ENTER && onWeekClick(weekNumber, week, e);
                  } : undefined
                },
                _this2.props.renderWeek(weekNumber, week, month)
              ),
              week.map(_this2.renderDay)
            );
          })
        )
      );
    }
  }]);
  return Month;
}(React.Component);

Month.propTypes = {
  classNames: propTypes.shape({
    body: propTypes.string.isRequired,
    month: propTypes.string.isRequired,
    outside: propTypes.string.isRequired,
    today: propTypes.string.isRequired,
    week: propTypes.string.isRequired
  }).isRequired,
  tabIndex: propTypes.number,

  month: propTypes.instanceOf(Date).isRequired,
  months: propTypes.arrayOf(propTypes.string),

  modifiersStyles: propTypes.object,

  showWeekDays: propTypes.bool,
  showOutsideDays: propTypes.bool,

  renderDay: propTypes.func.isRequired,
  renderWeek: propTypes.func.isRequired,

  captionElement: propTypes.oneOfType([propTypes.element, propTypes.func, propTypes.instanceOf(React__default.Component)]).isRequired,
  weekdayElement: propTypes.oneOfType([propTypes.element, propTypes.func, propTypes.instanceOf(React__default.Component)]),

  fixedWeeks: propTypes.bool,
  showWeekNumbers: propTypes.bool,

  locale: propTypes.string.isRequired,
  localeUtils: propTypes.object.isRequired,
  weekdaysLong: propTypes.arrayOf(propTypes.string),
  weekdaysShort: propTypes.arrayOf(propTypes.string),
  firstDayOfWeek: propTypes.number.isRequired,

  onCaptionClick: propTypes.func,
  onDayClick: propTypes.func,
  onDayFocus: propTypes.func,
  onDayKeyDown: propTypes.func,
  onDayMouseEnter: propTypes.func,
  onDayMouseLeave: propTypes.func,
  onDayMouseDown: propTypes.func,
  onDayMouseUp: propTypes.func,
  onDayTouchEnd: propTypes.func,
  onDayTouchStart: propTypes.func,
  onWeekClick: propTypes.func
};
Month.__docgenInfo = {
  'description': '',
  'methods': [{
    'name': 'renderDay',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'day',
      'type': null
    }],
    'returns': null
  }],
  'displayName': 'Month',
  'props': {
    'classNames': {
      'type': {
        'name': 'shape',
        'value': {
          'body': {
            'name': 'string',
            'required': true
          },
          'month': {
            'name': 'string',
            'required': true
          },
          'outside': {
            'name': 'string',
            'required': true
          },
          'today': {
            'name': 'string',
            'required': true
          },
          'week': {
            'name': 'string',
            'required': true
          }
        }
      },
      'required': true,
      'description': ''
    },
    'tabIndex': {
      'type': {
        'name': 'number'
      },
      'required': false,
      'description': ''
    },
    'month': {
      'type': {
        'name': 'instanceOf',
        'value': 'Date'
      },
      'required': true,
      'description': ''
    },
    'months': {
      'type': {
        'name': 'arrayOf',
        'value': {
          'name': 'string'
        }
      },
      'required': false,
      'description': ''
    },
    'modifiersStyles': {
      'type': {
        'name': 'object'
      },
      'required': false,
      'description': ''
    },
    'showWeekDays': {
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'showOutsideDays': {
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'renderDay': {
      'type': {
        'name': 'func'
      },
      'required': true,
      'description': ''
    },
    'renderWeek': {
      'type': {
        'name': 'func'
      },
      'required': true,
      'description': ''
    },
    'captionElement': {
      'type': {
        'name': 'union',
        'value': [{
          'name': 'element'
        }, {
          'name': 'func'
        }, {
          'name': 'instanceOf',
          'value': 'React.Component'
        }]
      },
      'required': true,
      'description': ''
    },
    'weekdayElement': {
      'type': {
        'name': 'union',
        'value': [{
          'name': 'element'
        }, {
          'name': 'func'
        }, {
          'name': 'instanceOf',
          'value': 'React.Component'
        }]
      },
      'required': false,
      'description': ''
    },
    'fixedWeeks': {
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'showWeekNumbers': {
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'locale': {
      'type': {
        'name': 'string'
      },
      'required': true,
      'description': ''
    },
    'localeUtils': {
      'type': {
        'name': 'object'
      },
      'required': true,
      'description': ''
    },
    'weekdaysLong': {
      'type': {
        'name': 'arrayOf',
        'value': {
          'name': 'string'
        }
      },
      'required': false,
      'description': ''
    },
    'weekdaysShort': {
      'type': {
        'name': 'arrayOf',
        'value': {
          'name': 'string'
        }
      },
      'required': false,
      'description': ''
    },
    'firstDayOfWeek': {
      'type': {
        'name': 'number'
      },
      'required': true,
      'description': ''
    },
    'onCaptionClick': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onDayClick': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onDayFocus': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onDayKeyDown': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onDayMouseEnter': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onDayMouseLeave': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onDayMouseDown': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onDayMouseUp': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onDayTouchEnd': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onDayTouchStart': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onWeekClick': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    }
  }
};

var Weekday = function (_Component) {
  inherits(Weekday, _Component);

  function Weekday() {
    classCallCheck(this, Weekday);
    return possibleConstructorReturn(this, (Weekday.__proto__ || Object.getPrototypeOf(Weekday)).apply(this, arguments));
  }

  createClass(Weekday, [{
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps) {
      return this.props !== nextProps;
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          weekday = _props.weekday,
          weekdaysLong = _props.weekdaysLong,
          weekdaysShort = _props.weekdaysShort,
          localeUtils = _props.localeUtils,
          locale = _props.locale,
          weekdayDisplay = _props.weekdayDisplay;


      var title = void 0;

      if (weekdaysLong) {
        title = weekdaysLong[weekday];
      } else {
        title = localeUtils.formatWeekdayLong(weekday, locale);
      }

      var content = void 0;

      if (weekdaysShort) {
        content = weekdaysShort[weekday];
      } else {
        content = localeUtils.formatWeekdayShort(weekday, locale);
      }

      var WeekdayDisplay = weekdayDisplay;

      return React__default.createElement(
        WeekdayDisplay,
        { role: 'columnheader' },
        React__default.createElement(
          'abbr',
          { title: title },
          content
        )
      );
    }
  }]);
  return Weekday;
}(React.Component);

Weekday.propTypes = {
  weekday: propTypes.number,
  className: propTypes.string,
  locale: propTypes.string,
  localeUtils: propTypes.object,
  weekdaysLong: propTypes.arrayOf(propTypes.string),
  weekdaysShort: propTypes.arrayOf(propTypes.string),
  weekdayDisplay: propTypes.any
};
Weekday.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Weekday',
  'props': {
    'weekday': {
      'type': {
        'name': 'number'
      },
      'required': false,
      'description': ''
    },
    'className': {
      'type': {
        'name': 'string'
      },
      'required': false,
      'description': ''
    },
    'locale': {
      'type': {
        'name': 'string'
      },
      'required': false,
      'description': ''
    },
    'localeUtils': {
      'type': {
        'name': 'object'
      },
      'required': false,
      'description': ''
    },
    'weekdaysLong': {
      'type': {
        'name': 'arrayOf',
        'value': {
          'name': 'string'
        }
      },
      'required': false,
      'description': ''
    },
    'weekdaysShort': {
      'type': {
        'name': 'arrayOf',
        'value': {
          'name': 'string'
        }
      },
      'required': false,
      'description': ''
    },
    'weekdayDisplay': {
      'type': {
        'name': 'any'
      },
      'required': false,
      'description': ''
    }
  }
};

var DayPicker = function (_Component) {
  inherits(DayPicker, _Component);

  function DayPicker(props) {
    classCallCheck(this, DayPicker);

    var _this = possibleConstructorReturn(this, (DayPicker.__proto__ || Object.getPrototypeOf(DayPicker)).call(this, props));

    _this.dayPicker = null;

    _this.showNextMonth = function (callback) {
      if (!_this.allowNextMonth()) {
        return;
      }
      var deltaMonths = _this.props.pagedNavigation ? _this.props.numberOfMonths : 1;
      var nextMonth = addMonths$1(_this.state.currentMonth, deltaMonths);
      _this.showMonth(nextMonth, callback);
    };

    _this.showPreviousMonth = function (callback) {
      if (!_this.allowPreviousMonth()) {
        return;
      }
      var deltaMonths = _this.props.pagedNavigation ? _this.props.numberOfMonths : 1;
      var previousMonth = addMonths$1(_this.state.currentMonth, -deltaMonths);
      _this.showMonth(previousMonth, callback);
    };

    _this.handleKeyDown = function (e) {
      e.persist();

      switch (e.keyCode) {
        case LEFT:
          _this.showPreviousMonth();
          break;
        case RIGHT:
          _this.showNextMonth();
          break;
        case UP:
          _this.showPreviousYear();
          break;
        case DOWN:
          _this.showNextYear();
          break;
        default:
          break;
      }

      if (_this.props.onKeyDown) {
        _this.props.onKeyDown(e);
      }
    };

    _this.handleDayKeyDown = function (day, modifiers, e) {
      e.persist();
      switch (e.keyCode) {
        case LEFT:
          cancelEvent(e);
          _this.focusPreviousDay(e.target);
          break;
        case RIGHT:
          cancelEvent(e);
          _this.focusNextDay(e.target);
          break;
        case UP:
          cancelEvent(e);
          _this.focusPreviousWeek(e.target);
          break;
        case DOWN:
          cancelEvent(e);
          _this.focusNextWeek(e.target);
          break;
        case ENTER:
        case SPACE:
          cancelEvent(e);
          if (_this.props.onDayClick) {
            _this.handleDayClick(day, modifiers, e);
          }
          break;
        default:
          break;
      }
      if (_this.props.onDayKeyDown) {
        _this.props.onDayKeyDown(day, modifiers, e);
      }
    };

    _this.handleDayClick = function (day, modifiers, e) {
      e.persist();

      if (modifiers[_this.props.classNames.outside] && _this.props.enableOutsideDaysClick) {
        _this.handleOutsideDayClick(day);
      }
      if (_this.props.onDayClick) {
        _this.props.onDayClick(day, modifiers, e);
      }
    };

    _this.handleTodayButtonClick = function (e) {
      var today = new Date();
      var month = new Date(today.getFullYear(), today.getMonth());
      _this.showMonth(month);
      e.target.blur();
      if (_this.props.onTodayButtonClick) {
        e.persist();
        _this.props.onTodayButtonClick(new Date(today.getFullYear(), today.getMonth(), today.getDate()), getModifiersForDay(today, _this.props.modifiers), e);
      }
    };

    var currentMonth = _this.getCurrentMonthFromProps(props);
    _this.state = { currentMonth: currentMonth };
    return _this;
  }

  createClass(DayPicker, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      // Changing the `month` props means changing the current displayed month
      if (prevProps.month !== this.props.month && !isSameMonth$1(prevProps.month, this.props.month)) {
        var currentMonth = this.getCurrentMonthFromProps(this.props);
        // eslint-disable-next-line react/no-did-update-set-state
        this.setState({ currentMonth: currentMonth });
      }
    }

    /**
     * Return the month to be shown in the calendar based on the component props.
     *
     * @param {Object} props
     * @returns Date
     * @memberof DayPicker
     * @private
     */

  }, {
    key: 'getCurrentMonthFromProps',
    value: function getCurrentMonthFromProps(props) {
      var initialMonth = startOfMonth$1(props.month || props.initialMonth);
      var currentMonth = initialMonth;

      if (props.pagedNavigation && props.numberOfMonths > 1 && props.fromMonth) {
        var fromMonth = startOfMonth$1(props.fromMonth);
        var diffInMonths = getMonthsDiff(fromMonth, currentMonth);
        currentMonth = addMonths$1(fromMonth, Math.floor(diffInMonths / props.numberOfMonths) * props.numberOfMonths);
      } else if (props.toMonth && props.numberOfMonths > 1 && getMonthsDiff(currentMonth, props.toMonth) <= 0) {
        currentMonth = addMonths$1(startOfMonth$1(props.toMonth), 1 - this.props.numberOfMonths);
      }
      return currentMonth;
    }
  }, {
    key: 'getNextNavigableMonth',
    value: function getNextNavigableMonth() {
      return addMonths$1(this.state.currentMonth, this.props.numberOfMonths);
    }
  }, {
    key: 'getPreviousNavigableMonth',
    value: function getPreviousNavigableMonth() {
      return addMonths$1(this.state.currentMonth, -1);
    }
  }, {
    key: 'allowPreviousMonth',
    value: function allowPreviousMonth() {
      var previousMonth = addMonths$1(this.state.currentMonth, -1);
      return this.allowMonth(previousMonth);
    }
  }, {
    key: 'allowNextMonth',
    value: function allowNextMonth() {
      var nextMonth = addMonths$1(this.state.currentMonth, this.props.numberOfMonths);
      return this.allowMonth(nextMonth);
    }
  }, {
    key: 'allowMonth',
    value: function allowMonth(d) {
      var _props = this.props,
          fromMonth = _props.fromMonth,
          toMonth = _props.toMonth,
          canChangeMonth = _props.canChangeMonth;

      if (!canChangeMonth || fromMonth && getMonthsDiff(fromMonth, d) < 0 || toMonth && getMonthsDiff(toMonth, d) > 0) {
        return false;
      }
      return true;
    }
  }, {
    key: 'allowYearChange',
    value: function allowYearChange() {
      return this.props.canChangeMonth;
    }
  }, {
    key: 'showMonth',
    value: function showMonth(d, callback) {
      var _this2 = this;

      if (!this.allowMonth(d)) {
        return;
      }
      this.setState({ currentMonth: startOfMonth$1(d) }, function () {
        if (callback) {
          callback();
        }
        if (_this2.props.onMonthChange) {
          _this2.props.onMonthChange(_this2.state.currentMonth);
        }
      });
    }
  }, {
    key: 'showNextYear',
    value: function showNextYear() {
      if (!this.allowYearChange()) {
        return;
      }
      var nextMonth = addMonths$1(this.state.currentMonth, 12);
      this.showMonth(nextMonth);
    }
  }, {
    key: 'showPreviousYear',
    value: function showPreviousYear() {
      if (!this.allowYearChange()) {
        return;
      }
      var nextMonth = addMonths$1(this.state.currentMonth, -12);
      this.showMonth(nextMonth);
    }
  }, {
    key: 'focusFirstDayOfMonth',
    value: function focusFirstDayOfMonth() {
      getDayNodes(this.dayPicker, this.props.classNames)[0].focus();
    }
  }, {
    key: 'focusLastDayOfMonth',
    value: function focusLastDayOfMonth() {
      var dayNodes = getDayNodes(this.dayPicker, this.props.classNames);
      dayNodes[dayNodes.length - 1].focus();
    }
  }, {
    key: 'focusPreviousDay',
    value: function focusPreviousDay(dayNode) {
      var _this3 = this;

      var dayNodes = getDayNodes(this.dayPicker, this.props.classNames);
      var dayNodeIndex = nodeListToArray(dayNodes).indexOf(dayNode);
      if (dayNodeIndex === -1) return;
      if (dayNodeIndex === 0) {
        this.showPreviousMonth(function () {
          return _this3.focusLastDayOfMonth();
        });
      } else {
        dayNodes[dayNodeIndex - 1].focus();
      }
    }
  }, {
    key: 'focusNextDay',
    value: function focusNextDay(dayNode) {
      var _this4 = this;

      var dayNodes = getDayNodes(this.dayPicker, this.props.classNames);
      var dayNodeIndex = nodeListToArray(dayNodes).indexOf(dayNode);
      if (dayNodeIndex === -1) return;
      if (dayNodeIndex === dayNodes.length - 1) {
        this.showNextMonth(function () {
          return _this4.focusFirstDayOfMonth();
        });
      } else {
        dayNodes[dayNodeIndex + 1].focus();
      }
    }
  }, {
    key: 'focusNextWeek',
    value: function focusNextWeek(dayNode) {
      var _this5 = this;

      var dayNodes = getDayNodes(this.dayPicker, this.props.classNames);
      var dayNodeIndex = nodeListToArray(dayNodes).indexOf(dayNode);
      var isInLastWeekOfMonth = dayNodeIndex > dayNodes.length - 8;

      if (isInLastWeekOfMonth) {
        this.showNextMonth(function () {
          var daysAfterIndex = dayNodes.length - dayNodeIndex;
          var nextMonthDayNodeIndex = 7 - daysAfterIndex;
          getDayNodes(_this5.dayPicker, _this5.props.classNames)[nextMonthDayNodeIndex].focus();
        });
      } else {
        dayNodes[dayNodeIndex + 7].focus();
      }
    }
  }, {
    key: 'focusPreviousWeek',
    value: function focusPreviousWeek(dayNode) {
      var _this6 = this;

      var dayNodes = getDayNodes(this.dayPicker, this.props.classNames);
      var dayNodeIndex = nodeListToArray(dayNodes).indexOf(dayNode);
      var isInFirstWeekOfMonth = dayNodeIndex <= 6;

      if (isInFirstWeekOfMonth) {
        this.showPreviousMonth(function () {
          var previousMonthDayNodes = getDayNodes(_this6.dayPicker, _this6.props.classNames);
          var startOfLastWeekOfMonth = previousMonthDayNodes.length - 7;
          var previousMonthDayNodeIndex = startOfLastWeekOfMonth + dayNodeIndex;
          previousMonthDayNodes[previousMonthDayNodeIndex].focus();
        });
      } else {
        dayNodes[dayNodeIndex - 7].focus();
      }
    }

    // Event handlers

  }, {
    key: 'handleOutsideDayClick',
    value: function handleOutsideDayClick(day) {
      var currentMonth = this.state.currentMonth;
      var numberOfMonths = this.props.numberOfMonths;

      var diffInMonths = getMonthsDiff(currentMonth, day);
      if (diffInMonths > 0 && diffInMonths >= numberOfMonths) {
        this.showNextMonth();
      } else if (diffInMonths < 0) {
        this.showPreviousMonth();
      }
    }
  }, {
    key: 'renderNavbar',
    value: function renderNavbar() {
      var _props2 = this.props,
          labels = _props2.labels,
          locale = _props2.locale,
          localeUtils = _props2.localeUtils,
          canChangeMonth = _props2.canChangeMonth,
          navbarElement = _props2.navbarElement,
          navBarDisplay = _props2.navBarDisplay,
          navBarPrevDisplay = _props2.navBarPrevDisplay,
          navBarNextDisplay = _props2.navBarNextDisplay,
          attributes = objectWithoutProperties(_props2, ['labels', 'locale', 'localeUtils', 'canChangeMonth', 'navbarElement', 'navBarDisplay', 'navBarPrevDisplay', 'navBarNextDisplay']);


      if (!canChangeMonth) return null;

      var props = {
        month: this.state.currentMonth,
        classNames: this.props.classNames,
        className: this.props.classNames.navBar,
        navBarDisplay: navBarDisplay,
        navBarPrevDisplay: navBarPrevDisplay,
        navBarNextDisplay: navBarNextDisplay,
        nextMonth: this.getNextNavigableMonth(),
        previousMonth: this.getPreviousNavigableMonth(),
        showPreviousButton: this.allowPreviousMonth(),
        showNextButton: this.allowNextMonth(),
        onNextClick: this.showNextMonth,
        onPreviousClick: this.showPreviousMonth,
        dir: attributes.dir,
        labels: labels,
        locale: locale,
        localeUtils: localeUtils
      };
      return React__default.isValidElement(navbarElement) ? React__default.cloneElement(navbarElement, props) : React__default.createElement(navbarElement, props);
    }
  }, {
    key: 'renderMonths',
    value: function renderMonths() {
      var months = [];
      var firstDayOfWeek = getFirstDayOfWeekFromProps(this.props);
      var _props3 = this.props,
          dayCellDisplay = _props3.dayCellDisplay,
          weekDisplay = _props3.weekDisplay,
          weeksDisplay = _props3.weeksDisplay,
          monthDisplay = _props3.monthDisplay,
          monthsDisplay = _props3.monthsDisplay,
          weekdayDisplay = _props3.weekdayDisplay,
          captionDisplay = _props3.captionDisplay;


      for (var i = 0; i < this.props.numberOfMonths; i += 1) {
        var month = addMonths$1(this.state.currentMonth, i);
        months.push(React__default.createElement(Month, _extends$1({
          key: i
        }, this.props, {
          month: month,
          firstDayOfWeek: firstDayOfWeek,
          onDayKeyDown: this.handleDayKeyDown,
          onDayClick: this.handleDayClick,
          dayCellDisplay: dayCellDisplay,
          weekDisplay: weekDisplay,
          weeksDisplay: weeksDisplay,
          monthDisplay: monthDisplay,
          monthsDisplay: monthsDisplay,
          weekdayElement: React__default.createElement(Weekday, { weekdayDisplay: weekdayDisplay }),
          captionElement: React__default.createElement(Caption, { captionDisplay: captionDisplay })
        })));
      }

      if (this.props.reverseMonths) {
        months.reverse();
      }
      return months;
    }
  }, {
    key: 'renderFooter',
    value: function renderFooter() {
      if (this.props.todayButton) {
        return React__default.createElement(
          'div',
          { className: this.props.classNames.footer },
          this.renderTodayButton()
        );
      }
      return null;
    }
  }, {
    key: 'renderTodayButton',
    value: function renderTodayButton() {
      return React__default.createElement(
        'button',
        {
          type: 'button',
          tabIndex: 0,
          className: this.props.classNames.todayButton,
          'aria-label': this.props.todayButton,
          onClick: this.handleTodayButtonClick
        },
        this.props.todayButton
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _this7 = this;

      var className = this.props.classNames.container;

      if (!this.props.onDayClick) {
        className = className + ' ' + this.props.classNames.interactionDisabled;
      }
      if (this.props.className) {
        className = className + ' ' + this.props.className;
      }

      var monthsDisplay = this.props.monthsDisplay;

      var MonthsDisplay = monthsDisplay;

      return React__default.createElement(
        'div',
        _extends$1({}, this.props.containerProps, {
          className: className,
          ref: function ref(el) {
            return _this7.dayPicker = el;
          },
          lang: this.props.locale
        }),
        React__default.createElement(
          'div',
          {
            style: { outline: 'none' },
            className: this.props.classNames.wrapper,
            tabIndex: this.props.canChangeMonth && typeof this.props.tabIndex !== 'undefined' ? this.props.tabIndex : -1,
            onKeyDown: this.handleKeyDown,
            onFocus: this.props.onFocus,
            onBlur: this.props.onBlur
          },
          this.renderNavbar(),
          React__default.createElement(
            MonthsDisplay,
            null,
            this.renderMonths()
          ),
          this.renderFooter()
        )
      );
    }
  }]);
  return DayPicker;
}(React.Component);

DayPicker.VERSION = '7.2.4';
DayPicker.propTypes = {
  // Rendering months
  initialMonth: propTypes.instanceOf(Date),
  month: propTypes.instanceOf(Date),
  numberOfMonths: propTypes.number,
  fromMonth: propTypes.instanceOf(Date),
  toMonth: propTypes.instanceOf(Date),
  canChangeMonth: propTypes.bool,
  reverseMonths: propTypes.bool,
  pagedNavigation: propTypes.bool,
  todayButton: propTypes.string,
  showWeekNumbers: propTypes.bool,
  showWeekDays: propTypes.bool,

  // Modifiers
  selectedDays: propTypes.oneOfType([propTypes.object, propTypes.func, propTypes.array]),
  disabledDays: propTypes.oneOfType([propTypes.object, propTypes.func, propTypes.array]),

  modifiers: propTypes.object,
  modifiersStyles: propTypes.object,

  // Localization
  dir: propTypes.string,
  firstDayOfWeek: propTypes.oneOf([0, 1, 2, 3, 4, 5, 6]),
  labels: propTypes.shape({
    nextMonth: propTypes.string.isRequired,
    previousMonth: propTypes.string.isRequired
  }),
  locale: propTypes.string,
  localeUtils: propTypes.shape({
    formatMonthTitle: propTypes.func,
    formatWeekdayShort: propTypes.func,
    formatWeekdayLong: propTypes.func,
    getFirstDayOfWeek: propTypes.func
  }),
  months: propTypes.arrayOf(propTypes.string),
  weekdaysLong: propTypes.arrayOf(propTypes.string),
  weekdaysShort: propTypes.arrayOf(propTypes.string),

  // Customization
  showOutsideDays: propTypes.bool,
  enableOutsideDaysClick: propTypes.bool,
  fixedWeeks: propTypes.bool,

  // CSS and HTML
  classNames: propTypes.shape({
    body: propTypes.string,
    container: propTypes.string,
    day: propTypes.string.isRequired,
    disabled: propTypes.string.isRequired,
    footer: propTypes.string,
    interactionDisabled: propTypes.string,
    months: propTypes.string,
    month: propTypes.string,
    navBar: propTypes.string,
    outside: propTypes.string.isRequired,
    selected: propTypes.string.isRequired,
    today: propTypes.string.isRequired,
    todayButton: propTypes.string,
    week: propTypes.string,
    wrapper: propTypes.string
  }),
  className: propTypes.string,
  containerProps: propTypes.object,
  tabIndex: propTypes.number,

  // Custom elements
  renderDay: propTypes.func,
  renderWeek: propTypes.func,
  weekdayElement: propTypes.oneOfType([propTypes.element, propTypes.func, propTypes.instanceOf(React.Component)]),
  navbarElement: propTypes.oneOfType([propTypes.element, propTypes.func, propTypes.instanceOf(React.Component)]),
  captionElement: propTypes.oneOfType([propTypes.element, propTypes.func, propTypes.instanceOf(React.Component)]),

  // Events
  onBlur: propTypes.func,
  onFocus: propTypes.func,
  onKeyDown: propTypes.func,
  onDayClick: propTypes.func,
  onDayKeyDown: propTypes.func,
  onDayMouseEnter: propTypes.func,
  onDayMouseLeave: propTypes.func,
  onDayMouseDown: propTypes.func,
  onDayMouseUp: propTypes.func,
  onDayTouchStart: propTypes.func,
  onDayTouchEnd: propTypes.func,
  onDayFocus: propTypes.func,
  onMonthChange: propTypes.func,
  onCaptionClick: propTypes.func,
  onWeekClick: propTypes.func,
  onTodayButtonClick: propTypes.func
};
DayPicker.defaultProps = {
  classNames: defaultClassNames,
  tabIndex: 0,
  initialMonth: new Date(),
  numberOfMonths: 1,
  labels: {
    previousMonth: 'Previous Month',
    nextMonth: 'Next Month'
  },
  locale: 'en',
  localeUtils: LocaleUtils$1,
  showOutsideDays: false,
  enableOutsideDaysClick: true,
  fixedWeeks: false,
  canChangeMonth: true,
  reverseMonths: false,
  pagedNavigation: false,
  showWeekNumbers: false,
  showWeekDays: true,
  renderDay: function renderDay(day) {
    return day.getDate();
  },
  renderWeek: function renderWeek(weekNumber) {
    return weekNumber;
  },
  weekdayElement: React__default.createElement(Weekday, null),
  navbarElement: React__default.createElement(Navbar, { classNames: defaultClassNames }),
  captionElement: React__default.createElement(Caption, { classNames: defaultClassNames })
};
DayPicker.DateUtils = DateUtils$1;
DayPicker.LocaleUtils = LocaleUtils$1;
DayPicker.ModifiersUtils = ModifiersUtils$1;
DayPicker.__docgenInfo = {
  'description': '',
  'methods': [{
    'name': 'getCurrentMonthFromProps',
    'docblock': 'Return the month to be shown in the calendar based on the component props.\n\n@param {Object} props\n@returns Date\n@memberof DayPicker\n@private',
    'modifiers': [],
    'params': [{
      'name': 'props',
      'description': null,
      'type': {
        'name': 'Object'
      },
      'optional': false
    }],
    'returns': {
      'description': 'Date',
      'type': null
    },
    'description': 'Return the month to be shown in the calendar based on the component props.'
  }, {
    'name': 'getNextNavigableMonth',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'getPreviousNavigableMonth',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'allowPreviousMonth',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'allowNextMonth',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'allowMonth',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'd',
      'type': null
    }],
    'returns': null
  }, {
    'name': 'allowYearChange',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'showMonth',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'd',
      'type': null
    }, {
      'name': 'callback',
      'type': null
    }],
    'returns': null
  }, {
    'name': 'showNextMonth',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'callback',
      'type': null
    }],
    'returns': null
  }, {
    'name': 'showPreviousMonth',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'callback',
      'type': null
    }],
    'returns': null
  }, {
    'name': 'showNextYear',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'showPreviousYear',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'focusFirstDayOfMonth',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'focusLastDayOfMonth',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'focusPreviousDay',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'dayNode',
      'type': null
    }],
    'returns': null
  }, {
    'name': 'focusNextDay',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'dayNode',
      'type': null
    }],
    'returns': null
  }, {
    'name': 'focusNextWeek',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'dayNode',
      'type': null
    }],
    'returns': null
  }, {
    'name': 'focusPreviousWeek',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'dayNode',
      'type': null
    }],
    'returns': null
  }, {
    'name': 'handleKeyDown',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'e',
      'type': null
    }],
    'returns': null
  }, {
    'name': 'handleDayKeyDown',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'day',
      'type': null
    }, {
      'name': 'modifiers',
      'type': null
    }, {
      'name': 'e',
      'type': null
    }],
    'returns': null
  }, {
    'name': 'handleDayClick',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'day',
      'type': null
    }, {
      'name': 'modifiers',
      'type': null
    }, {
      'name': 'e',
      'type': null
    }],
    'returns': null
  }, {
    'name': 'handleOutsideDayClick',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'day',
      'type': null
    }],
    'returns': null
  }, {
    'name': 'handleTodayButtonClick',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'e',
      'type': null
    }],
    'returns': null
  }, {
    'name': 'renderNavbar',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'renderMonths',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'renderFooter',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'renderTodayButton',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }],
  'displayName': 'DayPicker',
  'props': {
    'classNames': {
      'defaultValue': {
        'value': 'classNames',
        'computed': true
      },
      'type': {
        'name': 'shape',
        'value': {
          'body': {
            'name': 'string',
            'required': false
          },
          'container': {
            'name': 'string',
            'required': false
          },
          'day': {
            'name': 'string',
            'required': true
          },
          'disabled': {
            'name': 'string',
            'required': true
          },
          'footer': {
            'name': 'string',
            'required': false
          },
          'interactionDisabled': {
            'name': 'string',
            'required': false
          },
          'months': {
            'name': 'string',
            'required': false
          },
          'month': {
            'name': 'string',
            'required': false
          },
          'navBar': {
            'name': 'string',
            'required': false
          },
          'outside': {
            'name': 'string',
            'required': true
          },
          'selected': {
            'name': 'string',
            'required': true
          },
          'today': {
            'name': 'string',
            'required': true
          },
          'todayButton': {
            'name': 'string',
            'required': false
          },
          'week': {
            'name': 'string',
            'required': false
          },
          'wrapper': {
            'name': 'string',
            'required': false
          }
        }
      },
      'required': false,
      'description': ''
    },
    'tabIndex': {
      'defaultValue': {
        'value': '0',
        'computed': false
      },
      'type': {
        'name': 'number'
      },
      'required': false,
      'description': ''
    },
    'initialMonth': {
      'defaultValue': {
        'value': 'new Date()',
        'computed': false
      },
      'type': {
        'name': 'instanceOf',
        'value': 'Date'
      },
      'required': false,
      'description': ''
    },
    'numberOfMonths': {
      'defaultValue': {
        'value': '1',
        'computed': false
      },
      'type': {
        'name': 'number'
      },
      'required': false,
      'description': ''
    },
    'labels': {
      'defaultValue': {
        'value': '{\n  previousMonth: \'Previous Month\',\n  nextMonth: \'Next Month\',\n}',
        'computed': false
      },
      'type': {
        'name': 'shape',
        'value': {
          'nextMonth': {
            'name': 'string',
            'required': true
          },
          'previousMonth': {
            'name': 'string',
            'required': true
          }
        }
      },
      'required': false,
      'description': ''
    },
    'locale': {
      'defaultValue': {
        'value': '\'en\'',
        'computed': false
      },
      'type': {
        'name': 'string'
      },
      'required': false,
      'description': ''
    },
    'localeUtils': {
      'defaultValue': {
        'value': 'LocaleUtils',
        'computed': true
      },
      'type': {
        'name': 'shape',
        'value': {
          'formatMonthTitle': {
            'name': 'func',
            'required': false
          },
          'formatWeekdayShort': {
            'name': 'func',
            'required': false
          },
          'formatWeekdayLong': {
            'name': 'func',
            'required': false
          },
          'getFirstDayOfWeek': {
            'name': 'func',
            'required': false
          }
        }
      },
      'required': false,
      'description': ''
    },
    'showOutsideDays': {
      'defaultValue': {
        'value': 'false',
        'computed': false
      },
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'enableOutsideDaysClick': {
      'defaultValue': {
        'value': 'true',
        'computed': false
      },
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'fixedWeeks': {
      'defaultValue': {
        'value': 'false',
        'computed': false
      },
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'canChangeMonth': {
      'defaultValue': {
        'value': 'true',
        'computed': false
      },
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'reverseMonths': {
      'defaultValue': {
        'value': 'false',
        'computed': false
      },
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'pagedNavigation': {
      'defaultValue': {
        'value': 'false',
        'computed': false
      },
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'showWeekNumbers': {
      'defaultValue': {
        'value': 'false',
        'computed': false
      },
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'showWeekDays': {
      'defaultValue': {
        'value': 'true',
        'computed': false
      },
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'renderDay': {
      'defaultValue': {
        'value': 'day => day.getDate()',
        'computed': false
      },
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'renderWeek': {
      'defaultValue': {
        'value': 'weekNumber => weekNumber',
        'computed': false
      },
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'weekdayElement': {
      'defaultValue': {
        'value': '<Weekday />',
        'computed': false
      },
      'type': {
        'name': 'union',
        'value': [{
          'name': 'element'
        }, {
          'name': 'func'
        }, {
          'name': 'instanceOf',
          'value': 'Component'
        }]
      },
      'required': false,
      'description': ''
    },
    'navbarElement': {
      'defaultValue': {
        'value': '<Navbar classNames={classNames} />',
        'computed': false
      },
      'type': {
        'name': 'union',
        'value': [{
          'name': 'element'
        }, {
          'name': 'func'
        }, {
          'name': 'instanceOf',
          'value': 'Component'
        }]
      },
      'required': false,
      'description': ''
    },
    'captionElement': {
      'defaultValue': {
        'value': '<Caption classNames={classNames} />',
        'computed': false
      },
      'type': {
        'name': 'union',
        'value': [{
          'name': 'element'
        }, {
          'name': 'func'
        }, {
          'name': 'instanceOf',
          'value': 'Component'
        }]
      },
      'required': false,
      'description': ''
    },
    'month': {
      'type': {
        'name': 'instanceOf',
        'value': 'Date'
      },
      'required': false,
      'description': ''
    },
    'fromMonth': {
      'type': {
        'name': 'instanceOf',
        'value': 'Date'
      },
      'required': false,
      'description': ''
    },
    'toMonth': {
      'type': {
        'name': 'instanceOf',
        'value': 'Date'
      },
      'required': false,
      'description': ''
    },
    'todayButton': {
      'type': {
        'name': 'string'
      },
      'required': false,
      'description': ''
    },
    'selectedDays': {
      'type': {
        'name': 'union',
        'value': [{
          'name': 'object'
        }, {
          'name': 'func'
        }, {
          'name': 'array'
        }]
      },
      'required': false,
      'description': ''
    },
    'disabledDays': {
      'type': {
        'name': 'union',
        'value': [{
          'name': 'object'
        }, {
          'name': 'func'
        }, {
          'name': 'array'
        }]
      },
      'required': false,
      'description': ''
    },
    'modifiers': {
      'type': {
        'name': 'object'
      },
      'required': false,
      'description': ''
    },
    'modifiersStyles': {
      'type': {
        'name': 'object'
      },
      'required': false,
      'description': ''
    },
    'dir': {
      'type': {
        'name': 'string'
      },
      'required': false,
      'description': ''
    },
    'firstDayOfWeek': {
      'type': {
        'name': 'enum',
        'value': [{
          'value': '0',
          'computed': false
        }, {
          'value': '1',
          'computed': false
        }, {
          'value': '2',
          'computed': false
        }, {
          'value': '3',
          'computed': false
        }, {
          'value': '4',
          'computed': false
        }, {
          'value': '5',
          'computed': false
        }, {
          'value': '6',
          'computed': false
        }]
      },
      'required': false,
      'description': ''
    },
    'months': {
      'type': {
        'name': 'arrayOf',
        'value': {
          'name': 'string'
        }
      },
      'required': false,
      'description': ''
    },
    'weekdaysLong': {
      'type': {
        'name': 'arrayOf',
        'value': {
          'name': 'string'
        }
      },
      'required': false,
      'description': ''
    },
    'weekdaysShort': {
      'type': {
        'name': 'arrayOf',
        'value': {
          'name': 'string'
        }
      },
      'required': false,
      'description': ''
    },
    'className': {
      'type': {
        'name': 'string'
      },
      'required': false,
      'description': ''
    },
    'containerProps': {
      'type': {
        'name': 'object'
      },
      'required': false,
      'description': ''
    },
    'onBlur': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onFocus': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onKeyDown': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onDayClick': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onDayKeyDown': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onDayMouseEnter': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onDayMouseLeave': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onDayMouseDown': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onDayMouseUp': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onDayTouchStart': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onDayTouchEnd': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onDayFocus': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onMonthChange': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onCaptionClick': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onWeekClick': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onTodayButtonClick': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    }
  }
};

// When clicking on a day cell, overlay will be hidden after this timeout
var HIDE_TIMEOUT = 100;

/**
 * The default component used as Overlay.
 *
 * @param {Object} props
 */
function OverlayComponent(_ref) {
  var input = _ref.input,
      selectedDay = _ref.selectedDay,
      month = _ref.month,
      children = _ref.children,
      classNames = _ref.classNames,
      props = objectWithoutProperties(_ref, ['input', 'selectedDay', 'month', 'children', 'classNames']);

  return React__default.createElement(
    'div',
    _extends$1({ className: classNames.overlayWrapper }, props),
    React__default.createElement(
      'div',
      { className: classNames.overlay },
      children
    )
  );
}

OverlayComponent.propTypes = {
  input: propTypes.any,
  selectedDay: propTypes.any,
  month: propTypes.instanceOf(Date),
  children: propTypes.node,
  classNames: propTypes.object
};

/**
 * The default function used to format a Date to String, passed to the `format`
 * prop.
 * @param {Date} d
 * @return {String}
 */
function defaultFormat(d) {
  if (isDate$1(d)) {
    var year = d.getFullYear();
    var month = '' + (d.getMonth() + 1);
    var day = '' + d.getDate();
    return year + '-' + month + '-' + day;
  }
  return '';
}

/**
 * The default function used to parse a String as Date, passed to the `parse`
 * prop.
 * @param {String} str
 * @return {Date}
 */
function defaultParse(str) {
  if (typeof str !== 'string') {
    return undefined;
  }
  var split = str.split('-');
  if (split.length !== 3) {
    return undefined;
  }
  var year = parseInt(split[0], 10);
  var month = parseInt(split[1], 10) - 1;
  var day = parseInt(split[2], 10);
  if (isNaN(year) || String(year).length > 4 || isNaN(month) || isNaN(day) || day <= 0 || day > 31 || month < 0 || month >= 12) {
    return undefined;
  }

  return new Date(year, month, day);
}

var DayPickerInput = function (_React$Component) {
  inherits(DayPickerInput, _React$Component);

  function DayPickerInput(props) {
    classCallCheck(this, DayPickerInput);

    var _this = possibleConstructorReturn(this, (DayPickerInput.__proto__ || Object.getPrototypeOf(DayPickerInput)).call(this, props));

    _this.input = null;
    _this.daypicker = null;
    _this.clickTimeout = null;
    _this.hideTimeout = null;
    _this.inputBlurTimeout = null;
    _this.inputFocusTimeout = null;


    _this.state = _this.getInitialStateFromProps(props);
    _this.state.showOverlay = props.showOverlay;

    _this.hideAfterDayClick = _this.hideAfterDayClick.bind(_this);
    _this.handleInputClick = _this.handleInputClick.bind(_this);
    _this.handleInputFocus = _this.handleInputFocus.bind(_this);
    _this.handleInputBlur = _this.handleInputBlur.bind(_this);
    _this.handleInputChange = _this.handleInputChange.bind(_this);
    _this.handleInputKeyDown = _this.handleInputKeyDown.bind(_this);
    _this.handleInputKeyUp = _this.handleInputKeyUp.bind(_this);
    _this.handleDayClick = _this.handleDayClick.bind(_this);
    _this.handleMonthChange = _this.handleMonthChange.bind(_this);
    _this.handleOverlayFocus = _this.handleOverlayFocus.bind(_this);
    _this.handleOverlayBlur = _this.handleOverlayBlur.bind(_this);
    return _this;
  }

  createClass(DayPickerInput, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      var newState = {};

      // Current props
      var _props = this.props,
          value = _props.value,
          formatDate = _props.formatDate,
          format = _props.format,
          dayPickerProps = _props.dayPickerProps;

      // Update the input value if the `value` prop has changed

      if (value !== prevProps.value) {
        if (isDate$1(value)) {
          newState.value = formatDate(value, format, dayPickerProps.locale);
        } else {
          newState.value = value;
        }
      }

      // Update the month if the months from props changed
      var prevMonth = prevProps.dayPickerProps.month;
      if (dayPickerProps.month && dayPickerProps.month !== prevMonth && !isSameMonth$1(dayPickerProps.month, prevMonth)) {
        newState.month = dayPickerProps.month;
      }

      // Updated the selected days from props if they changed
      if (prevProps.dayPickerProps.selectedDays !== dayPickerProps.selectedDays) {
        newState.selectedDays = dayPickerProps.selectedDays;
      }

      if (Object.keys(newState).length > 0) {
        // eslint-disable-next-line react/no-did-update-set-state
        this.setState(newState);
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      clearTimeout(this.clickTimeout);
      clearTimeout(this.hideTimeout);
      clearTimeout(this.inputFocusTimeout);
      clearTimeout(this.inputBlurTimeout);
      clearTimeout(this.overlayBlurTimeout);
    }
  }, {
    key: 'getInitialMonthFromProps',
    value: function getInitialMonthFromProps(props) {
      var dayPickerProps = props.dayPickerProps,
          format = props.format;

      var day = void 0;
      if (props.value) {
        if (isDate$1(props.value)) {
          day = props.value;
        } else {
          day = props.parseDate(props.value, format, dayPickerProps.locale);
        }
      }
      return dayPickerProps.initialMonth || dayPickerProps.month || day || new Date();
    }
  }, {
    key: 'getInitialStateFromProps',
    value: function getInitialStateFromProps(props) {
      var dayPickerProps = props.dayPickerProps,
          formatDate = props.formatDate,
          format = props.format;
      var value = props.value;

      if (props.value && isDate$1(props.value)) {
        value = formatDate(props.value, format, dayPickerProps.locale);
      }
      return {
        value: value,
        month: this.getInitialMonthFromProps(props),
        selectedDays: dayPickerProps.selectedDays
      };
    }
  }, {
    key: 'getInput',
    value: function getInput() {
      return this.input;
    }
  }, {
    key: 'getDayPicker',
    value: function getDayPicker() {
      return this.daypicker;
    }

    /**
     * Update the component's state and fire the `onDayChange` event passing the
     * day's modifiers to it.
     *
     * @param {Date} day - Will be used for changing the month
     * @param {String} value - Input field value
     * @private
     */

  }, {
    key: 'updateState',
    value: function updateState(day, value, callback) {
      var _this2 = this;

      var _props2 = this.props,
          dayPickerProps = _props2.dayPickerProps,
          onDayChange = _props2.onDayChange;

      this.setState({ month: day, value: value, typedValue: undefined }, function () {
        if (callback) {
          callback();
        }
        if (!onDayChange) {
          return;
        }
        var modifiersObj = _extends$1({
          disabled: dayPickerProps.disabledDays,
          selected: dayPickerProps.selectedDays
        }, dayPickerProps.modifiers);
        var modifiers = getModifiersForDay(day, modifiersObj).reduce(function (obj, modifier) {
          return _extends$1({}, obj, defineProperty({}, modifier, true));
        }, {});
        onDayChange(day, modifiers, _this2);
      });
    }

    /**
     * Show the Day Picker overlay.
     *
     * @memberof DayPickerInput
     */

  }, {
    key: 'showDayPicker',
    value: function showDayPicker() {
      var _props3 = this.props,
          parseDate = _props3.parseDate,
          format = _props3.format,
          dayPickerProps = _props3.dayPickerProps;
      var _state = this.state,
          value = _state.value,
          showOverlay = _state.showOverlay;

      if (showOverlay) {
        return;
      }
      // Reset the current displayed month when showing the overlay
      var month = value ? parseDate(value, format, dayPickerProps.locale) // Use the month in the input field
      : this.getInitialMonthFromProps(this.props); // Restore the month from the props
      this.setState(function (state) {
        return {
          showOverlay: true,
          month: month || state.month
        };
      });
    }

    /**
     * Hide the Day Picker overlay
     *
     * @memberof DayPickerInput
     */

  }, {
    key: 'hideDayPicker',
    value: function hideDayPicker() {
      var _this3 = this;

      if (this.state.showOverlay === false) {
        return;
      }
      this.setState({ showOverlay: false }, function () {
        if (_this3.props.onDayPickerHide) _this3.props.onDayPickerHide();
      });
    }
  }, {
    key: 'hideAfterDayClick',
    value: function hideAfterDayClick() {
      var _this4 = this;

      if (!this.props.hideOnDayClick) {
        return;
      }
      this.hideTimeout = setTimeout(function () {
        return _this4.hideDayPicker();
      }, HIDE_TIMEOUT);
    }
  }, {
    key: 'handleInputClick',
    value: function handleInputClick(e) {
      this.showDayPicker();
      if (this.props.inputProps.onClick) {
        e.persist();
        this.props.inputProps.onClick(e);
      }
    }
  }, {
    key: 'handleInputFocus',
    value: function handleInputFocus(e) {
      var _this5 = this;

      this.showDayPicker();
      // Set `overlayHasFocus` after a timeout so the overlay can be hidden when
      // the input is blurred
      this.inputFocusTimeout = setTimeout(function () {
        _this5.overlayHasFocus = false;
      }, 2);
      if (this.props.inputProps.onFocus) {
        e.persist();
        this.props.inputProps.onFocus(e);
      }
    }

    // When the input is blurred, the overlay should disappear. However the input
    // is blurred also when the user interacts with the overlay (e.g. the overlay
    // get the focus by clicking it). In these cases, the overlay should not be
    // hidden. There are different approaches to avoid hiding the overlay when
    // this happens, but the only cross-browser hack we’ve found is to set all
    // these timeouts in code before changing `overlayHasFocus`.

  }, {
    key: 'handleInputBlur',
    value: function handleInputBlur(e) {
      var _this6 = this;

      this.inputBlurTimeout = setTimeout(function () {
        if (!_this6.overlayHasFocus) {
          _this6.hideDayPicker();
        }
      }, 1);
      if (this.props.inputProps.onBlur) {
        e.persist();
        this.props.inputProps.onBlur(e);
      }
    }
  }, {
    key: 'handleOverlayFocus',
    value: function handleOverlayFocus(e) {
      e.preventDefault();
      this.overlayHasFocus = true;
      if (!this.props.keepFocus || !this.input || typeof this.input.focus !== 'function') {
        return;
      }
      this.input.focus();
    }
  }, {
    key: 'handleOverlayBlur',
    value: function handleOverlayBlur() {
      var _this7 = this;

      // We need to set a timeout otherwise IE11 will hide the overlay when
      // focusing it
      this.overlayBlurTimeout = setTimeout(function () {
        _this7.overlayHasFocus = false;
      }, 3);
    }
  }, {
    key: 'handleInputChange',
    value: function handleInputChange(e) {
      var _props4 = this.props,
          dayPickerProps = _props4.dayPickerProps,
          format = _props4.format,
          inputProps = _props4.inputProps,
          onDayChange = _props4.onDayChange,
          parseDate = _props4.parseDate;


      if (inputProps.onChange) {
        e.persist();
        inputProps.onChange(e);
      }

      var value = e.target.value;


      if (value.trim() === '') {
        this.setState({ value: value, typedValue: ' ' });

        if (onDayChange) onDayChange(undefined, {}, this);
        return;
      }
      var day = parseDate(value, format, dayPickerProps.locale);

      if (!day) {
        // Day is invalid: we save the value in the typedValue state

        this.setState({ value: value, typedValue: value });
        if (onDayChange) onDayChange(undefined, {}, this);
        return;
      }

      this.updateState(day, value);
    }
  }, {
    key: 'handleInputKeyDown',
    value: function handleInputKeyDown(e) {
      if (e.keyCode === TAB) {
        this.hideDayPicker();
      } else {
        this.showDayPicker();
      }
      if (this.props.inputProps.onKeyDown) {
        e.persist();
        this.props.inputProps.onKeyDown(e);
      }
    }
  }, {
    key: 'handleInputKeyUp',
    value: function handleInputKeyUp(e) {
      if (e.keyCode === ESC) {
        this.hideDayPicker();
      } else {
        this.showDayPicker();
      }
      if (this.props.inputProps.onKeyUp) {
        e.persist();
        this.props.inputProps.onKeyUp(e);
      }
    }
  }, {
    key: 'handleMonthChange',
    value: function handleMonthChange(month) {
      var _this8 = this;

      this.setState({ month: month }, function () {
        if (_this8.props.dayPickerProps && _this8.props.dayPickerProps.onMonthChange) {
          _this8.props.dayPickerProps.onMonthChange(month);
        }
      });
    }
  }, {
    key: 'handleDayClick',
    value: function handleDayClick(day, modifiers, e) {
      var _this9 = this;

      var _props5 = this.props,
          clickUnselectsDay = _props5.clickUnselectsDay,
          dayPickerProps = _props5.dayPickerProps,
          onDayChange = _props5.onDayChange,
          formatDate = _props5.formatDate,
          format = _props5.format;

      if (dayPickerProps.onDayClick) {
        dayPickerProps.onDayClick(day, modifiers, e);
      }

      // Do nothing if the day is disabled
      if (modifiers.disabled || dayPickerProps && dayPickerProps.classNames && modifiers[dayPickerProps.classNames.disabled]) {
        return;
      }

      // If the clicked day is already selected, remove the clicked day
      // from the selected days and empty the field value
      if (modifiers.selected && clickUnselectsDay) {
        var selectedDays = this.state.selectedDays;

        if (Array.isArray(selectedDays)) {
          selectedDays = selectedDays.slice(0);
          var selectedDayIdx = selectedDays.indexOf(day);
          selectedDays.splice(selectedDayIdx, 1);
        } else if (selectedDays) {
          selectedDays = null;
        }
        this.setState({ value: '', typedValue: undefined, selectedDays: selectedDays }, this.hideAfterDayClick);
        if (onDayChange) {
          onDayChange(undefined, modifiers, this);
        }
        return;
      }

      var value = formatDate(day, format, dayPickerProps.locale);
      this.setState({ value: value, typedValue: undefined, month: day }, function () {
        if (onDayChange) {
          onDayChange(day, modifiers, _this9);
        }
        _this9.hideAfterDayClick();
      });
    }
  }, {
    key: 'renderOverlay',
    value: function renderOverlay() {
      var _this10 = this;

      var _props6 = this.props,
          classNames = _props6.classNames,
          dayPickerProps = _props6.dayPickerProps,
          parseDate = _props6.parseDate,
          formatDate = _props6.formatDate,
          format = _props6.format;
      var _state2 = this.state,
          selectedDays = _state2.selectedDays,
          value = _state2.value;

      var selectedDay = void 0;
      if (!selectedDays && value) {
        var day = parseDate(value, format, dayPickerProps.locale);
        if (day) {
          selectedDay = day;
        }
      } else if (selectedDays) {
        selectedDay = selectedDays;
      }
      var onTodayButtonClick = void 0;
      if (dayPickerProps.todayButton) {
        // Set the current day when clicking the today button
        onTodayButtonClick = function onTodayButtonClick() {
          return _this10.updateState(new Date(), formatDate(new Date(), format, dayPickerProps.locale), _this10.hideAfterDayClick);
        };
      }
      var Overlay = this.props.overlayComponent;
      var _props7 = this.props,
          dayCellDisplay = _props7.dayCellDisplay,
          weekDisplay = _props7.weekDisplay,
          weeksDisplay = _props7.weeksDisplay,
          monthDisplay = _props7.monthDisplay,
          monthsDisplay = _props7.monthsDisplay,
          weekdayDisplay = _props7.weekdayDisplay,
          weekdaysDisplay = _props7.weekdaysDisplay,
          weekdaysRowDisplay = _props7.weekdaysRowDisplay,
          captionDisplay = _props7.captionDisplay,
          dayDisplay = _props7.dayDisplay,
          showOutsideDays = _props7.showOutsideDays,
          navBarDisplay = _props7.navBarDisplay,
          navBarNextDisplay = _props7.navBarNextDisplay,
          navBarPrevDisplay = _props7.navBarPrevDisplay,
          fixedWeeks = _props7.fixedWeeks;

      return React__default.createElement(
        Overlay,
        {
          classNames: classNames,
          month: this.state.month,
          selectedDay: selectedDay,
          input: this.input,
          tabIndex: 0 // tabIndex is necessary to catch focus/blur events on Safari
          , onFocus: this.handleOverlayFocus,
          onBlur: this.handleOverlayBlur
        },
        React__default.createElement(DayPicker, _extends$1({
          ref: function ref(el) {
            return _this10.daypicker = el;
          },
          onTodayButtonClick: onTodayButtonClick
        }, dayPickerProps, {
          month: this.state.month,
          selectedDays: selectedDay,
          onDayClick: this.handleDayClick,
          onMonthChange: this.handleMonthChange,
          dayCellDisplay: dayCellDisplay,
          dayDisplay: dayDisplay,
          weekDisplay: weekDisplay,
          weekdayDisplay: weekdayDisplay,
          weeksDisplay: weeksDisplay,
          monthDisplay: monthDisplay,
          monthsDisplay: monthsDisplay,
          weekdaysDisplay: weekdaysDisplay,
          weekdaysRowDisplay: weekdaysRowDisplay,
          fixedWeeks: fixedWeeks,
          showOutsideDays: showOutsideDays,
          captionDisplay: captionDisplay,
          navBarDisplay: navBarDisplay,
          navBarNextDisplay: navBarNextDisplay,
          navBarPrevDisplay: navBarPrevDisplay
        }))
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _this11 = this;

      var Input = this.props.component;
      var inputProps = this.props.inputProps;


      return React__default.createElement(
        'div',
        { className: this.props.classNames.container },
        React__default.createElement(Input, _extends$1({
          ref: function ref(el) {
            return _this11.input = el;
          },
          placeholder: this.props.placeholder
        }, inputProps, {
          value: this.state.typedValue || this.state.value,
          onChange: this.handleInputChange,
          onFocus: this.handleInputFocus,
          onBlur: this.handleInputBlur,
          onKeyDown: this.handleInputKeyDown,
          onKeyUp: this.handleInputKeyUp,
          onClick: !inputProps.disabled ? this.handleInputClick : undefined,
          autocomplete: 'off'
        })),
        this.state.showOverlay && this.renderOverlay()
      );
    }
  }]);
  return DayPickerInput;
}(React__default.Component);

DayPickerInput.propTypes = {
  value: propTypes.oneOfType([propTypes.string, propTypes.instanceOf(Date)]),
  inputProps: propTypes.object,
  placeholder: propTypes.string,

  format: propTypes.oneOfType([propTypes.string, propTypes.arrayOf(propTypes.string)]),

  formatDate: propTypes.func,
  parseDate: propTypes.func,

  showOverlay: propTypes.bool,
  dayPickerProps: propTypes.object,
  hideOnDayClick: propTypes.bool,
  clickUnselectsDay: propTypes.bool,
  keepFocus: propTypes.bool,
  component: propTypes.any,
  overlayComponent: propTypes.any,

  classNames: propTypes.shape({
    container: propTypes.string,
    overlayWrapper: propTypes.string,
    overlay: propTypes.string.isRequired
  }),

  onDayChange: propTypes.func,
  onDayPickerHide: propTypes.func,
  onChange: propTypes.func,
  onClick: propTypes.func,
  onFocus: propTypes.func,
  onBlur: propTypes.func,
  onKeyUp: propTypes.func
};
DayPickerInput.defaultProps = {
  dayPickerProps: {},
  value: '',
  placeholder: 'YYYY-M-D',
  format: 'L',
  formatDate: defaultFormat,
  parseDate: defaultParse,
  showOverlay: false,
  hideOnDayClick: true,
  clickUnselectsDay: false,
  keepFocus: true,
  component: 'input',
  inputProps: {},
  overlayComponent: OverlayComponent,
  classNames: {
    container: 'DayPickerInput',
    overlayWrapper: 'DayPickerInput-OverlayWrapper',
    overlay: 'DayPickerInput-Overlay'
  }
};
OverlayComponent.__docgenInfo = {
  'description': 'The default component used as Overlay.\n\n@param {Object} props',
  'methods': [],
  'displayName': 'OverlayComponent',
  'props': {
    'input': {
      'type': {
        'name': 'any'
      },
      'required': false,
      'description': ''
    },
    'selectedDay': {
      'type': {
        'name': 'any'
      },
      'required': false,
      'description': ''
    },
    'month': {
      'type': {
        'name': 'instanceOf',
        'value': 'Date'
      },
      'required': false,
      'description': ''
    },
    'children': {
      'type': {
        'name': 'node'
      },
      'required': false,
      'description': ''
    },
    'classNames': {
      'type': {
        'name': 'object'
      },
      'required': false,
      'description': ''
    }
  }
};
DayPickerInput.__docgenInfo = {
  'description': '',
  'methods': [{
    'name': 'getInitialMonthFromProps',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'props',
      'optional': false,
      'type': null
    }],
    'returns': null
  }, {
    'name': 'getInitialStateFromProps',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'props',
      'optional': false,
      'type': null
    }],
    'returns': null
  }, {
    'name': 'getInput',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'getDayPicker',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'updateState',
    'docblock': 'Update the component\'s state and fire the `onDayChange` event passing the\nday\'s modifiers to it.\n\n@param {Date} day - Will be used for changing the month\n@param {String} value - Input field value\n@private',
    'modifiers': [],
    'params': [{
      'name': 'day',
      'description': 'Will be used for changing the month',
      'type': {
        'name': 'Date'
      },
      'optional': false
    }, {
      'name': 'value',
      'description': 'Input field value',
      'type': {
        'name': 'String'
      },
      'optional': false
    }, {
      'name': 'callback',
      'optional': false
    }],
    'returns': null,
    'description': 'Update the component\'s state and fire the `onDayChange` event passing the\nday\'s modifiers to it.'
  }, {
    'name': 'showDayPicker',
    'docblock': 'Show the Day Picker overlay.\n\n@memberof DayPickerInput',
    'modifiers': [],
    'params': [],
    'returns': null,
    'description': 'Show the Day Picker overlay.'
  }, {
    'name': 'hideDayPicker',
    'docblock': 'Hide the Day Picker overlay\n\n@memberof DayPickerInput',
    'modifiers': [],
    'params': [],
    'returns': null,
    'description': 'Hide the Day Picker overlay'
  }, {
    'name': 'hideAfterDayClick',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'handleInputClick',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'e',
      'optional': false,
      'type': null
    }],
    'returns': null
  }, {
    'name': 'handleInputFocus',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'e',
      'optional': false,
      'type': null
    }],
    'returns': null
  }, {
    'name': 'handleInputBlur',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'e',
      'optional': false,
      'type': null
    }],
    'returns': null
  }, {
    'name': 'handleOverlayFocus',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'e',
      'optional': false,
      'type': null
    }],
    'returns': null
  }, {
    'name': 'handleOverlayBlur',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'handleInputChange',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'e',
      'optional': false,
      'type': null
    }],
    'returns': null
  }, {
    'name': 'handleInputKeyDown',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'e',
      'optional': false,
      'type': null
    }],
    'returns': null
  }, {
    'name': 'handleInputKeyUp',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'e',
      'optional': false,
      'type': null
    }],
    'returns': null
  }, {
    'name': 'handleMonthChange',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'month',
      'optional': false,
      'type': null
    }],
    'returns': null
  }, {
    'name': 'handleDayClick',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'day',
      'optional': false,
      'type': null
    }, {
      'name': 'modifiers',
      'optional': false,
      'type': null
    }, {
      'name': 'e',
      'optional': false,
      'type': null
    }],
    'returns': null
  }, {
    'name': 'renderOverlay',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }],
  'displayName': 'DayPickerInput',
  'props': {
    'dayPickerProps': {
      'defaultValue': {
        'value': '{}',
        'computed': false
      },
      'type': {
        'name': 'object'
      },
      'required': false,
      'description': ''
    },
    'value': {
      'defaultValue': {
        'value': '\'\'',
        'computed': false
      },
      'type': {
        'name': 'union',
        'value': [{
          'name': 'string'
        }, {
          'name': 'instanceOf',
          'value': 'Date'
        }]
      },
      'required': false,
      'description': ''
    },
    'placeholder': {
      'defaultValue': {
        'value': '\'YYYY-M-D\'',
        'computed': false
      },
      'type': {
        'name': 'string'
      },
      'required': false,
      'description': ''
    },
    'format': {
      'defaultValue': {
        'value': '\'L\'',
        'computed': false
      },
      'type': {
        'name': 'union',
        'value': [{
          'name': 'string'
        }, {
          'name': 'arrayOf',
          'value': {
            'name': 'string'
          }
        }]
      },
      'required': false,
      'description': ''
    },
    'formatDate': {
      'defaultValue': {
        'value': 'function defaultFormat(d) {\n  if (isDate(d)) {\n    const year = d.getFullYear();\n    const month = `${d.getMonth() + 1}`;\n    const day = `${d.getDate()}`;\n    return `${year}-${month}-${day}`;\n  }\n  return \'\';\n}',
        'computed': false
      },
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'parseDate': {
      'defaultValue': {
        'value': 'function defaultParse(str) {\n  if (typeof str !== \'string\') {\n    return undefined;\n  }\n  const split = str.split(\'-\');\n  if (split.length !== 3) {\n    return undefined;\n  }\n  const year = parseInt(split[0], 10);\n  const month = parseInt(split[1], 10) - 1;\n  const day = parseInt(split[2], 10);\n  if (\n    isNaN(year) ||\n    String(year).length > 4 ||\n    isNaN(month) ||\n    isNaN(day) ||\n    day <= 0 ||\n    day > 31 ||\n    month < 0 ||\n    month >= 12\n  ) {\n    return undefined;\n  }\n\n  return new Date(year, month, day);\n}',
        'computed': false
      },
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'showOverlay': {
      'defaultValue': {
        'value': 'false',
        'computed': false
      },
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'hideOnDayClick': {
      'defaultValue': {
        'value': 'true',
        'computed': false
      },
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'clickUnselectsDay': {
      'defaultValue': {
        'value': 'false',
        'computed': false
      },
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'keepFocus': {
      'defaultValue': {
        'value': 'true',
        'computed': false
      },
      'type': {
        'name': 'bool'
      },
      'required': false,
      'description': ''
    },
    'component': {
      'defaultValue': {
        'value': '\'input\'',
        'computed': false
      },
      'type': {
        'name': 'any'
      },
      'required': false,
      'description': ''
    },
    'inputProps': {
      'defaultValue': {
        'value': '{}',
        'computed': false
      },
      'type': {
        'name': 'object'
      },
      'required': false,
      'description': ''
    },
    'overlayComponent': {
      'defaultValue': {
        'value': 'function OverlayComponent({ input, selectedDay, month, children, classNames, ...props }) {\n  return (\n    <div className={classNames.overlayWrapper} {...props}>\n      <div className={classNames.overlay}>{children}</div>\n    </div>\n  );\n}',
        'computed': false
      },
      'type': {
        'name': 'any'
      },
      'required': false,
      'description': ''
    },
    'classNames': {
      'defaultValue': {
        'value': '{\n  container: \'DayPickerInput\',\n  overlayWrapper: \'DayPickerInput-OverlayWrapper\',\n  overlay: \'DayPickerInput-Overlay\',\n}',
        'computed': false
      },
      'type': {
        'name': 'shape',
        'value': {
          'container': {
            'name': 'string',
            'required': false
          },
          'overlayWrapper': {
            'name': 'string',
            'required': false
          },
          'overlay': {
            'name': 'string',
            'required': true
          }
        }
      },
      'required': false,
      'description': ''
    },
    'onDayChange': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onDayPickerHide': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onChange': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onClick': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onFocus': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onBlur': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    },
    'onKeyUp': {
      'type': {
        'name': 'func'
      },
      'required': false,
      'description': ''
    }
  }
};

/**
 * Performs a deep comparison between two values to determine if they are
 * equivalent.
 *
 * **Note:** This method supports comparing arrays, array buffers, booleans,
 * date objects, error objects, maps, numbers, `Object` objects, regexes,
 * sets, strings, symbols, and typed arrays. `Object` objects are compared
 * by their own, not inherited, enumerable properties. Functions and DOM
 * nodes are compared by strict equality, i.e. `===`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 * @example
 *
 * var object = { 'a': 1 };
 * var other = { 'a': 1 };
 *
 * _.isEqual(object, other);
 * // => true
 *
 * object === other;
 * // => false
 */
function isEqual$1(value, other) {
  return _baseIsEqual(value, other);
}

var isEqual_1 = isEqual$1;

var FormSC = styled__default.form.withConfig({
  displayName: 'styled__FormSC',
  componentId: 'g4rb2j-0'
})(['width:100%;']);

var FormElementWrapper = styled__default.div.withConfig({
  displayName: 'styled__FormElementWrapper',
  componentId: 'g4rb2j-1'
})(['width:100%;position:relative;', ';'], space);

var ErrorWrapper = styled__default.div.withConfig({
  displayName: 'styled__ErrorWrapper',
  componentId: 'g4rb2j-2'
})(['position:absolute;bottom:-', 'px;'], themeGet('space.5'));

var SubmitButton = function SubmitButton(_ref) {
  var children = _ref.children,
      rest = objectWithoutProperties(_ref, ['children']);
  return React.createElement(
    Button,
    _extends$1({ type: 'submit' }, rest),
    children
  );
};
SubmitButton.defaultProps = {};
SubmitButton.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'SubmitButton',
  'props': {
    'onClick': {
      'required': true,
      'flowType': {
        'name': 'Function'
      },
      'description': ''
    },
    'children': {
      'required': true,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    }
  }
};

var Form = function (_React$Component) {
  inherits(Form, _React$Component);

  function Form() {
    var _ref;

    var _temp, _this, _ret;

    classCallCheck(this, Form);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = possibleConstructorReturn(this, (_ref = Form.__proto__ || Object.getPrototypeOf(Form)).call.apply(_ref, [this].concat(args))), _this), _this.onSubmit = function (e) {
      var onSubmit = _this.props.onSubmit;


      if (e) e.preventDefault();
      onSubmit();
    }, _temp), possibleConstructorReturn(_this, _ret);
  }

  createClass(Form, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          error = _props.error,
          loading = _props.loading,
          isSubmitting = _props.isSubmitting,
          children = _props.children,
          submitLabel = _props.submitLabel,
          noSubmit = _props.noSubmit,
          rest = objectWithoutProperties(_props, ['error', 'loading', 'isSubmitting', 'children', 'submitLabel', 'noSubmit']);


      return React.createElement(
        FormSC,
        { onKeyDown: function onKeyDown(e) {
            return e.key === 'Enter' && e.preventDefault();
          }, onSubmit: this.onSubmit },
        React.createElement('input', { type: 'hidden', value: 'something' }),
        error && React.createElement(
          Row,
          { mb: 7 },
          React.createElement(
            Text,
            { fontWeight: 4, fontSize: 2, color: 'danger' },
            error
          )
        ),
        children,
        !noSubmit && React.createElement(
          Row,
          { justifyContent: 'flex-end' },
          React.createElement(
            SubmitButton,
            { loading: loading, onClick: this.onSubmit, disabled: isSubmitting },
            submitLabel
          )
        )
      );
    }
  }]);
  return Form;
}(React.Component);

/**
 * Named exports
 */

Form.defaultProps = {
  submitLabel: 'Submit',
  onSubmit: function onSubmit() {}
};
var FormLabel = function FormLabel(_ref2) {
  var id = _ref2.id,
      label = _ref2.label,
      required = _ref2.required,
      hint = _ref2.hint;
  return React.createElement(
    _Label,
    { htmlFor: id, mb: 2 },
    label,
    required && hint ? React.createElement(
      Text,
      { fontSize: 2, color: 'grey', ml: 1 },
      '(',
      hint,
      ')'
    ) : !required ? React.createElement(
      Text,
      { fontSize: 2, color: 'grey', ml: 1 },
      '(optional)'
    ) : null
  );
};

var ErrorDisplay = function ErrorDisplay(_ref3) {
  var error = _ref3.error;
  return React.createElement(
    ErrorWrapper,
    null,
    React.createElement(
      Text,
      { fontSize: 2, color: 'danger' },
      error
    )
  );
};

var withFormComponents = function withFormComponents(ComponentToWrap) {
  var _class, _temp3;

  return _temp3 = _class = function (_React$Component2) {
    inherits(_class, _React$Component2);

    function _class() {
      var _ref4;

      var _temp2, _this2, _ret2;

      classCallCheck(this, _class);

      for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        args[_key2] = arguments[_key2];
      }

      return _ret2 = (_temp2 = (_this2 = possibleConstructorReturn(this, (_ref4 = _class.__proto__ || Object.getPrototypeOf(_class)).call.apply(_ref4, [this].concat(args))), _this2), _this2.onChange = function (value) {
        var _this2$props = _this2.props,
            id = _this2$props.id,
            setFieldValue = _this2$props.setFieldValue,
            onChange = _this2$props.onChange;


        if (setFieldValue) setFieldValue(id, value);
        if (onChange) onChange(value);
      }, _temp2), possibleConstructorReturn(_this2, _ret2);
    }

    createClass(_class, [{
      key: 'shouldComponentUpdate',


      /**
       * Compares values and errors for the component to determine if an update is necessary
       */
      value: function shouldComponentUpdate(nextProps) {
        var _props2 = this.props,
            id = _props2.id,
            values = _props2.values,
            value = _props2.value,
            errors = _props2.errors,
            error = _props2.error,
            fetchError = _props2.fetchError,
            loading = _props2.loading,
            touched = _props2.touched;

        // Current value and next values

        var _value = value || get_1(values, '' + id);
        var nvalue = nextProps.value || get_1(nextProps.values, '' + id);

        // Current value and next values
        var _error = error || get_1(errors, '' + id);
        var nerror = nextProps.error || get_1(nextProps.errors, '' + id);

        // Current value and next values
        var _touched = get_1(touched, '' + id);
        var ntouched = get_1(nextProps.touched, '' + id);

        if (isEqual_1(_value, nvalue) && isEqual_1(_error, nerror) && isEqual_1(_touched, ntouched) && fetchError === nextProps.fetchError && loading === nextProps.loading) return false;

        // Update
        return true;
      }
    }, {
      key: 'render',
      value: function render() {
        var _props3 = this.props,
            id = _props3.id,
            label = _props3.label,
            required = _props3.required,
            hint = _props3.hint;
        var _props4 = this.props,
            mb = _props4.mb,
            errors = _props4.errors,
            values = _props4.values,
            touched = _props4.touched,
            setFieldValue = _props4.setFieldValue,
            fetchError = _props4.fetchError,
            error = _props4.error,
            loading = _props4.loading,
            handleBlur = _props4.handleBlur,
            value = _props4.value,
            rest = objectWithoutProperties(_props4, ['mb', 'errors', 'values', 'touched', 'setFieldValue', 'fetchError', 'error', 'loading', 'handleBlur', 'value']);


        var _error = error || get_1(errors, '' + id);
        var _value = value || get_1(values, '' + id);
        var isTouched = get_1(touched, '' + id);
        var inValid = !!(isTouched && _error);
        var hasError = inValid || fetchError;

        var labelProps = {
          id: id,
          label: label,
          required: required,
          hint: hint
        };

        var componentProps = _extends$1({}, rest, {
          value: _value,
          onChange: this.onChange,
          hasError: hasError,
          loading: loading,
          onBlur: handleBlur
        });

        return React.createElement(
          FormElementWrapper,
          { mb: mb },
          React.createElement(FormLabel, labelProps),
          React.createElement(ComponentToWrap, componentProps),
          fetchError ? React.createElement(ErrorDisplay, { error: fetchError }) : inValid ? React.createElement(ErrorDisplay, { error: _error }) : null
        );
      }
    }]);
    return _class;
  }(React.Component), _class.defaultProps = {
    required: true,
    mb: 7,
    options: null,
    loading: false
  }, _temp3;
};
Form.__docgenInfo = {
  'description': '',
  'methods': [{
    'name': 'onSubmit',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'e',
      'type': {
        'name': 'any'
      }
    }],
    'returns': null
  }],
  'displayName': 'Form',
  'props': {
    'submitLabel': {
      'defaultValue': {
        'value': '\'Submit\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'onSubmit': {
      'defaultValue': {
        'value': '() => {}',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'Function'
      },
      'description': ''
    },
    'children': {
      'required': true,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    },
    'noSubmit': {
      'required': true,
      'flowType': {
        'name': 'boolean'
      },
      'description': ''
    },
    'isSubmitting': {
      'required': true,
      'flowType': {
        'name': 'boolean'
      },
      'description': ''
    },
    'loading': {
      'required': true,
      'flowType': {
        'name': 'boolean'
      },
      'description': ''
    },
    'error': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    }
  }
};
FormLabel.__docgenInfo = {
  'description': 'Named exports',
  'methods': [],
  'displayName': 'FormLabel',
  'props': {
    'id': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'label': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'required': {
      'required': true,
      'flowType': {
        'name': 'boolean'
      },
      'description': ''
    },
    'hint': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    }
  }
};
ErrorDisplay.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'ErrorDisplay',
  'props': {
    'error': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    }
  }
};

var Card = function Card(_ref) {
  var children = _ref.children,
      rest = objectWithoutProperties(_ref, ['children']);
  return React.createElement(
    Box,
    _extends$1({}, rest, { borderRadius: 1, bg: 'white' }),
    children
  );
};
Card.defaultProps = {
  p: 6,
  elevation: 4
};
Card.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Card',
  'props': {
    'p': {
      'defaultValue': {
        'value': '6',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': ''
    },
    'elevation': {
      'defaultValue': {
        'value': '4',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': ''
    },
    'children': {
      'required': true,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    }
  }
};

var getTextareaHeight = function getTextareaHeight(_ref) {
  var lines = _ref.lines,
      theme = _ref.theme;

  var index = lines * 5;
  return theme.height[index] + 'px';
};

var InputWrapper = styled__default.input.withConfig({
  displayName: 'styled__InputWrapper',
  componentId: 'sc-1u7jh8i-0'
})(['', ';box-shadow:', ';border:none;border-radius:', 'px;width:100%;outline:none;transition:all 200ms ease;padding-right:', 'px;padding-left:', 'px;padding-top:', 'px;padding-bottom:', 'px;font-size:12px;&:focus{', ';}', ';'], uiSize, function (props) {
  return props.theme.elevation[1];
}, themeGet('borderRadius.1'), function (props) {
  return props.theme.padding[3];
}, function (props) {
  return props.theme.padding[3];
}, function (props) {
  return props.theme.padding[1];
}, function (props) {
  return props.theme.padding[1];
}, inputFocus, inputError);

var TextareaWrapper = styled__default.textarea.withConfig({
  displayName: 'styled__TextareaWrapper',
  componentId: 'sc-1u7jh8i-1'
})(['min-height:', ';box-shadow:', ';border:none;border-radius:', 'px;width:100%;outline:none;transition:all 200ms ease;padding-right:', 'px;padding-left:', 'px;padding-top:', 'px;padding-bottom:', 'px;resize:none;font-size:12px;&:focus{', ';}', ';'], getTextareaHeight, function (props) {
  return props.theme.elevation[1];
}, themeGet('borderRadius.1'), function (props) {
  return props.theme.padding[3];
}, function (props) {
  return props.theme.padding[3];
}, function (props) {
  return props.theme.padding[2];
}, function (props) {
  return props.theme.padding[1];
}, inputFocus, inputError);

var Input = function Input(_ref) {
  var _ref$lines = _ref.lines,
      lines = _ref$lines === undefined ? 1 : _ref$lines,
      onChange = _ref.onChange,
      autoComplete = _ref.autoComplete,
      rest = objectWithoutProperties(_ref, ['lines', 'onChange', 'autoComplete']);
  return React.createElement(
    React.Fragment,
    null,
    lines > 1 ? React.createElement(TextareaWrapper, _extends$1({
      autoComplete: 'off',
      lines: lines,
      type: 'text'
    }, rest, {
      onChange: function (_onChange) {
        function onChange(_x) {
          return _onChange.apply(this, arguments);
        }

        onChange.toString = function () {
          return _onChange.toString();
        };

        return onChange;
      }(function (e) {
        return onChange(e.target.value);
      })
    })) : React.createElement(InputWrapper, _extends$1({ autoComplete: autoComplete, type: 'text' }, rest, { onChange: function (_onChange2) {
        function onChange(_x2) {
          return _onChange2.apply(this, arguments);
        }

        onChange.toString = function () {
          return _onChange2.toString();
        };

        return onChange;
      }(function (e) {
        return onChange(e.target.value);
      }) }))
  );
};
Input.displayName = 'Input';

Input.defaultProps = {
  lines: 1,
  uiSize: 'small',
  autoComplete: 'off'
};

var Input$1 = withFormComponents(Input);
Input.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Input',
  'props': {
    'lines': {
      'defaultValue': {
        'value': '1',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': ''
    },
    'uiSize': {
      'defaultValue': {
        'value': '\'small\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'autoComplete': {
      'defaultValue': {
        'value': '\'off\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'union',
        'raw': '\'off\' | any',
        'elements': [{
          'name': 'literal',
          'value': '\'off\''
        }, {
          'name': 'any'
        }]
      },
      'description': ''
    },
    'value': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'onChange': {
      'required': true,
      'flowType': {
        'name': 'Function'
      },
      'description': ''
    },
    'placeholder': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    }
  }
};

var InputComponent = styled__default(InputWrapper).withConfig({
  displayName: 'styled__InputComponent',
  componentId: 'sc-17at03y-0'
})(['height:32px;']);

var OverlayComponentWrapper = styled__default.div.withConfig({
  displayName: 'styled__OverlayComponentWrapper',
  componentId: 'sc-17at03y-1'
})(['position:absolute;top:64px;min-width:320px;z-index:1;']);

var DayCellDisplay = styled__default.div.withConfig({
  displayName: 'styled__DayCellDisplay',
  componentId: 'sc-17at03y-2'
})(['padding:', 'px;display:table-cell;text-align:center;vertical-align:middle;cursor:pointer;outline:none;'], themeGet('space.1'));

var DayDisplay = styled__default.div.withConfig({
  displayName: 'styled__DayDisplay',
  componentId: 'sc-17at03y-3'
})(['width:32px;height:32px;border-radius:32px;background:', ';display:flex;align-items:center;justify-content:center;margin:0 auto;transition:all 200ms ease;font-weight:600;color:', ';', ';', ';', ';', ';', ';'], themeGet('colors.greys.6'), themeGet('colors.greys.2'), function (_ref) {
  var isOutside = _ref.isOutside;
  return isOutside && styled.css(['color:', ';background:white;'], themeGet('colors.greys.3'));
}, function (_ref2) {
  var isSelected = _ref2.isSelected,
      isToday = _ref2.isToday;
  return !isSelected && styled.css(['&:hover{background:', ';color:', ';}'], function (props) {
    return curriedTransparentize(0.8, themeGet('colors.primary')(props));
  }, themeGet('colors.primary'));
}, function (_ref3) {
  var isSelected = _ref3.isSelected;
  return isSelected && styled.css(['background:', ';color:white !important;'], themeGet('colors.primary'));
}, function (_ref4) {
  var isToday = _ref4.isToday,
      isSelected = _ref4.isSelected;
  return isToday && !isSelected && styled.css(['background:white;box-shadow:0px 0px 0px 2px ', ';color:', ';'], function (props) {
    return curriedTransparentize(0.5, themeGet('colors.info')(props));
  }, themeGet('colors.info'));
}, function (_ref5) {
  var isToday = _ref5.isToday,
      isSelected = _ref5.isSelected;
  return isToday && isSelected && styled.css(['border:1px solid white;box-shadow:0px 0px 0px 2px ', ';'], function (props) {
    return curriedTransparentize(0.5, themeGet('colors.info')(props));
  });
});

var WeekDisplay = styled__default.div.withConfig({
  displayName: 'styled__WeekDisplay',
  componentId: 'sc-17at03y-4'
})(['display:table-row;']);

var WeeksDisplay = styled__default.div.withConfig({
  displayName: 'styled__WeeksDisplay',
  componentId: 'sc-17at03y-5'
})(['display:table-row-group;']);

var WeekdayDisplay = styled__default.div.withConfig({
  displayName: 'styled__WeekdayDisplay',
  componentId: 'sc-17at03y-6'
})(['display:table-cell;padding:', 'px;color:', ';text-align:center;font-size:', 'px;'], themeGet('space.3'), themeGet('colors.greys.2'), themeGet('fontSizes.1'));

var WeekdaysDisplay = styled__default.div.withConfig({
  displayName: 'styled__WeekdaysDisplay',
  componentId: 'sc-17at03y-7'
})(['display:table-header-group;margin-top:1em;']);

var WeekdaysRowDisplay = styled__default.div.withConfig({
  displayName: 'styled__WeekdaysRowDisplay',
  componentId: 'sc-17at03y-8'
})(['display:table-row;']);

var MonthDisplay = styled__default.div.withConfig({
  displayName: 'styled__MonthDisplay',
  componentId: 'sc-17at03y-9'
})(['width:100%;display:table;margin:0 1em;margin-top:1em;border-spacing:0;border-collapse:collapse;user-select:none;outline:none;']);

var MonthsDisplay = styled__default.div.withConfig({
  displayName: 'styled__MonthsDisplay',
  componentId: 'sc-17at03y-10'
})(['display:flex;flex-wrap:wrap;justify-content:center;']);

var CaptionDisplay = styled__default.div.withConfig({
  displayName: 'styled__CaptionDisplay',
  componentId: 'sc-17at03y-11'
})(['display:table-caption;margin-bottom:', 'px;padding:', 'px;text-align:center;color:', ';font-weight:600;font-size:', 'px;'], themeGet('space.6'), themeGet('space.0'), themeGet('colors.text'), themeGet('fontSizes.3'));

var NavBarDisplay = styled__default.div.withConfig({
  displayName: 'styled__NavBarDisplay',
  componentId: 'sc-17at03y-12'
})(['display:flex;height:32px;position:absolute;width:100%;left:0;padding:8px 32px;']);

var NavBarNextDisplay = function NavBarNextDisplay(props) {
  return React__default.createElement(Button, _extends$1({ ml: 'auto', buttonType: 'flat', icon: 'arrowRight' }, props));
};

var NavBarPrevDisplay = function NavBarPrevDisplay(props) {
  return React__default.createElement(Button, _extends$1({ buttonType: 'flat', icon: 'arrowLeft' }, props));
};

var OverlayComponent$1 = function OverlayComponent$$1(_ref) {
  var children = _ref.children,
      selectedDay = _ref.selectedDay,
      tabIndex = _ref.tabIndex,
      month = _ref.month,
      input = _ref.input,
      classNames = _ref.classNames,
      props = objectWithoutProperties(_ref, ['children', 'selectedDay', 'tabIndex', 'month', 'input', 'classNames']);
  return React__default.createElement(
    OverlayComponentWrapper,
    props,
    React__default.createElement(
      Card,
      null,
      children
    )
  );
};

var DatePicker = function DatePicker(_ref2) {
  var id = _ref2.id,
      onChange = _ref2.onChange,
      value = _ref2.value,
      placeholder = _ref2.placeholder,
      selectedDays = _ref2.selectedDays,
      rest = objectWithoutProperties(_ref2, ['id', 'onChange', 'value', 'placeholder', 'selectedDays']);
  return React__default.createElement(DayPickerInput, {
    id: id,
    value: value,
    showOutsideDays: true,
    fixedWeeks: true,
    placeholder: placeholder,
    selectedDays: selectedDays,
    component: Input,
    onDayChange: onChange,
    inputProps: { id: id, placeholder: placeholder },
    overlayComponent: OverlayComponent$1,
    dayCellDisplay: DayCellDisplay,
    dayDisplay: DayDisplay,
    weekDisplay: WeekDisplay,
    weeksDisplay: WeeksDisplay,
    monthDisplay: MonthDisplay,
    monthsDisplay: MonthsDisplay,
    captionDisplay: CaptionDisplay,
    weekdayDisplay: WeekdayDisplay,
    weekdaysDisplay: WeekdaysDisplay,
    weekdaysRowDisplay: WeekdaysRowDisplay,
    navBarDisplay: NavBarDisplay,
    navBarNextDisplay: NavBarNextDisplay,
    navBarPrevDisplay: NavBarPrevDisplay
  });
};
DatePicker.defaultProps = {
  onChange: function onChange() {}
};

var DatePicker$1 = withFormComponents(DatePicker);
DatePicker.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'DatePicker',
  'props': {
    'onChange': {
      'defaultValue': {
        'value': '() => {}',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'Function'
      },
      'description': ''
    },
    'value': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'placeholder': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'id': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    }
  }
};

var FormValueWrapper = styled__default.div.withConfig({
  displayName: 'FormValue__FormValueWrapper',
  componentId: 'sc-1cpgtc6-0'
})(['display:flex;', ';', ';', ';'], height, alignItems, justifyContent);

var FormValue = function FormValue(_ref) {
  var children = _ref.children,
      label = _ref.label,
      hint = _ref.hint,
      mb = _ref.mb,
      rest = objectWithoutProperties(_ref, ['children', 'label', 'hint', 'mb']);
  return React.createElement(
    FormElementWrapper,
    { mb: mb },
    React.createElement(
      _Label,
      { mb: 2 },
      label,
      hint && React.createElement(
        Text,
        { fontSize: 2, color: 'grey', ml: 1 },
        '(',
        hint,
        ')'
      )
    ),
    React.createElement(
      FormValueWrapper,
      rest,
      children
    )
  );
};
FormValue.defaultProps = {
  mb: 7,
  height: 8,
  alignItems: 'center'
};
FormValue.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'FormValue',
  'props': {
    'mb': {
      'defaultValue': {
        'value': '7',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': ''
    },
    'height': {
      'defaultValue': {
        'value': '8',
        'computed': false
      },
      'required': false
    },
    'alignItems': {
      'defaultValue': {
        'value': '\'center\'',
        'computed': false
      },
      'required': false
    },
    'children': {
      'required': true,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    },
    'label': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'hint': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    }
  }
};

var SelectInput = styled__default.div.withConfig({
  displayName: 'styled__SelectInput',
  componentId: 'v4vzx2-0'
})(['cursor:pointer;box-shadow:', ';border:none;border-radius:', 'px;width:100%;outline:none;transition:all 200ms ease;padding-right:', 'px;padding-left:', 'px;background:white;display:flex;align-items:center;', ';', ';', ' &:focus{', ';}'], function (props) {
  return props.theme.elevation[1];
}, themeGet('borderRadius.1'), function (props) {
  return props.theme.padding[3];
}, function (props) {
  return props.theme.padding[3];
}, uiSize, inputError, function (_ref) {
  var focused = _ref.focused;
  return focused && styled.css(['', ';'], inputFocus);
}, inputFocus);

var SelectWrapper = styled__default.div.withConfig({
  displayName: 'styled__SelectWrapper',
  componentId: 'v4vzx2-1'
})(['position:relative;']);

var SelectContainer = styled__default.div.withConfig({
  displayName: 'styled__SelectContainer',
  componentId: 'v4vzx2-2'
})(['width:100%;opacity:0;position:absolute;transition:all 200ms ease;background:white;box-shadow:', ';border-radius:', 'px;overflow:scroll;pointer-events:none;top:40px;height:0px;', ';'], function (props) {
  return props.theme.elevation[5];
}, themeGet('borderRadius.2'), function (_ref2) {
  var open = _ref2.open;
  return open && styled.css(['height:auto;pointer-events:all;opacity:1;z-index:9;max-height:256px;']);
});

var SelectItemWrapper = styled__default.div.withConfig({
  displayName: 'styled__SelectItemWrapper',
  componentId: 'v4vzx2-3'
})(['font-weight:400;font-size:17px;line-height:26px;', ';&:hover{background:', ';cursor:pointer;}'], space, function (props) {
  return curriedLighten(0.425, props.theme.color.primary);
});

var HiddenInput$1 = styled__default.input.withConfig({
  displayName: 'styled__HiddenInput',
  componentId: 'v4vzx2-4'
})(['pointer-events:none;opacity:0;position:absolute;']);

var SelectItemList = styled__default.div.withConfig({
  displayName: 'styled__SelectItemList',
  componentId: 'v4vzx2-5'
})(['display:flex;flex-direction:column;']);

var DropdownIcon = styled__default.div.withConfig({
  displayName: 'styled__DropdownIcon',
  componentId: 'v4vzx2-6'
})(['width:20px;height:20px;display:flex;justify-content:center;align-items:center;margin-left:auto;']);

var SelectItem = function SelectItem(_ref) {
  var label = _ref.label;
  return React.createElement(
    P,
    { mb: 0 },
    label
  );
};
SelectItem.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'SelectItem',
  'props': {
    'label': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    }
  }
};

var Select = function (_React$Component) {
  inherits(Select, _React$Component);

  function Select() {
    var _ref;

    var _temp, _this, _ret;

    classCallCheck(this, Select);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = possibleConstructorReturn(this, (_ref = Select.__proto__ || Object.getPrototypeOf(Select)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      open: false,
      focused: false,
      hovering: false
    }, _this.hiddenInputRef = React.createRef(), _this.toggleOpen = function () {
      var open = _this.state.open;

      if (!open) _this.focusInput();
      _this.setState({
        open: !open
      });
    }, _this.setOpen = function (value) {
      _this.setState({
        open: value
      });
    }, _this.setFocus = function (value) {
      if (!value) _this.setOpen(false);
      _this.setState({
        focused: value
      });
    }, _this.focusInput = function () {
      _this.hiddenInputRef.current.focus();
    }, _this.blurInput = function () {
      _this.hiddenInputRef.current.blur();
    }, _this.onItemClick = function (o) {
      var onChange = _this.props.onChange;

      if (onChange) onChange(o);
      _this.setOpen(false);
    }, _this.onKeyDown = function (e) {
      switch (e.key) {
        case 'Tab':
          _this.setFocus(false);
          break;
        case 'Enter':
        case 'Space':
        case 'ArrowDown':
        case 'ArrowUp':
          _this.toggleOpen();
          break;
        default:
          break;
      }
    }, _this.onBlur = function () {
      var hovering = _this.state.hovering;

      if (!hovering) {
        _this.setFocus(false);
        _this.blurInput();
      } else {
        _this.focusInput();
      }
    }, _this.onFocus = function () {
      _this.setFocus(true);
    }, _this.setHovering = function (value) {
      _this.setState({
        hovering: value
      });
    }, _temp), possibleConstructorReturn(_this, _ret);
  }

  createClass(Select, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          value = _props.value,
          options = _props.options,
          uiSize = _props.uiSize,
          placeholder = _props.placeholder,
          loading = _props.loading,
          renderItem = _props.renderItem,
          hasError = _props.hasError;
      var _state = this.state,
          open = _state.open,
          focused = _state.focused;


      return React.createElement(
        SelectWrapper,
        { onMouseEnter: function onMouseEnter() {
            return _this2.setHovering(true);
          }, onMouseLeave: function onMouseLeave() {
            return _this2.setHovering(false);
          } },
        React.createElement(HiddenInput$1, {
          onKeyDown: this.onKeyDown,
          innerRef: this.hiddenInputRef,
          onFocus: this.onFocus,
          onBlur: this.onBlur
        }),
        React.createElement(
          SelectInput,
          { hasError: hasError, uiSize: uiSize, focused: focused, onClick: this.toggleOpen },
          React.createElement(
            Col,
            { maxHeight: '32px', mr: 5 },
            value ? renderItem(value) : placeholder || ''
          ),
          React.createElement(
            DropdownIcon,
            null,
            loading ? React.createElement(Spinner, { size: 5 }) : React.createElement(Icon, { size: 16, icon: 'caretDown' })
          )
        ),
        React.createElement(
          SelectContainer,
          { open: open },
          React.createElement(
            SelectItemList,
            null,
            loading ? null : map_1(options, function (o, i) {
              return React.createElement(
                Clickable,
                { tabIndex: open ? 0 : -1, onClick: function onClick() {
                    return _this2.onItemClick(o);
                  }, key: i },
                React.createElement(
                  SelectItemWrapper,
                  { px: 3, py: 2 },
                  renderItem(o)
                )
              );
            })
          )
        )
      );
    }
  }]);
  return Select;
}(React.Component);

Select.defaultProps = {
  options: [],
  loading: false,
  uiSize: 'small',
  placeholder: 'Select',
  renderItem: function renderItem(obj) {
    return React.createElement(SelectItem, { label: obj.label });
  }
};
Select.displayName = 'Select';

var Select$1 = withFormComponents(Select);
Select.__docgenInfo = {
  'description': '',
  'methods': [{
    'name': 'toggleOpen',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'setOpen',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'value',
      'type': {
        'name': 'boolean'
      }
    }],
    'returns': null
  }, {
    'name': 'setFocus',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'value',
      'type': {
        'name': 'boolean'
      }
    }],
    'returns': null
  }, {
    'name': 'focusInput',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'blurInput',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'onItemClick',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'o',
      'type': {
        'name': 'signature',
        'type': 'object',
        'raw': '{\n  value: any,\n  label: string,\n  id: string,\n}',
        'signature': {
          'properties': [{
            'key': 'value',
            'value': {
              'name': 'any',
              'required': true
            }
          }, {
            'key': 'label',
            'value': {
              'name': 'string',
              'required': true
            }
          }, {
            'key': 'id',
            'value': {
              'name': 'string',
              'required': true
            }
          }]
        },
        'alias': 'Option'
      }
    }],
    'returns': null
  }, {
    'name': 'onKeyDown',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'e',
      'type': {
        'name': 'any'
      }
    }],
    'returns': null
  }, {
    'name': 'onBlur',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'onFocus',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'setHovering',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'value',
      'type': {
        'name': 'boolean'
      }
    }],
    'returns': null
  }],
  'displayName': 'Select',
  'props': {
    'options': {
      'defaultValue': {
        'value': '[]',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'Array',
        'elements': [{
          'name': 'signature',
          'type': 'object',
          'raw': '{\n  value: any,\n  label: string,\n  id: string,\n}',
          'signature': {
            'properties': [{
              'key': 'value',
              'value': {
                'name': 'any',
                'required': true
              }
            }, {
              'key': 'label',
              'value': {
                'name': 'string',
                'required': true
              }
            }, {
              'key': 'id',
              'value': {
                'name': 'string',
                'required': true
              }
            }]
          }
        }],
        'raw': 'Array<Option>'
      },
      'description': ''
    },
    'loading': {
      'defaultValue': {
        'value': 'false',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'boolean'
      },
      'description': ''
    },
    'uiSize': {
      'defaultValue': {
        'value': '\'small\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'literal',
        'value': '\'small\''
      },
      'description': ''
    },
    'placeholder': {
      'defaultValue': {
        'value': '\'Select\'',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'renderItem': {
      'defaultValue': {
        'value': 'obj => <SelectItem label={obj.label} />',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'Function'
      },
      'description': ''
    },
    'value': {
      'required': true,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    },
    'onChange': {
      'required': true,
      'flowType': {
        'name': 'Function'
      },
      'description': ''
    },
    'hasError': {
      'required': true,
      'flowType': {
        'name': 'boolean'
      },
      'description': ''
    }
  }
};

var TabSC = styled__default.div.withConfig({
  displayName: 'styled__TabSC',
  componentId: 'm3b1dj-0'
})(['width:100%;', ';'], function (_ref) {
  var height = _ref.height;
  return height && styled.css(['height:', 'px;'], height);
});

var TabLabelSC = styled__default.li.withConfig({
  displayName: 'styled__TabLabelSC',
  componentId: 'm3b1dj-1'
})(['display:inline-block;padding:16px 12px;transition:all 200ms ease;cursor:pointer;&:hover{box-shadow:0px 3px 0px ', ';}', ';'], function (_ref2) {
  var theme = _ref2.theme;
  return theme.color.primary;
}, function (_ref3) {
  var isActive = _ref3.isActive;
  return isActive && styled.css(['font-weight:600;box-shadow:0px 3px 0px ', ';'], function (_ref4) {
    var theme = _ref4.theme;
    return theme.color.primary;
  });
});

var TabList = styled__default.ol.withConfig({
  displayName: 'styled__TabList',
  componentId: 'm3b1dj-2'
})(['padding:2px 0px;border-bottom:1px solid #eff1f2;']);

var TabContent = styled__default.div.withConfig({
  displayName: 'styled__TabContent',
  componentId: 'm3b1dj-3'
})(['padding:24px 0px;']);

var TabLabel = function (_Component) {
  inherits(TabLabel, _Component);

  function TabLabel() {
    var _ref;

    var _temp, _this, _ret;

    classCallCheck(this, TabLabel);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = possibleConstructorReturn(this, (_ref = TabLabel.__proto__ || Object.getPrototypeOf(TabLabel)).call.apply(_ref, [this].concat(args))), _this), _this.onLabelClick = function () {
      var _this$props = _this.props,
          isActive = _this$props.isActive,
          label = _this$props.label,
          onClick = _this$props.onClick,
          id = _this$props.id;

      onClick && onClick({ id: id });
    }, _temp), possibleConstructorReturn(_this, _ret);
  }

  createClass(TabLabel, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          isActive = _props.isActive,
          onClick = _props.onClick,
          label = _props.label,
          id = _props.id;

      return React__default.createElement(
        TabLabelSC,
        { isActive: isActive, onClick: this.onLabelClick },
        label
      );
    }
  }]);
  return TabLabel;
}(React.Component);
TabLabel.__docgenInfo = {
  'description': '',
  'methods': [{
    'name': 'onLabelClick',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }],
  'displayName': 'TabLabel',
  'props': {
    'onClick': {
      'required': true,
      'flowType': {
        'name': 'Function'
      },
      'description': ''
    },
    'isActive': {
      'required': true,
      'flowType': {
        'name': 'boolean'
      },
      'description': ''
    },
    'label': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'id': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    }
  }
};

var Tab = function Tab(_ref) {
  var isActive = _ref.isActive,
      children = _ref.children,
      rest = objectWithoutProperties(_ref, ['isActive', 'children']);

  if (!isActive) return null;

  return React__default.createElement(
    TabSC,
    rest,
    children
  );
};
Tab.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Tab',
  'props': {
    'id': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'label': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'children': {
      'required': true,
      'flowType': {
        'name': 'any'
      },
      'description': ''
    },
    'isActive': {
      'required': true,
      'flowType': {
        'name': 'boolean'
      },
      'description': ''
    },
    'height': {
      'required': true,
      'flowType': {
        'name': 'union',
        'raw': 'number | string',
        'elements': [{
          'name': 'number'
        }, {
          'name': 'string'
        }]
      },
      'description': ''
    }
  }
};

var Tabs = function (_Component) {
  inherits(Tabs, _Component);

  function Tabs() {
    var _ref;

    var _temp, _this, _ret;

    classCallCheck(this, Tabs);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = possibleConstructorReturn(this, (_ref = Tabs.__proto__ || Object.getPrototypeOf(Tabs)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      activeTab: {}
    }, _this.componentDidMount = function () {
      _this.setFirsActiveTab();
    }, _this.setFirsActiveTab = function () {
      var _this$props = _this.props,
          children = _this$props.children,
          initialTab = _this$props.initialTab;


      if (!(children.length > 0)) return null;

      if (initialTab) {
        _this.setState({
          activeTab: { id: initialTab }
        });
        return null;
      }

      var activeId = _this.props.children[0].props.id;

      _this.setState({
        activeTab: { id: activeId }
      });
    }, _this.onClickTabItem = function (tab) {
      _this.setState({ activeTab: tab });
    }, _temp), possibleConstructorReturn(_this, _ret);
  }

  createClass(Tabs, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var children = this.props.children;
      var activeTab = this.state.activeTab;


      return React__default.createElement(
        TabSC,
        null,
        React__default.createElement(
          TabList,
          null,
          children && children.length > 1 ? children.map(function (child, i) {
            var _child$props = child.props,
                id = _child$props.id,
                label = _child$props.label;

            var isActive = activeTab ? id === activeTab.id : false;

            return React__default.createElement(TabLabel, {
              isActive: isActive,
              key: 'tablabel_' + id,
              id: id,
              label: label,
              onClick: _this2.onClickTabItem
            });
          }) : React__default.createElement(TabLabel, { isActive: true, id: children.props.id, label: children.props.label, onClick: this.onClickTabItem })
        ),
        React__default.createElement(
          TabContent,
          null,
          children && children.length > 1 ? children.map(function (child, i) {
            var id = child.props.id;

            var isActive = activeTab ? id === activeTab.id : false;

            return React__default.createElement(
              Tab,
              { key: 'tab_' + id, isActive: isActive },
              child.props.children
            );
          }) : React__default.createElement(
            Tab,
            { isActive: true },
            children.props.children
          )
        )
      );
    }
  }]);
  return Tabs;
}(React.Component);
Tabs.__docgenInfo = {
  'description': '',
  'methods': [{
    'name': 'componentDidMount',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'setFirsActiveTab',
    'docblock': null,
    'modifiers': [],
    'params': [],
    'returns': null
  }, {
    'name': 'onClickTabItem',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'tab',
      'type': null
    }],
    'returns': null
  }],
  'displayName': 'Tabs',
  'props': {
    'children': {
      'required': true,
      'flowType': {
        'name': 'React$Element'
      },
      'description': ''
    },
    'initialTab': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    }
  }
};

var CheckboxUI = styled__default.div.withConfig({
  displayName: 'styled__CheckboxUI',
  componentId: 'lhn7s7-0'
})(['position:absolute;height:', 'px;width:', 'px;border-radius:', 'px;border:1px solid ', ';pointer-events:none;cursor:pointer;background:white;transition:all 200ms ease;display:flex;align-content:center;justify-content:center;', ';'], themeGet('space.6'), themeGet('space.6'), themeGet('space.6'), function (_ref) {
  var theme = _ref.theme;
  return theme.color.border;
}, function (_ref2) {
  var checked = _ref2.checked,
      theme = _ref2.theme;
  return checked && styled.css(['box-shadow:', ';background:', ';border:1px solid ', ';'], themeGet('elevation.3'), themeGet('colors.primary'), themeGet('colors.primary'));
});

var CheckboxSC = styled__default.input.withConfig({
  displayName: 'styled__CheckboxSC',
  componentId: 'lhn7s7-1'
})(['height:', 'px;width:', 'px;opacity:0;cursor:pointer;&:focus{+ ', '{box-shadow:', ';border:1px solid ', ';}}'], themeGet('space.8'), themeGet('space.8'), CheckboxUI, themeGet('elevation.3'), themeGet('colors.primary'));

var CheckboxOuter = styled__default.div.withConfig({
  displayName: 'styled__CheckboxOuter',
  componentId: 'lhn7s7-2'
})(['height:', 'px;width:', 'px;position:relative;cursor:pointer;display:flex;justify-items:center;justify-content:center;align-items:center;&:hover{> div{border-color:', ';}}'], themeGet('space.8'), themeGet('space.8'), function (_ref3) {
  var theme = _ref3.theme;
  return theme.color.primary;
});

var Checkbox = function Checkbox(_ref) {
  var value = _ref.value,
      onChange = _ref.onChange,
      rest = objectWithoutProperties(_ref, ['value', 'onChange']);
  return React.createElement(
    CheckboxOuter,
    null,
    React.createElement(CheckboxSC, _extends$1({ type: 'checkbox', checked: value, onChange: function (_onChange) {
        function onChange(_x) {
          return _onChange.apply(this, arguments);
        }

        onChange.toString = function () {
          return _onChange.toString();
        };

        return onChange;
      }(function (e) {
        return onChange(e.target.checked);
      }) }, rest)),
    React.createElement(
      CheckboxUI,
      { checked: value },
      value && React.createElement(Icon, { color: 'white', icon: 'checkmark' })
    )
  );
};
Checkbox.defaultProps = {
  value: false
};

var Checkbox$1 = withFormComponents(Checkbox);
Checkbox.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Checkbox',
  'props': {
    'value': {
      'defaultValue': {
        'value': 'false',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'boolean'
      },
      'description': ''
    },
    'id': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    },
    'onChange': {
      'required': true,
      'flowType': {
        'name': 'Function'
      },
      'description': ''
    }
  }
};

var ModalWrapper = styled__default.div.withConfig({
  displayName: 'Modal__ModalWrapper',
  componentId: 'sc-53dr3k-0'
})(['']);

var Modal = function (_React$Component) {
  inherits(Modal, _React$Component);

  function Modal() {
    var _ref;

    var _temp, _this, _ret;

    classCallCheck(this, Modal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = possibleConstructorReturn(this, (_ref = Modal.__proto__ || Object.getPrototypeOf(Modal)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      visible: false
    }, _this.setVisible = function (value) {
      _this.setState({
        visible: value
      });
    }, _this.onModalHide = function (data) {
      var onHide = _this.props.onHide;

      onHide(data);
    }, _temp), possibleConstructorReturn(_this, _ret);
  }

  createClass(Modal, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var _this2 = this;

      setTimeout(function () {
        _this2.setVisible(true);
      }, 100);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var _props = this.props,
          hideOnBackdrop = _props.hideOnBackdrop,
          children = _props.children,
          _props$title = _props.title,
          title = _props$title === undefined ? 'Modal' : _props$title,
          rest = objectWithoutProperties(_props, ['hideOnBackdrop', 'children', 'title']);
      var visible = this.state.visible;


      if (!isFunction_1(children)) {
        logFromMethod({
          className: 'Modal',
          method: 'render()',
          type: 'warning',
          message: 'You cannot render a modal without a function as a child'
        });
        return null;
      }

      return React__default.createElement(
        Backdrop,
        { onClick: hideOnBackdrop ? function () {
            return _this3.onModalHide();
          } : null, opacity: visible ? 1 : 0 },
        React__default.createElement(
          ModalWrapper,
          { onClick: function onClick(e) {
              return e.stopPropagation();
            } },
          React__default.createElement(
            Card,
            _extends$1({}, rest, { m: 5, pb: 7, px: 6 }),
            React__default.createElement(
              Flex,
              null,
              React__default.createElement(
                H2$1,
                { height: 8 },
                title
              ),
              React__default.createElement(Button, { ml: 'auto', color: 'grey', onClick: function onClick() {
                  return _this3.onModalHide();
                }, buttonType: 'flat', icon: 'close' })
            ),
            React__default.createElement(Spacer, { height: 7 }),
            children({
              onHide: this.onModalHide
            })
          )
        )
      );
    }
  }]);
  return Modal;
}(React__default.Component);
Modal.defaultProps = {
  width: 500,
  hideOnBackdrop: true,
  onHide: function onHide() {}
};
Modal.__docgenInfo = {
  'description': '',
  'methods': [{
    'name': 'setVisible',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'value',
      'type': {
        'name': 'boolean'
      }
    }],
    'returns': null
  }, {
    'name': 'onModalHide',
    'docblock': null,
    'modifiers': [],
    'params': [{
      'name': 'data',
      'type': {
        'name': 'any'
      }
    }],
    'returns': null
  }],
  'displayName': 'Modal',
  'props': {
    'width': {
      'defaultValue': {
        'value': '500',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': ''
    },
    'hideOnBackdrop': {
      'defaultValue': {
        'value': 'true',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'boolean'
      },
      'description': ''
    },
    'onHide': {
      'defaultValue': {
        'value': '() => {}',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'Function'
      },
      'description': ''
    },
    'children': {
      'required': true,
      'flowType': {
        'name': 'Function'
      },
      'description': ''
    },
    'title': {
      'required': true,
      'flowType': {
        'name': 'string'
      },
      'description': ''
    }
  }
};

var colors = ['#E25554', '#FFD23F', '#6672E5', '#67D4F8', '#314756'];
var getSize = function getSize(_ref) {
  var size$$1 = _ref.size,
      theme = _ref.theme;
  return themeGet('space.' + size$$1)({ theme: theme });
};
var getInitials = function getInitials(name) {
  if (!name) return '';

  var names = name.split(' ');
  var initials = names[0].substring(0, 1).toUpperCase();

  if (names.length > 1) {
    initials += names[names.length - 1].substring(0, 1).toUpperCase();
  }
  return initials;
};

var AvatarWrapper = styled__default.div.withConfig({
  displayName: 'Avatar__AvatarWrapper',
  componentId: 'cseayw-0'
})(['height:', 'px;width:', 'px;border-radius:', 'px;box-sizing:content-box;position:relative;', ';'], getSize, getSize, getSize, function (_ref2) {
  var isStacked = _ref2.isStacked;
  return isStacked && styled.css(['box-shadow:', ';border:2px solid white;'], themeGet('elevation.2'));
});

var HighlightDot = styled__default.div.withConfig({
  displayName: 'Avatar__HighlightDot',
  componentId: 'cseayw-1'
})(['width:8px;height:8px;border-radius:8px;position:absolute;top:-2px;right:-1px;background-color:', ';box-shadow:', ';border:2px solid white;z-index:2;'], themeGet('colors.primary'), themeGet('elevation.2'));

var InitialsWrapper = styled__default.div.withConfig({
  displayName: 'Avatar__InitialsWrapper',
  componentId: 'cseayw-2'
})(['display:flex;align-items:center;justify-content:center;position:absolute;width:100%;height:100%;top:0;left:0;right:0;bottom:0;border-radius:', 'px;', ';'], getSize, background);

var Initials = styled__default.p.withConfig({
  displayName: 'Avatar__Initials',
  componentId: 'cseayw-3'
})(['color:white;text-transform:uppercase;font-weight:700;font-size:9px;line-height:10px;height:10px;letter-spacing:0.5px;']);

var InitialsDisplay = function (_React$Component) {
  inherits(InitialsDisplay, _React$Component);

  function InitialsDisplay() {
    var _ref3;

    var _temp, _this, _ret;

    classCallCheck(this, InitialsDisplay);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = possibleConstructorReturn(this, (_ref3 = InitialsDisplay.__proto__ || Object.getPrototypeOf(InitialsDisplay)).call.apply(_ref3, [this].concat(args))), _this), _this.state = {
      initials: '',
      bg: ''
    }, _this.buildState = function () {
      var name = _this.props.name;

      _this.setState({
        initials: getInitials(name),
        bg: colors[Math.floor(Math.random() * (colors.length - 1))]
      });
    }, _temp), possibleConstructorReturn(_this, _ret);
  }

  createClass(InitialsDisplay, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.buildState();
    }
  }, {
    key: 'render',
    value: function render() {
      var _state = this.state,
          initials = _state.initials,
          bg = _state.bg;
      var size$$1 = this.props.size;


      return React__default.createElement(
        InitialsWrapper,
        { key: initials, background: '#BDC5C9', size: size$$1 },
        React__default.createElement(
          Initials,
          null,
          initials
        )
      );
    }
  }]);
  return InitialsDisplay;
}(React__default.Component);

var Avatar = function Avatar(_ref4) {
  var profile = _ref4.profile,
      size$$1 = _ref4.size;
  return React__default.createElement(
    AvatarWrapper,
    { size: size$$1 },
    profile && profile.avatar ? React__default.createElement(Image, { width: size$$1, height: size$$1, borderRadius: size$$1, src: profile.avatar }) : profile && profile.name ? React__default.createElement(InitialsDisplay, { name: profile.name, size: size$$1 }) : null,
    React__default.createElement(HighlightDot, null)
  );
};

Avatar.defaultProps = {
  size: 6
};
Avatar.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Avatar',
  'props': {
    'size': {
      'defaultValue': {
        'value': '6',
        'computed': false
      },
      'required': false,
      'flowType': {
        'name': 'number'
      },
      'description': ''
    },
    'profile': {
      'required': true,
      'flowType': {
        'name': 'signature',
        'type': 'object',
        'raw': '{\n  avatar?: string,\n  name: string,\n}',
        'signature': {
          'properties': [{
            'key': 'avatar',
            'value': {
              'name': 'string',
              'required': false
            }
          }, {
            'key': 'name',
            'value': {
              'name': 'string',
              'required': true
            }
          }]
        }
      },
      'description': ''
    }
  }
};

var ProfileGroup = function ProfileGroup(_ref) {
  var profiles = _ref.profiles;
  return React__default.createElement(
    React__default.Fragment,
    null,
    map_1(profiles, function (profile) {
      return React__default.createElement(Avatar, { profile: profile });
    })
  );
};
ProfileGroup.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'ProfileGroup',
  'props': {
    'profiles': {
      'required': true,
      'flowType': {
        'name': 'Array',
        'elements': [{
          'name': 'signature',
          'type': 'object',
          'raw': '{\n  avatar: string,\n  name: string,\n}',
          'signature': {
            'properties': [{
              'key': 'avatar',
              'value': {
                'name': 'string',
                'required': true
              }
            }, {
              'key': 'name',
              'value': {
                'name': 'string',
                'required': true
              }
            }]
          }
        }],
        'raw': 'Array<Profile>'
      },
      'description': ''
    }
  }
};

var Profile = function Profile(_ref) {
  var profile = _ref.profile;

  if (!profile) return null;

  return React.createElement(
    Flex,
    { flexDirection: 'row' },
    React.createElement(
      Col,
      { flex: '0 0 24px', mr: 2 },
      React.createElement(Avatar, { profile: profile })
    ),
    React.createElement(
      Col,
      null,
      React.createElement(
        Text,
        { whiteSpace: 'nowrap' },
        profile.name
      )
    )
  );
};

Profile.defaultProps = {};
Profile.__docgenInfo = {
  'description': '',
  'methods': [],
  'displayName': 'Profile',
  'props': {
    'profile': {
      'required': true,
      'flowType': {
        'name': 'signature',
        'type': 'object',
        'raw': '{\n  avatar?: string | null,\n  name: string,\n}',
        'signature': {
          'properties': [{
            'key': 'avatar',
            'value': {
              'name': 'union',
              'raw': 'string | null',
              'elements': [{
                'name': 'string'
              }, {
                'name': 'unknown'
              }],
              'required': false
            }
          }, {
            'key': 'name',
            'value': {
              'name': 'string',
              'required': true
            }
          }]
        }
      },
      'description': ''
    }
  }
};

var utils = _extends$1({}, commonUtils);

exports.utils = utils;
exports.Row = Row;
exports.Col = Col;
exports.Box = Box;
exports.Flex = Flex;
exports.AppContainer = AppContainer;
exports.Accordion = Accordion;
exports.Backdrop = Backdrop;
exports.Button = Button;
exports.Label = _Label;
exports.H1 = H1$1;
exports.H2 = H2$1;
exports.H3 = H3$1;
exports.Icon = Icon;
exports.Logo = Logo;
exports.Tag = Tag;
exports.Link = Link;
exports.Text = Text;
exports.DateTime = DateTime;
exports.Spacer = Spacer;
exports.P = P;
exports.HoverTransition = HoverTransition;
exports.Separator = Separator;
exports.Spinner = Spinner;
exports.Image = Image;
exports.Dropdown = Dropdown;
exports.DatePicker = DatePicker$1;
exports.Form = Form;
exports.FormValue = FormValue;
exports.FormInput = Input$1;
exports.Input = Input;
exports.SubmitButton = SubmitButton;
exports.FormSelect = Select$1;
exports.SelectItem = SelectItem;
exports.Select = Select;
exports.Tabs = Tabs;
exports.Tab = Tab;
exports.FormCheckbox = Checkbox$1;
exports.Checkbox = Checkbox;
exports.Card = Card;
exports.Clickable = Clickable;
exports.Modal = Modal;
exports.ProfileGroup = ProfileGroup;
exports.Avatar = Avatar;
exports.Profile = Profile;
//# sourceMappingURL=index.js.map
