/**
 * TILE CLI
 */

const { addNewComponent } = require('./new-component');
const inquirer = require('inquirer');

const { spawn } = require('child_process');

const entrancePoints = [
  {
    type: 'list',
    name: 'action',
    message: 'What do you want to do?',
    choices: [
      {
        name: 'Add new component',
        value: 'a',
      },
    ],
  },
];

console.log('Welcome to TILE CLI');

const init = () => {
  inquirer.prompt(entrancePoints).then(answer => {
    const { action } = answer;

    switch (action) {
      case 'a':
        addNewComponent();
        break;
      default:
        console.log('invalid command');
        break;
    }
  });
};

init();
