// @flow
// Global types

// Style properties
export type FlexDirection = 'column' | 'row' | 'row-reverse' | 'column-reverse';
export type JustifyContent = 'flex-start' | 'flex-end' | 'center' | 'space-between' | 'space-around';
export type Display = 'flex' | 'block' | 'table' | 'inline' | 'inline-block';
export type TextAlign = 'left' | 'center' | 'right';

// Theme properties
export type ThemeColors = 'primary' | 'secondary' | 'info' | 'warning' | 'danger' | 'success' | 'grey' | 'text';
