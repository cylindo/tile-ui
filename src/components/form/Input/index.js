// @flow
export { default as FormInput } from './Input';
export { Input } from './Input';
export { InputWrapper } from './styled';
