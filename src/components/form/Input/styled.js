import styled, { css } from 'styled-components';
import { themeGet } from 'styled-system';
import { uiSize, inputError, inputFocus } from '../../../styled/styled-utils';

const getTextareaHeight = ({ lines, theme }) => {
  const index = lines * 5;
  return `${theme.height[index]}px`;
};

export const InputWrapper = styled.input`
  ${uiSize};
  box-shadow: ${props => props.theme.elevation[1]};
  border: none;
  border-radius: ${themeGet('borderRadius.1')}px;
  width: 100%;
  outline: none !important;
  transition: all 200ms ease;
  padding-right: ${props => props.theme.padding[3]}px;
  padding-left: ${props => props.theme.padding[3]}px;
  padding-top: ${props => props.theme.padding[1]}px;
  padding-bottom: ${props => props.theme.padding[1]}px;
  font-size: 12px;

  &:focus {
    ${inputFocus};
  }

  ${inputError};
`;

export const TextareaWrapper = styled.textarea`
  min-height: ${getTextareaHeight};
  box-shadow: ${props => props.theme.elevation[1]};
  border: none;
  border-radius: ${themeGet('borderRadius.1')}px;
  width: 100%;
  outline: none !important;
  transition: all 200ms ease;
  padding-right: ${props => props.theme.padding[3]}px;
  padding-left: ${props => props.theme.padding[3]}px;
  padding-top: ${props => props.theme.padding[2]}px;
  padding-bottom: ${props => props.theme.padding[1]}px;
  resize: none;
  font-size: 12px;

  &:focus {
    ${inputFocus};
  }

  ${inputError};
`;
