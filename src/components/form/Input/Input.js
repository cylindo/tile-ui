// @flow
import * as React from 'react';
import { InputWrapper, TextareaWrapper } from './styled';
import { withFormComponents } from '../Form';

type Props = {
  value: string,
  onChange: Function,
  lines?: number,
  placeholder: string,
  uiSize?: string,
  autoComplete: 'off' | any,
};

export const Input = ({ lines = 1, onChange, autoComplete, ...rest }: Props) => (
  <React.Fragment>
    {lines > 1 ? (
      <TextareaWrapper
        autoComplete="off"
        lines={lines}
        type="text"
        {...rest}
        onChange={e => onChange(e.target.value)}
      />
    ) : (
      <InputWrapper autoComplete={autoComplete} type="text" {...rest} onChange={e => onChange(e.target.value)} />
    )}
  </React.Fragment>
);

Input.displayName = 'Input';

Input.defaultProps = {
  lines: 1,
  uiSize: 'small',
  autoComplete: 'off',
};

export default withFormComponents(Input);
