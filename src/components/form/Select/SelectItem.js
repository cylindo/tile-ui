// @flow
import * as React from 'react';
import { SelectItemWrapper } from './styled';
import { P } from '../../elements/P';

type Props = {
  label: string,
};

export const SelectItem = ({ label }: Props) => <P mb={0}>{label}</P>;
