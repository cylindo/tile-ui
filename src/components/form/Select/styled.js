import styled, { css } from 'styled-components';
import { lighten, darken, transparentize } from 'polished';
import { themeGet, space } from 'styled-system';
import { uiSize, inputFocus, inputError } from '../../../styled/styled-utils';

export const SelectInput = styled.div`
  cursor: pointer;
  box-shadow: ${props => props.theme.elevation[1]};
  border: none;
  border-radius: ${themeGet('borderRadius.1')}px;
  width: 100%;
  outline: none;
  transition: all 200ms ease;
  padding-right: ${props => props.theme.padding[3]}px;
  padding-left: ${props => props.theme.padding[3]}px;
  background: white;
  display: flex;
  align-items: center;

  ${uiSize};
  ${inputError};

  ${({ focused }) =>
    focused &&
    css`
      ${inputFocus};
    `} &:focus {
    ${inputFocus};
  }
`;

export const SelectWrapper = styled.div`
  position: relative;
`;

export const SelectContainer = styled.div`
  width: 100%;
  opacity: 0;
  position: absolute;
  transition: all 200ms ease;
  background: white;
  box-shadow: ${props => props.theme.elevation[5]};
  border-radius: ${themeGet('borderRadius.2')}px;
  overflow: scroll;
  pointer-events: none;
  top: 40px;
  height: 0px;

  ${({ open }) =>
    open &&
    css`
      height: auto;
      pointer-events: all;
      opacity: 1;
      z-index: 9;
      max-height: 256px;
    `};
`;

export const SelectItemWrapper = styled.div`
  font-weight: 400;
  font-size: 17px;
  line-height: 26px;

  ${space};

  &:hover {
    background: ${props => lighten(0.425, props.theme.color.primary)};
    cursor: pointer;
  }
`;

export const HiddenInput = styled.input`
  pointer-events: none;
  opacity: 0;
  position: absolute;
`;

export const SelectItemList = styled.div`
  display: flex;
  flex-direction: column;
`;

export const DropdownIcon = styled.div`
  width: 20px;
  height: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: auto;
`;
