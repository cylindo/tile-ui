// @flow
import * as React from 'react';
import { map } from 'lodash';
import { P } from '../../elements/P';
import { Col } from '../../grid/Col';
import { Icon } from '../../elements/Icon';
import { Spinner } from '../../elements/Spinner';
import { Clickable } from '../../elements/Clickable';

import {
  SelectItemWrapper,
  SelectWrapper,
  SelectInput,
  SelectContainer,
  SelectItemList,
  DropdownIcon,
  HiddenInput,
} from './styled';
import { SelectItem } from './SelectItem';

import { withFormComponents } from '../Form';

type Option = {
  value: any,
  label: string,
  id: string,
};

type Props = {
  value: any,
  options?: Array<Option>,
  onChange: Function,
  uiSize?: 'small',
  placeholder: string,
  loading?: boolean,
  renderItem: Function,
  hasError: boolean,
};

type State = {
  open: boolean,
  focused: boolean,
  hovering: boolean,
};

export class Select extends React.Component<Props, State> {
  state = {
    open: false,
    focused: false,
    hovering: false,
  };

  static defaultProps = {
    options: [],
    loading: false,
    uiSize: 'small',
    placeholder: 'Select',
    renderItem: obj => <SelectItem label={obj.label} />,
  };

  hiddenInputRef: any = React.createRef();

  toggleOpen = () => {
    const { open } = this.state;
    if (!open) this.focusInput();
    this.setState({
      open: !open,
    });
  };

  setOpen = (value: boolean) => {
    this.setState({
      open: value,
    });
  };

  setFocus = (value: boolean) => {
    if (!value) this.setOpen(false);
    this.setState({
      focused: value,
    });
  };

  focusInput = () => {
    this.hiddenInputRef.current.focus();
  };

  blurInput = () => {
    this.hiddenInputRef.current.blur();
  };

  onItemClick = (o: Option) => {
    const { onChange } = this.props;
    if (onChange) onChange(o);
    this.setOpen(false);
  };

  onKeyDown = (e: any) => {
    switch (e.key) {
      case 'Tab':
        this.setFocus(false);
        break;
      case 'Enter':
      case 'Space':
      case 'ArrowDown':
      case 'ArrowUp':
        this.toggleOpen();
        break;
      default:
        break;
    }
  };

  onBlur = () => {
    const { hovering } = this.state;
    if (!hovering) {
      this.setFocus(false);
      this.blurInput();
    } else {
      this.focusInput();
    }
  };

  onFocus = () => {
    this.setFocus(true);
  };

  setHovering = (value: boolean) => {
    this.setState({
      hovering: value,
    });
  };

  render() {
    const { value, options, uiSize, placeholder, loading, renderItem, hasError } = this.props;
    const { open, focused } = this.state;

    return (
      <SelectWrapper onMouseEnter={() => this.setHovering(true)} onMouseLeave={() => this.setHovering(false)}>
        <HiddenInput
          onKeyDown={this.onKeyDown}
          innerRef={this.hiddenInputRef}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
        />
        <SelectInput hasError={hasError} uiSize={uiSize} focused={focused} onClick={this.toggleOpen}>
          <Col maxHeight="32px" mr={5}>
            {value ? renderItem(value) : placeholder || ''}
          </Col>

          <DropdownIcon>{loading ? <Spinner size={5} /> : <Icon size={16} icon="caretDown" />}</DropdownIcon>
        </SelectInput>
        <SelectContainer open={open}>
          <SelectItemList>
            {loading
              ? null
              : map(options, (o, i) => (
                  <Clickable tabIndex={open ? 0 : -1} onClick={() => this.onItemClick(o)} key={i}>
                    <SelectItemWrapper px={3} py={2}>
                      {renderItem(o)}
                    </SelectItemWrapper>
                  </Clickable>
                ))}
          </SelectItemList>
        </SelectContainer>
      </SelectWrapper>
    );
  }
}

Select.displayName = 'Select';

export default withFormComponents(Select);
