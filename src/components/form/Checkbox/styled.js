import styled, { css } from 'styled-components';
import { themeGet } from 'styled-system';

export const CheckboxUI = styled.div`
  position: absolute;
  height: ${themeGet('space.6')}px;
  width: ${themeGet('space.6')}px;
  border-radius: ${themeGet('space.6')}px;
  border: 1px solid ${({ theme }) => theme.color.border};
  pointer-events: none;
  cursor: pointer;
  background: white;
  transition: all 200ms ease;
  display: flex;
  align-content: center;
  justify-content: center;

  ${({ checked, theme }) =>
    checked &&
    css`
      box-shadow: ${themeGet('elevation.3')};
      background: ${themeGet('colors.primary')};
      border: 1px solid ${themeGet('colors.primary')};
    `};
`;

export const CheckboxSC = styled.input`
  height: ${themeGet('space.8')}px;
  width: ${themeGet('space.8')}px;
  opacity: 0;
  cursor: pointer;

  &:focus {
    + ${CheckboxUI} {
      box-shadow: ${themeGet('elevation.3')};
      border: 1px solid ${themeGet('colors.primary')};
    }
  }
`;

export const CheckboxOuter = styled.div`
  height: ${themeGet('space.8')}px;
  width: ${themeGet('space.8')}px;
  position: relative;
  cursor: pointer;
  display: flex;
  justify-items: center;
  justify-content: center;
  align-items: center;

  &:hover {
    > div {
      border-color: ${({ theme }) => theme.color.primary};
    }
  }
`;
