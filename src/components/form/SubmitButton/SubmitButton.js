// @flow
import * as React from 'react';
import { Button } from '../../elements/Button';

type Props = {
  onClick: Function,
  children: any,
};

export const SubmitButton = ({ children, ...rest }: Props) => (
  <Button type="submit" {...rest}>
    {children}
  </Button>
);

SubmitButton.defaultProps = {};
