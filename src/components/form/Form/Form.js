// @flow
import * as React from 'react';
import { get, isEqual } from 'lodash';
import { FormSC, ErrorWrapper, FormElementWrapper } from './styled';
import { Row } from '../../grid/Row';

import { SubmitButton } from '../SubmitButton';
import { Label } from '../../elements/Label';
import { Text } from '../../elements/Text';

type Props = {
  onSubmit: Function,
  children: any,
  noSubmit: boolean,
  isSubmitting: boolean,
  submitLabel?: string,
  loading: boolean,
  error: string,
};
export class Form extends React.Component<Props> {
  static defaultProps = {
    submitLabel: 'Submit',
    onSubmit: () => {},
  };

  onSubmit = (e: any) => {
    const { onSubmit } = this.props;

    if (e) e.preventDefault();
    onSubmit();
  };

  render() {
    const { error, loading, isSubmitting, children, submitLabel, noSubmit, ...rest } = this.props;

    return (
      <FormSC onKeyDown={e => e.key === 'Enter' && e.preventDefault()} onSubmit={this.onSubmit}>
        <input type="hidden" value="something" />

        {error && (
          <Row mb={7}>
            <Text fontWeight={4} fontSize={2} color="danger">
              {error}
            </Text>
          </Row>
        )}

        {children}

        {!noSubmit && (
          <Row justifyContent="flex-end">
            <SubmitButton loading={loading} onClick={this.onSubmit} disabled={isSubmitting}>
              {submitLabel}
            </SubmitButton>
          </Row>
        )}
      </FormSC>
    );
  }
}

/**
 * Named exports
 */

export const FormLabel = ({
  id,
  label,
  required,
  hint,
}: {
  id: string,
  label: string,
  required: boolean,
  hint: string,
}) => (
  <Label htmlFor={id} mb={2}>
    {label}

    {required && hint ? (
      <Text fontSize={2} color="grey" ml={1}>
        ({hint})
      </Text>
    ) : !required ? (
      <Text fontSize={2} color="grey" ml={1}>
        (optional)
      </Text>
    ) : null}
  </Label>
);

export const ErrorDisplay = ({ error }: { error: string }) => (
  <ErrorWrapper>
    <Text fontSize={2} color="danger">
      {error}
    </Text>
  </ErrorWrapper>
);

type FormComponentProps = {
  id: string,
  required: boolean,
  error: string,
  label: string,
  mb: number,
  onChange: Function,
  fetchError: string,
  value: any,
  loading?: boolean,
  hint: string,
};

type FormikTypes = {
  values: Object,
  errors: Object,
  touched: Object,
  setFieldValue: Function,
  handleBlur: Function,
};

type WithFormComponentsProps = FormComponentProps & FormikTypes;

export const withFormComponents = (ComponentToWrap: React.ComponentType<any>) =>
  class extends React.Component<WithFormComponentsProps> {
    static defaultProps = {
      required: true,
      mb: 7,
      options: null,
      loading: false,
    };

    /**
     * Compares values and errors for the component to determine if an update is necessary
     */
    shouldComponentUpdate(nextProps: WithFormComponentsProps): boolean {
      const { id, values, value, errors, error, fetchError, loading, touched } = this.props;

      // Current value and next values
      const _value = value || get(values, `${id}`);
      const nvalue = nextProps.value || get(nextProps.values, `${id}`);

      // Current value and next values
      const _error = error || get(errors, `${id}`);
      const nerror = nextProps.error || get(nextProps.errors, `${id}`);

      // Current value and next values
      const _touched = get(touched, `${id}`);
      const ntouched = get(nextProps.touched, `${id}`);

      if (
        isEqual(_value, nvalue) &&
        isEqual(_error, nerror) &&
        isEqual(_touched, ntouched) &&
        fetchError === nextProps.fetchError &&
        loading === nextProps.loading
      )
        return false;

      // Update
      return true;
    }

    onChange = (value: Date | string | number | boolean | null | typeof undefined | {} | []) => {
      const { id, setFieldValue, onChange } = this.props;

      if (setFieldValue) setFieldValue(id, value);
      if (onChange) onChange(value);
    };

    render() {
      const { id, label, required, hint } = this.props;
      const {
        mb,
        errors,
        values,
        touched,
        setFieldValue,
        fetchError,
        error,
        loading,
        handleBlur,
        value,
        ...rest
      } = this.props;

      const _error = error || get(errors, `${id}`);
      const _value = value || get(values, `${id}`);
      const isTouched = get(touched, `${id}`);
      const inValid = !!(isTouched && _error);
      const hasError = inValid || fetchError;

      const labelProps = {
        id,
        label,
        required,
        hint,
      };

      const componentProps = {
        ...rest,
        value: _value,
        onChange: this.onChange,
        hasError,
        loading,
        onBlur: handleBlur,
      };

      return (
        <FormElementWrapper mb={mb}>
          <FormLabel {...labelProps} />
          <ComponentToWrap {...componentProps} />
          {fetchError ? <ErrorDisplay error={fetchError} /> : inValid ? <ErrorDisplay error={_error} /> : null}
        </FormElementWrapper>
      );
    }
  };
