import styled, { css } from 'styled-components';
import { space, themeGet } from 'styled-system';

export const FormSC = styled.form`
  width: 100%;
`;

export const FormElementWrapper = styled.div`
  width: 100%;
  position: relative;
  ${space};
`;

export const ErrorWrapper = styled.div`
  position: absolute;
  bottom: -${themeGet('space.5')}px;
`;
