import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { Form } from './Form';
import TILE from '../../../index';

it('Should render a default Form component', () => {
  const { theme } = TILE.uiStore;
  const component = <Form theme={theme} />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
