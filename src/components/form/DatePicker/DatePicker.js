import React from 'react';
import DayPickerInput from './src/DayPickerInput';
import { withFormComponents } from '../Form';
import { Card } from '../../fragments/Card';
import { Button } from '../../elements/Button';
import { Input } from '../Input';

import {
  InputComponent,
  DayCellDisplay,
  DayDisplay,
  WeekDisplay,
  WeeksDisplay,
  MonthDisplay,
  MonthsDisplay,
  CaptionDisplay,
  WeekdayDisplay,
  WeekdaysDisplay,
  WeekdaysRowDisplay,
  NavBarDisplay,
  OverlayComponentWrapper,
} from './styled';

type DatePickerProps = {
  onChange: Function,
  value: string,
  placeholder: string,
  id: string,
};

const NavBarNextDisplay = props => <Button ml="auto" buttonType="flat" icon="arrowRight" {...props} />;

const NavBarPrevDisplay = props => <Button buttonType="flat" icon="arrowLeft" {...props} />;

const OverlayComponent = ({ children, selectedDay, tabIndex, month, input, classNames, ...props }) => (
  <OverlayComponentWrapper {...props}>
    <Card>{children}</Card>
  </OverlayComponentWrapper>
);

export const DatePicker = ({ id, onChange, value, placeholder, selectedDays, ...rest }: DatePickerProps) => (
  <DayPickerInput
    id={id}
    value={value}
    showOutsideDays
    fixedWeeks
    placeholder={placeholder}
    selectedDays={selectedDays}
    component={Input}
    onDayChange={onChange}
    inputProps={{ id, placeholder }}
    overlayComponent={OverlayComponent}
    dayCellDisplay={DayCellDisplay}
    dayDisplay={DayDisplay}
    weekDisplay={WeekDisplay}
    weeksDisplay={WeeksDisplay}
    monthDisplay={MonthDisplay}
    monthsDisplay={MonthsDisplay}
    captionDisplay={CaptionDisplay}
    weekdayDisplay={WeekdayDisplay}
    weekdaysDisplay={WeekdaysDisplay}
    weekdaysRowDisplay={WeekdaysRowDisplay}
    navBarDisplay={NavBarDisplay}
    navBarNextDisplay={NavBarNextDisplay}
    navBarPrevDisplay={NavBarPrevDisplay}
  />
);

DatePicker.defaultProps = {
  onChange: () => {},
};

export default withFormComponents(DatePicker);
