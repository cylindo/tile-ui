import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { DatePicker } from './DatePicker';
import TILE from '../../../index';

it('Should render a default DatePicker component', () => {
  const { theme } = TILE.uiStore;
  const component = <DatePicker theme={theme} />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
