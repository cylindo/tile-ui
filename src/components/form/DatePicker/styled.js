import styled, { css } from 'styled-components';
import { themeGet } from 'styled-system';
import { transparentize } from 'polished';
import { InputWrapper } from '../Input';

export const InputComponent = styled(InputWrapper)`
  height: 32px;
`;

export const OverlayComponentWrapper = styled.div`
  position: absolute;
  top: 64px;
  min-width: 320px;
  z-index: 1;
`;

export const DayCellDisplay = styled.div`
  padding: ${themeGet('space.1')}px;
  display: table-cell;
  text-align: center;
  vertical-align: middle;
  cursor: pointer;
  outline: none;
`;

export const DayDisplay = styled.div`
  width: 32px;
  height: 32px;
  border-radius: 32px;
  background: ${themeGet('colors.greys.6')};
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 auto;
  transition: all 200ms ease;
  font-weight: 600;
  color: ${themeGet('colors.greys.2')};

  ${({ isOutside }) =>
    isOutside &&
    css`
      color: ${themeGet('colors.greys.3')};
      background: white;
    `};

  ${({ isSelected, isToday }) =>
    !isSelected &&
    css`
      &:hover {
        background: ${props => transparentize(0.8, themeGet('colors.primary')(props))};
        color: ${themeGet('colors.primary')};
      }
    `};

  ${({ isSelected }) =>
    isSelected &&
    css`
      background: ${themeGet('colors.primary')};
      color: white !important;
    `};

  ${({ isToday, isSelected }) =>
    isToday &&
    !isSelected &&
    css`
      background: white;
      box-shadow: 0px 0px 0px 2px ${props => transparentize(0.5, themeGet('colors.info')(props))};
      color: ${themeGet('colors.info')};
    `};

  ${({ isToday, isSelected }) =>
    isToday &&
    isSelected &&
    css`
      border: 1px solid white;
      box-shadow: 0px 0px 0px 2px ${props => transparentize(0.5, themeGet('colors.info')(props))};
    `};
`;

export const WeekDisplay = styled.div`
  display: table-row;
`;

export const WeeksDisplay = styled.div`
  display: table-row-group;
`;

export const WeekdayDisplay = styled.div`
  display: table-cell;
  padding: ${themeGet('space.3')}px;
  color: ${themeGet('colors.greys.2')};
  text-align: center;
  font-size: ${themeGet('fontSizes.1')}px;
`;

export const WeekdaysDisplay = styled.div`
  display: table-header-group;
  margin-top: 1em;
`;

export const WeekdaysRowDisplay = styled.div`
  display: table-row;
`;

export const MonthDisplay = styled.div`
  width: 100%;
  display: table;
  margin: 0 1em;
  margin-top: 1em;
  border-spacing: 0;
  border-collapse: collapse;
  user-select: none;
  outline: none;
`;

export const MonthsDisplay = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

export const CaptionDisplay = styled.div`
  display: table-caption;
  margin-bottom: ${themeGet('space.6')}px;
  padding: ${themeGet('space.0')}px;
  text-align: center;
  color: ${themeGet('colors.text')};
  font-weight: 600;
  font-size: ${themeGet('fontSizes.3')}px;
`;

export const NavBarDisplay = styled.div`
  display: flex;
  height: 32px;
  position: absolute;
  width: 100%;
  left: 0;
  padding: 8px 32px;
`;
