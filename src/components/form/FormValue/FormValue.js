import * as React from 'react';
import styled from 'styled-components';
import { height, alignItems, justifyContent } from 'styled-system';
import { Label } from '../../elements/Label';
import { Text } from '../../elements/Text';
import { FormElementWrapper } from '../Form';

const FormValueWrapper = styled.div`
  display: flex;

  ${height};
  ${alignItems};
  ${justifyContent};
`;

type PropTypes = {
  children: any,
  label: string,
  mb: number,
  hint: string,
};

export const FormValue = ({ children, label, hint, mb, ...rest }: PropTypes) => (
  <FormElementWrapper mb={mb}>
    <Label mb={2}>
      {label}
      {hint && (
        <Text fontSize={2} color="grey" ml={1}>
          ({hint})
        </Text>
      )}
    </Label>
    <FormValueWrapper {...rest}>{children}</FormValueWrapper>
  </FormElementWrapper>
);

FormValue.defaultProps = {
  mb: 7,
  height: 8,
  alignItems: 'center',
};
