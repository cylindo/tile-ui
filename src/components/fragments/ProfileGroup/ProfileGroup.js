// @flow
import React from 'react';
import { map } from 'lodash';
import { Avatar } from '../Avatar';

type Profile = {
  avatar: string,
  name: string,
};

type PropTypes = {
  profiles: Array<Profile>,
};

export const ProfileGroup = ({ profiles }: PropTypes) => (
  <React.Fragment>
    {map(profiles, profile => (
      <Avatar profile={profile} />
    ))}
  </React.Fragment>
);
