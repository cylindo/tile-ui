// @flow
import React from 'react';
import styled from 'styled-components';
import { isFunction } from 'lodash';
import { Backdrop } from '../../elements/Backdrop';
import { Button } from '../../elements/Button';
import { H2 } from '../../elements/H2';
import { Flex } from '../../grid/Flex';
import { Card } from '../Card';
import { Spacer } from '../../elements/Spacer';
import { logFromMethod } from '../../../utils/console';

type PropTypes = {
  children: Function,
  title: string,
  width?: number,
  onHide: Function,
  hideOnBackdrop?: boolean,
};

type State = {
  visible: boolean,
};

const ModalWrapper = styled.div``;

export class Modal extends React.Component<PropTypes, State> {
  state = {
    visible: false,
  };

  static defaultProps = {
    width: 500,
    hideOnBackdrop: true,
    onHide: () => {},
  };

  componentDidMount() {
    setTimeout(() => {
      this.setVisible(true);
    }, 100);
  }

  setVisible = (value: boolean) => {
    this.setState({
      visible: value,
    });
  };

  onModalHide = (data: any) => {
    const { onHide } = this.props;
    onHide(data);
  };

  render() {
    const { hideOnBackdrop, children, title = 'Modal', ...rest } = this.props;
    const { visible } = this.state;

    if (!isFunction(children)) {
      logFromMethod({
        className: 'Modal',
        method: 'render()',
        type: 'warning',
        message: 'You cannot render a modal without a function as a child',
      });
      return null;
    }

    return (
      <Backdrop onClick={hideOnBackdrop ? () => this.onModalHide() : null} opacity={visible ? 1 : 0}>
        <ModalWrapper onClick={e => e.stopPropagation()}>
          <Card {...rest} m={5} pb={7} px={6}>
            <Flex>
              <H2 height={8}>{title}</H2>
              <Button ml="auto" color="grey" onClick={() => this.onModalHide()} buttonType="flat" icon="close" />
            </Flex>
            <Spacer height={7} />
            {children({
              onHide: this.onModalHide,
            })}
          </Card>
        </ModalWrapper>
      </Backdrop>
    );
  }
}
