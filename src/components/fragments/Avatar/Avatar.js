// @flow
import React from 'react';
import styled, { css } from 'styled-components';
import { themeGet, background } from 'styled-system';
import { sample } from 'lodash';
import { Image } from '../../elements/Image';

type Profile = {
  avatar?: string,
  name: string,
};

type PropTypes = {
  profile: Profile,
  size?: number,
};

const colors = ['#E25554', '#FFD23F', '#6672E5', '#67D4F8', '#314756'];
const getSize = ({ size, theme }) => themeGet(`space.${size}`)({ theme });
const getInitials = (name: string): string => {
  if (!name) return '';

  const names = name.split(' ');
  let initials = names[0].substring(0, 1).toUpperCase();

  if (names.length > 1) {
    initials += names[names.length - 1].substring(0, 1).toUpperCase();
  }
  return initials;
};

const AvatarWrapper = styled.div`
  height: ${getSize}px;
  width: ${getSize}px;
  border-radius: ${getSize}px;
  box-sizing: content-box;
  position: relative;

  ${({ isStacked }) =>
    isStacked &&
    css`
      box-shadow: ${themeGet('elevation.2')};
      border: 2px solid white;
    `};
`;

const HighlightDot = styled.div`
  width: 8px;
  height: 8px;
  border-radius: 8px;
  position: absolute;
  top: -2px;
  right: -1px;
  background-color: ${themeGet('colors.primary')};
  box-shadow: ${themeGet('elevation.2')};
  border: 2px solid white;
  z-index: 2;
`;

const InitialsWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  border-radius: ${getSize}px;
  ${background};
`;

const Initials = styled.p`
  color: white;
  text-transform: uppercase;
  font-weight: 700;
  font-size: 9px;
  line-height: 10px;
  height: 10px;
  letter-spacing: 0.5px;
`;

class InitialsDisplay extends React.Component<{ name: string, size?: number }, { initials: string, bg: string }> {
  state = {
    initials: '',
    bg: '',
  };

  componentDidMount() {
    this.buildState();
  }

  buildState = () => {
    const { name } = this.props;
    this.setState({
      initials: getInitials(name),
      bg: colors[Math.floor(Math.random() * (colors.length - 1))],
    });
  };

  render() {
    const { initials, bg } = this.state;
    const { size } = this.props;

    return (
      <InitialsWrapper key={initials} background="#BDC5C9" size={size}>
        <Initials>{initials}</Initials>
      </InitialsWrapper>
    );
  }
}

export const Avatar = ({ profile, size }: PropTypes) => (
  <AvatarWrapper size={size}>
    {profile && profile.avatar ? (
      <Image width={size} height={size} borderRadius={size} src={profile.avatar} />
    ) : profile && profile.name ? (
      <InitialsDisplay name={profile.name} size={size} />
    ) : null}
    <HighlightDot />
  </AvatarWrapper>
);

Avatar.defaultProps = {
  size: 6,
};
