// @flow
import * as React from 'react';
import { Box } from '../../grid/Box';

type Props = {
  children: any,
  p?: number,
  elevation?: number,
};

export const Card = ({ children, ...rest }: Props) => (
  <Box {...rest} borderRadius={1} bg="white">
    {children}
  </Box>
);

Card.defaultProps = {
  p: 6,
  elevation: 4,
};
