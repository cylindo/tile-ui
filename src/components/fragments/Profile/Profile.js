import * as React from 'react';
import styled from 'styled-components';
import { Avatar } from '../Avatar';
import { Text } from '../../elements/Text';
import { Col } from '../../grid/Col';
import { Flex } from '../../grid/Flex';

type PropTypes = {
  profile: {
    avatar?: string | null,
    name: string,
  },
};

export const Profile = ({ profile }: PropTypes) => {
  if (!profile) return null;

  return (
    <Flex flexDirection="row">
      <Col flex="0 0 24px" mr={2}>
        <Avatar profile={profile} />
      </Col>
      <Col>
        <Text whiteSpace="nowrap">{profile.name}</Text>
      </Col>
    </Flex>
  );
};

Profile.defaultProps = {};
