// @flow
import * as React from 'react';
import { Flex } from '../Flex';
import { flexDirection } from 'styled-system';

type Props = {
  children: any,
  flexDirection?: flexDirection.propTypes,
};

export const Row = ({ children, ...rest }: Props) => <Flex {...rest}>{children}</Flex>;

Row.displayName = 'Row';
Row.defaultProps = {
  flexDirection: 'row',
};
