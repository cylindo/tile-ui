// @flow
import * as React from 'react';
import { Flex } from '../Flex';
import { flexDirection } from 'styled-system';

type Props = {
  children: any,
  flexDirection?: flexDirection.propTypes,
};

export const Col = ({ children, ...rest }: Props) => <Flex {...rest}>{children}</Flex>;

Col.displayName = 'Col';
Col.defaultProps = {
  flexDirection: 'column',
  flex: 1,
};
