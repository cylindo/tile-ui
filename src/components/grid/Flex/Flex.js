// @flow
import * as React from 'react';
import {
  space,
  width,
  fontSize,
  color,
  flex,
  order,
  alignSelf,
  flexWrap,
  flexDirection,
  alignItems,
  justifyContent,
} from 'styled-system';
import styled from 'styled-components';
import { BoxWrapper } from '../Box';

type Props = {};

export const FlexWrapper = styled(BoxWrapper)`
  display: flex;

  ${flexWrap}
  ${flexDirection}
  ${alignItems}
  ${justifyContent}
`;

export const Flex = ({ children, ...rest }: Props) => <FlexWrapper {...rest}>{children}</FlexWrapper>;

Flex.displayName = 'Flex';
Flex.defaultProps = {};
