// @flow
import * as React from 'react';
import {
  space,
  width,
  fontSize,
  color,
  flex,
  order,
  alignSelf,
  flexWrap,
  flexDirection,
  alignItems,
  justifyContent,
  borderRadius,
  height,
} from 'styled-system';
import { elevation, maxWidth, minWidth } from '../../../styled/styled-utils';
import styled from 'styled-components';

type Props = {
  children: any,
};

export const BoxWrapper = styled.div`
  box-sizing: border-box;

  ${height}
  ${width}
  ${space}
  ${fontSize}
  ${color}
  ${flex}
  ${order}
  ${alignSelf}
  ${elevation}
  ${maxWidth}
  ${minWidth}
  ${borderRadius}
`;

export const Box = ({ children, ...rest }: Props) => <BoxWrapper {...rest}>{children}</BoxWrapper>;

Box.displayName = 'Box';
Box.defaultProps = {};
