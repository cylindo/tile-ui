// @flow
import * as React from 'react';
import { TagSC } from './styled';
import type { ThemeColors } from '../../../types';

type Props = {
  children: string,
  color?: ThemeColors | string,
};

export const Tag = ({ children, color, ...rest }: Props) => (
  <TagSC {...rest} bg={color}>
    {children}
  </TagSC>
);

Tag.defaultProps = {
  color: 'info',
};
