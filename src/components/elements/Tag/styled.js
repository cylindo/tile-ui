import styled, { css } from 'styled-components';
import { darken, transparentize } from 'polished';
import { themeGet, space } from 'styled-system';

export const TagSC = styled.span`
  padding: 0 12px;
  height: 20px;
  border-radius: 50px;
  font-weight: 500;
  line-height: 20px;
  white-space: nowrap;
  display: inline-block;
  font-size: 10px;
  letter-spacing: 1px;
  font-weight: 500;
  ${space};

  ${props => {
    const color = themeGet(`colors.${props.bg}`)(props);

    if (!color) return null;

    return css`
      background-color: ${transparentize(0.9, color)};
      color: ${darken(0.05, color)};
    `;
  }};
`;
