import styled from 'styled-components';
import { isNumber } from 'lodash';

const getDimension = (props, property) => {
  if (!props[property]) return null;

  if (isNumber(props[property])) {
    const val = props.theme[property][props[property]] || props[property];
    return `${val}px`;
  }
  return props[property];
};

export const SpacerSC = styled.div`
  width: 100%;
  height: ${props => getDimension(props, 'height')};
  pointer-events: none;
`;
