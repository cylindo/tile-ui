// @flow

import * as React from 'react';
import { Psc } from './styled';
import { type TextAlign, type ThemeColors } from '../../../types';

type Props = {
  children: any,
  fontSize?: string | number,
  textAlign?: TextAlign,
  overflow?: any,
  color?: string,
  mb?: number,
  fontWeight?: number,
};

const P = ({ children, ...rest }: Props) => <Psc {...rest}>{children}</Psc>;

/**
 * defaultProps
 */
P.defaultProps = {
  color: 'text',
  textAlign: 'left',
  fontWeight: 5,
  fontSize: 2,
  overflow: null,
  mb: 5,
};

export default P;
