import styled, { css } from 'styled-components';
import { fontSize, color, space, size, textAlign, fontStyle, themeGet } from 'styled-system';
import { isString } from '../../../utils';

export const Psc = styled.p`
  ${textAlign};
  ${fontSize};
  ${space};
  ${size};
  ${color};
  ${fontStyle};

  line-height: ${({ lineHeight, ...rest }) =>
    isString(lineHeight) ? lineHeight : themeGet(`lineHeights.${lineHeight}`)(rest)}

  text-transform: ${({ textTransform }) => textTransform};

  ${props =>
    props.overflow &&
    css`
      overflow: hidden;
      display: -webkit-box;
      -webkit-line-clamp: 3;
      -webkit-box-orient: vertical;
      text-overflow: ellipsis;
    `};
`;
