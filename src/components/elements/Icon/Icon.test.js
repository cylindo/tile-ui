import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import Icon from './Icon';

it('Should render an Icon tag', () => {
  const component = <Icon icon="user" />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
