// @flow
import * as React from 'react';
import styled from 'styled-components';
import { themeGet, space } from 'styled-system';

import * as icons from './icons';
import { type ThemeColors } from '../../../types';

type Props = {
  icon: string,
  size?: number,
  color?: ThemeColors,
};

export const IconWrapper = styled.i`
  display: inline-flex;
  ${space};
`;

export const SvgWrapper = styled.svg`
  fill: ${({ color, ...rest }) => themeGet(`colors.${color}`)(rest)};
`;

const Icon = ({ icon = 'questionmark', size = 20, color, ...rest }: Props) => {
  const iconMarkup = icons[icon];

  if (!iconMarkup) return null;

  return (
    <IconWrapper {...rest}>
      <SvgWrapper height={size} viewBox="0 0 24 24" width={size} color={color} xmlns="http://www.w3.org/2000/svg">
        {iconMarkup()}
      </SvgWrapper>
    </IconWrapper>
  );
};

Icon.defaultProps = {
  size: 20,
  color: 'grey',
};

export default Icon;
