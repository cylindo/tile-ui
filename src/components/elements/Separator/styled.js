import styled from 'styled-components';
import { space, themeGet } from 'styled-system';

const color = themeGet('colors.separator', 'grey');

export const SeparatorSC = styled.hr`
  height: 1px;
  width: 100%;
  border: none;
  color: ${color};
  background-color: ${color};

  ${space};
`;
