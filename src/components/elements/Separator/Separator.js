// @flow
import * as React from 'react';
import { SeparatorSC } from './styled';

type PropTypes = {
  /**
   * Margin top and bottom
   */
  my?: number,
};

export const Separator = (props: PropTypes) => <SeparatorSC {...props} />;

Separator.defaultProps = {
  my: 5,
};
