// @flow
import * as React from 'react';
import { P } from '../../elements/P';

type Props = {
  label: string,
};

export const ActionItem = ({ label }: Props) => <P mb={0}>{label}</P>;
