// @flow
import * as React from 'react';
import styled, { css } from 'styled-components';
import { lighten } from 'polished';
import { themeGet, space } from 'styled-system';
import { map } from 'lodash';
import { toPx } from '../../../styled/styled-utils';
import { Clickable } from '../Clickable';
import { ActionItem } from './ActionItem';

const DropdownWrapper = styled.div`
  display: inline-flex;
  position: relative;
`;

const DropdownContainer = styled.div`
  opacity: 0;
  position: absolute;
  transition: all 200ms ease;
  background: white;
  box-shadow: ${themeGet('elevation.5')};
  border-radius: ${themeGet('borderRadius.2')}px;
  overflow: scroll;
  pointer-events: none;
  height: 0px;
  width: ${({ width }) => toPx(width)};

  ${({ posX }) =>
    posX === 'left'
      ? css`
          right: 0;
        `
      : css`
          left: 0;
        `};

  ${({ posY, elementHeight }) =>
    posY === 'bottom'
      ? css`
          top: ${elementHeight}px;
        `
      : css`
          bottom: ${elementHeight}px;
        `};

  ${({ isOpen }) =>
    isOpen &&
    css`
      height: auto;
      pointer-events: all;
      opacity: 1;
      z-index: 9;
      max-height: 256px;
    `};
`;

const ActionItemList = styled.div`
  display: flex;
  flex-direction: column;
`;

const ActionItemWrapper = styled.div`
  font-weight: 400;
  font-size: 17px;
  line-height: 26px;

  ${space};

  &:hover {
    background: ${props => lighten(0.425, themeGet('colors.primary')(props))};
    cursor: pointer;
  }
`;

const HiddenInput = styled.input`
  pointer-events: none;
  opacity: 0;
  position: absolute;
`;

type PropTypes = {
  children: Function,
  actions: Array<any>,
  onAction: Function,
  renderItem: Function,
  posX: 'right' | 'left',
  posY: 'top' | 'bottom',
  elementHeight: number,
  width: number,
};

type State = {
  isOpen: boolean,
  isHovering: boolean,
  isFocused: boolean,
};

export class Dropdown extends React.Component<PropTypes, State> {
  state = {
    isOpen: false,
    isHovering: false,
    isFocused: false,
  };

  static defaultProps = {
    actions: [],
    renderItem: (obj: any) => <ActionItem label={obj.label} />,
    width: 200,
    posX: 'left',
    posY: 'bottom',
    elementHeight: 40,
  };

  hiddenInputRef: any = React.createRef();

  toggleOpen = (e: any) => {
    const { isOpen } = this.state;

    if (e) e.stopPropagation();
    if (!isOpen) this.focusInput();

    this.setState({
      isOpen: !isOpen,
    });
  };

  setFocus = (value: boolean) => {
    if (!value) this.setOpen(false);
    this.setState({
      isFocused: value,
    });
  };

  focusInput = () => {
    this.hiddenInputRef.current.focus();
  };

  blurInput = () => {
    this.hiddenInputRef.current.blur();
  };

  setOpen = (value: boolean) => {
    this.setState({
      isOpen: value,
    });
  };

  onBlur = () => {
    const { isHovering } = this.state;
    if (!isHovering) {
      this.setFocus(false);
      this.blurInput();
    } else {
      this.focusInput();
    }
  };

  onFocus = () => {
    this.setFocus(true);
  };

  setHovering = (value: boolean) => {
    this.setState({
      isHovering: value,
    });
  };

  onKeyDown = (e: any) => {
    switch (e.key) {
      case 'Tab':
        this.setFocus(false);
        break;
      case 'Enter':
      case 'Space':
      case 'ArrowDown':
      case 'ArrowUp':
        this.toggleOpen();
        break;
      default:
        break;
    }
  };

  onAction = (e: any, action: any) => {
    const { onAction } = this.props;

    if (e) e.stopPropagation();

    if (onAction) onAction(action);
    this.setOpen(false);
  };

  render() {
    const { width: w, posX, posY, children, actions, elementHeight, renderItem, ...rest } = this.props;
    const { isOpen, isFocused } = this.state;

    return (
      <DropdownWrapper onMouseEnter={() => this.setHovering(true)} onMouseLeave={() => this.setHovering(false)}>
        <HiddenInput
          onKeyDown={this.onKeyDown}
          innerRef={this.hiddenInputRef}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
        />
        <Clickable onClick={this.toggleOpen}>
          {children({
            active: isOpen,
            focused: isFocused,
            ...rest,
          })}
        </Clickable>
        <DropdownContainer posX={posX} posY={posY} elementHeight={elementHeight} width={w} isOpen={isOpen}>
          <ActionItemList>
            {map(actions, (a, i) => (
              <Clickable tabIndex={isOpen ? 0 : -1} onClick={(e: any) => this.onAction(e, a)} key={i}>
                <ActionItemWrapper px={3} py={2}>
                  {renderItem(a)}
                </ActionItemWrapper>
              </Clickable>
            ))}
          </ActionItemList>
        </DropdownContainer>
      </DropdownWrapper>
    );
  }
}
