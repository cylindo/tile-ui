import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { Dropdown } from './Dropdown';
import TILE from '../../../index';

it('Should render a default Dropdown component', () => {
  const { theme } = TILE.uiStore;
  const component = <Dropdown theme={theme} />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
