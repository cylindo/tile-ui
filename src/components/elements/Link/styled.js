import styled, { css } from 'styled-components';
import { display, height, lineHeight, space, fontSize, color, themeGet } from 'styled-system';

export const LinkSC = styled.a`
  cursor: pointer;
  text-decoration: none;
  transition: all 200ms ease;
  font-weight: ${props => (props.active ? themeGet('fontWeight.5')(props) : themeGet('fontWeight.4')(props))};
  opacity: 1;

  ${props => {
    switch (props.transition) {
      case 'opacity':
        return css`
          &:hover {
            opacity: 0.8;
          }
        `;
      case 'scale':
        return css`
          transform: scale(1);
          &:hover {
            transform: scale(1.01);
            opacity: 0.9;
          }
        `;
      default:
        return null;
    }
  }};

  ${lineHeight};
  ${height};
  ${display};
  ${color};
  ${fontSize};
  ${space};
`;
