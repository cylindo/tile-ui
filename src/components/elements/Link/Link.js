// @flow
import * as React from 'react';
import { LinkSC } from './styled';

type Props = {
  children: any,
  transition?: 'opacity' | 'scale',
  onClick?: () => {},
  href?: string,
  display?: 'inline-block' | 'block',
  height?: number,
  newPage?: boolean,
  color?: string,
  fontSize?: number | string,
};

export const Link = ({ children, href, newPage, ...rest }: Props) => (
  <LinkSC target={newPage ? '_blank' : '_self'} href={href} {...rest}>
    {children}
  </LinkSC>
);

Link.defaultProps = {
  height: null,
  transition: 'opacity',
  onClick: null,
  href: '#',
  newPage: false,
  color: 'primary',
  fontSize: 'inherit',
};
