import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { Link } from './Link';
import TILE from '../../../index';

it('Should render a default Link component', () => {
  const { theme } = TILE.uiStore;
  const component = <Link theme={theme} />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
