import React from 'react';
import styled, { keyframes, css } from 'styled-components';
import { themeGet } from 'styled-system';

const animation = keyframes`
  to {
    transform: rotate(1turn)
  }
`;

export const Spinner = styled.div`
  position: absolute;
  width: ${({ size, ...rest }) => themeGet(`space.${size}`)(rest)}px;
  height: ${({ size, ...rest }) => themeGet(`space.${size}`)(rest)}px;
  border: ${({ size, ...rest }) => themeGet(`space.${size}`)(rest) * 0.1}px solid rgba(189, 189, 189, 0.1);
  border-left-color: ${({ color, ...rest }) => themeGet(`colors.${color}`)(rest)};
  border-radius: 50%;
  transition: opacity 200ms ease;
  animation: ${animation} 700ms infinite linear;
  opacity: 0.7;
`;

Spinner.defaultProps = {
  size: 20,
  color: 'primary',
};
