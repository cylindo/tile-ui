// @flow

import React from 'react';
import { TabSC } from './styled';

type Props = {
  id: string,
  label: string,
  children: any,
  isActive: boolean,
  height: number | string,
};

export const Tab = ({ isActive, children, ...rest }: Props) => {
  if (!isActive) return null;

  return <TabSC {...rest}>{children}</TabSC>;
};
