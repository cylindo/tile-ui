import React, { Component } from 'react';
import { TabLabelSC } from '../Tabs/styled';

type Props = {
  onClick: Function,
  isActive: boolean,
  label: string,
  id: string,
};

export class TabLabel extends Component<Props> {
  onLabelClick = () => {
    const { isActive, label, onClick, id } = this.props;
    onClick && onClick({ id });
  };

  render() {
    const { isActive, onClick, label, id } = this.props;
    return (
      <TabLabelSC isActive={isActive} onClick={this.onLabelClick}>
        {label}
      </TabLabelSC>
    );
  }
}
