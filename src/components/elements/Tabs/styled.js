import styled, { css } from 'styled-components';

export const TabSC = styled.div`
  width: 100%;

  ${({ height }) =>
    height &&
    css`
      height: ${height}px;
    `};
`;

export const TabLabelSC = styled.li`
  display: inline-block;
  padding: 16px 12px;
  transition: all 200ms ease;
  cursor: pointer;

  &:hover {
    box-shadow: 0px 3px 0px ${({ theme }) => theme.color.primary};
  }

  ${({ isActive }) =>
    isActive &&
    css`
      font-weight: 600;
      box-shadow: 0px 3px 0px ${({ theme }) => theme.color.primary};
    `};
`;

export const TabList = styled.ol`
  padding: 2px 0px;
  border-bottom: 1px solid #eff1f2;
`;

export const TabContent = styled.div`
  padding: 24px 0px;
`;
