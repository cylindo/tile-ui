import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { Tabs } from './Tabs';
import TILE from '../../../index';

it('Should render a default Tabs component', () => {
  const { theme } = TILE.uiStore;
  const component = <Tabs theme={theme} />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
