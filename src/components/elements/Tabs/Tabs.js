import React, { Component } from 'react';
import { TabLabel } from './TabLabel';
import { Tab } from './Tab';
import { TabList, TabSC, TabContent } from './styled';

type Props = {
  children: React$Element,
  initialTab: string,
};

type State = {
  activeTab: {
    id: string,
  },
};

export class Tabs extends Component<Props, State> {
  state = {
    activeTab: {},
  };

  componentDidMount = () => {
    this.setFirsActiveTab();
  };

  setFirsActiveTab = () => {
    const { children, initialTab } = this.props;

    if (!(children.length > 0)) return null;

    if (initialTab) {
      this.setState({
        activeTab: { id: initialTab },
      });
      return null;
    }

    const activeId = this.props.children[0].props.id;

    this.setState({
      activeTab: { id: activeId },
    });
  };

  onClickTabItem = tab => {
    this.setState({ activeTab: tab });
  };

  render() {
    const { children } = this.props;
    const { activeTab } = this.state;

    return (
      <TabSC>
        <TabList>
          {children && children.length > 1 ? (
            children.map((child, i) => {
              const { id, label } = child.props;
              const isActive = activeTab ? id === activeTab.id : false;

              return (
                <TabLabel
                  isActive={isActive}
                  key={`tablabel_${id}`}
                  id={id}
                  label={label}
                  onClick={this.onClickTabItem}
                />
              );
            })
          ) : (
            <TabLabel isActive id={children.props.id} label={children.props.label} onClick={this.onClickTabItem} />
          )}
        </TabList>
        <TabContent>
          {children && children.length > 1 ? (
            children.map((child, i) => {
              const { id } = child.props;
              const isActive = activeTab ? id === activeTab.id : false;

              return (
                <Tab key={`tab_${id}`} isActive={isActive}>
                  {child.props.children}
                </Tab>
              );
            })
          ) : (
            <Tab isActive>{children.props.children}</Tab>
          )}
        </TabContent>
      </TabSC>
    );
  }
}
