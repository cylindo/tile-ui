// @flow
import * as React from 'react';
import PropTypes from 'prop-types';
import { H3 as H3CS } from './styled';

type Props = {
  children: any,
  multiLine?: boolean,
  textAlign?: 'left' | 'center' | 'right',
  fontWeight?: number | string,
  color?: 'white' | 'default' | string,
  fontSize?: number,
};

export const H3 = ({ children, ...rest }: Props) => <H3CS {...rest}>{children}</H3CS>;

/**
 * defaultProps
 */
H3.defaultProps = {
  color: 'text',
  multiLine: true,
  fontWeight: 5,
  fontSize: 5,
};
