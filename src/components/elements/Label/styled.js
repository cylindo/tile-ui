import styled from 'styled-components';
import { display, space, themeGet } from 'styled-system';

export const Label = styled.label`
  font-weight: 600;
  font-size: 12px;
  color: ${themeGet('colors.text')};
  line-height: 12px;
  height: 12px;
  align-items: center;

  ${display};
  ${space};
`;
