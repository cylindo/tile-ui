import * as React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import Label from './Label';
import TILE from '../../../index';

it('Should render an Label tag', () => {
  const { theme } = TILE.uiStore;
  const defaults = theme.component.Label;

  const component = <Label theme={theme}>Hello!</Label>;
  const tree = renderer.create(component).toJSON();
  // expect(tree).toHaveStyleRule('color', colors.greys[1]);
  // expect(tree).toHaveStyleRule('font-size', defaults.fontSize);
  expect(tree).toMatchSnapshot();
});
