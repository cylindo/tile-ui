import * as React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import H2 from './H2';
import TILE from '../../../index';

it('Should render an H2 tag', () => {
  const { theme } = TILE.uiStore;
  const defaults = theme.component.H2;

  const component = <H2 theme={theme}>Hello!</H2>;
  const tree = renderer.create(component).toJSON();
  // expect(tree).toHaveStyleRule('color', colors.text);
  expect(tree).toHaveStyleRule('font-size', `${defaults.fontSize.default}px`);
  expect(tree).toMatchSnapshot();
});
