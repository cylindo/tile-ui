// @flow
import * as React from 'react';
import { H2 as H2CS } from './styled';

type Props = {
  children: any,
  fontWeight?: number | string,
  fontSize?: number | string,
  color?: string,
};

const H2 = ({ children, ...rest }: Props) => <H2CS {...rest}>{children}</H2CS>;

/**
 * defaultProps
 */
H2.defaultProps = {
  color: 'text',
  fontWeight: 6,
  fontSize: 5,
};

export default H2;
