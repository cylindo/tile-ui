import styled, { css } from 'styled-components';
import {
  space,
  fontSize,
  textColor,
  color,
  fontFamily,
  textAlign,
  lineHeight,
  fontWeight,
  fontStyle,
  letterSpacing,
  bgColor,
  display,
} from 'styled-system';

export const H2 = styled.h2`
  ${fontSize};
  ${textColor};
  ${color};
  ${fontFamily};
  ${textAlign};
  ${lineHeight};
  ${fontWeight};
  ${fontStyle};
  ${letterSpacing};
  ${bgColor};
  ${space};
  ${display};
`;
