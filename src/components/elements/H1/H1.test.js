import * as React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import H1 from './H1';
import TILE from '../../../index';

it('Should render an H1 tag', () => {
  const { theme } = TILE.uiStore;
  const defaults = theme.component.H1;

  const component = <H1 theme={theme}>Hello!</H1>;
  const tree = renderer.create(component).toJSON();
  expect(tree).toHaveStyleRule('color', defaults.color.default);
  expect(tree).toHaveStyleRule('font-size', `${defaults.fontSize.default}px`);
  expect(tree).toMatchSnapshot();
});
