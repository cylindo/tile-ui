// @flow
import * as React from 'react';
import { H1 as H1Wrapper } from './styled';

type Props = {
  children: any,
  fontWeight?: number,
  color?: string,
  multiLine?: boolean,
  fontSize?: number,
};

const H1 = ({ children, ...rest }: Props) => <H1Wrapper {...rest}>{children}</H1Wrapper>;

H1.defaultProps = {
  color: 'text',
  fontWeight: 6,
  multiLine: true,
  fontSize: 7,
};

export default H1;
