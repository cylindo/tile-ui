import styled, { css } from 'styled-components';
import {
  space,
  fontSize,
  textColor,
  color,
  fontFamily,
  textAlign,
  lineHeight,
  fontWeight,
  fontStyle,
  letterSpacing,
  bgColor,
  display,
} from 'styled-system';

export const H1 = styled.h1`
  ${fontSize};
  ${textColor};
  ${color};
  ${fontFamily};
  ${textAlign};
  ${lineHeight};
  ${fontWeight};
  ${fontStyle};
  ${letterSpacing};
  ${bgColor};
  ${space};
  ${display};
`;
