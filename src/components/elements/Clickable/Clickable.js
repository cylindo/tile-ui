// @flow
import * as React from 'react';
import styled from 'styled-components';

type PropTypes = {
  children: any,
  tabIndex?: any,
  type?: 'submit' | 'reset' | 'button',
};

export const ClickableWrapper = styled.div`
  border: none;
  outline: none;
  cursor: pointer;
  user-select: none;
  padding: 0;
`;

export const Clickable = ({ children, type, tabIndex, ...rest }: PropTypes) => (
  <ClickableWrapper {...rest} type={type} role="button" tabIndex={tabIndex}>
    {children}
  </ClickableWrapper>
);

Clickable.defaultProps = {
  tabIndex: 0,
  type: 'button',
};
