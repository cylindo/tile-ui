import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { Clickable } from './Clickable';
import TILE from '../../../index';

it('Should render a default Clickable component', () => {
  const { theme } = TILE.uiStore;
  const component = <Clickable theme={theme} />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
