// @flow
import * as React from 'react';
import { TextSC } from './styled';

type Props = {
  children: string,
  fontSize?: number,
  fontWeight?: number,
  color?: string,
  whiteSpace?: 'normal' | 'nowrap',
  textAlign: string,
};

export const Text = ({ children, ...rest }: Props) => <TextSC {...rest}>{children}</TextSC>;

Text.defaultProps = {
  color: 'inherit',
  fontWeight: 3,
  whiteSpace: 'nowrap',
  fontSize: 'inherit',
  textAlign: 'inherit',
};
