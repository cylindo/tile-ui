import styled, { css } from 'styled-components';
import { whiteSpace } from '../../../styled/styled-utils';
import {
  space,
  fontSize,
  textColor,
  color,
  fontFamily,
  textAlign,
  lineHeight,
  fontWeight,
  fontStyle,
  letterSpacing,
  bgColor,
  display,
} from 'styled-system';

export const TextSC = styled.span`
  ${fontSize};
  ${textColor};
  ${color};
  ${fontFamily};
  ${textAlign};
  ${lineHeight};
  ${fontWeight};
  ${fontStyle};
  ${letterSpacing};
  ${bgColor};
  ${space};
  ${display};
  ${whiteSpace};
`;
