// @flow
import * as React from 'react';
import styled from 'styled-components';
import { themeGet, opacity } from 'styled-system';
import { transparentize } from 'polished';

type PropTypes = {
  children: any,
  opacity?: number,
};

const BackdropWrapper = styled.div`
  display: flex;
  position: fixed;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  background: ${props => transparentize(0.6, themeGet('color.greys.1')(props))};
  overflow: scroll;
  justify-content: center;
  align-items: center;
  ${opacity};

  transition: all 300ms ease-in-out;
`;

export const Backdrop = ({ children, ...rest }: PropTypes) => <BackdropWrapper {...rest}>{children}</BackdropWrapper>;

Backdrop.defaultProps = {
  opacity: 1,
};
