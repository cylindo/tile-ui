import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { Backdrop } from './Backdrop';
import TILE from '../../../index';

it('Should render a default Backdrop component', () => {
  const { theme } = TILE.uiStore;
  const component = <Backdrop theme={theme} />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
