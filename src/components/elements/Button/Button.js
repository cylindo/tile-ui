// @flow
import * as React from 'react';
import { RaisedButton, HollowButton, FlatButton, ContentWrapper, LoadingWrapper } from './styled';
import { buttonTypes } from './constants';
import { Icon } from '../Icon';
import { Text } from '../Text';

import { Spinner } from '../Spinner';
import { type ThemeColors } from '../../../types';

type Props = {
  children?: React.Node,
  buttonType?: 'raised' | 'hollow' | 'flat',
  icon?: string,
  disabled?: boolean,
  onClick: any,
  shape?: 'square' | 'round',
  color?: ThemeColors,
  active?: boolean,
  stretch?: boolean,
  width?: any,
  size?: 'large' | 'small',
  loading: boolean,
};

const Button = ({ children, disabled, loading, buttonType, icon, ...rest }: Props) => {
  const onlyIcon = icon && !children;

  const { onClick } = rest;
  const _disabled = disabled || loading;

  const content = () => (
    <React.Fragment>
      <ContentWrapper loading={loading}>
        {icon && <Icon size={16} icon={icon} />}
        {children && (
          <Text fontWeight={4} fontSize="inherit">
            {children}
          </Text>
        )}
      </ContentWrapper>
      <LoadingWrapper loading={loading}>{loading && <Spinner size={5} color="white" />}</LoadingWrapper>
    </React.Fragment>
  );

  switch (buttonType) {
    case buttonTypes.raised:
      return (
        <RaisedButton
          onKeyDown={e => e.key === 'Enter' && onClick()}
          onlyIcon={onlyIcon}
          {...rest}
          disabled={_disabled}
        >
          {content()}
        </RaisedButton>
      );
    case buttonTypes.hollow:
      return (
        <HollowButton
          onKeyDown={e => e.key === 'Enter' && onClick()}
          onlyIcon={onlyIcon}
          {...rest}
          disabled={_disabled}
        >
          {content()}
        </HollowButton>
      );
    case buttonTypes.flat:
      return (
        <FlatButton onKeyDown={e => e.key === 'Enter' && onClick()} onlyIcon={onlyIcon} {...rest} disabled={_disabled}>
          {content()}
        </FlatButton>
      );
    default:
      console.log('unknown button type');
      return (
        <RaisedButton
          onKeyDown={e => e.key === 'Enter' && onClick()}
          onlyIcon={onlyIcon}
          {...rest}
          disabled={_disabled}
        >
          {content()}
        </RaisedButton>
      );
  }
};

/**
 * defaultProps
 */
Button.defaultProps = {
  color: 'primary',
  buttonType: 'raised',
  shape: 'round',
  stretch: false,
  width: null,
  uiSize: 'small',
  type: 'button',
  mb: 0,
  mr: 0,
};

export default Button;
