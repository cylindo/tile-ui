import styled, { css } from 'styled-components';
import { lighten, darken, transparentize } from 'polished';
import { disabled, transition, focused, focusShadow } from '../../../styled/variants';
import { buttonShapes } from './constants';
import { uiSize } from '../../../styled/styled-utils';
import { themeGet, space, width } from 'styled-system';

export const ContentWrapper = styled.div`
  opacity: 1;
  transition: opacity 200ms ease-in-out;
  display: flex;
  align-items: center;
  justify-content: center;

  ${({ loading }) =>
    loading &&
    css`
      opacity: 0;
    `};
`;

export const LoadingWrapper = styled.div`
  position: absolute;
  opacity: 0;
  transition: opacity 200ms ease-in-out;
  width: 20px;
  height: 20px;

  ${({ loading }) =>
    loading &&
    css`
      opacity: 1;
    `};
`;

const Base = styled.button`
  border: none;
  padding: 0px ${props => (!props.onlyIcon ? props.theme.uiSize[5] : 0)}px;
  text-align: center;
  outline: none;
  cursor: pointer;
  font-size: 12px;
  user-select: none;
  font-weight: ${props => props.theme.fontWeight[4]};
  white-space: nowrap;
  line-height: 0;
  display: flex;
  align-items: center;
  justify-content: center;

  ${({ shape, theme }) =>
    shape === buttonShapes.round
      ? css`
          border-radius: ${theme.borderRadius[10]}px;
        `
      : css`
          border-radius: ${theme.borderRadius[1]}px;
        `};

  ${uiSize};
  ${space};
  ${width};

  ${props => props.disabled && disabled};
  ${transition};

  ${props =>
    props.onlyIcon &&
    css`
      width: ${themeGet(`uiSizes.${props.uiSize}.height`)(props)};
    `};

  ${props =>
    props.stretch &&
    css`
      width: 100%;
    `};

  &:focus {
    ${focused};
  }

  i {
    margin-right: ${props => !props.onlyIcon && props.theme.uiSize[2]}px;
  }
`;

/**
 * Raised
 */

const raisedActive = css`
  background: ${({ theme, color }) => lighten(0.05, themeGet(`colors.${color}`)({ theme }))};
  box-shadow: ${props => props.theme.elevation[2]} ${focusShadow};
`;

export const RaisedButton = styled(Base)`
  background: ${({ color, theme }) => themeGet(`colors.${color}`)({ theme })};
  box-shadow: ${props => props.theme.elevation[3]};
  color: white;

  &:hover,
  &:focus {
    ${raisedActive};
  }

  ${props => props.active && raisedActive};

  &:focus {
    box-shadow: ${focusShadow};
    background: ${({ theme, color }) => darken(0.07, themeGet(`colors.${color}`)({ theme }))};
  }

  i > svg {
    fill: white;
  }
`;

/**
 * Hollow
 */
const hollowActive = css`
  background: ${({ theme, color }) => transparentize(0.85, themeGet(`colors.${color}`)({ theme }))};
  box-shadow: 0px 0px 0px 1px ${({ theme, color }) => transparentize(0.6, themeGet(`colors.${color}`)({ theme }))};
`;

const hollowBackground = ({ theme, color }) => transparentize(0.95, themeGet(`colors.${color}`)({ theme }));

export const HollowButton = styled(Base)`
  color: ${({ theme, color }) => themeGet(`colors.${color}`)({ theme })};
  box-shadow: 0px 0px 0px 1px ${({ theme, color }) => transparentize(0.75, themeGet(`colors.${color}`)({ theme }))};
  background: ${hollowBackground};

  &:hover,
  &:focus {
    ${hollowActive};
  }
  &:focus {
    ${focused};
  }

  ${props => props.active && hollowActive};

  i > svg {
    fill: ${({ theme, color }) => themeGet(`colors.${color}`)({ theme })};
  }
`;

/**
 * Flat
 */
const flatActive = css`
  background: ${({ theme, color }) => transparentize(0.9, themeGet(`colors.${color}`)({ theme }))};
`;

export const FlatButton = styled(Base)`
  color: ${({ theme, color }) => themeGet(`colors.${color}`)({ theme })};
  background: transparent;

  &:hover,
  &:focus {
    ${flatActive};
  }

  ${props => props.active && flatActive};

  &:focus {
    ${focused};
  }

  i > svg {
    fill: ${({ theme, color }) => themeGet(`colors.${color}`)({ theme })};
  }
`;
