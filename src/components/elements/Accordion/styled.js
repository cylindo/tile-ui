import styled, { css } from 'styled-components';

// TODO: fix style for component
export const AccordionSC = styled.div``;

export const Title = styled.div`
  padding: ${props => props.theme.padding[3]}px;
  border-bottom: 1px solid ${props => props.theme.color.border};
  transition: all 200ms 200ms ease-in-out;
  cursor: pointer;
  user-select: none;

  &:hover {
    color: #49c9bc;
  }

  ${props =>
    props.toggled &&
    css`
      background: #eef9f8;
      color: #49c9bc;
      border-color: #7bc7c7;
      box-shadow: 0px -1px 0px #7bc7c7;
    `};
`;

export const AccordionBody = styled.div`
  height: 0px;
  transition: height 200ms ease;
  overflow: hidden;
  margin-top: -1px;

  ${props =>
    props.toggled &&
    css`
      height: ${props.height}px;
    `};
`;

export const Item = styled.div`
  border-bottom: 1px solid ${props => props.theme.color.border};
  width: 100%;

  ${props =>
    props.last &&
    css`
      border-bottom: none;
    `};
`;
