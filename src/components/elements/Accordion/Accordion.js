// @flow
import * as React from 'react';
import { set, forEach, map, get } from 'lodash';
import { AccordionSC, Item, Title, AccordionBody as Body } from './styled';
import { Box } from '../../grid/Box';

type ItemType = {
  height: number,
  id: string,
  title: string,
  render: () => {},
  startToggled: boolean,
};

type Props = {
  items: [ItemType],
  forceSingleToggle: boolean,
  bg: string,
};

type State = {
  toggled: {},
  total: number,
};
class Accordion extends React.Component<Props, State> {
  state = {
    toggled: {},
    total: 0,
  };

  static defaultProps = {
    forceSingleToggle: true,
    bg: '#fff',
  };

  componentDidMount() {
    const { items } = this.props;

    if (items.length) {
      this.buildState(items);
    }
  }

  buildState = (items: any) => {
    const toggled = {};

    forEach(items, (obj: ItemType) => {
      const initalValue = obj.startToggled || false;
      set(toggled, `${obj.id}`, initalValue);
    });

    this.setState({
      toggled,
      total: items.length,
    });
  };

  toggleItem = (id: any) => {
    const { forceSingleToggle } = this.props;

    this.setState(({ toggled }) => {
      const nextToggled = { ...toggled };

      /**
       * If forceSingleToggle is enabled,
       * only allow one item to be toggled open.
       */
      if (forceSingleToggle) {
        forEach(Object.keys(nextToggled), (key, i) => {
          nextToggled[key] = false;
        });
      }

      set(nextToggled, `${id}`, !get(toggled, id));

      return {
        toggled: nextToggled,
      };
    });
  };

  render() {
    const { items, bg } = this.props;
    const { toggled, total } = this.state;

    return (
      <AccordionSC>
        <Box bg={bg} p={0} elevation={2}>
          {map(items, (obj, i) => (
            <Item last={total === i + 1} key={obj.id}>
              <Title onClick={() => this.toggleItem(obj.id)} toggled={toggled[obj.id]}>
                {obj.title}
              </Title>
              <Body height={obj.height} toggled={toggled[obj.id]}>
                {obj.render()}
              </Body>
            </Item>
          ))}
        </Box>
      </AccordionSC>
    );
  }
}

export default Accordion;
