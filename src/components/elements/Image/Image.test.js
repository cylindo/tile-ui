import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { Image } from './Image';
import TILE from '../../../index';

it('Should render a default Image component', () => {
  const { theme } = TILE.uiStore;
  const component = <Image theme={theme} />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
