// @flow
import * as React from 'react';
import styled, { withTheme } from 'styled-components';
import { themeGet, borderRadius, space } from 'styled-system';
import { toPx } from '../../../styled/styled-utils';

type PropTypes = {
  src: string,
  width: number | string,
  height: number | string,
};

const ImageOuter = styled.div`
  width: ${({ width }) => toPx(width)};
  height: ${({ height }) => toPx(height)};
  overflow: hidden;
  background-color: ${themeGet('colors.greys.5')};

  ${space};
  ${borderRadius};
`;

const ImageWrapper = styled.img`
  display: block;
  object-fit: cover;
`;

// $FlowFixMe
export const Image = withTheme(({ src, width, height, theme, ...rest }: PropTypes) => {
  const w = themeGet(`space.${width}`)({ theme });
  const h = themeGet(`space.${height}`)({ theme });

  return (
    <ImageOuter {...rest} width={w} height={h}>
      <ImageWrapper width={w} height={h} src={src} />
    </ImageOuter>
  );
});

Image.defaultProps = {
  width: 10,
  height: 10,
};
