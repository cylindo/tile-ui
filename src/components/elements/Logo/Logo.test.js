import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { Logo } from './Logo';
import TILE from '../../../index';

it('Should render a default Logo component', () => {
  const { theme } = TILE.uiStore;
  const component = <Logo theme={theme} />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
