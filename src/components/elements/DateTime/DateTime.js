// @flow
import * as React from 'react';
import { DateTimeSC } from './styled';
import { format as f } from '../../../utils';
import type { DateTimeProps } from '../../../utils/dateTime';

export const DateTime = ({ value, customFormat, mode, compact, ...rest }: DateTimeProps) => (
  <DateTimeSC {...rest}>{f({ value, customFormat, mode, compact })}</DateTimeSC>
);

DateTime.defaultProps = {
  compact: false,
  mode: 'dateTime',
  customFormat: null,
};
