import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { DateTime } from './DateTime';
import TILE from '../../../index';

it('Should render a default DateTime component', () => {
  const { theme } = TILE.uiStore;
  const component = <DateTime theme={theme} />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
