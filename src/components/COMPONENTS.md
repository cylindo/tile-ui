# Components

The Components folder stores all components in TILE. Here is an overview of the contents:

## Containers

Stateful, usually internal HOC components.

## Elements

"Dumb" functional ui components used to display common elements

## Fragments

Fragments refer to a composition of Element, Grid and Container components. These are very useful for making re-useable layouts or advanced component strutures.

## Grid

Grid components deal with layout only.
