import styled, { css } from 'styled-components';

export const AppContainerSC = styled.div`
  position: ${props => props.position};
  height: ${props => props.height};
  width: 100vw;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  transition: all 200ms ease;
  overflow-y: auto;
  z-index: 1;

  /* Resets */

  /**
   * Props
   */
  ${props =>
    props.isInfoBarVisible &&
    css`
      top: ${props.theme.uiSize[10]}px;
      height: calc(100vh - ${props.theme.uiSize[10]}px);
    `};
`;

export const ChildContainerSC = styled.div`
  ${props =>
    props.paddingTop &&
    css`
      padding-top: ${props.theme.uiSize[props.paddingTop]}px;
    `};

  ${props =>
    props.paddingBottom &&
    css`
      padding-top: ${props.theme.uiSize[props.paddingBottom]}px;
    `};

  ${props =>
    props.paddingLeft &&
    css`
      padding-top: ${props.theme.uiSize[props.paddingLeft]}px;
    `};

  ${props =>
    props.paddingRight &&
    css`
      padding-top: ${props.theme.uiSize[props.paddingRight]}px;
    `};
`;
