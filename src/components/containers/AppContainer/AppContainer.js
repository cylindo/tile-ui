// @flow
import * as React from 'react';
import { ThemeProvider, injectGlobal } from 'styled-components';
import { theme as defaultTheme } from '../../../theme/theme';

/**
 * Global resets
 */
injectGlobal`

html,
body,
div,
span,
applet,
object,
iframe,
h1,
h2,
h3,
h4,
h5,
h6,
p,
blockquote,
pre,
a,
abbr,
acronym,
address,
big,
cite,
code,
del,
dfn,
em,
img,
ins,
kbd,
q,
s,
samp,
small,
strike,
strong,
sub,
sup,
tt,
var,
b,
u,
i,
center,
dl,
dt,
dd,
ol,
ul,
li,
fieldset,
form,
label,
legend,
table,
caption,
tbody,
tfoot,
thead,
tr,
th,
td,
article,
aside,
canvas,
details,
embed,
figure,
figcaption,
footer,
header,
hgroup,
menu,
nav,
output,
ruby,
section,
summary,
time,
mark,
audio,
video {
  margin: 0;
  padding: 0;
  border: 0;
  vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article,
aside,
details,
figcaption,
figure,
footer,
header,
hgroup,
menu,
nav,
section {
  display: block;
}

body {
  line-height: 1;
  font-variant-ligatures: none;
  text-rendering: optimizeLegibility;
  -webkit-font-smoothing: antialiased;
  text-decoration-skip-ink: auto;
  font-size: 12px;
  color: #314756;
}

ol,
ul {
  list-style: none;
}

blockquote,
q {
  quotes: none;
}

a {
  text-decoration: none;
}

blockquote:before,
blockquote:after,
q:before,
q:after {
  content: '';
  content: none;
}
table {
  border-collapse: collapse;
  border-spacing: 0;
}

.ReactVirtualized__Grid {
  outline: none;
}

  button {
      cursor: pointer;
      outline: none !important;
  }

  * {
    font-family: 'Montserrat', -apple-system, system-ui, Arial, sans-serif;
    line-height: 1.6;
    box-sizing: border-box;
    margin-top: 0;
  }

`;

type Props = {
  theme: {},
  children: React.Node,
};
class AppContainer extends React.Component<Props, {}> {
  static defaultProps = {
    theme: defaultTheme,
  };

  render() {
    const { children, theme } = this.props;

    return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
  }
}

export default AppContainer;
