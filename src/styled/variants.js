import styled, { css } from 'styled-components';
import { lighten, desaturate, transparentize } from 'polished';
/**
 * Variables
 */
// TODO: as part of theme
export const focusShadow = `0px 0px 0px 2px ${transparentize(0.3, '#49C7BA')};`;

/**
 * Common css styles
 */

// disabled component style
export const disabled = css`
  opacity: 0.5;
  pointer-events: none;
`;

// transition
export const transition = css`
  transition: opacity 0.2s ease-out, background 0.2s ease-out, box-shadow 0.2s cubic-bezier(0.47, 0.03, 0.49, 1.38);
`;

// focused component style
export const focused = css`
  box-shadow: ${focusShadow};
`;
