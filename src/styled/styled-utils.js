import { style, themeGet, variant } from 'styled-system';
import { css } from 'styled-components';
import { isNumber, isString } from 'lodash';

/**
 * constants
 */

/**
 * Transformers
 */
export const is = (n): boolean => n !== undefined && n !== null;
/**
 * Converts a number to string with px
 * @param {*} n
 */
export const toPx = n => (is(n) ? (isNumber(n) ? `${n}px` : n) : '0px');

const getWidth = n => (!isNumber(n) || n > 1 ? toPx(n) : `${n * 100}%`);

/**
 * Utils
 */

export const whiteSpace = style({
  prop: 'whiteSpace',
});

export const elevation = style({
  prop: 'elevation',
  key: 'elevation',
  cssProperty: 'boxShadow',
});

export const maxWidth = style({
  prop: 'maxWidth',
  transformValue: getWidth,
});

export const minWidth = style({
  prop: 'minWidth',
  transformValue: getWidth,
});

export const uiSize = variant({
  key: 'uiSizes',
  prop: 'uiSize',
});

/**
 * Style mixins
 */

export const inputError = props => {
  if (props.hasError)
    return css`
      box-shadow: 0 0 0 1px ${themeGet('colors.danger')(props)};
    `;
};

export const inputFocus = props => css`
  box-shadow: 0 0 0 1px ${themeGet('colors.primary')(props)};
`;
