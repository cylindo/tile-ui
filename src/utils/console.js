// @flow
const debug = true;

type LogType = 'warning' | 'log' | 'info';

type Log = {
  type: LogType,
  body: string,
};

type MethodLog = {
  className?: string,
  method: string,
  type: LogType,
  message: string,
};

const dispatch = (log: Log) => {
  if (debug !== true) return null;

  const { type, body } = log;

  switch (type) {
    case 'warning':
      console.warn(body);
      break;
    case 'info':
      console.info(body);
      break;
    default:
      console.log(body);
      break;
  }
};

export const logFromMethod = ({ className, method, type, message }: MethodLog) => {
  const classString = className ? `${className}: ` : '';
  const body = `[TILE] [${classString}${method}] ${message}`;

  dispatch({
    type,
    body,
  });
};
