// @flow
import { map, get } from 'lodash';
import { isArray } from './common';
import { toDate, isDateAfter } from './dateTime';

/**
 * Validations
 */

/**
 * isEmailValid
 * @param {*} email
 */
export const isEmail = (email: string): boolean => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

export const isDateInFuture = (date: Date | string = new Date(), dateToCompare: Date | string): boolean => {
  if (!date || !dateToCompare) return false;

  const dateA = toDate(date);
  const dateB = toDate(dateToCompare);

  return isDateAfter(dateA, dateB);
};

/**
 * Utils
 */
type SelectMapProps = {
  idPath: string,
  valuePath: string,
  labelPath: string,
};

type SelectInputType = {
  id: string,
  value: string,
  label: string,
};

export const mapForSelect = (array: Array<any>, properties: SelectMapProps): Array<SelectInputType> => {
  if (!isArray(array)) return [];

  const { idPath, valuePath, labelPath } = properties;

  return map(array, (obj, index) => ({
    id: get(obj, idPath, index),
    value: get(obj, valuePath, index),
    label: get(obj, labelPath, get(obj, 'idPath', index)),
  }));
};
