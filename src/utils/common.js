import { isNumber as isN, isObject as isO } from 'lodash';

export const isString = s => typeof s === 'string' || s instanceof String;
export const isDefined = n => n !== undefined && n !== null;
export const isNumber = n => isN(n);
export const isArray = n => Array.isArray(n);
export const isObject = obj => isO(obj);
export const toString = s => (isString(s) ? s : s.toString());
