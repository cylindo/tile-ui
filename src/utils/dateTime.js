// @flow
import dateFns, { isDate, format as f, distanceInWords, compareAsc, compareDesc } from 'date-fns';
import daLocale from 'date-fns/locale/da';
import enLocale from 'date-fns/locale/en';

import { isString, isObject } from './common';

export type DateTimeProps = {
  value: string | Date,
  customFormat?: string | null,
  compact?: boolean,
  mode?: 'date' | 'time' | 'dateTime' | 'timeAgo',
};

/**
 * Returns a new Date based on the input
 */
export const toDate = (date: Date | string | typeof undefined): Date => {
  if (!date) return new Date();
  if (isDate(date)) return date;
  if (isString(date)) return toDate(new Date(date));

  console.warn('[newDate]: could not create date object from:', date);
  return new Date();
};

/**
 * Checks wether a date is after a specified date
 */
export const isDateAfter = (dateA: Date, dateB: Date): boolean => {
  const result = compareDesc(dateA, dateB);

  if (result === 1) return true;
  return false;
};

/**
 * returns a new date
 */
export const now = () => new Date();

/**
 * Formats a date or time to a standardized format
 * @param {*} param0
 */
export const format = ({ value, customFormat, compact = false, mode = 'dateTime' }: DateTimeProps) => {
  const getDisplayFormat = m => {
    if (customFormat && isString(customFormat)) return customFormat;

    switch (m) {
      case 'date':
        if (compact) return 'MM-D-YY';
        return 'MMMM Mo YYYY';
      case 'time':
        return 'HH:mm a';
      case 'dateTime':
        return `${getDisplayFormat('date')}, ${getDisplayFormat('time')}`;
      default:
        return '';
    }
  };

  if (mode === 'timeAgo') {
    const dateToCompare = toDate(value);
    const date = now();

    return distanceInWords(dateToCompare, date);
  }

  return f(toDate(value), getDisplayFormat(mode), { locale: enLocale });
};
